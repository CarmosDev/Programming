import socket
import sys

# Create a TCP/IP socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 9999)
print(sys.stderr, 'connecting to %s port %s' % server_address)
client_socket.connect(server_address)

try:

    # Send data
    message = 'This is the message.  It will be repeated.'
    print(sys.stderr, 'sending "%s"' % message)
    client_socket.sendall(message.encode())

    # Look for the response
    amount_received = 0
    amount_expected = len(message)

    while amount_received < amount_expected:
        data = client_socket.recv(4096)
        amount_received += len(data)
        print(sys.stderr, 'received "%s"' % str(data))

finally:
    print(sys.stderr, 'closing socket')
    client_socket.close()