import socket
import sys

#Create a TCP/IP Socket
tcp_ip_socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

#Bind the socket to a port
server_address = ('localhost',9999)
print(sys.stderr, 'starting up on %s port %s' % server_address)
tcp_ip_socket.bind(server_address)

# Listen for incoming connections
tcp_ip_socket.listen(1)
while True:
    # Wait for a connection
    print(sys.stderr, 'waiting for a connection')
    connection, client_address = tcp_ip_socket.accept()

    try:
        print(sys.stderr, 'connection from', client_address)

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(4096)
            print(sys.stderr, 'received "%s"' % str(data))
            if data:
                print(sys.stderr, 'sending data back to the client')
                connection.sendall(data)
            else:
                print(sys.stderr, 'no more data from', client_address)
            break
    finally:
        # Clean up the connection
        connection.close()