class Employee:
    'Common class for all employees'
    emp_count =0

    def __init__(self,name,salary):
        self.name = name
        self.salary = salary
        Employee.emp_count+=1

    def display_count(self):
        print("Total number of employees is{}".format(Employee.emp_count))

    def display_employee(self):
        print("Employee name is : {} and salary is: {}".format(self.name,self.salary))



employee_1 = Employee("Sandra",20000)
employee_2 = Employee("Kylian",30000)

print("Number of employees: {}".format(Employee.emp_count))

print("Employee.__doc__ = {}".format(Employee.__doc__))


class Vector:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __str__(self):
        return 'Vector (%d, %d)' % (self.a, self.b)

    def __add__(self, other):
        return Vector(self.a + other.a, self.b + other.b)


v1 = Vector(2, 10)
v2 = Vector(5, -2)

v3 = v1+v2

print(v3)
