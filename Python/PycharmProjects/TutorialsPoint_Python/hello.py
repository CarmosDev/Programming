string = "Hello World"

print(string[0])

print(string[0:5])

print(string * 2)

print(string[6:])

list = [0,1,2,3,4,5,6,7,8,9]

print(list)

list.append("END")

print(list)

tuple = (0,1,2,3,4,5,6,7,8,9)

print(tuple)

dict = {'name':'John','Year':'First','DOB':1997}

keys = dict.keys()

print(keys)

