string = "string"

print(string[0])

print(string[0:3])

print(string[2:])

print(string[:4]+"pe")

print(string.encode())

print(string.isalnum())

print(string.isalpha())

print(string.isdecimal())

print(string.split())

print(string.replace("t","p"))

intab = "aeiou"
outtab = "12345"
transtab = string.maketrans(intab,outtab)

string = "this is string example....wow!!!";
print(string.translate(transtab))