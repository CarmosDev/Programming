def new_function(param1,param2):
    "This is a sample funtion"

def printinfo( name, age ):
   "This prints a passed info into this function"
   print("Name: ", name)
   print("Age ", age)
   return

# Now you can call printinfo function
printinfo(name = "Mikhi",age = 30)

printinfo(age = 30,name = "Martial")

# Function definition is here
def printinfo(arg1,*vartuple):
   "This prints a variable passed arguments"
   print("Output is: ")
   print(arg1)
   for var in vartuple:
      print(var)
   return

# Now you can call printinfo function
printinfo( 10 )
printinfo( 70, 60, 50 )
printinfo("My names are: ","Carlton","Maranga","Moseti")

# Function definition is here
sum = lambda arg1, arg2: arg1 + arg2;

# Now you can call sum as a function
print("Value of total : ", sum(10, 20))
print("Value of total : ", sum(20, 20))



