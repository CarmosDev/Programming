import time
import calendar

print(time.time())

localtime = time.localtime(time.time())

print(localtime)

print(str(localtime.tm_hour)+":"+str(localtime.tm_min)+":"+str(localtime.tm_sec))

human_time = time.asctime(localtime)

print(str(human_time))

cal = calendar.month(localtime.tm_year,localtime.tm_mon)

print("---------------------")
print("This month's calendar")
print("---------------------")

print(cal)

print(time.clock())