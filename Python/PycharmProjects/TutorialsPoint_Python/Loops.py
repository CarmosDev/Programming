count = 0

while(count < 10):
    print("Count is : " + str(count))
    count+=1

# var = 1
# while var == 1:
#     num = input("Enter a number : ")
#     print("You entered num: "+ str(num))

# flag = 1
#
# while flag: print('Given flag is really true!')

for letter in "Python":
    print("Letter : "+letter)

fruits = ["Mango","Apple","Orange","Passion"]

fruits.append("Banana")

for fruit in fruits:
    print("Fruit : "+fruit)

fruits = ['banana', 'apple',  'mango']
for index in range(len(fruits)):
   print('Current fruit :', fruits[index])

for number in range(5):
    print(number)

name = "Carlton"

year = "III"

print("My name is {} and I am in year {}".format(name,year))