import math

var1 = 10
var2 = 10.3
var3 = 3.55j
var4 = 13434353

print(complex(var3))

print(complex(10,3))

negative = -100

print(abs(negative))

print(math.ceil(100.5))

print(math.pi)

print(math.ceil(math.pi))

def cmp(a, b):
    return (a > b) - (a < b)

def compare(x,y):
    if(cmp(x,y) == -1):
        return "{} is less than {}".format(x,y)
    elif(cmp(x,y) == 0):
        return "{} is equal to {}".format(x, y)
    else:
        return "{} is more than {}".format(x, y)

print(compare(100,100))

print(compare(80,100))

print(compare(120,100))

print(cmp(100,100))

print(cmp(80,100))

print(cmp(120,100))

print(True - False)

print(False - False)

print(False - True)

print(math.exp(10))

print(math.floor(1.6))

print(min([4,1,2,3,5,9]))

print(max([4,1,2,3,5,9]))

print(math.modf(100.23))

print(math.pow(2,-2))

print(math.sqrt(4))

print(math.sqrt(10))

print(math.sin(math.radians(90)))

print(round(math.cos(math.radians(90)),4))