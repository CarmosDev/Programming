a = 5
b = 10

print(a+b)

print(a**b) #Exponent

print(a%b) #Modulus

print(a//b) #Floor division

print(a==b)

print(a!=b)

print(a>b)

print(a<b)

a = 60
b = 13

print(a&b)

print(a|b)

print("a and b is : " + str(a and b))

print("a or b is : " + str(a or b))

print("not a and b is : " + str(not(a and b)))

print("not a or b is : " + str(not(a or b)))

#Membership operators
a = 1
b = 20
list = [0,1,2,3,4,5,6,7,8,9]

print("Is a in the list? " + str(a in list))

print("Is b in the list? " + str(b in list))

needle = "Kenya"
haystack = ["Uganda","Kenya","Tanzania","Rwanda","Sudan"]

print("Is 'Kenya' in the list? "+str(needle in haystack))

a = 10
b = 10

print(a is b)

print(a is not b)