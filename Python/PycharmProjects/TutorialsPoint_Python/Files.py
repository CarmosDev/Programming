import os

# value = input("Type somehing and press ENTER key...\n")
# print("Received input is: "+value)

nums = [x*5 for x in range(2,10,2)]
print(nums)

my_file = open("sample_file.txt",'a+')

print(str(my_file))

print(my_file.name)

my_file.write("This is a sample file")

my_file.close()

my_file = open("sample_file.txt",'r')

print(str(my_file.read()))

my_file.close()

# os.rename("sample_file.txt","new_file.txt")
# os.remove("sample_file.txt")
# os.mkdir("new_directory")
# os.chdir("new_directory")
print(os.getcwd())

