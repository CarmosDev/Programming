def add_data():
    "Enter list of items in a file"
    amount = input("How many items?")
    file = open("Items.list",'a+')

    for index in range(1,int(amount)+1):
        item = input("Item {}?".format(index))
        file.write("{}\n".format(item))

    file.close()

def read_data():
    "Read data in a file"
    file = open("Items.list", 'r')
    return file.readlines()

def print_data():
    "Print file data"
    data = read_data()

    for item in data:
        print("Item: {}".format(item))


# add_data()

print_data()