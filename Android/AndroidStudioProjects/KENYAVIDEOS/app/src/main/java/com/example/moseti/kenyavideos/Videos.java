package com.example.moseti.kenyavideos;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.net.URL;
import java.util.ArrayList;

public class Videos extends AppCompatActivity {
    ListView listView;
    TextView textView_label;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView)findViewById(R.id.list_view_videos);
        textView_label = (TextView)findViewById(R.id.text_number_label);
        String XML_URL = getIntent().getStringExtra("xml_url");
        new LoadVideos().execute(XML_URL);
    }
    public class LoadVideos extends AsyncTask<String,Void,Void> {
        int NUMBER_OF_ENTRIES;
        String AUTHOR_NAME;
        ArrayList<String> TITLES = new ArrayList<>();
        ArrayList<String> LINKS = new ArrayList<>();
        ArrayList<String> IMAGES = new ArrayList<>();
        ArrayList<String> DESCRIPTIONS = new ArrayList<>();
        ArrayList<String> VIEWS = new ArrayList<>();
        LayoutInflater layoutInflater=(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(Videos.this,ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            try{
                Document document = Jsoup.connect(params[0]).get();
                NUMBER_OF_ENTRIES = document.getElementsByTag("entry").size();
                for(int a=0;a<NUMBER_OF_ENTRIES;a++){
                    TITLES.add(document.getElementsByTag("entry").get(a).getElementsByTag("title").get(0).text());
                    LINKS.add(document.getElementsByTag("entry").get(a).getElementsByTag("link").get(0).attr("href"));
                    AUTHOR_NAME = document.getElementsByTag("entry").get(a).getElementsByTag("author").get(0).child(0).text();
                    IMAGES.add(document.getElementsByTag("entry").get(a).getElementsByTag("media:group").get(0).child(2).attr("url"));
                    DESCRIPTIONS.add(document.getElementsByTag("entry").get(a).getElementsByTag("media:group").get(0).child(3).text());
                    VIEWS.add(document.getElementsByTag("entry").get(a).getElementsByTag("media:group").get(0).child(4).child(1).attr("views"));
                }
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            textView_label.setText(""+NUMBER_OF_ENTRIES);
            if(listView!=null){
                listView.setAdapter(new BaseAdapter() {
                    @Override
                    public int getCount() {
                        return TITLES.toArray().length;
                    }

                    @Override
                    public Object getItem(int position) {
                        return position;
                    }

                    @Override
                    public long getItemId(int position) {
                        return position;
                    }
                    class Holder{
                        TextView video_title;
                        ImageView video_image;
                        TextView video_author;
                        TextView video_views;
                        TextView video_description;
                    }
                    @Override
                    public View getView(final int position, View convertView, ViewGroup parent) {
                        convertView = layoutInflater.inflate(R.layout.listview_videos,null);
                        Holder holder = new Holder();
                        holder.video_title = (TextView)convertView.findViewById(R.id.video_title);
                        holder.video_image = (ImageView)convertView.findViewById(R.id.video_image);
                        holder.video_author = (TextView)convertView.findViewById(R.id.video_author);
                        holder.video_views = (TextView)convertView.findViewById(R.id.video_views);
                        holder.video_description = (TextView)convertView.findViewById(R.id.video_description);

                        holder.video_title.setText(TITLES.get(position));
                        try{
                            holder.video_image.setImageBitmap(new LoadImageBitmap().execute(IMAGES.get(position)).get());
                        }catch (Exception e){
                            System.out.println("IMAGE ERROR : "+e);
                        }
                        holder.video_author.setText(AUTHOR_NAME);
                        holder.video_views.setText(VIEWS.get(position));
                        holder.video_description.setText(DESCRIPTIONS.get(position));

                        convertView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String PLAY_URL=LINKS.get(position);
                                Intent intent = new Intent(Videos.this,PlayVideo.class);
                                intent.putExtra("web_url", PLAY_URL);
                                intent.putExtra("title", TITLES.get(position));
                                startActivity(intent);
                            }
                        });
                        return convertView;
                    }
                });
            }
            progressDialog.dismiss();
        }
    }
    public class LoadImageBitmap extends AsyncTask<String,Void,Bitmap>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            try{
                return BitmapFactory.decodeStream(new URL(params[0]).openStream());
            }catch (Exception e){
                System.out.println("BITMAP ERROR : "+e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }
    }
}
