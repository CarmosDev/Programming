package com.example.moseti.kenyavideos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

public class SelectTv extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_tv);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Object[] button_labels={"KBC TV","CITIZEN TV","KTN TV","NTV","CAPITAL FM TV","QTV","K24 TV","CHURCHILL SHOW","GHAFLA TV","THE REAL HOUSEHELPS OF KAWANGWARE TV","HAPA KULE NEWS TV","MPASHO TV","ERIC OMONDI TV","KISS 100 TV","THE STAR KENYA TV"};
        final Object[] button_values={"kbckenya2011","kenyacitizentv","standardgroupkenya","NTVKenya","capitalfmkenya","qtvkenya","K24TV","ChurchillTVKenya","ghaflakenya","TRHHOK","HapaKuleNews","MpashoNews","UC4Hvs9PzuXKhEY_CeTS1wvg","UCbxud_mmyLzNUS5QKMNPwFQ","UC-aT2XsRXmTHxnCeUiMNvTw"};

        Button button_link_app = (Button)findViewById(R.id.button_link_app);
        if(button_link_app!=null){
            button_link_app.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent =new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.carmosdevelopers.carmos.newspocket&hl=en"));
                    startActivity(intent);
                }
            });
        }
        final LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ListView listView = (ListView)findViewById(R.id.list_view_buttons);
        if(listView!=null) {
            listView.setAdapter(new BaseAdapter() {
                String LINK;

                @Override
                public int getCount() {
                    return button_labels.length;
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    convertView = layoutInflater.inflate(R.layout.listview_buttons, null);
                    Button button_label = (Button) convertView.findViewById(R.id.button_label);
                    button_label.setText(button_labels[position].toString());

                    button_label.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (position >= 12) {
                                LINK = "https://www.youtube.com/feeds/videos.xml?channel_id=" + button_values[position].toString();
                            } else {
                                LINK = "https://www.youtube.com/feeds/videos.xml?user=" + button_values[position].toString();
                            }
                            Intent intent = new Intent(SelectTv.this, Videos.class);
                            intent.putExtra("xml_url", LINK);
                            startActivity(intent);
                        }
                    });
                    return convertView;
                }
            });
        }
    }
}
