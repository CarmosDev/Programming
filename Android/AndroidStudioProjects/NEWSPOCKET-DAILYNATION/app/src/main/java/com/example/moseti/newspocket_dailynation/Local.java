package com.example.moseti.newspocket_dailynation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

public class Local extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView list_view_local = (ListView)findViewById(R.id.list_stories_local);
        new ParseRSS_DN(Local.this,list_view_local,"http://www.nation.co.ke/news/-/1056/1056/-/view/asFeed/-/wr4x5yz/-/index.xml").execute();
    }

}
