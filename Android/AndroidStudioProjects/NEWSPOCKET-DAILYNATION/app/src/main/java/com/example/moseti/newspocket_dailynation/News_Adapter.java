package com.example.moseti.newspocket_dailynation;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by MOSETI on 27/03/2016.
 */
public class News_Adapter extends BaseAdapter {
    Context CONTEXT;
    Object[] TITLES;
    Object[] LINKS;
    Object[] DESCRIPTIONS;
    String NEWS_SOURCE;
    public  LayoutInflater layoutInflater;
    public News_Adapter(Context context,Object[] titles_array,Object[] links_array,Object[] descriptions_array,String news_source){
        CONTEXT = context;
        TITLES = titles_array;
        LINKS = links_array;
        DESCRIPTIONS = descriptions_array;
        NEWS_SOURCE =news_source;
    }
    public class Holder{
        TextView news_source_text;
        TextView news_title_text;
        TextView news_description_text;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater)CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.story_view,null);
        Holder holder = new Holder();
        holder.news_source_text = (TextView)convertView.findViewById(R.id.news_source);
        holder.news_title_text = (TextView)convertView.findViewById(R.id.news_title);
        holder.news_description_text = (TextView)convertView.findViewById(R.id.news_description);

        holder.news_source_text.setText(NEWS_SOURCE);
        holder.news_title_text.setText(TITLES[position].toString());
        holder.news_description_text.setText(DESCRIPTIONS[position].toString());
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CONTEXT,WebStory.class);
                intent.putExtra("url",LINKS[position].toString());
                intent.putExtra("title",TITLES[position].toString());
                CONTEXT.startActivity(intent);
            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }
}
