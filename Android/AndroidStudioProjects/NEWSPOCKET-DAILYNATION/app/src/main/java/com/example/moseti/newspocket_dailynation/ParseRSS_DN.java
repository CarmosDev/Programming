package com.example.moseti.newspocket_dailynation;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

/**
 * Created by MOSETI on 28/03/2016.
 */
public class ParseRSS_DN extends AsyncTask<Void,Void,Void> {
    ProgressDialog progressDialog;
    ListView LIST_VIEW;
    Context CONTEXT;
    String XML_URL;
    ArrayList<String> titles_list = new ArrayList<>();
    ArrayList<String> links_list = new ArrayList<>();
    ArrayList<String> descriptions_list = new ArrayList<>();
    String source;

    public ParseRSS_DN(Context context,ListView listView,String xml_url){
        CONTEXT = context;
        LIST_VIEW = listView;
        XML_URL = xml_url;
    }
    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(CONTEXT,ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try{
            Document document = Jsoup.connect(XML_URL).get();
            source = document.getElementsByTag("title").get(0).text();
            int items_length=document.getElementsByTag("item").size();
            for(int i=0;i<items_length;i++){
                titles_list.add(document.getElementsByTag("item").get(i).child(0).text());
                links_list.add(document.getElementsByTag("item").get(i).child(1).text());
                descriptions_list.add(document.getElementsByTag("item").get(i).child(2).text());
            }
        }
        catch (Exception e){
            System.out.println("DOCUMENT ERROR : "+e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        LIST_VIEW.setAdapter(new News_Adapter(CONTEXT,titles_list.toArray(),links_list.toArray(),descriptions_list.toArray(),source));
        progressDialog.dismiss();
    }
}
