package com.example.moseti.newspocket_dailynation;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

public class Lifestyle extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lifestyle);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ListView list_view_lifestyle = (ListView)findViewById(R.id.list_stories_lifestyle);
        new ParseRSS_DN(Lifestyle.this,list_view_lifestyle,"http://www.nation.co.ke/lifestyle/-/1190/1190/-/view/asFeed/-/tgsua2z/-/index.xml").execute();
    }

}
