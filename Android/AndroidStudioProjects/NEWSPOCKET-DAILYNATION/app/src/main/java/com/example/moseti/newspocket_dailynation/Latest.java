package com.example.moseti.newspocket_dailynation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

public class Latest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView list_view_latest =(ListView)findViewById(R.id.list_stories_latest);
        new ParseRSS_DN(Latest.this,list_view_latest,"http://www.nation.co.ke/-/1148/1148/-/view/asFeed/-/vtvnjq/-/index.xml").execute();
    }
}
