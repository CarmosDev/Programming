package com.example.moseti.newspocket_dailynation;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

public class Soccer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soccer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ListView list_view_soccer =(ListView)findViewById(R.id.list_stories_soccer);
        new ParseRSS_DN(Soccer.this,list_view_soccer,"http://www.nation.co.ke/sports/football/-/1102/1102/-/view/asFeed/-/dq0624z/-/index.xml").execute();
    }

}
