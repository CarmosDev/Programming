package com.example.moseti.newspocket_dailynation;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

public class Business extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ListView list_view_business = (ListView)findViewById(R.id.list_stories_business);
        new ParseRSS_DN(Business.this,list_view_business,"http://www.nation.co.ke/business/-/996/996/-/view/asFeed/-/14lpkvc/-/index.xml").execute();
    }

}
