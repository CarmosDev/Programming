package com.example.moseti.newspocket_dailynation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button_latest = (Button)findViewById(R.id.news_latest);
        button_latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Latest.class));
            }
        });
        Button button_local = (Button)findViewById(R.id.news_local);
        button_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Local.class));
            }
        });
        Button button_business = (Button)findViewById(R.id.news_business);
        button_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Business.class));
            }
        });
        Button button_international = (Button)findViewById(R.id.news_international);
        button_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,International.class));
            }
        });
        Button button_sports = (Button)findViewById(R.id.news_sports);
        button_sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Sports.class));
            }
        });
        Button button_soccer = (Button)findViewById(R.id.news_soccer);
        button_soccer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Soccer.class));
            }
        });
        Button button_blogs_and_opinion = (Button)findViewById(R.id.news_opinion);
        button_blogs_and_opinion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Opinion.class));
            }
        });
        Button button_lifestyle = (Button)findViewById(R.id.news_life_and_style);
        button_lifestyle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Lifestyle.class));
            }
        });
    }
}
