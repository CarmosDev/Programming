package com.example.moseti.newspocket_dailynation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

public class International extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_international);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView list_view_international =(ListView)findViewById(R.id.list_stories_international);
        new ParseRSS_DN(International.this,list_view_international,"http://www.nation.co.ke/news/world/-/1068/1068/-/view/asFeed/-/kira34z/-/index.xml").execute();
    }

}
