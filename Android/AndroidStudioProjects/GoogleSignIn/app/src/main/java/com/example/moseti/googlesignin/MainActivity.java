package com.example.moseti.googlesignin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInApi;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Account;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    int RC_SIGN_IN = 9001;
    Button sign_out_button;
    SignInButton signInButton;
    GoogleApiClient googleApiClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.
                Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                requestScopes(new Scope(Scopes.PROFILE),new Scope(Scopes.PLUS_ME)).
                requestEmail().
                build();

        googleApiClient = new GoogleApiClient.Builder(MainActivity.this).
                enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(MainActivity.this,connectionResult.getErrorMessage(),Toast.LENGTH_LONG).show();
                    }
                }).
                addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions).
                addApi(Plus.API).
                build();

        signInButton = (SignInButton) findViewById(R.id.google_sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,RC_SIGN_IN);
            }
        });

        sign_out_button = (Button) findViewById(R.id.google_sign_out_button);
        sign_out_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        view.setVisibility(View.GONE);
                        signInButton.setVisibility(View.VISIBLE);
                        Toast.makeText(MainActivity.this,status.getStatusMessage(),Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        if(googleApiClient.isConnected()){
            signInButton.setVisibility(View.GONE);
            sign_out_button.setVisibility(View.VISIBLE);
        }else{
            signInButton.setVisibility(View.VISIBLE);
            sign_out_button.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()){
                GoogleSignInAccount googleSignInAccount = result.getSignInAccount();

                Person person  = Plus.PeopleApi.getCurrentPerson(googleApiClient);
                Toast.makeText(MainActivity.this,"Person: "+person.getAboutMe(),Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this,"Person: "+person.getBirthday(),Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this,"Person: "+person.getBraggingRights(),Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this,"Person: "+person.getCurrentLocation(),Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this,"Person: "+person.getUrl(),Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this,"Person: "+person.getId(),Toast.LENGTH_LONG).show();
//                Toast.makeText(MainActivity.this,"Person: "+person.getAgeRange().toString(),Toast.LENGTH_LONG).show();
//                Log.i(TAG, "--------------------------------");
//                Log.i(TAG, "Display Name: " + person.getDisplayName());
//                Log.i(TAG, "Gender: " + person.getGender());
//                Log.i(TAG, "AboutMe: " + person.getAboutMe());
//                Log.i(TAG, "Birthday: " + person.getBirthday());
//                Log.i(TAG, "Current Location: " + person.getCurrentLocation());
//                Log.i(TAG, "Language: " + person.getLanguage());

                try{
                    String text = "Sign Out of Account Name: "+googleSignInAccount.getEmail();
                    sign_out_button.setText(text);
                    sign_out_button.setVisibility(View.VISIBLE);
//                    signInButton.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this,"Welcome: "+googleSignInAccount.getId(),Toast.LENGTH_LONG).show();
                    Toast.makeText(MainActivity.this,"Welcome: "+googleSignInAccount.getDisplayName(),Toast.LENGTH_LONG).show();
                    Toast.makeText(MainActivity.this,"Welcome: "+googleSignInAccount.getEmail(),Toast.LENGTH_LONG).show();
                    Toast.makeText(MainActivity.this,"Welcome: "+googleSignInAccount.getFamilyName(),Toast.LENGTH_LONG).show();
                    Toast.makeText(MainActivity.this,"Welcome: "+googleSignInAccount.getGivenName(),Toast.LENGTH_LONG).show();
                    Toast.makeText(MainActivity.this,"Welcome: "+googleSignInAccount.getPhotoUrl(),Toast.LENGTH_LONG).show();
                }
                catch (Exception e){
                    Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }else {
                Toast.makeText(MainActivity.this,"Failed to sign in!!!",Toast.LENGTH_LONG).show();
            }
        }
    }

//    private void revokeAccess(){
//        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                        // ...
//                    }
//                });
//    }

}

