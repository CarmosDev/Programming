package com.example.moseti.webviewer;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
        String url_NATION="http://www.nation.co.ke/lifestyle/lifestyle/school-that-hopes-to-turn-boys-into-KENYA-future-leaders/-/1214/3036718/-/dt9ld3z/-/index.html";
        try{
            System.out.println("DAILY NATION : "+new DailyNationImage().execute(url_NATION).get());
        }catch (Exception e){
            System.out.println("OUTPUT ERROR : "+e);
        }*/

        /*
        String url_NAIROBINEWS="http://nairobinews.nation.co.ke/news/itumbi-makes-pledge-towards-under-the-tree-class-and-twitter-goes-ham/";
        try {
            System.out.println("NAIROBI NEWS : "+new NairobiNewsImage().execute(url_NAIROBINEWS).get());
        }catch (Exception e){
            System.out.println("OUTPUT ERROR : "+e);
        }*/

        /*
        String url_SDE="http://www.sde.co.ke/pulse/article/2000188529/tv-girl-sarah-hassan-and-fianc-move-to-the-us";
        try {
            System.out.println("SDE : "+new SDEIMAGE().execute(url_SDE).get());
        }catch (Exception e){
            System.out.println("EXCEPTION : "+e);
        }*/

        /*
        String url_STANDARDMEDIA="http://www.standardmedia.co.ke/?articleID=2000188567&story_title=Kenya: if-you-can-t-reach-your-loved-ones-in-somalia-call-these-numbers";
        try {
            System.out.println("STANDARD MEDIA : "+new STANDARDMEDIAIMAGE().execute(url_STANDARDMEDIA).get());
        }catch (Exception e){
            System.out.println("OUTPUT ERROR : "+e);
        }*/

        /*
        String url_CITIZEN="https://citizentv.co.ke/news/three-children-from-one-family-perish-in-grisly-road-accident-112073/";
        try {
            System.out.println("RESULT : "+new CITIZENIMAGE().execute(url_CITIZEN).get());
        }catch (Exception e){
            System.out.println("OUTPUT ERROR : "+e);
        }*/
        /*
        String url_MEDIAMAX="http://www.mediamaxnetwork.co.ke/k24-tv/194256/marabou-stork-nairobis-dirty-angry-birds/";
        try{
            System.out.println("RESULT : "+new MEDIAMAXIMAGE().execute(url_MEDIAMAX).get());
        }catch(Exception e){
            System.out.println("OUTPUT ERROR : "+e);
        }*/
        /*
        String url_GHAFLA="http://www.ghafla.co.ke/blogs/music/6743-exposed-rich-kikuyu-man-behind-chipukeezy-s-leaked-nude-photos";
        try{
            System.out.println("RESULT : "+new GHAFLAIMAGE().execute(url_GHAFLA).get());
        }catch (Exception e){
            System.out.println("OUTPUT ERROR : "+e);
        }*/
    }
    private class GHAFLAIMAGE extends AsyncTask<String,Void,String>{
        String result;
        String HOST_NAME="http://www.ghafla.co.ke";
        @Override
        protected String doInBackground(String... params) {
            try{
                Document document=Jsoup.connect(params[0]).get();
                result=HOST_NAME+document.getElementsByAttributeValue("class","pull-right item-image").get(0).child(0).attr("src");
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return result;
        }
    }

    private class MEDIAMAXIMAGE extends AsyncTask<String,Void,String>{
        String result;
        @Override
        protected String doInBackground(String... params) {
            try {
                Document document = Jsoup.connect(params[0]).get();
                result=document.getElementsByTag("img").get(10).attr("src");
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return result;
        }
    }
    private class CITIZENIMAGE extends AsyncTask<String,Void,String>{
        String result;
        @Override
        protected String doInBackground(String... params) {
            try {
                Document document=Jsoup.connect(params[0]).get();
                result=document.getElementsByClass("subpage-content").get(0).getElementsByTag("figure").get(0).child(0).attr("src");
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return result;
        }
    }

    private class STANDARDMEDIAIMAGE extends AsyncTask<String,Void,String>{
        String result;
        String HOST_NAME="http://www.standardmedia.co.ke";
        @Override
        protected String doInBackground(String... params) {
            try{
                Document document=Jsoup.connect(params[0]).get();
                if(document.getElementsByTag("figure").size()>0){
                    result=HOST_NAME+document.getElementsByTag("figure").get(0).child(0).attr("src");
                }else{
                    result="http://www.standardmedia.co.ke/common/i/standard-digital-world-inner-page.png";
                }
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return result;
        }
    }
    private class SDEIMAGE extends AsyncTask<String,Void,String>{
        String result;
        String HOST_NAME="http://www.sde.co.ke";
        @Override
        protected String doInBackground(String... params) {
            try{
                Document document=Jsoup.connect(params[0]).get();
                if(document.getElementsByAttributeValue("class","content").size()>0){
                    result=HOST_NAME+document.getElementsByAttributeValue("class","content").get(0).child(0).getElementsByTag("img").get(0).attr("src");
                }else{
                    result="http://www.standardmedia.co.ke/common/i/standard-digital-world-inner-page.png";
                }
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return result;
        }
    }
    private class NAIROBINEWSIMAGE extends AsyncTask<String,Void,String>{
        String result;
        @Override
        protected String doInBackground(String... params) {
            try{
                Document document=Jsoup.connect(params[0]).get();
                if(document.getElementsByAttributeValue("class","attachment-post-full wp-post-image").size()>0){
                    result=document.getElementsByAttributeValue("class","attachment-post-full wp-post-image").get(0).attr("src");
                }else{
                    result="http://nairobinews.nation.co.ke/wp-content/themes/nairobinews/images/nnmasthead.png";
                }
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return result;
        }
    }

    private class DAILYNATION_BUSINESSDAILYImage extends AsyncTask<String,Void,String>{
        String HOST_NAME="http://www.nation.co.ke";
        String result;
        @Override
        protected String doInBackground(String... params) {
            try{
                Document document= Jsoup.connect(params[0]).get();
                if(document.getElementsByAttributeValue("class","photo_article").size()>0){
                    Elements images=document.getElementsByAttributeValue("class", "photo_article");
                    result=HOST_NAME+images.get(0).attr("src");
                }else{
                    result="http://nairobinews.nation.co.ke/wp-content/themes/nairobinews/images/nnmasthead.png";
                }
            }catch (Exception e){
                System.out.println("DOCUMENT ERROR : "+e);
            }
            return result;
        }
    }

}
