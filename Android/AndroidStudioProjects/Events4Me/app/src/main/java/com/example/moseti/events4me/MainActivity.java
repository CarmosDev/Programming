package com.example.moseti.events4me;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.SearchManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.SearchView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,OnMapReadyCallback {

    private GoogleMap events_map;
    private NavigationView navigationView;
    public static GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Twitter.initialize(this);
//        googleApiClient = SignInActivity.google_api_client;
//        if(googleApiClient!=null) googleApiClient.connect();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Load events
        GridView events_grid_view = (GridView) findViewById(R.id.events_grid_view);
        events_grid_view.setAdapter(new LoadEventsList(MainActivity.this));

        //Events Map
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.events_map_view);
        mapFragment.getMapAsync(this);

        //Login/Logout Updates
//        if(getIntent().hasExtra("profile_name")){
//            TextView profile_name = (TextView) findViewById(R.id.profile_display_name);
//            Toast.makeText(this,profile_name.getText(),Toast.LENGTH_LONG).show();
//
////            profile_name.setText(getIntent().getStringExtra("profile_name"));
//            TextView profile_email = (TextView) findViewById(R.id.profile_email);
//            profile_email.setText(getIntent().getStringExtra("profile_email"));
//            Toast.makeText(this,getIntent().getStringExtra("profile_name"),Toast.LENGTH_LONG).show();
//            Toast.makeText(this,getIntent().getStringExtra("profile_email"),Toast.LENGTH_LONG).show();
//        }
        //Check Login
        login_update();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        events_map = googleMap;
        events_map.getUiSettings().setMyLocationButtonEnabled(true);
        // Add a marker in Sydney and move the camera
        LatLng KICC = new LatLng(-1.288629,36.822976);
        LatLng BClub = new LatLng(-1.290690,36.783006);
        LatLng _1824 = new LatLng(-1.322787,36.802631);
        LatLng Tribeka = new LatLng(-1.283492,36.822008);
        LatLng UON = new LatLng(-1.280359,36.816264);
        events_map.addMarker(new MarkerOptions().position(KICC).title("Wizkid in Kenya"));
        events_map.addMarker(new MarkerOptions().position(BClub).title("BClub event"));
        events_map.addMarker(new MarkerOptions().position(_1824).title("Sunday School"));
        events_map.addMarker(new MarkerOptions().position(Tribeka).title("Tribeka event"));
        events_map.addMarker(new MarkerOptions().position(UON).title("Jack Ma Motivational Talk"));
        events_map.moveCamera(CameraUpdateFactory.newLatLng(KICC));
        events_map.moveCamera(CameraUpdateFactory.zoomTo(13));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.app_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id==R.id.pick_date){
            DialogFragment dialogFragment = new EventDatePicker();
            dialogFragment.show(getFragmentManager(),"Event Date Picker");
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_sign_in) {
            if(getSharedPreferences("SIGN_IN_PREFERENCE",MODE_PRIVATE).getBoolean("SIGNED_IN",false)){
                //Sign Out
                log_out();
            }else{
                startActivity(new Intent(MainActivity.this,SignInActivity.class));
            }
        } else if (id == R.id.nav_acc_settings) {

        } else if (id == R.id.nav_purchases) {

        } else if (id == R.id.nav_terms) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_privacy) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean checkLogin(){
        return checkTwitter() || checkFacebook() || checkGoogle();
    }

    private boolean checkTwitter(){
        return TwitterCore.getInstance().getSessionManager().getActiveSession()!=null;
    }

    private boolean checkFacebook(){
        return AccessToken.getCurrentAccessToken() != null ||
                Profile.getCurrentProfile() !=null;
    }

    private boolean checkGoogle(){
        return googleApiClient!=null;
//        return (getSharedPreferences("SIGN_IN_PREFERENCE", MODE_PRIVATE).getString("SIGN_IN_TYPE","").equals("GOOGLE"));
    }

    private void log_out(){
        Toast.makeText(this,"You are logged out.",Toast.LENGTH_LONG).show();
        if(checkTwitter()){
            TwitterCore.getInstance().getSessionManager().clearActiveSession();
        }
//        if(checkGoogle()){
//            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//                        Toast.makeText(MainActivity.this,status.getStatusMessage(),Toast.LENGTH_LONG).show();
//                    }
//                });
//        }
        if(checkFacebook()) LoginManager.getInstance().logOut();

        SharedPreferences.Editor a = getSharedPreferences("SIGN_IN_PREFERENCE",MODE_PRIVATE).edit();
        a.clear();
        a.apply();
        update_drawer("","","",false);
    }

    private void update_drawer(String name,String email,String image_url,boolean sign_in){
        TextView profile_name = navigationView.getHeaderView(0).findViewById(R.id.profile_display_name);
        TextView profile_email = navigationView.getHeaderView(0).findViewById(R.id.profile_email);
        CircleImageView profile_image = navigationView.getHeaderView(0).findViewById(R.id.profile_image);
        if(name!=null) profile_name.setText(name);
        if(email!=null) profile_email.setText(email);
        try{
            if(image_url!=null) profile_image.setImageBitmap(new LoadRemoteImage().execute(image_url).get());
        }catch (Exception e){
            Toast.makeText(MainActivity.this,"Unable to load profile image",Toast.LENGTH_LONG).show();
        }
        Menu menu = navigationView.getMenu();
        MenuItem item = menu.findItem(R.id.nav_sign_in);
        if(sign_in) item.setTitle("Sign Out"); else item.setTitle("Sign In");
    }

    public void login_update(){
        if(checkLogin()){
            if(checkTwitter()){
                TwitterApiClient twitterApiClient = new TwitterApiClient(TwitterCore.getInstance().getSessionManager().getActiveSession());
                AccountService accountService = twitterApiClient.getAccountService();
                accountService.verifyCredentials(true,false,true).enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
                        User user = result.data;
                        update_drawer(user.name,user.email,user.profileImageUrl,true);
//                        Toast.makeText(SignInActivity.this,""+user.id,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.name,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.email,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.profileBackgroundImageUrlHttps,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.profileImageUrlHttps,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.description,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.location,Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(TwitterException exception) {
//                        Toast.makeText(SignInActivity.this,exception.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
                Toast.makeText(MainActivity.this,"Logged in using Twitter",Toast.LENGTH_LONG).show();
                return;
            }
            if(checkFacebook()){
                GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(),
                        "/" + AccessToken.getCurrentAccessToken().getUserId(),
                        null,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                JSONObject object = response.getJSONObject();
//                                {
//                                    "id": "12345678",
//                                    "birthday": "1/1/1950",
//                                    "first_name": "Chris",
//                                    "gender": "male",
//                                    "last_name": "Colm",
//                                    "link": "http://www.facebook.com/12345678",
//                                    "location": {
//                                          "id": "110843418940484",
//                                          "name": "Seattle, Washington"
//                                    },
//                                    "locale": "en_US",
//                                    "name": "Chris Colm",
//                                    "timezone": -8,
//                                    "updated_time": "2010-01-01T16:40:43+0000",
//                                    "verified": true
//                                }
                                try{
                                    update_drawer(object.getString("name"),object.getString("email"),object.getString("picture"),true);
                                }catch (Exception e){
                                    Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,gender,location,birthday,picture,age_range");
                request.setParameters(parameters);
                request.executeAsync();

//                profile_name.setText(Profile.getCurrentProfile().getName());
//                profile_email.setText(Profile.getCurrentProfile().getName());
                Toast.makeText(MainActivity.this,"Logged in using Facebook",Toast.LENGTH_LONG).show();
                return;
            }
            if(checkGoogle()){
                SharedPreferences google_session = getSharedPreferences("SIGN_IN_PREFERENCE",MODE_PRIVATE);
                update_drawer(google_session.getString("GOOGLE_USER_NAME",null),
                        google_session.getString("GOOGLE_USER_EMAIL",null),
                        google_session.getString("GOOGLE_USER_IMAGE",null),
                        true);
                Toast.makeText(MainActivity.this,"Logged in using Google",Toast.LENGTH_LONG).show();
                return;
            }
        }else{
            Toast.makeText(MainActivity.this,"Not Logged in",Toast.LENGTH_LONG).show();
        }
    }
}
