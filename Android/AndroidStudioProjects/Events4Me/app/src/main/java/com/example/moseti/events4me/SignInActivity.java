package com.example.moseti.events4me;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONObject;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

public class SignInActivity extends AppCompatActivity {

    /*Google*/
    int GOOGLE_SIGN_IN_RC = 9001;
    SignInButton google_sign_in_button;
    public static GoogleApiClient google_api_client;

    /*Facebook*/
    CallbackManager facebook_callback_manager;

    /*Twitter*/
    TwitterLoginButton twitter_sign_in_button;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Twitter.initialize(this);
        setContentView(R.layout.activity_sign_in);
        setTitle("Sign In");

        /*Google*/

        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.
                Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).
                requestScopes(new Scope(Scopes.PROFILE),new Scope(Scopes.PLUS_ME)).
                requestEmail().
                build();

        google_api_client = new GoogleApiClient.Builder(SignInActivity.this).
                enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(SignInActivity.this,connectionResult.getErrorMessage(),Toast.LENGTH_LONG).show();
                    }
                }).
                addApi(Auth.GOOGLE_SIGN_IN_API,googleSignInOptions).
                addApi(Plus.API).
                build();

        google_sign_in_button = (SignInButton) findViewById(R.id.google_sign_in_button);
        google_sign_in_button.setSize(SignInButton.SIZE_STANDARD);
        google_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(google_api_client);
                startActivityForResult(intent,GOOGLE_SIGN_IN_RC);
            }
        });

        /*Facebook*/

        facebook_callback_manager = CallbackManager.Factory.create();

        LoginButton login_button = (LoginButton) findViewById(R.id.facebook_sign_in_button);
        login_button.setReadPermissions("email","user_location");
        login_button.registerCallback(facebook_callback_manager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                Set set = loginResult.getAccessToken().getPermissions();
                GraphRequest request = new GraphRequest(loginResult.getAccessToken(),
                        "/" + loginResult.getAccessToken().getUserId(),
                        null,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                JSONObject object = response.getJSONObject();
                                SharedPreferences google_session = getSharedPreferences("SIGN_IN_PREFERENCE",MODE_PRIVATE);
                                SharedPreferences.Editor editor = google_session.edit();
                                editor.putString("SIGN_IN_TYPE","FACEBOOK");
                                editor.putBoolean("SIGNED_IN",true);
                                editor.apply();
//                                {
//                                    "id": "12345678",
//                                    "birthday": "1/1/1950",
//                                    "first_name": "Chris",
//                                    "gender": "male",
//                                    "last_name": "Colm",
//                                    "link": "http://www.facebook.com/12345678",
//                                    "location": {
//                                          "id": "110843418940484",
//                                          "name": "Seattle, Washington"
//                                    },
//                                    "locale": "en_US",
//                                    "name": "Chris Colm",
//                                    "timezone": -8,
//                                    "updated_time": "2010-01-01T16:40:43+0000",
//                                    "verified": true
//                                }
                                try{
                                    System.out.print(object.toString());
                                }catch (Exception e){
                                    Toast.makeText(SignInActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,gender,location,birthday,picture,age_range");
                request.setParameters(parameters);
                request.executeAsync();

                go_home();
//                Toast.makeText(SignInActivity.this,,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(SignInActivity.this,"Login attempt cancelled!!!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(SignInActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
        //Check Status
//        AccessToken.getCurrentAccessToken() || Profile.getCurrentProfile()

        /*Twitter*/
//        TwitterConfig config = new TwitterConfig.Builder(this)
//                .logger(new DefaultLogger(Log.DEBUG))
//                .twitterAuthConfig(new TwitterAuthConfig("lch56Ohq0IQtTHyCG5XzSDXCr","6ZkABbyfnhlbc0tgCHMBSOZboRWSRjo7DHxe58Lhgt9GysMFlh"))
//                .debug(true)
//                .build();
        twitter_sign_in_button = (TwitterLoginButton) findViewById(R.id.twitter_sign_in_button);
        twitter_sign_in_button.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                final String user_name = session.getUserName();
                final long user_id = session.getUserId();

                TwitterApiClient twitterApiClient = new TwitterApiClient(session);
                AccountService accountService = twitterApiClient.getAccountService();
                accountService.verifyCredentials(true,false,true).enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
                        User user = result.data;
                        SharedPreferences google_session = getSharedPreferences("SIGN_IN_PREFERENCE",MODE_PRIVATE);
                        SharedPreferences.Editor editor = google_session.edit();
                        editor.putString("SIGN_IN_TYPE","TWITTER");
                        editor.putBoolean("SIGNED_IN",true);
                        editor.apply();
//                        Toast.makeText(SignInActivity.this,""+user.id,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.name,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.email,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.profileBackgroundImageUrlHttps,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.profileImageUrlHttps,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.description,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,user.location,Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Toast.makeText(SignInActivity.this,exception.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

//                TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
//                twitterAuthClient.requestEmail(session, new Callback<String>() {
//                    @Override
//                    public void success(Result<String> result) {
//                        Toast.makeText(SignInActivity.this,user_name,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,""+user_id,Toast.LENGTH_LONG).show();
//                        Toast.makeText(SignInActivity.this,result.data,Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void failure(TwitterException exception) {
//                        Toast.makeText(SignInActivity.this,exception.getMessage(),Toast.LENGTH_LONG).show();
//                    }
//                }
                go_home();
            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(SignInActivity.this,exception.getMessage(),Toast.LENGTH_LONG).show();
            }
        });
        //Check Status
//        TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
//        TwitterAuthToken authToken = session.getAuthToken();
//        String token = authToken.token;
//        String secret = authToken.secret;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*Google*/
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_SIGN_IN_RC) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()){
                GoogleSignInAccount googleSignInAccount = result.getSignInAccount();

//                startActivity(new Intent(SignInActivity.this,SignInActivity.class));

//                Person person  = Plus.PeopleApi.getCurrentPerson(google_api_client);
//                Toast.makeText(SignInActivity.this,"Person: "+person.getAboutMe(),Toast.LENGTH_LONG).show();
//                Toast.makeText(SignInActivity.this,"Person: "+person.getBirthday(),Toast.LENGTH_LONG).show();
//                Toast.makeText(SignInActivity.this,"Person: "+person.getBraggingRights(),Toast.LENGTH_LONG).show();
//                Toast.makeText(SignInActivity.this,"Person: "+person.getCurrentLocation(),Toast.LENGTH_LONG).show();
//                Toast.makeText(SignInActivity.this,"Person: "+person.getUrl(),Toast.LENGTH_LONG).show();
//                Toast.makeText(SignInActivity.this,"Person: "+person.getId(),Toast.LENGTH_LONG).show();
////                Toast.makeText(SignInActivity.this,"Person: "+person.getAgeRange().toString(),Toast.LENGTH_LONG).show();
//                Log.i(TAG, "--------------------------------");
//                Log.i(TAG, "Display Name: " + person.getDisplayName());
//                Log.i(TAG, "Gender: " + person.getGender());
//                Log.i(TAG, "AboutMe: " + person.getAboutMe());
//                Log.i(TAG, "Birthday: " + person.getBirthday());
//                Log.i(TAG, "Current Location: " + person.getCurrentLocation());
//                Log.i(TAG, "Language: " + person.getLanguage());

                try{
                    SharedPreferences google_session = getSharedPreferences("SIGN_IN_PREFERENCE",MODE_PRIVATE);
                    SharedPreferences.Editor editor = google_session.edit();
                    editor.putString("SIGN_IN_TYPE","GOOGLE");
                    editor.putString("GOOGLE_USER_ID",googleSignInAccount.getId());
                    editor.putString("GOOGLE_USER_NAME",googleSignInAccount.getDisplayName());
                    editor.putString("GOOGLE_USER_EMAIL",googleSignInAccount.getEmail());
                    if(googleSignInAccount.getPhotoUrl()!=null) editor.putString("GOOGLE_USER_IMAGE",googleSignInAccount.getPhotoUrl().getPath());
                    editor.putBoolean("SIGNED_IN",true);
                    editor.apply();

                    Intent intent = new Intent(SignInActivity.this,MainActivity.class);
                    startActivity(intent);
//                    String text = "Sign Out of Account Name: "+googleSignInAccount.getEmail();
//                    signInButton.setVisibility(View.GONE);
//                    Toast.makeText(SignInActivity.this,"Welcome: "+googleSignInAccount.getId(),Toast.LENGTH_LONG).show();
//                    Toast.makeText(SignInActivity.this,"Welcome: "+googleSignInAccount.getDisplayName(),Toast.LENGTH_LONG).show();
//                    Toast.makeText(SignInActivity.this,"Welcome: "+googleSignInAccount.getEmail(),Toast.LENGTH_LONG).show();
//                    Toast.makeText(SignInActivity.this,"Welcome: "+googleSignInAccount.getFamilyName(),Toast.LENGTH_LONG).show();
//                    Toast.makeText(SignInActivity.this,"Welcome: "+googleSignInAccount.getGivenName(),Toast.LENGTH_LONG).show();
//                    Toast.makeText(SignInActivity.this,"Welcome: "+googleSignInAccount.getPhotoUrl(),Toast.LENGTH_LONG).show(
                }
                catch (Exception e){
                    Toast.makeText(SignInActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }else {
                Toast.makeText(SignInActivity.this,"Failed to sign in!!!",Toast.LENGTH_LONG).show();
            }
        
        }
//        sign_out_button = (Button) findViewById(R.id.google_sign_out_button);
//        sign_out_button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(@NonNull Status status) {
//                        view.setVisibility(View.GONE);
//                        signInButton.setVisibility(View.VISIBLE);
//                        Toast.makeText(SignInActivity.this,status.getStatusMessage(),Toast.LENGTH_LONG).show();
//                    }
//                });
//            }
//        });
//
//        if(googleApiClient.isConnected()){
//            signInButton.setVisibility(View.GONE);
//            sign_out_button.setVisibility(View.VISIBLE);
//        }else{
//            signInButton.setVisibility(View.VISIBLE);
//            sign_out_button.setVisibility(View.INVISIBLE);
//        }
//    private void revokeAccess(){
//        Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                        // ...
//                    }
//                });
//    }

        /*Facebook*/
        facebook_callback_manager.onActivityResult(requestCode, resultCode, data);

        /*Twitter*/
        twitter_sign_in_button.onActivityResult(requestCode,resultCode,data);
    }

    public void go_home(){
        startActivity(new Intent(SignInActivity.this,MainActivity.class));
    }

}
