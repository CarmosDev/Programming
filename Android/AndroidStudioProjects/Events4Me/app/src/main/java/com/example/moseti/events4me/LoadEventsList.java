package com.example.moseti.events4me;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by MOSETI on 27/07/2017.
 */

public class LoadEventsList extends BaseAdapter {

    Context CONTEXT;
    LayoutInflater layoutInflater;

    public LoadEventsList(Context context){
        CONTEXT = context;
    }

    public class Holder{
        ImageView event_image;
        FloatingActionButton event_favoriter;
        TextView event_name;
        TextView event_time;
        TextView event_venue;
        TextView event_cost;
        TextView event_buy_tickets;
    }

    @Override
    public int getCount() {
        return 4; //Events.Number
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        layoutInflater = (LayoutInflater) CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.event_card,null);

        Holder holder = new Holder();
        holder.event_image = view.findViewById(R.id.event_card_image);
        holder.event_favoriter = view.findViewById(R.id.event_card_favorite);
        holder.event_name = view.findViewById(R.id.event_card_name);
        holder.event_time = view.findViewById(R.id.event_card_time);
        holder.event_venue = view.findViewById(R.id.event_card_venue);
        holder.event_cost = view.findViewById(R.id.event_card_cost);
        holder.event_buy_tickets = view.findViewById(R.id.event_card_buy_tickets);

        //Print Data From DB
        try{
            holder.event_image.setImageBitmap(new LoadRemoteImage().execute("https://i2.wp.com/www.gigwapi.com/wp-content/uploads/2017/05/daddy-yo.jpg?fit=1080%2C1080").get());
        }catch (Exception e){
            Toast.makeText(CONTEXT,e.getMessage(),Toast.LENGTH_LONG).show();
        }

        return view;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }
}
