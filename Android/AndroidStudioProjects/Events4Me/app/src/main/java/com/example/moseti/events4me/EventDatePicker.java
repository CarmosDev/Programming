package com.example.moseti.events4me;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by MOSETI on 27/07/2017.
 */

public class EventDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public Context context;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        context = getActivity();
//        if (isBrokenSamsungDevice()) {
//            context = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog);
//        }

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(context, this, year, month, day);
    }

//    private static boolean isBrokenSamsungDevice() {
//        return (Build.MANUFACTURER.equalsIgnoreCase("samsung")
//                && isBetweenAndroidVersions(
//                Build.VERSION_CODES.LOLLIPOP,
//                Build.VERSION_CODES.LOLLIPOP_MR1));
//    }
//
//    private static boolean isBetweenAndroidVersions(int min, int max) {
//        return Build.VERSION.SDK_INT >= min && Build.VERSION.SDK_INT <= max;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Toast.makeText(context,"Fetching events for date: "+day+"/"+month+"/"+year,Toast.LENGTH_LONG).show();
    }
}
