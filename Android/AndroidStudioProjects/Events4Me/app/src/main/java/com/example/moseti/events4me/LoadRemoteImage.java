package com.example.moseti.events4me;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by MOSETI on 26/07/2017.
 */

public class LoadRemoteImage extends AsyncTask<String,Void,Bitmap> {

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            InputStream inputStream=new URL(params[0]).openStream();
            return  BitmapFactory.decodeStream(inputStream);
        }catch (Exception e){
            System.out.println("Exception : "+e);
            return null;
        }
    }
}
