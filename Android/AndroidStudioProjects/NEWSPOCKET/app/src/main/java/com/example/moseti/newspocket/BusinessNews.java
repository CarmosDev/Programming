package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class BusinessNews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button_dailynation_business = (Button)findViewById(R.id.businessNews_button_dailynation);
        button_dailynation_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BusinessNews.this, Stories.class);
                intent.putExtra("source_type", "nation");
                intent.putExtra("xml_url", "http://www.nation.co.ke/business/-/996/996/-/view/asFeed/-/14lpkvc/-/index.xml");
                startActivity(intent);
            }
        });
        Button button_standard_business = (Button) findViewById(R.id.businessNews_button_standard);
        button_standard_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BusinessNews.this, Stories.class);
                intent.putExtra("source_type", "standard_aljazeera");
                intent.putExtra("xml_url", "http://www.standardmedia.co.ke/rss/business.php");
                startActivity(intent);
            }
        });
        Button button_citizen_business = (Button) findViewById(R.id.businessNews_button_citizen);
        button_citizen_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BusinessNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://citizentv.co.ke/business/feed/");
                startActivity(intent);
            }
        });
        Button button_capitalfm_business = (Button) findViewById(R.id.businessNews_button_capitalfm);
        button_capitalfm_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BusinessNews.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://www.capitalfm.co.ke/business/feed/");
                startActivity(intent);
            }
        });
        Button button_businessdaily_business = (Button)findViewById(R.id.businessNews_button_businessdaily);
        button_businessdaily_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BusinessNews.this, Stories.class);
                intent.putExtra("source_type", "nation");
                intent.putExtra("xml_url", "http://www.businessdailyafrica.com/-/539444/539444/-/view/asFeed/-/xs2vhb/-/index.xml");
                startActivity(intent);
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_business);

        AdView adView = new AdView(BusinessNews.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/8655327476");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }

}
