package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final InterstitialAd publisherInterstitialAd = new InterstitialAd(MainActivity.this);
        publisherInterstitialAd.setAdUnitId("ca-app-pub-5355225842445508/5775266279");
        publisherInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                startActivity(new Intent(MainActivity.this,NewsCategories.class));
            }
        });
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        publisherInterstitialAd.loadAd(adRequest);

        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(publisherInterstitialAd.isLoaded()){
                    publisherInterstitialAd.show();
                }else{
                    startActivity(new Intent(MainActivity.this, NewsCategories.class));
                }
            }
        }, 4000);
    }
}
