package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class NewsCategories extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_categories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button latest_news_button = (Button)findViewById(R.id.category_button_latestnews);
        latest_news_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this, LatestNews.class));
            }
        });

        Button local_news_button = (Button)findViewById(R.id.category_button_localnews);
        local_news_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this,LocalNews.class));
            }
        });

        Button international_news_button = (Button)findViewById(R.id.category_button_internationalnews);
        international_news_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this,InternationalNews.class));
            }
        });

        Button business_news_button = (Button)findViewById(R.id.category_button_business);
        business_news_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this,BusinessNews.class));
            }
        });

        Button entertainment_button = (Button)findViewById(R.id.category_button_entertainment);
        entertainment_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this,Entertainment.class));
            }
        });

        Button sports_news_button = (Button)findViewById(R.id.category_button_sports);
        sports_news_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this,SportsNews.class));
            }
        });

        Button soccer_news_button = (Button)findViewById(R.id.category_button_soccer);
        soccer_news_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this,SoccerNews.class));
            }
        });
        Button opinion_news_button = (Button)findViewById(R.id.category_button_opinion);
        opinion_news_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsCategories.this, Opinion.class));
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_categories);

        AdView adView = new AdView(NewsCategories.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/1271661471");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }

}
