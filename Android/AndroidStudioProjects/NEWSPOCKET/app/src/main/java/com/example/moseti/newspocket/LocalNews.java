package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class LocalNews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button localnews_button_dailynation = (Button)findViewById(R.id.localNews_button_dailynation);
        localnews_button_dailynation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocalNews.this, Stories.class);
                intent.putExtra("source_type", "nation");
                intent.putExtra("xml_url", "http://www.nation.co.ke/counties/-/1107872/1107872/-/view/asFeed/-/3ycob6/-/index.xml");
                startActivity(intent);
            }
        });
        Button localnews_button_standard = (Button)findViewById(R.id.localNews_button_standard);
        localnews_button_standard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocalNews.this, Stories.class);
                intent.putExtra("source_type", "standard_aljazeera");
                intent.putExtra("xml_url", "http://www.standardmedia.co.ke/rss/kenya.php");
                startActivity(intent);
            }
        });
        Button localnews_button_citizen = (Button)findViewById(R.id.localNews_button_citizen);
        localnews_button_citizen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocalNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://citizentv.co.ke/feed/");
                startActivity(intent);
            }
        });
        Button localnews_button_nairobinews = (Button)findViewById(R.id.localNews_button_nairobinews);
        localnews_button_nairobinews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocalNews.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://nairobinews.nation.co.ke/category/news/feed/");
                startActivity(intent);
            }
        });
        Button localnews_button_capitalfm = (Button)findViewById(R.id.localNews_button_capitalfm);
        localnews_button_capitalfm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LocalNews.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://www.capitalfm.co.ke/news/feed/");
                startActivity(intent);
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_local);

        AdView adView = new AdView(LocalNews.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/5701861071");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }

}
