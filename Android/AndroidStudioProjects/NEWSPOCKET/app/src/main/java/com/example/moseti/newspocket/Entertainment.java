package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class Entertainment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertainment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button_ghafla_entertainment = (Button)findViewById(R.id.entertainment_button_ghafla);
        button_ghafla_entertainment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Entertainment.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://www.ghafla.co.ke/component/ninjarsssyndicator/?feed_id=1&format=raw");
                startActivity(intent);
            }
        });
        Button button_mpasho_entertainment = (Button)findViewById(R.id.entertainment_button_mpasho);
        button_mpasho_entertainment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Entertainment.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "https://mpasho.co.ke/feed/");
                startActivity(intent);
            }
        });
        Button button_dailynation_entertainment = (Button)findViewById(R.id.entertainment_button_dailynation);
        button_dailynation_entertainment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Entertainment.this, Stories.class);
                intent.putExtra("source_type", "nation");
                intent.putExtra("xml_url", "http://www.nation.co.ke/lifestyle/-/1190/1190/-/view/asFeed/-/tgsua2z/-/index.xml");
                startActivity(intent);
            }
        });
        Button button_capitalfm_entertainment = (Button)findViewById(R.id.entertainment_button_capitalfm);
        button_capitalfm_entertainment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Entertainment.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://www.capitalfm.co.ke/lifestyle/feed/");
                startActivity(intent);
            }
        });
        Button button_standard_entertainment = (Button) findViewById(R.id.entertainment_button_standard);
        button_standard_entertainment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Entertainment.this,Stories.class);
                intent.putExtra("source_type","standard_aljazeera");
                intent.putExtra("xml_url","http://www.standardmedia.co.ke/rss/entertainment.php");
                startActivity(intent);
            }
        });
        Button button_nairobinews_entertainment = (Button)findViewById(R.id.entertainment_button_nairobinews);
        button_nairobinews_entertainment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Entertainment.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://nairobinews.nation.co.ke/category/chillax/feed/");
                startActivity(intent);
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_entertainment);

        AdView adView = new AdView(Entertainment.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/1132060677");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }
}
