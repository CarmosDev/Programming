package com.example.moseti.newspocket;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

/**
 * Created by MOSETI on 08/04/2016.
 */
public class LoadStories extends AsyncTask<Void,Void,Void> {
    Context CONTEXT;
    ListView LISTVIEW;
    String SOURCE_TYPE;
    String XML_URL;
    TextView NUMBER;
    String number;
    ProgressDialog progressDialog;
    ArrayList<String> TITLES = new ArrayList<>();
    ArrayList<String> LINKS = new ArrayList<>();
    ArrayList<String> DESCRIPTIONS = new ArrayList<>();
    ArrayList<String> IMAGES = new ArrayList<>();
    String SOURCE;

    public LoadStories(Context context,ListView list_view,String source_type,String xml_url,TextView number_label){
        CONTEXT = context;
        LISTVIEW = list_view;
        SOURCE_TYPE = source_type;
        XML_URL = xml_url;
        NUMBER = number_label;
    }
    @Override
    protected void onPreExecute() {
        progressDialog=new ProgressDialog(CONTEXT,ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        switch (SOURCE_TYPE){
            case "nation":
                nation();
                break;
            case "standard_aljazeera":
                standard_aljazeera();
                break;
            case "citizen_star_nairobinews_capitalfm":
                citizen_star_nairobinews_capitalfm();
                break;
            case "soccer":
                soccer();
                break;
            default:
                break;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        NUMBER.setText(number);
        switch (SOURCE_TYPE){
            case "nation":
                LISTVIEW.setAdapter(new Stories_Adapter(CONTEXT, TITLES.toArray(), LINKS.toArray(), DESCRIPTIONS.toArray(), null, SOURCE));
                break;
            case "standard_aljazeera":
                LISTVIEW.setAdapter(new Stories_Adapter(CONTEXT, TITLES.toArray(), LINKS.toArray(), DESCRIPTIONS.toArray(), null, SOURCE));
                break;
            case "citizen_star_nairobinews_capitalfm":
                LISTVIEW.setAdapter(new Stories_Adapter(CONTEXT, TITLES.toArray(), LINKS.toArray(), null, null, SOURCE));
                break;
            case "soccer":
                LISTVIEW.setAdapter(new Stories_Adapter(CONTEXT,TITLES.toArray(),LINKS.toArray(),null,IMAGES.toArray(),SOURCE));
                break;
            default:
                break;
        }
        progressDialog.dismiss();
        super.onPostExecute(aVoid);
    }
    public Void nation(){
        try{
            Document document = Jsoup.connect(XML_URL).get();
            int items_number = document.getElementsByTag("item").size();
            number = ""+items_number;
            SOURCE = document.child(0).getElementsByTag("title").get(0).text();
            for(int a=0;a<items_number;a++){
                TITLES.add(document.getElementsByTag("item").get(a).child(0).text());
                LINKS.add(document.getElementsByTag("item").get(a).child(1).text());
                DESCRIPTIONS.add(document.getElementsByTag("item").get(a).child(2).text());
            }
        }catch (Exception e){
            System.out.println("Exception : "+e);
        }
        return null;
    }
    public Void standard_aljazeera(){
        try{
            Document document = Jsoup.connect(XML_URL).get();
            int items_number = document.getElementsByTag("item").size();
            number = ""+items_number;
            SOURCE = document.child(0).getElementsByTag("title").get(0).text();
            for(int a=0;a<items_number;a++){
                TITLES.add(document.getElementsByTag("item").get(a).child(0).text());
                DESCRIPTIONS.add(document.getElementsByTag("item").get(a).child(1).text());
                LINKS.add(document.getElementsByTag("item").get(a).child(2).text());
            }
        }catch (Exception e){
            System.out.println("Exception : "+e);
        }
        return null;
    }
    public Void citizen_star_nairobinews_capitalfm(){
        try{
            Document document = Jsoup.connect(XML_URL).get();
            int items_number = document.getElementsByTag("item").size();
            number = ""+items_number;
            SOURCE = document.child(0).getElementsByTag("title").get(0).text();
            for(int a=0;a<items_number;a++){
                TITLES.add(document.getElementsByTag("item").get(a).child(0).text());
                LINKS.add(document.getElementsByTag("item").get(a).child(1).text());
            }
        }catch (Exception e){
            System.out.println("Exception : "+e);
        }
        return null;
    }
    public Void soccer(){
        try{
            Document document = Jsoup.connect(XML_URL).get();
            int items_number = document.getElementsByTag("item").size();
            number = ""+items_number;
            SOURCE = document.child(0).getElementsByTag("title").get(0).text();
            for(int a=0;a<items_number;a++){
                TITLES.add(document.getElementsByTag("item").get(a).getElementsByTag("title").get(0).text());
                LINKS.add(document.getElementsByTag("item").get(a).getElementsByTag("link").get(0).text());
                IMAGES.add(document.getElementsByTag("item").get(a).getElementsByTag("enclosure").get(0).attr("url"));
            }
        }catch (Exception e){
            System.out.println("Exception : "+e);
        }
        return null;
    }
}