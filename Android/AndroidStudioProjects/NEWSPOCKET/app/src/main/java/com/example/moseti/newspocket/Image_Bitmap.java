package com.example.moseti.newspocket;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by MOSETI on 08/04/2016.
 */
public class Image_Bitmap extends AsyncTask<String,Void,Bitmap>{
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    @Override
    protected Bitmap doInBackground(String... params) {
        try{
            InputStream inputStream=new URL(params[0]).openStream();
            return BitmapFactory.decodeStream(inputStream);
        }catch (Exception e){
            System.out.println("Bitmap Error : "+e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
    }
}