package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class SoccerNews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soccer_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button soccer_button_bbc = (Button)findViewById(R.id.soccerNews_button_bbc);
        soccer_button_bbc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this, Stories.class);
                intent.putExtra("source_type", "standard_aljazeera");
                intent.putExtra("xml_url", "http://feeds.bbci.co.uk/sport/0/football/rss.xml?edition=uk");
                startActivity(intent);
            }
        });
        Button soccer_button_cnn = (Button)findViewById(R.id.soccerNews_button_cnn);
        soccer_button_cnn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://rss.cnn.com/rss/edition_football.rss");
                startActivity(intent);
            }
        });
        Button soccer_button_goal = (Button)findViewById(R.id.soccerNews_button_goal);
        soccer_button_goal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this, Stories.class);
                intent.putExtra("source_type", "soccer");
                intent.putExtra("xml_url", "http://www.goal.com/en-ke/feeds/news?fmt=rss&ICID=HP");
                startActivity(intent);
            }
        });
        Button soccer_button_uefa = (Button)findViewById(R.id.soccerNews_button_uefa);
        soccer_button_uefa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this,Stories.class);
                intent.putExtra("source_type","soccer");
                intent.putExtra("xml_url","http://www.uefa.com/rssfeed/news/rss.xml");
                startActivity(intent);
            }
        });
        Button soccer_button_fox = (Button)findViewById(R.id.soccerNews_button_fox);
        soccer_button_fox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this,Stories.class);
                intent.putExtra("source_type","soccer");
                intent.putExtra("xml_url","http://api.foxsports.com/v1/rss?partnerKey=zBaFxRyGKCfxBagJG9b8pqLyndmvo7UU&tag=soccer");
                startActivity(intent);
            }
        });
        Button soccer_button_espn = (Button)findViewById(R.id.soccerNews_button_espn);
        soccer_button_espn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this,Stories.class);
                intent.putExtra("source_type","soccer");
                intent.putExtra("xml_url","http://www.espnfc.com/rss");
                startActivity(intent);
            }
        });
        Button soccer_button_yahoo = (Button)findViewById(R.id.soccerNews_button_yahoo);
        soccer_button_yahoo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this,Stories.class);
                intent.putExtra("source_type","standard_aljazeera");
                intent.putExtra("xml_url","http://sports.yahoo.com/soccer/rss.xml");
                startActivity(intent);
            }
        });
        Button soccer_button_reuters = (Button)findViewById(R.id.soccerNews_button_reuters);
        soccer_button_reuters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://mf.feeds.reuters.com/reuters/UKSportsNews");
                startActivity(intent);
            }
        });
        Button soccer_button_fa = (Button)findViewById(R.id.soccerNews_button_fa);
        soccer_button_fa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SoccerNews.this,Stories.class);
                intent.putExtra("source_type","soccer");
                intent.putExtra("xml_url","http://www.thefa.com/thefacup/rss");
                startActivity(intent);
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_soccer);

        AdView adView = new AdView(SoccerNews.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/2608793875");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }

}
