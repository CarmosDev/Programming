package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class InternationalNews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_international_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button_dailynation_international = (Button)findViewById(R.id.internationalNews_button_dailynation);
        button_dailynation_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InternationalNews.this, Stories.class);
                intent.putExtra("source_type", "nation");
                intent.putExtra("xml_url", "http://www.nation.co.ke/news/world/-/1068/1068/-/view/asFeed/-/kira34z/-/index.xml");
                startActivity(intent);
            }
        });
        Button button_standard_international = (Button) findViewById(R.id.internationalNews_button_standard);
        button_standard_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InternationalNews.this, Stories.class);
                intent.putExtra("source_type", "standard_aljazeera");
                intent.putExtra("xml_url", "http://www.standardmedia.co.ke/rss/world.php");
                startActivity(intent);
            }
        });
        Button button_aljazeera_international = (Button) findViewById(R.id.internationalNews_button_aljazeera);
        button_aljazeera_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InternationalNews.this, Stories.class);
                intent.putExtra("source_type", "standard_aljazeera");
                intent.putExtra("xml_url", "http://www.aljazeera.com/xml/rss/all.xml");
                startActivity(intent);
            }
        });
        Button button_eastafrican_international = (Button) findViewById(R.id.internationalNews_button_eastafrican);
        button_eastafrican_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InternationalNews.this,Stories.class);
                intent.putExtra("source_type","nation");
                intent.putExtra("xml_url","http://www.theeastafrican.co.ke/-/2456/2456/-/view/asFeed/-/13blr5d/-/index.xml");
                startActivity(intent);
            }
        });
        Button button_bbc_international = (Button) findViewById(R.id.internationalNews_button_bbc);
        button_bbc_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InternationalNews.this,Stories.class);
                intent.putExtra("source_type","standard_aljazeera");
                intent.putExtra("xml_url","http://feeds.bbci.co.uk/news/rss.xml");
                startActivity(intent);
            }
        });
        Button button_cnn_international = (Button)findViewById(R.id.internationalNews_button_cnn);
        button_cnn_international.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InternationalNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://rss.cnn.com/rss/edition_world.rss");
                startActivity(intent);
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_international);

        AdView adView = new AdView(InternationalNews.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/7178594272");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }

}
