package com.example.moseti.newspocket;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by MOSETI on 08/04/2016.
 */
public class Stories_Adapter extends BaseAdapter{
    Context CONTEXT;
    Object[] TITLES;
    Object[] LINKS;
    Object[] DESCRIPTIONS;
    Object[] IMAGES;
    String SOURCE;
    LayoutInflater layoutInflater;
    public Stories_Adapter(Context context,Object[] titles_array,Object[]links_array,Object[]descriptions_array,Object[] images_array,String news_source){
        CONTEXT = context;
        TITLES = titles_array;
        LINKS = links_array;
        DESCRIPTIONS = descriptions_array;
        IMAGES = images_array;
        SOURCE=news_source;
    }
    public class Holder{
        TextView newsSource;
        TextView storyTitle;
        TextView storyDescription;
        ImageView storyImage;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater)CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = layoutInflater.inflate(R.layout.story_element,null);
        Holder holder=new Holder();
        holder.newsSource = (TextView)convertView.findViewById(R.id.story_element_news_source);
        holder.storyTitle = (TextView)convertView.findViewById(R.id.story_element_news_title);

        holder.newsSource.setText(SOURCE);
        holder.storyTitle.setText(TITLES[position].toString());

        if(DESCRIPTIONS!=null){
            holder.storyDescription = (TextView)convertView.findViewById(R.id.story_element_news_description);
            holder.storyDescription.setText(DESCRIPTIONS[position].toString());
        }

        if(IMAGES!=null){
            holder.storyImage = (ImageView)convertView.findViewById(R.id.story_element_news_image);
            try{
                holder.storyImage.setImageBitmap(new Image_Bitmap().execute(IMAGES[position].toString()).get());
            }catch (Exception e){
                System.out.println("Image Error : "+e);
            }
        }
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CONTEXT,ViewStory.class);
                intent.putExtra("story_url",LINKS[position].toString());
                intent.putExtra("story_title",TITLES[position].toString());
                CONTEXT.startActivity(intent);
            }
        });
        return convertView;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
