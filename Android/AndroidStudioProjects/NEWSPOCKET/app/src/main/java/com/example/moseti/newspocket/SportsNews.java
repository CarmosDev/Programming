package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class SportsNews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sports_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button_dailynation_sports = (Button)findViewById(R.id.sportsNews_button_dailynation);
        button_dailynation_sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SportsNews.this, Stories.class);
                intent.putExtra("source_type", "nation");
                intent.putExtra("xml_url", "http://www.nation.co.ke/sports/-/1090/1090/-/view/asFeed/-/hlukmj/-/index.xml");
                startActivity(intent);
            }
        });
        Button button_standard_sports = (Button) findViewById(R.id.sportsNews_button_standard);
        button_standard_sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SportsNews.this, Stories.class);
                intent.putExtra("source_type", "standard_aljazeera");
                intent.putExtra("xml_url", "http://www.standardmedia.co.ke/rss/sports.php");
                startActivity(intent);
            }
        });
        Button button_citizen_sports = (Button) findViewById(R.id.sportsNews_button_citizen);
        button_citizen_sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SportsNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://citizentv.co.ke/sports/feed/");
                startActivity(intent);
            }
        });
        Button button_capitalfm_sports = (Button) findViewById(R.id.sportsNews_button_capitalfm);
        button_capitalfm_sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SportsNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://www.capitalfm.co.ke/sports/feed/");
                startActivity(intent);
            }
        });
        Button button_nairobinews_sports = (Button)findViewById(R.id.sportsNews_button_nairobinews);
        button_nairobinews_sports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SportsNews.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://nairobinews.nation.co.ke/category/sports/feed/");
                startActivity(intent);
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_sports);

        AdView adView = new AdView(SportsNews.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/4085527079");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }
}
