package com.example.moseti.newspocket;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class LatestNews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_latest_news);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button_daily_nation_latest = (Button)findViewById(R.id.latestNews_button_dailynation);
        button_daily_nation_latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LatestNews.this, Stories.class);
                intent.putExtra("source_type", "nation");
                intent.putExtra("xml_url", "http://www.nation.co.ke/news/-/1056/1056/-/view/asFeed/-/wr4x5yz/-/index.xml");
                startActivity(intent);
            }
        });
        Button button_standard_latest = (Button) findViewById(R.id.latestNews_button_standard);
        button_standard_latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LatestNews.this, Stories.class);
                intent.putExtra("source_type", "standard_aljazeera");
                intent.putExtra("xml_url", "http://www.standardmedia.co.ke/rss/headlines.php");
                startActivity(intent);
            }
        });
        Button button_citizen_latest = (Button) findViewById(R.id.latestNews_button_citizen);
        button_citizen_latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LatestNews.this, Stories.class);
                intent.putExtra("source_type", "citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url", "http://citizentv.co.ke/news/feed/");
                startActivity(intent);
            }
        });
        Button button_star_latest = (Button) findViewById(R.id.latestNews_button_thestar);
        button_star_latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LatestNews.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://www.the-star.co.ke/taxonomy/term/all/rss.xml");
                startActivity(intent);
            }
        });
        Button button_nairobinews_latest = (Button) findViewById(R.id.latestNews_button_nairobinews);
        button_nairobinews_latest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LatestNews.this,Stories.class);
                intent.putExtra("source_type","citizen_star_nairobinews_capitalfm");
                intent.putExtra("xml_url","http://nairobinews.co.ke/feed/");
                startActivity(intent);
            }
        });
        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linearlayout_latest);

        AdView adView = new AdView(LatestNews.this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-5355225842445508/4225127877");
        AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
        adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(adRequestBuilder.build());
    }

}
