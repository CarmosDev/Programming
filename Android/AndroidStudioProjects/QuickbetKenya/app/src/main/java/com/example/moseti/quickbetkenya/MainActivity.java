package com.example.moseti.quickbetkenya;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final InterstitialAd interstitialAd=new InterstitialAd(MainActivity.this);
        interstitialAd.setAdUnitId("ca-app-pub-5355225842445508/5002563470");
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                startActivity(new Intent(MainActivity.this, Select.class));
            }
        });
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        interstitialAd.loadAd(adRequest);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(interstitialAd.isLoaded()){
                    interstitialAd.show();
                }else{
                startActivity(new Intent(MainActivity.this,Select.class));
                }
            }
        },4000);
    }
}