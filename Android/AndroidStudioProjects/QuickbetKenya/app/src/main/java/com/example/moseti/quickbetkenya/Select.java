package com.example.moseti.quickbetkenya;

import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class Select extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        LinearLayout linearLayout = (LinearLayout)findViewById(R.id.linear_layout_select);
        ListView listView = (ListView)findViewById(R.id.list_sites);
        final Object[] button_labels={"Sportpesa","Betway Kenya","BetIn Kenya","Bet Yetu","mCheza","Elitebet Kenya","Just Bet","Bet Planet Kenya"};
        final Object[] button_sites={  "https://www.sportpesa.com/mobile/#/home",
                                "https://www.betway.co.ke/",
                                "https://mobile.betin.co.ke/Home.aspx",
                                "https://m.betyetu.co.ke/",
                                "http://www.mcheza.co.ke/en/",
                                "http://www.elitebetkenya.com/coupon.php",
                                "http://justbet.co.ke/",
                                "https://www.betplanetkenya.com/sports/football"};
        final InterstitialAd interstitialAd=new InterstitialAd(Select.this);
        interstitialAd.setAdUnitId("ca-app-pub-5355225842445508/6479296678");
        AdRequest adRequest = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        interstitialAd.loadAd(adRequest);

        AdView adView = new AdView(Select.this);
        adView.setAdUnitId("ca-app-pub-5355225842445508/7956029870");
        adView.setAdSize(AdSize.BANNER);
        AdRequest.Builder builder = new AdRequest.Builder();
        builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        linearLayout.addView(adView);
        adView.loadAd(builder.build());

        if(listView!=null){
            listView.setAdapter(new BaseAdapter() {
                LayoutInflater layoutInflater=(LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                @Override
                public int getCount() {
                    return button_labels.length;
                }

                @Override
                public Object getItem(int position) {
                    return position;
                }

                @Override
                public long getItemId(int position) {
                    return position;
                }

                @Override
                public View getView(final int position, View convertView, ViewGroup parent) {
                    convertView=layoutInflater.inflate(R.layout.listview_buttons,null);
                    TextView button_label=(TextView)convertView.findViewById(R.id.button_site);
                    button_label.setText(button_labels[position].toString());
                    button_label.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Intent intent = new Intent(Select.this, OpenSite.class);
                                    intent.putExtra("site_url", button_sites[position].toString());
                                    intent.putExtra("site_title", button_labels[position].toString());
                                    startActivity(intent);
                                }
                            });
                            if (interstitialAd.isLoaded()) {
                                interstitialAd.show();
                            } else {
                                Intent intent = new Intent(Select.this, OpenSite.class);
                                intent.putExtra("site_url", button_sites[position].toString());
                                intent.putExtra("site_title", button_labels[position].toString());
                                startActivity(intent);
                            }
                        }
                    });
                    return convertView;
                }
            });
        }
    }

}
