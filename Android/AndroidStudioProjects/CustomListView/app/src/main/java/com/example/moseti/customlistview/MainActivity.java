package com.example.moseti.customlistview;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final String[] planets={"Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune","Pluto",
                          "Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune","Pluto",
                          "Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune","Pluto",
                          "Mercury","Venus","Earth","Mars","Jupiter","Saturn","Uranus","Neptune","Pluto"};
        final String[] planetIndex={"1","2","3","4","5","6","7","8","9",
                              "1","2","3","4","5","6","7","8","9",
                              "1","2","3","4","5","6","7","8","9",
                              "1","2","3","4","5","6","7","8","9"};
        final ListView listView=(ListView)findViewById(R.id.customListView);
        listView.setAdapter(new STORIES_ADAPTER(MainActivity.this,planets,planetIndex));
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if((firstVisibleItem+visibleItemCount)==totalItemCount && totalItemCount!=0){
                    Toast.makeText(MainActivity.this,"End of scroll List!!!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    public class STORIES_ADAPTER extends BaseAdapter{
        public String[] content1;
        public String[] content2;
        public Context story_context;
        public LayoutInflater layoutInflater;
        public STORIES_ADAPTER(Context context,String[]listNumber,String[]list){
            content1=listNumber;
            content2=list;
            story_context=context;
            layoutInflater=(LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public int getCount() {
            return content2.length;
        }
        public class Holder{
            TextView textView1;
            TextView textView2;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Holder holder=new Holder();
            View rowView=layoutInflater.inflate(R.layout.story_elements,null);
            holder.textView1=(TextView)rowView.findViewById(R.id.textView1);
            holder.textView2=(TextView)rowView.findViewById(R.id.textView2);
            holder.textView1.setText(content1[position]);
            holder.textView2.setText(content2[position]);
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(story_context,"You Clicked : "+content1[position],Toast.LENGTH_LONG).show();
                }
            });
            return  rowView;
        }
    }
}
