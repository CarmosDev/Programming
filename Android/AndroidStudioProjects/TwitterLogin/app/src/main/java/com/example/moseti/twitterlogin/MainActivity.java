package com.example.moseti.twitterlogin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Query;

public class MainActivity extends AppCompatActivity {

    TwitterLoginButton loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        TwitterConfig config = new TwitterConfig.Builder(this)
//                .logger(new DefaultLogger(Log.DEBUG))
//                .twitterAuthConfig(new TwitterAuthConfig("CONSUMER_KEY", "CONSUMER_SECRET"))
//                .debug(true)
//                .build();
//        Twitter.initialize(config);

        Twitter.initialize(this);

        loginButton = (TwitterLoginButton) findViewById(R.id.twitter_login_button);
        loginButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                final String user_name = session.getUserName();
                final long user_id = session.getUserId();

                TwitterApiClient twitterApiClient = new TwitterApiClient(session);
                AccountService accountService = twitterApiClient.getAccountService();
                accountService.verifyCredentials(true,false,true).enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> result) {
                        User user = result.data;
                        Toast.makeText(MainActivity.this,""+user.id,Toast.LENGTH_LONG).show();
                        Toast.makeText(MainActivity.this,user.name,Toast.LENGTH_LONG).show();
                        Toast.makeText(MainActivity.this,user.email,Toast.LENGTH_LONG).show();
                        Toast.makeText(MainActivity.this,user.profileBackgroundImageUrlHttps,Toast.LENGTH_LONG).show();
                        Toast.makeText(MainActivity.this,user.profileImageUrlHttps,Toast.LENGTH_LONG).show();
                        Toast.makeText(MainActivity.this,user.description,Toast.LENGTH_LONG).show();
                        Toast.makeText(MainActivity.this,user.location,Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Toast.makeText(MainActivity.this,exception.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });

//                TwitterAuthClient twitterAuthClient = new TwitterAuthClient();
//                twitterAuthClient.requestEmail(session, new Callback<String>() {
//                    @Override
//                    public void success(Result<String> result) {
//                        Toast.makeText(MainActivity.this,user_name,Toast.LENGTH_LONG).show();
//                        Toast.makeText(MainActivity.this,""+user_id,Toast.LENGTH_LONG).show();
//                        Toast.makeText(MainActivity.this,result.data,Toast.LENGTH_LONG).show();
//                    }
//
//                    @Override
//                    public void failure(TwitterException exception) {
//                        Toast.makeText(MainActivity.this,exception.getMessage(),Toast.LENGTH_LONG).show();
//                    }
//                });

            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(MainActivity.this,exception.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Pass the activity result to the login button.
        loginButton.onActivityResult(requestCode, resultCode, data);
    }

}
