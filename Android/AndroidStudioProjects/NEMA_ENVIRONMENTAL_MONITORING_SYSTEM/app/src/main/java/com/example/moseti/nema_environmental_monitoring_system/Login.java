package com.example.moseti.nema_environmental_monitoring_system;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final EditText username = (EditText)findViewById(R.id.login_user_name);
        final EditText userpassword = (EditText)findViewById(R.id.login_user_password);
        Button userlogin_button = (Button)findViewById(R.id.login_user);

        userlogin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().length()==0){
                    Toast.makeText(Login.this,"Missing Username!!!",Toast.LENGTH_SHORT).show();
                }else if(userpassword.getText().toString().length()==0){
                    Toast.makeText(Login.this,"Missing Password!!!",Toast.LENGTH_SHORT).show();
                }else{
                    final ProgressDialog progressDialog=new ProgressDialog(Login.this);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setMessage("Logging In...");
                    progressDialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            startActivity(new Intent(Login.this,Tasks.class));
                            progressDialog.dismiss();
                        }
                    }, 5000);
                }
            }
        });
    }
}
