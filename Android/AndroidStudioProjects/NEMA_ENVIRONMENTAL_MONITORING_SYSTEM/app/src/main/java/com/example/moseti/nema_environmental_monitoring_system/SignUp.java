package com.example.moseti.nema_environmental_monitoring_system;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button button_sign_up_user = (Button)findViewById(R.id.sign_up_user);
        final EditText username_sign_up = (EditText)findViewById(R.id.sign_up_username);
        final EditText username_sign_up_password = (EditText)findViewById(R.id.sign_up_password);
        final EditText username_sign_up_confirm_password = (EditText)findViewById(R.id.sign_up_confirm_password);

        button_sign_up_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username_sign_up.getText().toString().length()==0){
                    Toast.makeText(SignUp.this,"Missing username!!!",Toast.LENGTH_SHORT).show();
                }else if(username_sign_up_password.getText().toString().length()==0){
                    Toast.makeText(SignUp.this,"Missing user password!!!",Toast.LENGTH_SHORT).show();
                }else if(username_sign_up_confirm_password.getText().toString().length()==0){
                    Toast.makeText(SignUp.this,"Confirm your password!!!",Toast.LENGTH_SHORT).show();
                }else{
                    if(!username_sign_up_password.getText().toString().equals(username_sign_up_confirm_password.getText().toString())){
                        Toast.makeText(SignUp.this,"Password mismatch!!!",Toast.LENGTH_SHORT).show();
                    }else{
                        final ProgressDialog progressDialog = new ProgressDialog(SignUp.this);
                        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progressDialog.setMessage("We are now registering you in a moment...");
                        progressDialog.show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(SignUp.this,"Registered Successfully!!!",Toast.LENGTH_LONG).show();
                                startActivity(new Intent(SignUp.this, Login.class));
                            }
                        }, 5000);
                    }
                }
            }
        });
    }

}
