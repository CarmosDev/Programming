package com.example.moseti.loadremoteimage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new getImage().execute();
    }
    private class getImage extends AsyncTask<Void, Void, Bitmap> {
        protected Bitmap doInBackground(Void...Voids) {
            Bitmap image=null;
            try{
                InputStream inputStream=new URL("http://i1.goal.com/web/goal/2015.12.16-v1/images/default/logo500x500.pn").openStream();
                image=BitmapFactory.decodeStream(inputStream);
            }catch (Exception e){
                System.out.println("EXCEPTION : "+e);
            }
            return image;
        }
        protected void onPostExecute(Bitmap result){
            ((ImageView) findViewById(R.id.remoteImage)).setImageBitmap(result);
        }
    }

}
