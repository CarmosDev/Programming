package com.example.moseti.tenjeradioapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ContactUs extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        Button send_mail=(Button)findViewById(R.id.button_submit_message);
        send_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sender_name=((EditText)findViewById(R.id.contact_user_name)).getText().toString();
                String sender_email=((EditText)findViewById(R.id.contact_user_email)).getText().toString();
                String sender_message=((EditText)findViewById(R.id.contact_user_message)).getText().toString();

                if(sender_name.length()>0 && sender_email.length()>0 && sender_message.length()>0){
                    Intent email=new Intent(Intent.ACTION_SENDTO);
                    email.setType("message/rfc822");
                    email.putExtra(Intent.EXTRA_EMAIL,"carmosdevelopers@gmail.com");
                    email.setData(Uri.parse("mailto:carmosdevelopers@gmail.com"));
                    email.putExtra(Intent.EXTRA_SUBJECT,"Tenje Radio App");
                    email.putExtra(Intent.EXTRA_TEXT,"\nFrom : ".concat(sender_name)
                            .concat("\nEmail Address : ")
                            .concat(sender_email)
                            .concat("\nMessage : \n")
                            .concat(sender_message));
                    try{
                        startActivity(Intent.createChooser(email,"Send message"));
                    }catch (android.content.ActivityNotFoundException e){
                        final AlertDialog alertDialog;
                        AlertDialog.Builder builder=new AlertDialog.Builder(ContactUs.this);
                        builder.setMessage("This app requires an email app in your phone to send the message");
                        builder.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                            }
                        });
                        alertDialog=builder.create();
                        alertDialog.show();
                    }
                }else{
                    AlertDialog alertDialog;
                    AlertDialog.Builder builder=new AlertDialog.Builder(ContactUs.this);
                    builder.setMessage("Incomplete Form!!!");
                    builder.setPositiveButton("Go Back", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    alertDialog=builder.create();
                    alertDialog.show();
                }
            }
        });
    }
}
