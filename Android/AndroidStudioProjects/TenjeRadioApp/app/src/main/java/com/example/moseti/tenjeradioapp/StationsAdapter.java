package com.example.moseti.tenjeradioapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.io.Console;

/**
 * Created by MOSETI on 28/12/2016.
 */

public class StationsAdapter extends BaseAdapter{
    Object[] station_name={
            "Sound Asia","Radio Maria","Radio Waumini","Ghetto Radio", "Truth FM",
            "Kass FM","ATG Radio","Kenya Bible","Hope FM", "Milele FM",
            "Capital FM","Egesa FM","PiliPili FM", "Kiss FM","Kameme FM",
            "Ramogi FM","Musyi FM","BHB FM","Homeboyz Radio","Radio 316",
            "Classic 105","X FM","Voice Of America FM","Unverified Radio","EAST FM",
            "Radio Citizen","Mulembe FM","Radio Jambo","Salaam FM","KBC radio",
            "Mbaitu FM","Baraka FM","Jesus is Lord Radio","Bahari FM","Metro FM",
            "Hero FM"
    };
    Object[] station_frequency={
            "88.0","88.1","88.3","89.5","90.7",
            "91.0","91.1","91.3","93.3","93.6",
            "98.4","98.6","99.5","100.3","101.1",
            "101.7","102.2","102.5","103.5","103.9",
            "105.2","105.5","107.5","","106.3",
            "106.7","97.9","97.5","90.7","95.6",
            "92.5","95.5","105.9","94.2","",
            "99.0"
    };
    Object[] station_website={
            "http://www.soundasiafm.com/",
            "http://www.radiomaria.co.ke/",
            "http://www.radiowaumini.org/",
            "http://ghettoradio.co.ke/",
            "http://truthfm.org/",
            "http://www.kassfm.co.ke/new/",
            "http://www.atgdeliveranceministry.com/",
            "https://www.facebook.com/kenyabibletruthradio/",
            "http://www.hopefm.org/",
            "http://www.mediamaxnetwork.co.ke/milele-radio/",
            "https://www.capitalfm.co.ke/",
            "https://rmsradio.co.ke/brands/egesa-fm/",
            "http://www.mediamaxnetwork.co.ke/pilipili-radio/",
            "https://kiss100.co.ke/",
            "http://www.mediamaxnetwork.co.ke/kameme-radio/",
            "https://rmsradio.co.ke/brands/ramogi-fm/",
            "https://rmsradio.co.ke/brands/musyi-fm/",
            "http://bibliahusema.org/",
            "http://hbr.co.ke/",
            "http://www.radio316.net/",
            "https://classic105.com/",
            "https://www.facebook.com/1055XFM/",
            "http://www.voanews.com/",
            "http://www.unverified.co.ke/",
            "http://eastfm.com/",
            "https://rmsradio.co.ke/brands/radio-citizen/",
            "https://rmsradio.co.ke/brands/mulembe-fm/",
            "https://radiojambo.co.ke/",
            "http://salaamfm.com/",
            "http://radiotaifa.co.ke/",
            "http://mbaitufm.com/",
            "http://barakafm.org/",
            "http://www.jesusislordradio.net/",
            "https://rmsradio.co.ke/brands/bahari-fm/",
            "https://twitter.com/metrofm254?lang=en",
            "http://www.hero.co.ke/"
    };
    Object[] station_url={
            "http://196.207.21.198:88/stream",
            "http://50.7.181.186:8108/",
            "http://41.215.8.70:8000/live",
            "http://149.202.90.221:8096/;&type=mp3",
            "http://uk1-vn.mixstream.net:9960/;stream.mp3",
            "http://media.kassfm.co.ke:8006/live",
            "http://uk1-vn.mixstream.net:10024/",
            "http://sc3.spacialnet.com:32714/",
            "http://41.207.65.69:8000/",
            "http://uk1-vn.mixstream.net:9986/",
            "rtmp://162.obj.netromedia.net/capitalfmflash/capitalfmflash",
            "rtmp://livestream.5centscdn.com/egesafm//70ab015706f9a345e1dcf4c407d8113e.sdp",
            "http://soho.wavestreamer.com:7406/",
            "http://streams.radioafricagroup.co.ke:88/broadwave.mp3?src=4",
            "http://uk1-vn.mixstream.net:10018/",
            "rtmp://livestream.5centscdn.com/ramogifmlive//aaa674aac6b4820dff37773dec5c2120.sdp",
            "rtmp://livestream.5centscdn.com/musyifmlive//adea9534ce3a2febfeba63eb31c7b252.sdp",
            "http://uk1-vn.mixstream.net:9958/",
            "http://91.121.165.88:8116/",
            "http://icy-e-01-cr.sharp-stream.com/familymedia.mp3?device=website",
            "http://streams.radioafricagroup.co.ke:88/broadwave.mp3?src=5&rate=1&kbps=128",
            "http://streams.radioafricagroup.co.ke:88/broadwave.mp3?src=2",
            "http://voa-17.akacast.akamaistream.net/7/315/322029/v1/ibb.akacast.akamaistream.net/voa-17",
            "http://142.4.215.64:8000/",
            "http://streams.radioafricagroup.co.ke:88/broadwave.mp3?src=3",
            "http://livestream.5centscdn.com/radiocitizen/b033a25a551899a4dfe4d3a4f6bbe7b7.sdp/playlist.m3u8",
            "rtmp://livestream.5centscdn.com/mulembefm//18ccf8cf72a6a758703f655eae709c55.sdp",
            "http://streams.radioafricagroup.co.ke:88/broadwave.mp3?src=1",
            "http://ice7.securenetsystems.net/90.7",
            "http://91.121.165.88:8116/stream/",
            "rtmp://165.obj.netromedia.net/MbaituFM001//MbaituFM001",
            "http://s2.myradiostream.com:5788/",
            "http://shaincast.caster.fm:33352/listen.mp3",
            "http://livestream.5centscdn.com/baharifmlive/76edac4d02963adae001b61aa0bdb5cb.sdp/tracks-a1/index.m3u8",
            "http://17733.live.streamtheworld.com:3690/METRO_FM2_SC",
            "http://197.248.106.138/broadwave.mp3"
    };
    Object[] ad_ids={
            "ca-app-pub-5355225842445508/5772963477",
            "ca-app-pub-5355225842445508/8578325874",
            "ca-app-pub-5355225842445508/4008525474",
            "ca-app-pub-5355225842445508/8438725071",
            "ca-app-pub-5355225842445508/2392191471",
            "ca-app-pub-5355225842445508/9775857479",
            "ca-app-pub-5355225842445508/2252590676",
            "ca-app-pub-5355225842445508/3729323871",
            "ca-app-pub-5355225842445508/5206057070"};
    LayoutInflater layoutInflater;
    Context CONTEXT;
    InterstitialAd interstitialAd;

    public StationsAdapter(Context context){
        CONTEXT=context;

        interstitialAd= new InterstitialAd(CONTEXT);
        interstitialAd.setAdUnitId("ca-app-pub-5355225842445508/9298179475");
        AdRequest adRequest=new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
        interstitialAd.loadAd(adRequest);
    }
    public class Holder{
        LinearLayout station_container;
        TextView station_name_text;
        TextView station_frequency;
        Button station_play;
        Button station_website;
        Button station_share;
    }
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        layoutInflater=(LayoutInflater)CONTEXT.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=layoutInflater.inflate(R.layout.station_container,null);

        Holder holder=new Holder();

        holder.station_name_text=(TextView)view.findViewById(R.id.station_name);
        holder.station_name_text.setText(station_name[i].toString());

        holder.station_frequency=(TextView)view.findViewById(R.id.station_frequency);
        holder.station_frequency.setText(station_frequency[i].toString());

        holder.station_play=(Button) view.findViewById(R.id.station_play);
        holder.station_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                play(station_url[i].toString(),(Button) view);
            }
        });

        holder.station_website=(Button)view.findViewById(R.id.station_website);
        holder.station_website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent = new Intent(CONTEXT,WebsiteActivity.class);
                intent.putExtra("web_url",station_website[i].toString());
                intent.putExtra("station_title",station_name[i].toString());

                interstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdClosed() {
                        AdRequest adRequest=new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).build();
                        interstitialAd.loadAd(adRequest);
                        CONTEXT.startActivity(intent);
                        super.onAdClosed();
                    }
                });
                if(interstitialAd.isLoaded()){
                    interstitialAd.show();
                }else{
                    CONTEXT.startActivity(intent);
                }
            }
        });

        if(i%4==0){
            AdView adView =  new AdView(CONTEXT);
            adView.setAdSize(AdSize.BANNER);
            adView.setPadding(0,15,0,5);
            adView.setAdUnitId(ad_ids[i/4].toString());
            //System.out.println("https://"+ad_ids[i/4].toString());
            AdRequest.Builder adRequestBuilder= new AdRequest.Builder();
            adRequestBuilder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
            holder.station_container = (LinearLayout) view.findViewById(R.id.station_container);
            holder.station_container.addView(adView);
            adView.loadAd(adRequestBuilder.build());
        }
        return view;
    }

    @Override
    public int getCount() {
        return station_name.length;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    private LibVLC libVLC = new LibVLC();
    private MediaPlayer mediaPlayer = new MediaPlayer(libVLC);
    private Button previous_button=null;
    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;

    private void play(String url,Button button){
        Media media = new Media(libVLC,Uri.parse(url));

        progressDialog=new ProgressDialog(CONTEXT);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mediaPlayer.setEventListener(new MediaPlayer.EventListener() {
            @Override
            public void onEvent(MediaPlayer.Event event) {
                switch (event.type){
                    case MediaPlayer.Event.EncounteredError:
                        AlertDialog.Builder builder=new AlertDialog.Builder(CONTEXT);
                        builder.setMessage("Error in opening steam! Try again");
                        builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                progressDialog.dismiss();
                            }
                        });
                        builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        });
                        builder.setTitle("Tenje Radio App");
                        alertDialog=builder.create();
                        alertDialog.show();
                        Toast.makeText(CONTEXT,"Error!!!",Toast.LENGTH_SHORT).show();
                        break;
                    case MediaPlayer.Event.Opening:
                        progressDialog.setMessage("Opening stream");
                        progressDialog.show();
                        break;
                    case MediaPlayer.Event.MediaChanged:
                        Toast.makeText(CONTEXT,"Changing station",Toast.LENGTH_SHORT).show();
                        break;
                    case MediaPlayer.Event.Stopped:
                        Toast.makeText(CONTEXT,"Station paused",Toast.LENGTH_SHORT).show();
                        break;
                    case MediaPlayer.Event.Playing:
                        progressDialog.dismiss();
                        Toast.makeText(CONTEXT,"Playing",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            if(previous_button==button){
                previous_button.setText("RESUME");
            }else{
                previous_button.setText("PLAY");
                mediaPlayer.setMedia(media);
                mediaPlayer.play();
                button.setText("STOP");
                previous_button=button;
            }
        }else{
            if(previous_button!=null){
                previous_button.setText("PLAY");
            }
            mediaPlayer.setMedia(media);
            mediaPlayer.play();
            button.setText("STOP");
            previous_button=button;
        }
    }
}
