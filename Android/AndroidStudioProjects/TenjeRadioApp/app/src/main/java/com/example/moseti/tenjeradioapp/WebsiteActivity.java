package com.example.moseti.tenjeradioapp;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebsiteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_website);

        setTitle(getIntent().getStringExtra("station_title"));
        final ProgressDialog progressDialog=new ProgressDialog(WebsiteActivity.this,ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading");
        progressDialog.setTitle("Tenje Radio App");
        progressDialog.show();

        WebView webView = (WebView) findViewById(R.id.station_website_view);
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                progressDialog.dismiss();
                super.onPageFinished(view, url);
            }
        });
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(getIntent().getStringExtra("web_url"));
    }
}
