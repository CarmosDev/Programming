package com.example.moseti.helloworld;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by MOSETI on 25/11/2015.
 */
public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context,Intent intent){
        if(intent.getAction()=="android.intent.action.AIRPLANE_MODE"){
            Toast.makeText(context,"Airplane mode detected.",Toast.LENGTH_LONG).show();
        }
        if(intent.getAction()=="com.tutorialspoint.CUSTOM_INTENT"){
            Toast.makeText(context,"Custom intent detected.",Toast.LENGTH_LONG).show();
        }
    }
}
