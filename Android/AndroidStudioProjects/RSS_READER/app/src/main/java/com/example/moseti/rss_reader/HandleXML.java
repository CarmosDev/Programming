package com.example.moseti.rss_reader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by MOSETI on 19/12/2015.
 */
public class HandleXML {
    private String title="title";
    private String link="link";
    private String description="description";
    private String urlString=null;
    private XmlPullParserFactory xmlFactoryObject;
    public volatile boolean parsingComplete=true;

    public HandleXML(String url){
        this.urlString=url;
    }

    public String getTitle(){
        return title;
    }
    public String getDescription(){
        return description;
    }
    public String getLink(){
        return link;
    }
    public void parseXMLAndStoreIt(XmlPullParser myParser){
        int event;
        String text=null;
        try{
            event=myParser.getEventType();
            while (event!=XmlPullParser.END_DOCUMENT){
                String name=myParser.getName();
                switch(event){
                    case XmlPullParser.START_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        text=myParser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if(name.equals("title")){
                            title=text;
                        }else if(name.equals("link")){
                            link=text;
                        }else if(name.equals("description")){
                            description=text;
                        }
                        break;
                }
                event=myParser.next();
            }
            parsingComplete=false;
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    public void fetchXML(){
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    URL url=new URL(urlString);
                    HttpURLConnection conn=(HttpURLConnection)url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);

                    conn.connect();
                    InputStream stream=conn.getInputStream();

                    xmlFactoryObject=XmlPullParserFactory.newInstance();
                    XmlPullParser myParser=xmlFactoryObject.newPullParser();

                    myParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES,false);
                    myParser.setInput(stream, null);

                    parseXMLAndStoreIt(myParser);
                    stream.close();
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
}
