package com.example.moseti.rss_reader;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText title,link,description;
    Button b1,b2;
    private  String finalUrl="http://www.nation.co.ke/-/1148/1148/-/view/asFeed/-/vtvnjq/-/index.xml";
    private HandleXML obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title=(EditText) findViewById(R.id.editText);
        link=(EditText) findViewById(R.id.editText2);
        description=(EditText) findViewById(R.id.editText3);

        b1=(Button) findViewById(R.id.button);
        b2=(Button) findViewById(R.id.button2);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obj=new HandleXML(finalUrl);
                obj.fetchXML();

                while(obj.parsingComplete){
                    title.setText(obj.getTitle());
                    link.setText(obj.getLink());
                    description.setText(obj.getDescription());
                }
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in=new Intent(MainActivity.this,second.class);
                startActivity(in);
            }
        });
    }
}
