package com.example.moseti.dailynationrssfeed;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public Object[] STORY_TITLES;
    public Object[] STORY_LINKS;
    public Object[] STORY_DESCRIPTIONS;
    public String NEWS_SOURCE;
    public ListView listView;
    public Button button;
    public int POSITION;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=(ListView)findViewById(R.id.listStories);
        button=(Button)findViewById(R.id.buttonResumer);
        //get stories and store them in arrays
        if(savedInstanceState==null){
            DAILYNATIONFEED dailynationfeed= new DAILYNATIONFEED();
            dailynationfeed.execute();
        }
        String SCROLL_STATE_FILE="MyScrollState";
        SharedPreferences scrollState=getSharedPreferences(SCROLL_STATE_FILE,MODE_PRIVATE);
        POSITION=scrollState.getInt("position", 0);
        final LinearLayout linearLayout=(LinearLayout)findViewById(R.id.linearLayout);
        if(POSITION<3){
            linearLayout.removeView(button);
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listView.smoothScrollToPosition(POSITION);
                linearLayout.removeView(v);
            }
        });
    }
    public class STORIES_ADAPTER extends BaseAdapter{
        Object[] TITLES;
        Object[] LINKS;
        Object[] DESCRIPTIONS;
        String SOURCE;
        Context CONTEXT;
        LayoutInflater layoutInflater;
        public STORIES_ADAPTER(Context context,Object[] titlesArray,Object[] linksArray,Object[] descriptionsArray,String source){
            TITLES=titlesArray;
            LINKS=linksArray;
            DESCRIPTIONS=descriptionsArray;
            SOURCE=source;
            CONTEXT=context;
        }
        public class Holder{
            TextView textView_SOURCE;
            TextView textView_TITLE;
            TextView textView_DESCRIPTIONS;
        }
        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            Holder holder=new Holder();
            layoutInflater=(LayoutInflater)CONTEXT.getSystemService(LAYOUT_INFLATER_SERVICE);
            if(convertView==null){
                convertView=layoutInflater.inflate(R.layout.story_elements,null);
            }
            holder.textView_SOURCE=(TextView)convertView.findViewById(R.id.newsSource);
            holder.textView_TITLE=(TextView)convertView.findViewById(R.id.newsTitle);
            holder.textView_DESCRIPTIONS=(TextView)convertView.findViewById(R.id.newsDescription);

            holder.textView_SOURCE.setText(NEWS_SOURCE);
            holder.textView_TITLE.setText(TITLES[position].toString());
            holder.textView_DESCRIPTIONS.setText(DESCRIPTIONS[position].toString());
            convertView.setTag(holder);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(MainActivity.this,"POSITION : "+listView.getFirstVisiblePosition(),Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(MainActivity.this, Web_Story_Load.class);
                    intent.putExtra("link", LINKS[position].toString());
                    intent.putExtra("title", TITLES[position].toString());
                    intent.putExtra("position",position);
                    startActivity(intent);
                }
            });
            return convertView;
        }
        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }
    public class DAILYNATIONFEED extends AsyncTask<Void,Void,Void>{
        ArrayList<String> storyTitles=new ArrayList<>();
        ArrayList<String> storyLinks=new ArrayList<>();
        ArrayList<String> storyDescriptions=new ArrayList<>();
        ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
            progressDialog=new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Loading Feed...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            String SOURCE="http://www.nation.co.ke/-/1148/1148/-/view/asFeed/-/vtvnjq/-/index.xml";
            try{
                Document document=Jsoup.connect(SOURCE).get();
                NEWS_SOURCE=document.getElementsByTag("channel").get(0).child(0).text();
                int ITEM_NUMBER=document.getElementsByTag("item").size();
                for(int i=0;i<ITEM_NUMBER;i++){
                    storyTitles.add(document.getElementsByTag("item").get(i).child(0).text());
                    storyLinks.add(document.getElementsByTag("item").get(i).child(1).text());
                    storyDescriptions.add(document.getElementsByTag("item").get(i).child(2).text());
                }
                STORY_TITLES=storyTitles.toArray();
                STORY_LINKS=storyLinks.toArray();
                STORY_DESCRIPTIONS=storyDescriptions.toArray();
            }catch (Exception e){
                System.out.println("DOCEUMENT ERROR : "+e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            //LIST
            ListView listView=(ListView)findViewById(R.id.listStories);
            listView.setAdapter(new STORIES_ADAPTER(MainActivity.this,STORY_TITLES,STORY_LINKS,STORY_DESCRIPTIONS,NEWS_SOURCE));
            progressDialog.dismiss();
        }
    }
}
