package com.example.moseti.dailynationrssfeed;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Toast;

public class Web_Story_Load extends AppCompatActivity {
    public int POSITION;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web__story__load);

        WebView webView=(WebView)findViewById(R.id.webViewStory);
        webView.getSettings().setJavaScriptEnabled(true);

        Intent intent=getIntent();
        String link=intent.getStringExtra("link");
        webView.loadUrl(link);
        String title=intent.getStringExtra("title");
        setTitle(title);
        POSITION=intent.getIntExtra("position",0);

        String SCROLL_STATE_FILE="MyScrollState";
        SharedPreferences scrollState=getSharedPreferences(SCROLL_STATE_FILE,MODE_PRIVATE);
        SharedPreferences.Editor scrollEditor=scrollState.edit();
        scrollEditor.putInt("position",POSITION);
        scrollEditor.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
