package com.example.moseti.goalrss;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new RSS().execute();
    }
    public class RSS extends AsyncTask<Void,Void,Object[]>{
        public boolean isTitle=false;
        public boolean isImage=false;
        public String newsLogo;
        public ArrayList<String> titles=new ArrayList<>();
        public ArrayList<String> images=new ArrayList<>();
        public ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog=new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Loading Feed...");
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
        }

        @Override
        protected Object[] doInBackground(Void... params) {
            String feedURL="http://www.goal.com/en/feeds/news?fmt=rss&ICID=HP";
            try {
                InputStream inputStream=new URL(feedURL).openStream();
                XmlPullParserFactory xmlPullParserFactory=XmlPullParserFactory.newInstance();
                xmlPullParserFactory.setNamespaceAware(true);
                XmlPullParser xmlPullParser=xmlPullParserFactory.newPullParser();
                xmlPullParser.setInput(inputStream,null);
                int eventType=xmlPullParser.getEventType();
                while(eventType!=XmlPullParser.END_DOCUMENT){
                    if(eventType==XmlPullParser.START_TAG){
                        if(xmlPullParser.getName().equals("title")){
                            isTitle=true;
                        }
                        if(xmlPullParser.getName().equals("url")){
                            isImage=true;
                        }
                        if(xmlPullParser.getName().equals("enclosure")){
                            images.add(xmlPullParser.getAttributeValue(null, "url"));
                        }
                    }
                    if(eventType==XmlPullParser.TEXT && isTitle){
                        titles.add(xmlPullParser.getText());
                        isTitle=false;
                    }
                    if(eventType==XmlPullParser.TEXT && isImage){
                        newsLogo=xmlPullParser.getText();
                        isImage=false;
                    }
                    eventType=xmlPullParser.next();
                }


            }catch (Exception e){
                System.out.println("EXC : "+e);
            }
            return titles.toArray();
        }
        @Override
        protected void onPostExecute(Object s[]) {
            try{
                Bitmap bitmapLogo= new loadRemoteBitmap().execute(newsLogo).get();
                for(int i=2;i<s.length;i++){
                    LinearLayout linearLayout=(LinearLayout) findViewById(R.id.parentElements);
                    LayoutInflater layoutInflater=(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View newView=layoutInflater.inflate(R.layout.story_elements,null);
                    TextView storyPublisher=(TextView)newView.findViewById(R.id.storyPublisher);
                    TextView storyTitle=(TextView)newView.findViewById(R.id.storyTitle);
                    String publisher=s[0].toString();
                    String story=s[i].toString();
                    storyPublisher.setText(publisher);
                    storyTitle.setText(story);
                    ImageView logo=(ImageView)newView.findViewById(R.id.newsLogo);
                    logo.setImageURI(Uri.parse(newsLogo));
                    Bitmap storyBitmap=new loadRemoteBitmap().execute(images.toArray()[i-2].toString()).get();
                    ImageView storyImage=(ImageView)newView.findViewById(R.id.storyImage);
                    storyImage.setImageBitmap(storyBitmap);
                    linearLayout.addView(newView);
                }
                progressDialog.dismiss();
            }catch (Exception e) {
                System.out.println("Exception : " + e);
            }
        }
    }
    public class loadRemoteBitmap extends AsyncTask<String,Void,Bitmap>{
        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                InputStream inputStream=new URL(params[0]).openStream();
                return  BitmapFactory.decodeStream(inputStream);
            }catch (Exception e){
                System.out.println("Exception : "+e);
                return null;
            }
        }
    }
}
