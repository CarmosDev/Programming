package com.example.moseti.playremotevideo;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String link="http://a526.v.phobos.apple.com/us/r30/Video5/v4/42/80/59/42805922-cf5e-9843-e28e-f3d93abd1572/mzvf_2933503493624942955.640x316.h264lc.U.p.m4v";
        VideoView videoView=(VideoView)findViewById(R.id.videoView);
        MediaController mediaController=new MediaController(this);
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(Uri.parse(link));
        videoView.start();
    }
}
