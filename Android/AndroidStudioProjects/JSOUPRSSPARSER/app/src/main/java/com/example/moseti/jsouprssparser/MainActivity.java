package com.example.moseti.jsouprssparser;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new SAMPLEJSOUPFEED().execute();
    }
    private class SAMPLEJSOUPFEED extends AsyncTask<Void,Void,ArrayList>{
        public LinearLayout linearLayout=(LinearLayout)findViewById(R.id.linearLayoutScroll);
        public ArrayList<String>titles=new ArrayList<>();
        public ArrayList<String>links=new ArrayList<>();
        public ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog=new ProgressDialog(MainActivity.this);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Loading Feed...");
            progressDialog.show();
        }

        @Override
        protected ArrayList doInBackground(Void... params) {
            try{
                Document document= Jsoup.connect("http://www.nation.co.ke/-/1148/1148/-/view/asFeed/-/vtvnjq/-/index.xml").get();
                int items_number=document.getElementsByTag("item").size();
                for(int i=0;i<items_number;i++){
                    titles.add(document.getElementsByTag("item").get(i).child(0).text());
                    links.add(document.getElementsByTag("item").get(i).child(1).text());
                }
            }catch(Exception e){
                System.out.println("EXCEPTION : "+e);
            }
            return titles;
        }

        @Override
        protected void onPostExecute(ArrayList arrayList) {
            for(int i=0;i<arrayList.toArray().length;i++) {
                TextView textView = new TextView(MainActivity.this);
                textView.setText(arrayList.toArray()[i].toString());
                textView.setPadding(0, 10, 10, 10);
                linearLayout.addView(textView);
            }
            progressDialog.dismiss();
        }
    }
}
