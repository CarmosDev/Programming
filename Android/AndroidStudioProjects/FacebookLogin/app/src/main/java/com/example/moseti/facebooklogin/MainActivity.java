package com.example.moseti.facebooklogin;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//            View view = inflater.inflate(R.layout.splash, container, false);
//
//            loginButton = (LoginButton) view.findViewById(R.id.login_button);
//            loginButton.setReadPermissions("email");
//            // If using in a fragment
//            loginButton.setFragment(this);
//            // Other app specific specialization
//
//            // Callback registration
//            loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//                @Override
//                public void onSuccess(LoginResult loginResult) {
//                    // App code
//                }
//
//                @Override
//                public void onCancel() {
//                    // App code
//                }
//
//                @Override
//                public void onError(FacebookException exception) {
//                    // App code
//                }
//            });


//        AccessToken.getCurrentAccessToken();
//        Profile.getCurrentProfile();

//        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));

//        final TextView textView = (TextView) findViewById(R.id.fb_profile_details);
//
//        Profile profile = Profile.getCurrentProfile();
//
//        if(profile!=null){
//            String name = "Name: "+ profile.getName();
//            textView.setText(name);
//        }

        callbackManager = CallbackManager.Factory.create();

        LoginButton login_button = (LoginButton) findViewById(R.id.login_button);
        login_button.setReadPermissions("email","user_location");
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
//                Set set = loginResult.getAccessToken().getPermissions();
                GraphRequest request = new GraphRequest(loginResult.getAccessToken(),
                        "/" + loginResult.getAccessToken().getUserId(),
                        null,
                        HttpMethod.GET,
                        new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                JSONObject object = response.getJSONObject();
//                                {
//                                    "id": "12345678",
//                                    "birthday": "1/1/1950",
//                                    "first_name": "Chris",
//                                    "gender": "male",
//                                    "last_name": "Colm",
//                                    "link": "http://www.facebook.com/12345678",
//                                    "location": {
//                                          "id": "110843418940484",
//                                          "name": "Seattle, Washington"
//                                    },
//                                    "locale": "en_US",
//                                    "name": "Chris Colm",
//                                    "timezone": -8,
//                                    "updated_time": "2010-01-01T16:40:43+0000",
//                                    "verified": true
//                                }
                                try{
                                    System.out.print(object.toString());
                                    Toast.makeText(MainActivity.this,object.getString("name"),Toast.LENGTH_LONG).show();
                                }catch (Exception e){
                                    Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,gender,location,birthday,picture,age_range");
                request.setParameters(parameters);
                request.executeAsync();
//                Toast.makeText(MainActivity.this,,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(MainActivity.this,"Login attempt cancelled!!!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(MainActivity.this,error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });


        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
