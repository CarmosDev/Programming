package com.example.moseti.xmlparse;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.InputStream;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new RSS().execute();
    }
    public class RSS extends AsyncTask<Void,Void,String>{
        protected String doInBackground(Void...Voids){
            try
            {
                InputStream inputStream=new URL("http://www.goal.com/en/feeds/news?fmt=rss&ICID=HP").openStream();
                XmlPullParserFactory xmlPullParserFactory=XmlPullParserFactory.newInstance();
                xmlPullParserFactory.setNamespaceAware(true);
                XmlPullParser xmlPullParser=xmlPullParserFactory.newPullParser();
                xmlPullParser.setInput(inputStream,null);
                int eventType=xmlPullParser.getEventType();
                while(eventType!=XmlPullParser.END_DOCUMENT){
                    if(eventType==XmlPullParser.START_DOCUMENT){
                        System.out.println("Start Document");
                    }
                    if(eventType==XmlPullParser.START_TAG){
                        System.out.println("Start tag : "+xmlPullParser.getName());
                        int no_of_attributes=xmlPullParser.getAttributeCount();
                        System.out.println("Number of Attributes : "+no_of_attributes);
                        for(int i=0;i<no_of_attributes;i++){
                            System.out.println((i+1)+". Attribute Name : "+xmlPullParser.getAttributeName(i)+" and Attribute Value : "+xmlPullParser.getAttributeValue(i));
                        }
                    }
                    if(eventType==XmlPullParser.END_TAG){
                        System.out.println("End tag : "+xmlPullParser.getName());
                    }
                    if(eventType==XmlPullParser.TEXT){
                        System.out.println("Value : "+xmlPullParser.getText());
                    }
                    eventType=xmlPullParser.next();
                }
                System.out.println("End Document");
            }catch (Exception e){
                System.out.println("EXCEPTION : "+e);
            }
            return "Parsed RSS";
        }
        protected void onPostExecute(String s) {
            System.out.println(s);
        }
    }

}
