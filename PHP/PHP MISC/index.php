<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>PHP Miscellaneous</title>
    <!--JQuery-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!--Semantic UI CSS-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.4/semantic.min.css">
    <!--Semantic UI JS-->
    <script src="https://cdn.jsdelivr.net/semantic-ui/2.2.4/semantic.min.js"></script>
</head>
<body>
    <h1>PHP Generated option</h1>
    <select class="ui search selection dropdown" >
        <?php
            $i=1;
            for($i;$i<=100;$i++){
                echo "<option>Option ".$i."</option>";
            }
        ?>
    </select>
    <script>
        $('.dropdown').dropdown();
    </script>
</body>
</html>