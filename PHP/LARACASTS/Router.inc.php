<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 15/02/2017
 * Time: 18:54
 */

class Router{
    protected $routes=[];

    public function define(Array $routes){
        $this->routes=$routes;
    }

    public function direct_to($uri){
        if(array_key_exists($uri,$this->routes)){
            return $this->routes[$uri];
        }

        throw new Exception("Sorry!Route not found");
    }
}