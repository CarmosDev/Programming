<?php

require "Router.inc.php";

    $router=new Router();
    $router->define(require "routes.php");

    $uri=trim($_SERVER['REQUEST_URI'],"/");

    require $router->direct_to($uri);