/**
 * Created by MOSETI on 03/06/2016.
 */
/**
 *
 * Function Processes Input Text Into ITUNES parameter specifications
 * @param text
 * @returns {string}
 */
function searchtext(text) {
    var a=text.trim().split(" ");
    var b="";
    var c=a.length-1;
    for(var i=0;i<a.length;i++){
        if(a[i]!=""){
            b+=a[i];
            if(i!=c){
                b+="+";
            }
        }
    }
    return b;
}

function search(searchElement,outputElement) {
    /**
     *CREATE HTTP OBJECT**/
    var httpRequest;
    if(window.XMLHttpRequest){
        httpRequest=new XMLHttpRequest();
    }else if(window.ActiveXObject){
        httpRequest=new ActiveXObject('Microsoft.XMLHTTP');
    }
    httpRequest.onreadystatechange=function(){
        if(httpRequest.readyState===XMLHttpRequest.DONE){
            if(httpRequest.status===200){
                var response=httpRequest.responseText;
                process_result(outputElement,response.
                    replace('<!-- Hosting24 Analytics Code -->',"").
                    replace('<script type="text/javascript" src="http://stats.hosting24.com/count.php"></script>',"").
                    replace('<!-- End Of Analytics Code -->',""));
            }
        }
    };
    //Monitor Progress
    httpRequest.addEventListener("loadstart",function (e) {
       outputElement.innerHTML="<div class='center'><img src='./resources/716.gif'></div>";
    });
    httpRequest.open("GET","http://carmosdevelopers.comxa.com/itunes.php?term="+searchtext(searchElement.value),true);
    httpRequest.send();
}

function process_result(outputElement,json) {
    var result= JSON.parse(json);
    var a=document.getElementById("resultCount");
    a.innerHTML=result.resultCount;

    outputElement.innerHTML="";
    for(var i=0;i<result.resultCount;i++){
        outputElement.innerHTML+='<div class="result-container"><img src="'+result.results[i].artworkUrl100.replace('100x100bb','250x250bb')+
            '" data-artwork="'+result.results[i].artworkUrl100.replace('100x100bb','50x50bb')+
            '"><i class="material-icons preview" onclick="initialisePlayer(this)" data-song-src="'+result.results[i].previewUrl+'">play_circle_outline</i><a href="'+result.results[i].trackViewUrl+
            '" target="_blank"><h6 class="result-attribute main-title" data-song-name="'+result.results[i].trackCensoredName +
            '">Song : '+result.results[i].trackCensoredName+
            '</h6></a><a href="'+result.results[i].artistViewUrl+'" target="_blank"><h6 class="result-attribute" data-artist-name="'+result.results[i].artistName +
            '">Artist Name : '+result.results[i].artistName+
            '</h6></a><h6 class="result-attribute" >Genre : '+result.results[i].primaryGenreName+
            '</h6><a href="'+result.results[i].collectionViewUrl+'" target="_blank"><h6 class="result-attribute" >Album : '+result.results[i].collectionCensoredName+
            '</h6></a><h6 class="result-attribute" >Track Price : '+result.results[i].trackPrice+' '+result.results[i].currency+
            '</h6><h6 class="result-attribute" >Album Price : '+result.results[i].collectionPrice+' '+result.results[i].currency+
            '</h6><h6 class="result-attribute" >Song Length : '+process_time(seconds(result.results[i].trackTimeMillis))+
            '</h6> </div>';
    }
}
function seconds(time) {
    return time/1000;
}
function process_time(time) {
    if(time<59){
        return human_time(Math.floor(time));
    }else{
        return process_time(time/60) + ":"+human_time(Math.floor(time%60));
    }
}
function human_time(time) {
    if(time>9){
        return time;
    }else{
        return "0"+time;
    }
}