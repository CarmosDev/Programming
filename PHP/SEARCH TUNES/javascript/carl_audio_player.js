/**
 * Created by MOSETI on 07/06/2016.
 */

var sound=new Audio();
var previous=null;
function initialisePlayer(play) {
    var audioPlayer=document.getElementsByClassName("carl-player")[0];
    if(previous!==null){
        previous.innerHTML="play_circle_outline";
    }
    previous=play;
    audioPlayer.style.display="block";
    sound.src=play.getAttribute("data-song-src");
    audioPlayer.getElementsByTagName("img")[0].src=play.parentElement.firstChild.getAttribute("data-artwork");
    audioPlayer.getElementsByClassName("carl-player-song-attributes")[0].getElementsByTagName('h6')[0].innerHTML="Artist : "+play.parentElement.getElementsByClassName("result-attribute")[1].getAttribute("data-artist-name");
    audioPlayer.getElementsByClassName("carl-player-song-attributes")[0].getElementsByTagName('h6')[1].innerHTML="Song : "+
    play.parentElement.getElementsByClassName("result-attribute main-title")[0].getAttribute("data-song-name");

    sound.currentTime=0;
    sound.play();
    refresh_buttons("pause");
    play.innerHTML="pause_circle_outline";
    audioPlayer.getElementsByClassName("carl-player-controls")[1].onclick=function () {
        if(sound.paused){
            sound.play();
            refresh_buttons("pause")
        }else{
            sound.pause();
            refresh_buttons("play")
        }
    };
    audioPlayer.getElementsByClassName("carl-player-controls")[4].onclick=function () {
        if(!sound.paused){
            sound.pause();
        }
        sound.currentTime=0;
        sound.play();
        refresh_buttons("pause");
    };
    audioPlayer.getElementsByClassName("carl-player-controls")[3].onclick=function () {
        if(sound.muted){
            sound.volume=1;
            sound.muted=false;
            refresh_buttons("unmute");
        }else{
            sound.volume=0;
            sound.muted=true;
            refresh_buttons("mute");
        }
    };
    sound.addEventListener("loadedmetadata",function () {
        audioPlayer.getElementsByClassName("carl-player-time")[0].
            innerHTML="<span class='glyphicon glyphicon-time'></span> "+process_time(sound.currentTime,1)+" - "+process_time(sound.buffered.end(0),1)+"/"+process_time(sound.duration,1);
        audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
            getElementsByClassName("carl-player-progress-buffered")[0].style.width=(sound.buffered.end(0)/sound.duration)*100+"%";
    });
    sound.addEventListener("timeupdate",function () {
        audioPlayer.getElementsByClassName("carl-player-time")[0].
            innerHTML="<span class='glyphicon glyphicon-time'></span> "+process_time(sound.currentTime,1)+" - "+process_time(sound.buffered.end(0),1)+"/"+process_time(sound.duration,1);
        audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
            getElementsByClassName("carl-player-progress-current")[0].style.width=(sound.currentTime/sound.duration)*100+"%";
    });
    sound.onprogress=function () {
        audioPlayer.getElementsByClassName("carl-player-time")[0].
            innerHTML="<span class='glyphicon glyphicon-time'></span> "+process_time(sound.currentTime,1)+" - "+process_time(sound.buffered.end(0),1)+"/"+process_time(sound.duration,1);
        audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
            getElementsByClassName("carl-player-progress-buffered")[0].style.width=(sound.buffered.end(0)/sound.duration)*100+"%";
    };

    //Skip for all 3 progress bars
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
        getElementsByClassName("carl-player-progress-buffered")[0].onclick=skip;
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].onclick=skip;
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
    getElementsByClassName("carl-player-progress-current")[0].onclick=skip;

    //Progress Hover Listeners
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
    getElementsByClassName("carl-player-progress-buffered")[0].onmouseover=hover_position;
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].onmouseover=hover_position;
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
    getElementsByClassName("carl-player-progress-current")[0].onmouseover=hover_position;

    //Progress MouseMove Listeners
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
    getElementsByClassName("carl-player-progress-buffered")[0].onmousemove=hover_position;
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].onmousemove=hover_position;
    audioPlayer.getElementsByClassName("carl-player-controls carl-player-progress-bar")[0].
    getElementsByClassName("carl-player-progress-current")[0].onmousemove=hover_position;

    function hover_position(e) {
        var tooltip=document.getElementsByClassName("carl-player-hover-time")[0];
        var default_position=-30;
        tooltip.style.opacity=0.7;
            if(this.getAttribute("class")=="carl-player-controls carl-player-progress-bar"){
                tooltip.style.left=(e.pageX-this.offsetLeft-this.clientLeft-this.parentElement.offsetLeft)+default_position+"px";
                tooltip.innerHTML=process_time(((e.pageX-this.offsetLeft-this.clientLeft-this.parentElement.offsetLeft)/this.offsetWidth)*sound.duration,1);
            }else{
                tooltip.style.left=(e.pageX-this.offsetLeft-this.parentElement.offsetLeft-this.parentElement.parentElement.offsetLeft)+default_position+"px";
                tooltip.innerHTML=process_time(((e.pageX-this.offsetLeft-this.parentElement.offsetLeft-this.parentElement.parentElement.offsetLeft)/this.parentElement.offsetWidth)*sound.duration,1);
            }
        this.onmouseout=function () {
            tooltip.style.opacity=0;
        };
    }

    function skip(e) {
        if(this.getAttribute("class")=="carl-player-controls carl-player-progress-bar"){
            sound.currentTime=((e.pageX-this.offsetLeft-this.clientLeft-this.parentElement.offsetLeft)/this.offsetWidth)*sound.duration;
        }else{
            sound.currentTime=((e.pageX-this.offsetLeft-this.parentElement.offsetLeft-this.parentElement.parentElement.offsetLeft)/this.parentElement.offsetWidth)*sound.duration;
        }
    }
    function refresh_buttons(action) {
        switch (action){
            case "play":
                audioPlayer.getElementsByClassName("carl-player-controls")[1].firstChild.innerHTML="play_arrow";
                break;
            case "pause":
                audioPlayer.getElementsByClassName("carl-player-controls")[1].firstChild.innerHTML="pause";
                break;
            case "mute":
                audioPlayer.getElementsByClassName("carl-player-controls")[3].firstChild.innerHTML="volume_off";
                break;
            case "unmute":
                audioPlayer.getElementsByClassName("carl-player-controls")[3].firstChild.innerHTML="volume_up";
                break;
        }
    }
    function process_time(time,i) {
        if(time<59){
            if(i==1){
                return "00"+":"+human_time(Math.floor(time));
            }else{
                return human_time(Math.floor(time));
            }
        }else{
            return process_time(time/60,i+1) + ":"+human_time(Math.floor(time%60));
        }
    }
    function human_time(time) {
        if(time>9){
            return time;
        }else{
            return "0"+time;
        }
    }
}