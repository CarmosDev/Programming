<?php
	ini_set("display_errors","0");
	$video=new DOMDocument;
	$video->load("https://www.youtube.com/feeds/videos.xml?user=goal");
	
	$video_author=$video->getElementsByTagName("author")->item(0);
	$video_author_name=$video_author->getElementsByTagName("name")->item(0)->nodeValue;
	$video_author_uri=$video_author->getElementsByTagName("uri")->item(0)->nodeValue;
	$video_entry=$video->getElementsByTagName("entry");
	$number_of_videos=$video_entry->length;
	
	
	echo "<b>Number Of Videos : ".$number_of_videos."</b><hr>";
	for($a=0;$a<$number_of_videos;$a++){
		$video_entry_title=$video_entry->item($a)->getElementsByTagName("title")->item(0)->childNodes->item(0)->nodeValue;
		$video_entry_link=$video_entry->item($a)->getElementsByTagName("link")->item(0)->getAttribute("href");
		$video_entry_mediaGroup=$video_entry->item($a)->getElementsByTagName("group")->item(0);
		$video_entry_mediaGroup_thumbnail=$video_entry_mediaGroup->getElementsByTagName("thumbnail")->item(0)->getAttribute("url");
		$video_entry_mediaGroup_description=$video_entry_mediaGroup->getElementsByTagName("description")->item(0)->childNodes->item(0)->nodeValue;
		$video_entry_mediaGroup_community=$video_entry_mediaGroup->getElementsByTagName("community")->item(0);
		$video_entry_mediaGroup_community_views=$video_entry_mediaGroup_community->getElementsByTagName("statistics")->item(0)->getAttribute("views");
		echo "<div style='display:inline-block;height:auto;width:400px;margin-bottom:5px;margin-right:10px;box-shadow:0px 0px 3px grey;'>";
		echo "<img src=".$video_entry_mediaGroup_thumbnail." style='height:200px;width:400px;'/>";
		echo "<center><h3><a href=".$video_entry_link." target='_blank'>".$video_entry_title."</a></h3></center>";
		echo "<p style='padding:10px;'>".$video_entry_mediaGroup_description."</p>";
		echo "<b><a href=".$video_author_uri." target='_blank' style='margin-left:10px;'>".$video_author_name."</a></b>";
		echo "<span style='color:red;float:right;margin-right:10px;'>Views : ".$video_entry_mediaGroup_community_views."</span><br><br>";
		echo "</div>";
	}
?>