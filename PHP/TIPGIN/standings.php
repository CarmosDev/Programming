<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 31/07/2016
 * Time: 20:00
 */
    require 'connect.inc.php';
    echo
    "<style>
        *{
            font-family: 'Segoe UI';
        }
        table{
            border: solid 1px grey;
            border-collapse: collapse;
            width: 100%;
        }
        th{
            background:#00a0df;
        }
        td{
            border: solid 1px grey;
            text-align: center;
        }
        .form-button{
            margin: 2px;
            font-size: 12px;
            height: 25px;
            width: 25px;
            text-align: center;
            font-weight: 900;
            color: white;
            border: none;
            outline: none;
            border-radius: 20%;
        }
        .form-bar{
            text-align: left;!important;
        }
    </style>";

    $standings=new \Tipgin\Connect("http://tipgin.net/examples/soccer_standings.xml");
    $standings->show_league_standings();
?>