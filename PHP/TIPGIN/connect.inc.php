<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 30/07/2016
 * Time: 16:38
 */

    namespace Tipgin;

    class Connect{
        protected $ch;
        protected $xml;

        function __construct($xml_url){
            $this->ch=curl_init();

            curl_setopt($this->ch,CURLOPT_URL,$xml_url);
            curl_setopt($this->ch,CURLOPT_TIMEOUT,10);
            curl_setopt($this->ch,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($this->ch,CURLOPT_ENCODING,'');
            curl_setopt($this->ch,CURLOPT_HEADER,0);

            $this->xml=curl_exec($this->ch);
        }
        function __destruct(){
            // TODO: Implement __destruct() method.
            curl_close($this->ch);
        }

        public function show_livescores(){
            $livescore_data=new \SimpleXMLElement($this->xml);
            echo "<table>";
            foreach ($livescore_data->league as $league){
                    echo "<tr><th colspan='6'>".$league->attributes()->name."</th></tr>";

                    foreach ($league->match as $match){
                        echo "<tr>";
                            echo "<td>".$match->attributes()->date."</td>";
                            echo "<td>".$match->attributes()->time."</td>";
                            echo "<td>".$match->home->attributes()->name."</td>";
                            echo "<td>".$match->home->attributes()->goals." - ".$match->away->attributes()->goals."</td>";
                            echo "<td>".$match->away->attributes()->name."</td>";
                            echo "<td>".$match->attributes()->status."</td>";
                        echo "</tr>";
                    }
            }
            echo "</table>";
        }

        public function show_fixtures(){
            $fixtures_data=new \SimpleXMLElement($this->xml);
            echo "<table>";
                foreach ($fixtures_data->league as $league){
                    echo "<tr><th colspan='5'>".$league->attributes()->name."</th></tr>";
                    foreach ($league->match as $match){
                        echo "<tr>";
                            echo "<td>".$match->attributes()->date."</td>";
                            echo "<td>".$match->attributes()->time."</td>";
                            echo "<td>".$match->home->attributes()->name."</td>";
                            echo "<td>".$match->away->attributes()->name."</td>";
                            echo "<td>".$match->attributes()->status."</td>";
                        echo "</tr>";
                    }
                }
            echo "</table>";
        }

        public function show_recent_results(){
            $results_data=new \SimpleXMLElement($this->xml);
            echo "<table>";
            foreach ($results_data->league as $league){
                echo "<tr><th colspan='6'>".$league->attributes()->name."</th></tr>";

                foreach ($league->match as $match){
                    echo "<tr>";
                    echo "<td>".$match->attributes()->date."</td>";
                    echo "<td>".$match->attributes()->time."</td>";
                    echo "<td>".$match->home->attributes()->name."</td>";
                    echo "<td>".$match->home->attributes()->goals." - ".$match->away->attributes()->goals."</td>";
                    echo "<td>".$match->away->attributes()->name."</td>";
                    echo "<td>".$match->attributes()->status."</td>";
                    echo "</tr>";
                }
            }
            echo "</table>";
        }
        public function show_league_standings(){
            $standings_data=new \SimpleXMLElement($this->xml);
            echo "<table>";
                foreach ($standings_data->league as $league){
                    echo "<tr><th colspan='12'>".$league->attributes()->name." - ".$league->attributes()->season."</th></tr>";
                    echo
                        "<tr>
                            <th></th>
                            <th></th>
                            <th>P</th>
                            <th>W</th>
                            <th>D</th>
                            <th>L</th>
                            <th>GF</th>
                            <th>GA</th>
                            <th>GD</th>
                            <th>Points</th>
                            <th></th>
                            <th>Form</th>
                        </tr>";
                    foreach ($league->team as $team){
                        echo "<tr>";
                            echo "<td>".$team->attributes()->position."</td>";
                            echo "<td>".$team->attributes()->name."</td>";
                            echo "<td>".$team->overall->attributes()->played."</td>";
                            echo "<td>".$team->overall->attributes()->win."</td>";
                            echo "<td>".$team->overall->attributes()->draw."</td>";
                            echo "<td>".$team->overall->attributes()->lose."</td>";
                            echo "<td>".$team->overall->attributes()->goals_scored."</td>";
                            echo "<td>".$team->overall->attributes()->goals_attempted."</td>";
                            echo "<td>".$team->totals->attributes()->goal_difference."</td>";
                            echo "<td>".$team->totals->attributes()->points."</td>";
                            echo "<td>".$team->special->attributes()->name."</td>";
                            echo "<td class='form-bar'>".$this->process_form($team->attributes()->recent_form)."</td>";
                        echo "</tr>";
                    }
                }
            echo "</table>";
        }

        private function process_form($recent_form){
            $result='';
            foreach (str_split($recent_form,1) as $form){
                switch ($form){
                    case 'W':
                        $result.="<button class='form-button' style='background: mediumseagreen;'>$form</button>";
                        break;
                    case 'D':
                        $result.="<button class='form-button' style='background:darkorange;'>$form</button>";
                        break;
                    case 'L':
                        $result.="<button class='form-button' style='background: red;'>$form</button>";
                        break;
                    default :
                        break;
                }
            }
            return $result;
        }

        public function show_odds(){
            $odds_data=new \SimpleXMLElement($this->xml);
            echo "<table>";
                foreach ($odds_data->league as $league){
                    echo "<tr><th colspan='12'>".$league->attributes()->name."</th></tr>";
                    foreach ($league->match as $match){
                        echo "<tr>";
                            echo "<td>".$match->attributes()->date." - ".$match->attributes()->time."</td>";
                            echo "<td colspan='11'>".$match->home->attributes()->name." vs ".$match->away->attributes()->name."</td>";
                        echo "</tr>";
                        echo "<tr>";
                            foreach ($match->odds->type as $type){
                                echo "<td>".$type->attributes()->name."</td>";
                            }
                        echo "</tr>";
                    }
                }
            echo "</table>";
        }
    }

?>