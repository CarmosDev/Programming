<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 30/07/2016
 * Time: 12:56
 */

namespace Oracle_Db;

Class Db{

    protected $database='localhost/PDBORCL';
    protected $connection;
    protected $statement_id;
    protected $prefetch=100;

    function __construct($user,$password){
        $this->connection=oci_connect($user,$password,$this->database);
        if(!$this->connection){
            $message=oci_error();
            throw new \Exception('Cannot connect to database : '.$message['message']);
        }
    }
    function __destruct(){
        // TODO: Implement __destruct() method.
        if($this->connection){
            oci_close($this->connection);
        }
        if($this->statement_id){
            oci_free_statement($this->statement_id);
        }
    }

    protected function execute($sql){
        $this->statement_id=oci_parse($this->connection,$sql);

        oci_set_prefetch($this->statement_id,$this->prefetch);

        oci_execute($this->statement_id);
    }

    public function get_rows($sql){
        $this->execute($sql);

        oci_fetch_all($this->statement_id,$results,0,-1,OCI_FETCHSTATEMENT_BY_ROW);
        $this->statement_id=null;
        return $results;
    }
}

?>