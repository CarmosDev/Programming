<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 30/07/2016
 * Time: 13:07
 */

    require 'db.inc.php';

    $db=new \Oracle_Db\Db('CARMOS_DEVELOPERS','qaboos2016');

    $array_result=$db->get_rows('SELECT * FROM employees');

    echo
    "<style>
        *{
            font-family: 'Segoe UI';
        }    
        table{
            border: solid 1px #000000;
            border-collapse: collapse;
        }
        tr{
            border: solid 1px #000000;
        }
        tr:hover{
            background: lightgrey;
            cursor: pointer;
        }
        td{
            border: solid 1px #000000;
            text-align: center;
        }
        th{
            border: solid 1px #000000;
            padding: 10px;
            background: darkseagreen!important;
        }
    </style>";

    print_table($array_result);

    function print_table($associative_array){
        echo "<table>";
            print_header($associative_array[0]);
            foreach ($associative_array as $row){
                print_row($row);
            }
        echo "</table>";
    }

    function print_header($first_row_array){
        echo "<tr>";
            foreach (array_keys($first_row_array) as $column_headers){
                echo "<th>$column_headers</th>";
            }
        echo "</tr>";
    }

    function print_row($row){
        echo "<tr>";
            foreach ($row as $column_data){
                echo "<td>$column_data</td>";
            }
        echo "</tr>";
    }

?>