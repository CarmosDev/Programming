<!DOCTYPE html>
<html>
<head>
<title>Edit Student data</title>
		<meta charset="utf-8">
		  <!--For responsivity-->
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>
</body>
</html>
<?php

	//include
	include('connect.php');

	//get the employee number
		$Student_ID=$_GET['Student_ID'];
		
		$sql="SELECT * FROM `student` WHERE `Student_ID`=$Student_ID";
		
		$result= $conn->query($sql);
		
		$column=$result->fetch_assoc();
		
		$id=$column['Student_ID'];
		$name=$column['Student_Name'];
		$gender=$column['Gender'];
		$DOB=$column['Date_Of_Birth'];
		$loc=$column['Address'];
		$sub=$column['Subject'];
		$DOR=$column['Date_Of_Registration'];
		/*
		//display
		echo "<b>Welcome to edit</b><br/>";
		echo "$id<br/>";
		echo "$name<br/>";
		echo "$gender<br/>";
		echo "$DOB<br/>";
		echo "$loc<br/>";
		echo "$sub<br/>";
		echo "$DOR<br/>";
		*/
		echo"<nav class='navbar navbar-inverse navbar-fixed-top'>
  <div class='container-fluid'>
    <div class='navbar-header'>
		<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#myNavbar'>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>                        
      </button>
      <a class='navbar-brand' href='index.php'><span class='glyphicon glyphicon-home'></span><font color='red'>Zeta School</font></a>
    </div>
  </div>
</nav>";
		echo "<br/><br/>";
		echo "<div style='background:brown;'>";
echo "<br/><center><form role='form' method='post' action='' style='border-radius:25px;
	width:60%;
	border:solid 1px green;
	padding:20px;
	background:beige;'>
	<fieldset style= 'border:dashed 1px green;'>
	<legend><font face='Nyala' size='20px'><b>Zeta School Student Update Info</b></font></legend>
	<div class='form-group'>
		<input class='form-control' type='text' name='name' value='$name' style='width:300px; border:solid green 1px;
height:40px;
' />
	</div>
	<div class='form-group'>
		Gender:
						<select class='form-control' name='gender' style='height:30px; width:150px;' value='$gender'>
							<option></option>
							<option>Male</option>
							<option>Female</option>
						</select>
	</div>
	<div class='form-group'>
		<input class='form-control' type='text' name='DOB' value='$DOB' style='width:300px; border:solid green 1px;
height:40px;
'/>
	</div>
	<div class='form-group'>
		<input class='form-control' type='text' name='loc' value='$loc' style='width:300px; border:solid green 1px;
height:40px;
'/>
	</div>
	<div class='form-group'>
		Subject:
						<select class='form-control' name='subject' style='height:30px; width:150px;' value='$sub'>
							<option></option>
							<option>C++</option>
							<option>C#</option>
							<option>C</option>
							<option>PHP & MySQL</option>
							<option>Javascript</option>
							<option>VB.NET</option>
						</select>
	</div>
	<div class='form-group'>
		<input class='btn btn-info' type='submit' name='change' value='Save Changes' '/>
	</div>
	</fieldset>
		</form></center><br/>";
echo "</div>";

		
		//check if edit has been clicked
if(isset($_POST['change']))
{
//get data
extract($_POST);

//update sql statement
$sql="UPDATE `student` SET `Student_ID`='$id',`Student_Name`='$name',`Gender`='$gender',`Date_Of_Birth`='$DOB',`Address`='$loc', `Subject`='$sub' WHERE `Student_ID`='$id'";

$result=$conn->query($sql);

if($result==1)
{
echo "<center>Data updated</center>";
header('location: allstudents.php');
}
else
{
echo "<center>Data not updated</center>";
}

}
?>