-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 10, 2015 at 06:41 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `Student_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Student_Name` varchar(50) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `Date_Of_Birth` date NOT NULL,
  `Address` varchar(40) NOT NULL,
  `Subject` text NOT NULL,
  `Date_Of_Registration` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Student_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`Student_ID`, `Student_Name`, `Gender`, `Date_Of_Birth`, `Address`, `Subject`, `Date_Of_Registration`) VALUES
(9, 'Jane Kilili', 'Female', '1992-02-29', '1213224 Nairobi', 'PHP & MySQL', '2015-02-28 17:58:36'),
(10, 'Nadhif Jama', 'Male', '1995-05-25', '5592717-00100 Cairo', 'PHP & MySQL', '2015-02-28 18:23:22'),
(14, 'Danson Haolis', 'Male', '1991-01-24', '5412323-001100 Berlin', 'C', '2015-02-28 18:27:15'),
(15, 'Grusha Vashnadze', 'Female', '1994-04-10', '2468237-09090 Glasglow', 'Javascript', '2015-02-28 18:28:32'),
(16, 'Jan Koum', 'Male', '1993-03-15', '686545-00100 Kiev', 'Javascript', '2015-02-28 18:29:11'),
(18, 'Sean Parker', 'Male', '1990-09-10', '5462345-00100 Johannesburg', 'VB.NET', '2015-02-28 18:33:16'),
(21, 'Garry Ballen', 'Female', '0000-00-00', '7325135-Nairobi', 'Javascript', '2015-03-08 17:38:53'),
(22, 'MIke Ross', 'Male', '0000-00-00', '312435-New York', 'C', '2015-03-10 17:16:48'),
(23, 'Franc Loius', 'Female', '0000-00-00', '018244-Madrid', 'PHP & MySQL', '2015-03-10 17:18:44'),
(24, 'Gregor Fincs', 'Male', '0000-00-00', '5621463-Paris', 'C++', '2015-03-10 17:20:03'),
(25, 'Trevor Mwangi ', 'Male', '0000-00-00', '6457841-Thika', 'Javascript', '2015-03-10 17:31:41');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
