<?php



?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>All Students</title>
<meta charset="utf-8">
		  <!--For responsivity-->
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
<script>
tr:hover
{
background:blue;
color:white;
}
</script>
</head>
<body>
</body>
</html>


<?php

//open connection to the server hosting the database
	$conn = mysqli_connect("localhost","root","","school") or die(mysqli_error());
// Check connection
	if ($conn->connect_error) 
	{
    die("Connection failed: " . $conn->connect_error);
	}
echo "<nav class='navbar navbar-inverse navbar-fixed-top'>
  <div class='container-fluid'>
    <div class='navbar-header'>
		<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#myNavbar'>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>
        <span class='icon-bar'></span>                        
      </button>
      <a class='navbar-brand' href='index.php'><span class='glyphicon glyphicon-home'></span><font color='red'>Zeta School</font></a>
    </div>
  </div>
</nav>";
echo "<div class='container-fluid' style='background:maroon; padding:10px;'  ><br/><br/>";
#write an sql statement to retrieve data from the database
$sql=" SELECT * FROM `student` ";
$result= $conn->query($sql);

//Execute the query
	
	if ($result->num_rows > 0)
	{
		echo "<div class='table-responsive'>";
		echo "<table class='table table-hover table-condensed' border='1' align='center' width='70%' height='100%' style='border-collapse: collapse; background: beige; border: solid white; ' >";
		echo "<caption><font face='arial' size='5px'><h1>Welcome To Student</h1></b></font><br/><a class='btn btn-link' href='index.php' style='text-decoration:none; color:blue; margin-right:800px;'><span class='glyphicon glyphicon-pencil'></span>Add Record</a><form role='form' method='post' action='' style='margin-left:500px;'><div class='form-group'><select class='form-control' name='searchlist'><option>Student_ID</option><option>Student_Name</option><option>Gender</option><option>Address</option></select></div><div class='form-group'><input class='form-control' type='text' name='search' placeholder='Type to search'/></div><div class='form-group'><button class='btn btn-warning' type='submit' name='searchgo' ><span class='glyphicon glyphicon-search'></span></button></div></form></caption>";
		//echo "";
		echo "<tr>
		<th style='background:black; color:white;'>Student Number</th>
		<th style='background:black; color:white;'>Student Name</th>
		<th style='background:black; color:white;'>Gender</th>
		<th style='background:black; color:white;'>Date Of Birth</th>
		<th style='background:black; color:white;'>Address</th>
		<th style='background:black; color:white;'>Subject</th>
		<th style='background:black; color:white;'>Date Of Registration</th>
	   </tr>";
#store retrieved data in an array
	While($column=$result->fetch_assoc())
	{
		//var_dump($column['Student_Name']);
		echo "<tr class='active' >";
		echo "<td>{$column['Student_ID']}</td>";
		echo "<td>{$column['Student_Name']}</td>";
		echo "<td>{$column['Gender']}</td>";
		echo "<td>{$column['Date_Of_Birth']}</td>";
		echo "<td>{$column['Address']}</td>";
		echo "<td>{$column['Subject']}</td>";
		echo "<td>{$column['Date_Of_Registration']}</td>";
		echo "</tr>";
	}
echo "</table>";	
echo "</div>";
	}
	
	else
		{
    echo "Error: " . $sql . "<br>" . $conn->error;
		}
	echo "</div>";
	echo "<br/><br/>";
	
	//Check if search button has been clicked
	if(isset($_POST['searchgo']))
		{
			//Get the search criteria
			$category=$_POST['searchlist'];
			$name=$_POST['search'];
			
			//Query the database
			$sql=" SELECT * FROM `student` WHERE $category = '$name' ";
			$result= $conn->query($sql);
		
	
	
//Execute the query
	if ($result->num_rows > 0)
	{
		echo"<div class='container-fluid' style='background:yellow; padding:10px;'>";
		echo "<table border='1' align='center' style='border-collapse: collapse; background: beige; border: solid white; ' >";
		echo "<caption><font face='arial' size='5px'><b>Searched Student(s)</b></font><br/></caption>";
		//echo "";
		echo "<tr>
		<th style='background:black; color:white;'>Student Number</th>
		<th style='background:black; color:white;'>Student Name</th>
		<th style='background:black; color:white;'>Gender</th>
		<th style='background:black; color:white;'>Date Of Birth</th>
		<th style='background:black; color:white;'>Address</th>
		<th style='background:black; color:white;'>Subject</th>
		<th style='background:black; color:white;'>Date Of Registration</th>
		<th style='background:black; color:white;'>Edit/Delete</th>
	   </tr>";
#store retrieved data in an array
	While($column=$result->fetch_assoc())
	{
		//var_dump($column['Student_Name']);
		echo "<tr>";
		echo "<td>{$column['Student_ID']}</td>";
		echo "<td>{$column['Student_Name']}</td>";
		echo "<td>{$column['Gender']}</td>";
		echo "<td>{$column['Date_Of_Birth']}</td>";
		echo "<td>{$column['Address']}</td>";
		echo "<td>{$column['Subject']}</td>";
		echo "<td>{$column['Date_Of_Registration']}</td>";
		echo "<td><a href='edit.php?Student_ID={$column['Student_ID']}'>Edit</a>/<a href='allstudents.php?Student_ID={$column['Student_ID']}'>Delete</a></td>";		
		echo "</tr>";
	}
echo "</table>";	
	}
	
		}
	
	if(isset($_GET['Student_ID']))
	{
		
		//get the student number
		$Student_ID=$_GET['Student_ID'];
		
		
		$sql="DELETE FROM `student` WHERE `Student_ID`=$Student_ID";
		
		//Execute the query
		$result=$conn->query($sql);
		
		header('location: allstudents.php');
		
	}
		

		
	$conn->close();

echo "</div>";


?>
