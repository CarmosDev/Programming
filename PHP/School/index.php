<!DOCTYPE html>
<html lang="en">
<head>
<title>Zeta School</title>
		<meta charset="utf-8">
		  <!--For responsivity-->
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="bootstrap-3.3.2-dist/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="bootstrap-3.3.2-dist/js/bootstrap.min.js"></script>
</head>
<body>

		<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="index.php"><span class='glyphicon glyphicon-home'></span><font color="red">Zeta School</font></a>
    </div>
  </div>
</nav>
	<div class="container-fluid">
		<center>
			<div style="background:grey; height:90%;">
				<br/><br/><br/><br/>
				<form role="form" method="post" action="" style="border-radius:25px; border:solid 2px blue; height:70%; width:70%; background:beige;" >
				<br/>
					<fieldset style="height:85%; width:70%;">
						<legend><font face="calibri" size="5px" color="red" >Zeta School Students Registry Portal</font></legend>
							<div class="form-group">
							<input class="form-control" type="text" name="studentname" placeholder="Enter Student's Name Here" style="width:300px; height:30px;">
							</div>
							<div class="form-group">
							Gender:
							<select class="form-control" name="gender" style="height:30px; width:150px;">
								<option></option>
								<option>Male</option>
								<option>Female</option>
							</select>
							</div>
							<div class="form-group">
							<input type="date"  class="form-control" name="DOB" Placeholder="Enter Date Of Birth Here" style="width:300px; height:30px;" />
							</div>
							<div class="form-group">
							<input type="text" name="address" class="form-control" Placeholder="Enter Your Address Here" style="width:300px; height:30px;" />
							</div>
							<div class="form-group">
							Subject:
							<select name="subject" style="height:30px; width:150px;" class="form-control">
								<option></option>
								<option>C++</option>
								<option>C#</option>
								<option>C</option>
								<option>PHP & MySQL</option>
								<option>Javascript</option>
								<option>VB.NET</option>
							</select>
							</div>
							<div class="form-group" >
							<button type="submit" class="btn btn-info" name="register" ><span class='glyphicon glyphicon-user'></span>Register Student </button>
							</div>
							<a href="allstudents.php" style="text-decoration:none; color:orange; margin-left:30px; " class="btn btn-link"><font face="nyala"><h4>View Students</h4></font></a>
					</fieldset>
				</form>
				<br/><br/><br/><br/>
			</div>
		</center>
	</div>
</body>
</html>
<?php


//Check if button register is clicked
if(isset($_POST['register']))
{
	//create connection to database
	$conn = mysqli_connect("localhost","root","","school");
	// Check connection
	if ($conn->connect_error) 
	{
    die("Connection failed: " . $conn->connect_error);
	} 
	//get the form data
	extract($_POST);
	
	//Create query to insert data
	$sql="INSERT INTO `student`(`Student_Name`, `Gender`, `Date_Of_Birth`, `Address`, `Subject`) VALUES ('$studentname','$gender','$DOB','$address','$subject')";
	
	//Execute the query
	if ($conn->query($sql) === TRUE)
	{
		echo "<center>New record created successfully</center>";
	}
	else
		{
    echo "Error: " . $sql . "<br>" . $conn->error;
		}
		$conn->close();
}


?>