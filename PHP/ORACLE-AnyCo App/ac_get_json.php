<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 06/08/2016
 * Time: 14:35
 */

    /**
    * ac_get_json.php: Service returning equipment counts in JSON
    * @package WebService
    */
    require('ac_db.inc.php');
    if (!isset($_POST['username'])) {
        header('Location: index.php');
        exit;
    }
    $db = new \Oracle\Db("Equipment", 'CARMOS');//$_POST['username']);
    $sql = "select UPPER(equip_name) as equipment, count(equip_name) as equipment_number
from equipment
group by equip_name";
    $res = $db->execFetchAll($sql, "Get Equipment Counts");
    $mydata = array();
    foreach ($res as $row) {
        $mydata[$row['EQUIPMENT']] = (int) $row['EQUIPMENT_NUMBER'];
    }
    echo json_encode($mydata);
?>