<?php
	
	require('ac_db.inc.php');
	
	$db = new \Oracle\Db("test_db","Chris");
	$sql = "SELECT FIRST_NAME,PHONE_NUMBER FROM employees ORDER BY EMPLOYEE_ID";
	$res = $db->execFetchAll($sql,"Query Example");
	
	echo "<table border='1'>".
			"<tr style='background:#FF6000;color:#FFFFFF;'>".
				"<td> FIRST NAME </td>".
				"<td> PHONE NUMBER </td>".
			"</tr>";
	foreach($res as $row){
		echo "<tr>";
			echo "<td>".htmlspecialchars($row['FIRST_NAME'],ENT_NOQUOTES,'UTF-8')."</td>";
			echo "<td>".htmlspecialchars($row['PHONE_NUMBER'],ENT_NOQUOTES,'UTF-8')."</td>";
		echo "</tr>";
	}
	echo "</table>";
?>