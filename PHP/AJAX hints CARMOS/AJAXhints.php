<?php

//requested for passed string
$q = $_REQUEST['q'];

//array with suggestions
$a[] = "Anna";
$a[] = "Carlton";
$a[] = "Brittany";
$a[] = "Cinderella";
$a[] = "Diana";
$a[] = "Eva";
$a[] = "Fiona";
$a[] = "Gunda";
$a[] = "Hege";
$a[] = "Inga";
$a[] = "Johanna";
$a[] = "Kitty";
$a[] = "Linda";
$a[] = "Nina";
$a[] = "Ophelia";
$a[] = "Petunia";
$a[] = "Amanda";
$a[] = "Raquel";
$a[] = "Cindy";
$a[] = "Doris";
$a[] = "Eve";
$a[] = "Evita";
$a[] = "Sunniva";
$a[] = "Tove";
$a[] = "Unni";
$a[] = "Violet";
$a[] = "Liza";
$a[] = "Elizabeth";
$a[] = "Ellen";
$a[] = "Wenche";
$a[] = "Vicky";
$a[] = "Laureen";



//define variable hint as string
$hint = "";

// look up hints from the array if $q is different from " "
if($q !== ""){
	//convert to lowercase
	$q = strtolower($q);
	//get its length
	$len = strlen($q);
	//loop through results and set $name to the list array $a
	foreach($a as $name){
		// loop to find letters contained in the words
		if(stristr($name,$q)){
			if($hint === ""){
				$i = 1;
				$hint = "<span class='glyphicon glyphicon-search'></span>"."      ".str_ireplace($q,"<u>$q</u>",$name)."<br/>";
				$res = "result";
			}else{
				$i++;
				$hint .="<span class='glyphicon glyphicon-search'></span>"."      ".str_ireplace($q,"<u>$q</u>",$name)."<br/>";
				$res = "results";
			}
		}	
	}

}

// Output "no suggestion" if no hint was found or output correct values
echo $hint ==="" ? "<div class='alert alert-danger' >no suggestion<div>" : "<div class='alert alert-success'><kbd>$i $res below...</kbd><br/>$hint </div>";
?>