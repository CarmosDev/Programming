<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 01/09/2016
 * Time: 14:07
 */

    //$minimum=1000000000;
    $library='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-';
    $library_array=str_split($library);
    $public_id="";

    echo "<style>*{font-family: 'Segoe UI'}</style>";
    //echo count($library_array);
    echo "<hr/><b>BASE 64 : </b><br>";
    $id=get_random_id();
    echo "Raw ID : ".$id."&nbsp;<b>(".strlen($id).")</b><br>";
    base64($id,$library_array,$public_id);
    echo "Public ID : ".$public_id."<br>";
    echo "<hr/>";

    echo "Vardump Info : <br>";
    var_dump($public_id);

    var_dump($id);
    var_dump(decode_id($public_id,$library_array));

    function decode_id($public_id,$library){
        $split_array=str_split($public_id);
        $private_id=0;
        for($i=0;$i<count($split_array);$i++){
            $private_id += bcmul(get_key($split_array[$i],$library),bcpow(64,$i));
        }
        return $private_id;
    }
    function get_key($letter,$library){
        $key=array_keys($library,$letter);
        return $key[0];
    }
    function get_random_id()
    {
        $random_value = bindec(openssl_random_pseudo_bytes(5000, $is_strong));
        while ($random_value < 1000000000 || !$is_strong || $random_value > 1000000000000) {
            $random_value = bindec(openssl_random_pseudo_bytes(5000, $is_strong));
        }
        return $random_value;
    }
    function base64($decimal,$library,&$return_id){
        if($decimal<64){
            $return_id.="".$library[$decimal];
        }else{
            $return_id.="".$library[bcmod($decimal,64)];
            base64(bcdiv($decimal,64),$library,$return_id);
        }
    }
?>