<?php

    function curl_download($Url){
     
        // is cURL installed yet?
        if (!function_exists('curl_init')){
            die('Sorry cURL is not installed!');
        }
     
        // OK cool - then let's create a new cURL resource handle
        $ch = curl_init();
     
        // Now set some options (most are optional)
     
        // Set URL to download
        curl_setopt($ch, CURLOPT_URL, $Url);
     
        // Include header in result? (0 = yes, 1 = no)
        curl_setopt($ch, CURLOPT_HEADER, 0);
     
        // Should cURL return or print out the data? (true = return, false = print)
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
     
        // Timeout in seconds
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        // Set Data Decompression
        curl_setopt($ch,CURLOPT_ENCODING, '');
     
        // Download the given URL, and return output
        $output = curl_exec($ch);

        // Close the cURL resource, and free system resources
        curl_close($ch);

        // Save data to local xml file
        $file = fopen('livescore-feed.xml', 'w');
        fwrite($file, $output);
        fclose($file);

    }

    // Cron can only be set to run every minute, so start a for loop to execute script 11 times every minute (every 5 seconds)
    for ($x=0; $x<=10; $x++) {
      
      // Execute script (replace with the URL we sent you)
      curl_download("http://www.tipgin.net/{your-account}/soccer/livescore-feed.xml");

      // Wait 5 seconds
      sleep(5);
    } 

?>