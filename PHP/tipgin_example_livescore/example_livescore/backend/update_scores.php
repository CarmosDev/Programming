 <?php

	// Retrieve XML File
    $file = file_get_contents('livescore-feed.xml');

    // Parse XML with SimpleXML
	$livescore_data = new SimpleXMLElement($file);

	// Iterate over data and print leagues, teams and scores
	foreach ($livescore_data->league as $league) {

		// Begin Table for individual league
		echo "<table class='table table-striped table-bordered table-condensed'>";

		// Print League Name
		echo "<thead><tr><th colspan='6' id='league'>" . $league->attributes()->name . "</th></tr></thead>";
		
		// Iterate over matches to print teams and scores
		echo "<tbody>";
		foreach ($league->match as $match) {

			// Set match variables
			$status = $match->attributes()->status;
			$home = $match->home->attributes()->name;
			$away = $match->away->attributes()->name;
			$score = $match->home->attributes()->goals . " - " . $match->away->attributes()->goals;

			// If match not yet started, there will be a ":" in 'status' attribute
			if (strpos($match->attributes()->status,':') !== false) {
				$score = "-";
			}

			// Print Match Status, Teams and Score
			echo "<tr><td class='status' id='match'>" . $status . "</td><td id='match' colspan='2'>" . 
			$home . "</td><td class='score' id='match'>" . $score . "</td><td id='match' colspan='2'>" . 
			$away . "</td></tr>";
		}

		// End Table for individual league
		echo "</tbody></table>";
	}

?>