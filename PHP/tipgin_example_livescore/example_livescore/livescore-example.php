<html>
 <head>

	<meta charset="utf-8">
	<title>TipGin Feeds Example Livescore</title>

	<!-- Link CSS Stylesheet -->
	<link rel="stylesheet" type="text/css" href="frontend/bootstrap.css"></link>
	<link rel="stylesheet" type="text/css" href="frontend/styles.css"></link>
 
 </head>
 <body>

 	<!-- Livescore Wrap -->
 	<div class="livescore-wrap"></div>

	<!-- Import JQuery -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script>

		// Define get scores function
		function get_scores() {
		  $.get( "backend/update_scores.php", function( data ) {
			$( ".livescore-wrap" ).html(data);
			});
		}

		// On page load
		$( document ).ready(function() {

			// First run
			get_scores();

			// Update every 5 seconds
			window.setInterval(function() {
				get_scores();
			}, 5000);
		});

	</script>
 </body>
</html>