<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 09/03/2017
 * Time: 19:16
 */

require 'PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer();
$mail->setFrom('from@example.com', 'Example');
$mail->addAddress('carmosdevelopers@gmail.com', 'Carmos Developers');
$mail->Subject  = 'First PHPMailer Message';
$mail->Body     = 'Hi! This is my first e-mail sent through PHPMailer.';
$html='<html><head><title></title><link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.8/semantic.min.css"></head><body><i class="american express icon"></i>
<i class="credit card alternative icon"></i>
<i class="diners club icon"></i>
<i class="discover icon"></i>
<i class="google wallet icon"></i>
<i class="japan credit bureau icon"></i>
<i class="mastercard icon"></i>
<i class="paypal card icon"></i>
<i class="paypal icon"></i>
<i class="stripe icon"></i>
<i class="visa icon"></i></body></html>';
$mail->isHTML(true);
if(!$mail->send()) {
    echo 'Message was not sent.';
    echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
    echo 'Message has been sent.';
}
