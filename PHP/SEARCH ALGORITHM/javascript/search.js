function search(){
	var searchBox=document.getElementById('search');
	var searchResults=document.getElementById('searchResults');
	
	searchBox.addEventListener('keydown',function(){
		var searchTerm=searchBox.value;
		if(searchTerm=="" || searchTerm.length==0){
			searchResults.style.display="none";
			return;
		}else{
			searchResults.innerHTML="<center><img src='images/728.GIF' /></center>";
			searchResults.style.display="block";
			if(window.XMLHttpRequest){
				var newsRequest= new XMLHttpRequest();
			}else{
				var newsRequest=new ActiveXObject("Microsoft.XMLHTTP");
			}
			newsRequest.onreadystatechange=function(){
				if(newsRequest.readyState==3 || newsRequest.readyState==4 && newsRequest.status==200){
					searchResults.innerHTML=newsRequest.responseText;
				}
			};
			newsRequest.open("GET","search.php?q="+searchTerm,true);
			newsRequest.send();
		}
	});
	searchBox.addEventListener('keyup',function(){
		var searchTerm=searchBox.value;
		if(searchTerm=="" || searchTerm.length==0){
			searchResults.style.display="none";
			return;
		}else{
			searchResults.innerHTML="<center><img src='images/728.GIF' /></center>";
			searchResults.style.display="block";
			if(window.XMLHttpRequest){
				var newsRequest= new XMLHttpRequest();
			}else{
				var newsRequest=new ActiveXObject("Microsoft.XMLHTTP");
			}
			newsRequest.onreadystatechange=function(){
				if(newsRequest.readyState==3 || newsRequest.readyState==4 && newsRequest.status==200){
					searchResults.innerHTML=newsRequest.responseText;
				}
			};
			newsRequest.open("GET","search.php?q="+searchTerm,true);
			newsRequest.send();
		}
	});
}
function content(){
	var content=document.getElementById('content');
	var loadMore=document.getElementById('loadMore');
	loadMore.innerHTML="<center><img src='images/350.GIF'/></center>";
	
	if(window.XMLHttpRequest){
		var loader=new XMLHttpRequest();
	}else{
		var loader=new ActiveXObject('Microsoft.XMLHTTP');
	}
	loader.onreadystatechange=function(){
		if(loader.readyState==3 || loader.readyState==4 && loader.status==200){
			content.innerHTML=loader.responseText;
		}
		if(loader.readyState==4){
			loadMore.innerHTML="<center><h3>All Latest Stories Loaded...</h3></center>";
		}
	};
	loader.open("GET","loadstories.php",true);
	loader.send();
}
window.addEventListener('load',content);
window.addEventListener('load',search);