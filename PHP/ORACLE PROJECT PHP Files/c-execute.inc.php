<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 12/05/2016
 * Time: 18:45
 */
    namespace Oracle;

    class Database{
        protected $connection=null;
        protected $prefetch=100;
        protected $statement;
        function __construct()
        {
            require("c-connect.inc.php");
            $this->connection=$CONNECTION;
        }
        function execute($sql,$bindvars=array()){
            $this->statement=oci_parse($this->connection,$sql);
            if($this->prefetch>=0){
                oci_set_prefetch($this->statement,$this->prefetch);
            }
            foreach ($bindvars as $bindvar){
                oci_bind_by_name($this->statement,$bindvar[0],$bindvar[1],-1);
            }
            oci_execute($this->statement);
        }
        function execute_fetch_rows($sql,$bindvars=array()){
            $this->execute($sql,$bindvars);
            oci_fetch_all($this->statement,$rows,0,-1,OCI_FETCHSTATEMENT_BY_ROW);
            $this->statement=null;
            return($rows);
        }
    }
?>