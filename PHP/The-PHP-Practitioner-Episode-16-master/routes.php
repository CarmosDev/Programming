<?php

$router->define([
    'The-PHP-Practitioner-Episode-16-master' => 'controllers/index.php',
    'The-PHP-Practitioner-Episode-16-master/about' => 'controllers/about.php',
    'The-PHP-Practitioner-Episode-16-master/about/culture' => 'controllers/about-culture.php',
    'The-PHP-Practitioner-Episode-16-master/contact' => 'controllers/contact.php'
]);
