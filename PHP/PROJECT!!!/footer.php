<?php

echo "
<!DOCTYPE html>
<html>
<head>
<title>Past Papers Online Kenya&trade;</title>
 <!-- Latest compiled and minified CSS -->
<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>

<!-- jQuery library -->
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>

<!-- Latest compiled JavaScript -->
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'></script>
</head>
<body>
	<nav class='navbar navbar-inverse navbar-fixed-bottom'>
		<div class='container-fluid'>
			<div class='navbar-header'>
				<a class='navbar-brand' href='Home.html' data-toggle='tooltip' title='Kenya first online past papers portal for students' >Past Papers Online Kenya&trade;</a>
			</div>
			<div>
				<ul class='nav navbar-nav'>
					<li><a href='TermsAndConditions'>Terms & Conditions</a></li>
					<li><a href='About'>About</a></li>
					<li style='font-size:28px;'>&copy; 2015 Past Papers Online Kenya&trade;</li>
				</ul>
			</div>
		</div>
	</nav>
</body>
</html>

";

?>