<?php

echo " 

<!DOCTYPE html>
<html lang='en'>
<head>
<title>Past Papers Online Kenya&trade;</title>
 <!-- Latest compiled and minified CSS -->
<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>

<!-- jQuery library -->
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>

<!-- Latest compiled JavaScript -->
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'></script>
</head>
<body>
	<!--Navigation Bar top of page-->
		<nav class='navbar navbar-default navbar-fixed-top'>
			<div class='container-fluid'>
				<div class='navbar-header'>
					<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#myNavbar'>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
					</button>
					<a class='navbar-brand' href='Home.html' data-toggle='tooltip' title='Kenya first online past papers portal for students' >Past Papers Online Kenya&trade;</a>
				</div>
				<div class='collapse navbar-collapse' id='myNavbar'>
					<ul class='nav navbar-nav'>
						<li class='active'><a href='Home.html'><span class='glyphicon glyphicon-home'></span>     Home</a></li>
						<li><a href='KCPE.html' data-toggle='tooltip' title='Kenya Certificate of Primary Education'>KCPE</a></li>
						<li><a href='KCSE.html' data-toggle='tooltip' title='Kenya Certificate of Secondary Education'>KCSE</a></li>
						<li><a href='SAT.html' data-toggle='tooltip' title='Scholarship Aptitude Test'>SAT&reg;</a></li>
						<li><a href='TOEFL.html' data-toggle='tooltip' title='Test Of English as a Foreign Language'>TOEFL&reg;</a></li>
						<li><a href='Books.html'><span class='glyphicon glyphicon-book'></span>     Books</a></li>
						<li><a href='LogIn.html'><span class='glyphicon glyphicon-log-in'></span>     Log In</a></li>
						<li><a href='SignUp.html'><span class='glyphicon glyphicon-user'></span>     Sign Up</a></li>
						<li><a href='ContactUs.html'><span class='glyphicon glyphicon-earphone'></span>     Contact Us</a></li>
					</ul>
					<ul class='nav navbar-nav' style='margin-top:5px;'>
						<form class='form-inline' role='form'>
							<div class='form-group'>
								<input type='text' id='searchQuery' class='form-control' placeholder='Enter search query here!!!'>
								<button type='submit' class='btn btn-info'><span class='glyphicon glyphicon-search'></span>     Search</button>
							</div>
						</form>
					</ul>
				</div>
			</div>
		</nav>
	<!--Navigation at the top of the page ends here!!!-->
	<!--JavaScript to enable tooltips-->
		<script>
			$(document).ready(function(){
			$('[data-toggle='tooltip']').tooltip();   
		});
		</script>
</body>
</html>

";
?>