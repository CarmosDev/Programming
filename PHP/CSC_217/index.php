<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student Registration</title>
</head>
<body>
    <form action="index.php" enctype="multipart/form-data" method="post">
        <h1>Student Registration Form</h1>
        <label for="first_name">First Name</label>&nbsp;<input type="text" name="first_name" placeholder="First Name..."><br><br>
        <label for="last_name">Last Name</label>&nbsp;<input type="text" name="last_name" placeholder="Last Name..."><br><br>
        <label for="phone_number">Phone Number</label>&nbsp;<input type="text" name="phone_number" placeholder="Phone Number..."><br><br>
        <label for="gender">Gender</label>&nbsp;<input type="radio" name="gender" value="male">&nbsp;Male&nbsp;<input type="radio" name="gender" value="female">&nbsp;Female<br><br>
        <label for="course">Course</label>&nbsp;
        <select name="course">
            <option value="CSC 211: Data Structures & Algorithms">CSC 211: Data Structures & Algorithms</option>
            <option value="CSC 212: Systems Analysis & Design">CSC 212: Systems Analysis & Design</option>
            <option value="CSC 213: Computer Architecture">CSC 213: Computer Architecture</option>
            <option value="CSC 214: Computer Architecture">CSC 214: Digital Electronics</option>
            <option value="CSC 215: Introduction to Artificial Intelligence">CSC 215: Introduction to Artificial Intelligence</option>
        </select>
        <br><br>
        <label for="year">Year</label>&nbsp;
        <select name="year">
            <option value="Year 1">Year 1</option>
            <option value="Year 2">Year 2</option>
            <option value="Year 3">Year 3</option>
            <option value="Year 4">Year 4</option>
        </select>
        <br><br>
        <label for="comments">Comments</label>&nbsp;<textarea name="comments" ></textarea><br><br>
        <input type="submit" value="Register" name="send">
    </form>
    <?php
        if(isset($_POST['send'])){
            $connection=new mysqli("localhost","root","","csc_217");
            if($connection->connect_error){
                echo "Unable to connect right now. Try again later";
            }else{
                $query_string="INSERT INTO `school`(`First_Name`, `Last_Name`, `Phone_Number`, `Gender`, `Course`, `Year`, `Comments`)VALUES ('".$_POST['first_name']."','".$_POST['last_name']."','".$_POST['phone_number']."','".$_POST['gender']."','".$_POST['course']."','".$_POST['year']."','".$_POST['comments']."')";

                if($connection->query($query_string)==TRUE){
                    echo "Registered successfully!!!";
                }else{
                    echo "Registration has failed. Error : ".$connection->error;
                }
                $connection->close();
            }
        }
    ?>
    <br>
    <div>
        <a href="students.php" target="_blank">List of Students table</a>
    </div>
</body>
</html>