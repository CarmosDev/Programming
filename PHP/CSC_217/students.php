
<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 03/11/2016
 * Time: 23:48
 */

    $connection=new mysqli("localhost","root","","csc_217");
    if($connection->connect_error){
        echo "Unable to connect right now. Try again later";
    }else{
        $query_string="SELECT * FROM `school` WHERE 1";
        $results=$connection->query($query_string);

        if($results->num_rows>0){
            $results_array=$results->fetch_all(MYSQLI_ASSOC);
            //var_dump($results_array);

            echo "<table style='width: 100%;border: solid 1px grey;'>";
                echo "<tr>";
                    echo "<th style='background: lightgrey;'>Student ID</th>";
                    echo "<th style='background: lightgrey;'>First Name</th>";
                    echo "<th style='background: lightgrey;'>Last Name</th>";
                    echo "<th style='background: lightgrey;'>Phone Number</th>";
                    echo "<th style='background: lightgrey;'>Gender</th>";
                    echo "<th style='background: lightgrey;'>Course Name</th>";
                    echo "<th style='background: lightgrey;'>Year</th>";
                    echo "<th style='background: lightgrey;'>Comments</th>";
                echo "</tr>";
                foreach ($results_array as $row){
                    echo "<tr>";
                        foreach ($row as $field){
                            echo "<td style='border: solid 1px grey;'>".$field."</td>";
                        }
                    echo "</tr>";
                }
            echo "</table>";
        }else{
            echo "Error : Unable to fetch data!!!";
        }
        $connection->close();
    }

?>