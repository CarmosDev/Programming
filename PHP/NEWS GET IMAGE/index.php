<?php
	ini_set('display_errors','0');
	
	$HTML_LINK=$_GET['link'];
	$NEWS_SOURCE=$_GET['source'];
	//echo $HTML_LINK.PHP_EOL;
	//echo $NEWS_SOURCE;
	
	switch($NEWS_SOURCE){
		case 'dailynation':
		echo DAILYNATION($HTML_LINK);
		break;
		case 'standard':
		echo STANDARD($HTML_LINK);
		break;
		case 'citizen':
		echo CITIZEN($HTML_LINK);
		break;
		default:
		echo 'Error!!!';
		break;
	}
	
	/*__________DAILY NATION__________*/
	function DAILYNATION($LINK){
		$cURL_HANDLER=curl_init($LINK);
		curl_setopt($cURL_HANDLER,CURLOPT_RETURNTRANSFER,TRUE);
		$HTML_STRING=curl_exec($cURL_HANDLER);
		curl_close($cURL_HANDLER);
		
		$DOCUMENT=new DOMDocument();
		$DOCUMENT->loadHTML($HTML_STRING);
		$IMAGES=$DOCUMENT->getElementsByTagName('img');
		$HOST_URL="http://www.nation.co.ke";
		foreach($IMAGES as $IMAGE){
			$IMAGE_ATTRIBUTES=$IMAGE->attributes;
			$TARGET_IMAGE=$IMAGE_ATTRIBUTES->getNamedItem('class');
			if($TARGET_IMAGE!=null){
				if($TARGET_IMAGE->nodeValue=='photo_article'){
					$TARGET_IMAGE_SRC=$HOST_URL.$IMAGE_ATTRIBUTES->getNamedItem('src')->nodeValue;
					return $TARGET_IMAGE_SRC;
				}
			}
		}
	}
	/*__________THE STANDARD__________*/
	function STANDARD($LINK){
		$cURL_HANDLER=curl_init($LINK);
		curl_setopt($cURL_HANDLER,CURLOPT_RETURNTRANSFER,TRUE);
		$HTML_STRING=curl_exec($cURL_HANDLER);
		curl_close($cURL_HANDLER);
		
		$DOCUMENT=new DOMDocument();
		$DOCUMENT->loadHTML($HTML_STRING);
		$FIGURES=$DOCUMENT->getElementsByTagName('figure');
		$HOST_URL="http://www.standardmedia.co.ke";
		foreach($FIGURES as $FIGURE){
			foreach($FIGURE->childNodes as $FIGURE_CHILD){
				if($FIGURE_CHILD->nodeName == 'img'){
					$TARGET_IMAGE_SRC=$HOST_URL.$FIGURE_CHILD->attributes->getNamedItem('src')->nodeValue;
					return $TARGET_IMAGE_SRC;
				}
			}
		}
	}
	/*__________CITIZEN__________*/
	function CITIZEN($LINK){
		$cURL_HANDLER=curl_init($LINK);
		curl_setopt($cURL_HANDLER,CURLOPT_RETURNTRANSFER,TRUE);
		$HTML_STRING=curl_exec($cURL_HANDLER);
		curl_close($cURL_HANDLER);
		
		$DOCUMENT=new DOMDocument();
		$DOCUMENT->loadHTML($HTML_STRING);
		$SECTIONS=$DOCUMENT->getElementsByTagName('section');
		foreach($SECTIONS as $SECTION){
			if($SECTION->attributes->getNamedItem('class')->nodeValue=='subpage-content'){
				foreach($SECTION->childNodes as $SECTION_CHILD){
					if($SECTION_CHILD->nodeName=='figure'){
						foreach($SECTION_CHILD->childNodes as $SECTION_CHILD_NODE){
							if($SECTION_CHILD_NODE->nodeName=='img'){
								$TARGET_IMAGE_SRC=$SECTION_CHILD_NODE->attributes->getNamedItem('src')->nodeValue;
								return $TARGET_IMAGE_SRC;
							}
						}
					}
				}
			}
		}
	}
?>