<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 19/07/2016
 * Time: 19:44
 */

    class mpesa_c2b_interface{

        protected $URL="https://safaricom.co.ke/mpesa_online/lnmo_checkout_server.php?wsdl";

        private $CHECKOUT_HEADER;
        private $CHECKOUT_REQUEST;
        public $process_checkout_response=null;

        function __construct(){

        }

        function processCheckout($header_object,$request_object){
            $this->CHECKOUT_HEADER=$header_object;
            $this->CHECKOUT_REQUEST=$request_object;
            $this->encrypt_password();

            $soap_message='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns="tns:ns">
                <soapenv:Header>
                  <tns:CheckOutHeader>
                    <MERCHANT_ID>'.$this->CHECKOUT_HEADER->MERCHANT_ID.'</MERCHANT_ID>
                    <PASSWORD>'.$this->CHECKOUT_HEADER->PASSWORD.'</PASSWORD>
                    <TIMESTAMP>'.$this->CHECKOUT_HEADER->TIMESTAMP.'</TIMESTAMP>
                  </tns:CheckOutHeader>
                </soapenv:Header>
                <soapenv:Body>
                  <tns:processCheckOutRequest>
                    <MERCHANT_TRANSACTION_ID>'.$this->CHECKOUT_REQUEST->MERCHANT_TRANSACTION_ID.'</MERCHANT_TRANSACTION_ID>
                    <REFERENCE_ID>'.$this->CHECKOUT_REQUEST->REFERENCE_ID.'</REFERENCE_ID>
                    <AMOUNT>'.$this->CHECKOUT_REQUEST->AMOUNT.'</AMOUNT>
                    <MSISDN>'.$this->CHECKOUT_REQUEST->MSISDN.'</MSISDN>
                    <!--Optional:-->
                    <ENC_PARAMS>'.$this->CHECKOUT_REQUEST->ENC_PARAMS.'</ENC_PARAMS>
                    <CALL_BACK_URL>'.$this->CHECKOUT_REQUEST->CALL_BACK_URL.'</CALL_BACK_URL>
                    <CALL_BACK_METHOD>'.$this->CHECKOUT_REQUEST->CALL_BACK_METHOD.'</CALL_BACK_METHOD>
                    <TIMESTAMP>'.$this->CHECKOUT_REQUEST->TIMESTAMP.'</TIMESTAMP>
                  </tns:processCheckOutRequest>
                </soapenv:Body>
                </soapenv:Envelope>';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_URL,$this->URL);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $soap_message);
            curl_setopt($ch,CURLOPT_ENCODING,'');

            // converting
            $this->process_checkout_response = curl_exec($ch);
            curl_close($ch);
        }

        private function encrypt_password(){
            $this->CHECKOUT_HEADER->PASSWORD=base64_encode(hash("sha256",$this->CHECKOUT_HEADER->MERCHANT_ID
                .$this->CHECKOUT_HEADER->PASSWORD
                .$this->CHECKOUT_HEADER->TIMESTAMP));
        }
    };

    class checkout_header_object{
        public $MERCHANT_ID;
        public $PASSWORD;
        public $TIMESTAMP;

        function __construct($merchant_id,$raw_password,$timestamp){
            $this->MERCHANT_ID=$merchant_id;
            $this->PASSWORD=$raw_password;
            $this->TIMESTAMP=$timestamp;
        }
    };

    class checkout_request_object{
        public $MERCHANT_TRANSACTION_ID;
        public $REFERENCE_ID;
        public $AMOUNT;
        public $MSISDN;
        public $ENC_PARAMS;
        public $CALL_BACK_URL;
        public $CALL_BACK_METHOD;
        public $TIMESTAMP;

        function __construct($merchant_transaction_id
            ,$reference_id
            ,$amount
            ,$msisdn
            ,$enc_params
            ,$call_back_url
            ,$call_back_method
            ,$timestamp){

            $this->MERCHANT_TRANSACTION_ID=$merchant_transaction_id;
            $this->REFERENCE_ID=$reference_id;
            $this->AMOUNT=$amount;
            $this->MSISDN=$msisdn;
            $this->ENC_PARAMS=$enc_params;
            $this->CALL_BACK_URL=$call_back_url;
            $this->CALL_BACK_METHOD=$call_back_method;
            $this->TIMESTAMP=$timestamp;
        }
    };

?>