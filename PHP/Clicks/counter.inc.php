 <?php
if (!isset($_SESSION))    {
    session_start();
}
$page = urlencode($_SERVER['PHP_SELF'].$_SERVER['QUERY_STRING'].'.counter');

if (!file_exists('counter/'))    {
    mkdir('counter');
}

$visits = file_exists('counter/'.$page) ? (int)file_get_contents('counter/'.$page) : 0;
if (!isset($_SESSION['counter']))    {
    $visits++;
    createFile('counter/'.$page,$visits);
    $_SESSION['counter'] = 'counted';
}

function createFile($file,$content)    {
    $mode='w+';
    $fp = fopen($file,$mode);
    fwrite($fp,$content);
    fclose($fp);
}

?> 