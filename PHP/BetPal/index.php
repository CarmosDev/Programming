<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <title>BetPal: Welcome</title>
    <!--JQuery-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!--Semantic UI CSS-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/semantic-ui/2.2.4/semantic.min.css">
    <!--Semantic UI JS-->
    <script src="https://cdn.jsdelivr.net/semantic-ui/2.2.4/semantic.min.js"></script>
    <!--Scripts-->
    <script src="libs/javascript/base_ajax.js"></script>
    <script src="libs/javascript/main.js"></script>

    <?php
        require 'libs/php/countries.inc.php';
        require 'libs/php/livescore.inc.php';
        require 'libs/php/carlcrypt.inc.php';
    ?>
</head>
<body>
    <!--Navigation Bar-->
    <div class="ui top fixed inverted menu">
        <div class="link item"><i class="home icon"></i><a href="index.php">Home</a></div>
        <div class="link item"><a href="#">About BetPal &trade;</a></div>
        <div class="right link item"><a href="#">Contact Us</a></div>
    </div>
    <br>
    <br>
    <br>
    <h1 class="ui centered header">BetPal&trade; Predictions</h1>
    <div class="ui divider"></div>
    <!--Main Content-->
    <div class="ui divided grid">
        <div class="three wide column" >
            <h2 class="ui red center aligned dividing header">Competitions</h2>
            <div class="ui centered dividing small header">Countries</div>
            <?php
                $countries=new Countries();
                $countries->print_all_countries();
            ?>
        </div>
        <div class="ten wide column">
            <h2 class="ui green center aligned dividing header">Today's Games</h2>
            <?php
                $livescore=new Livescore();
                $livescore->print_today_fixtures();
            ?>
        </div>
        <div class="three wide column">
        </div>
    </div>

    <!--Initialisations-->
    <script>
        $('.ui.accordion').accordion();
    </script>
</body>
</html>