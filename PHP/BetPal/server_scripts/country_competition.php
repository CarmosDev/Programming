<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 11/01/2017
 * Time: 19:12
 */
    require "../libs/php/carlcrypt.inc.php";

    $country="".$_GET['id'];
    $url="http://tipgin.net/examples/soccer_extended_fixtures.xml";

    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch,CURLOPT_ENCODING, '');
    $xml = curl_exec($ch);
    curl_close($ch);

    $extended_fixtures=new SimpleXMLElement($xml);

    if($extended_fixtures->attributes()->country == $country){
        echo "<div class='ui selection animated list'>";
            foreach ($extended_fixtures->league as $league){
                echo "<div class='item'><a class='header' >".$league->attributes()->name."</a></div>";
            }
        echo "</div>";
        return;
    }

    echo "<div class='list'>No competitions for this country</div>";
?>