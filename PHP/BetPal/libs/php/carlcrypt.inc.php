<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 04/01/2017
 * Time: 14:27
 */

    namespace CarlCrypt;

    Class CarlCrypt{
        protected $minimum=1000000000;
        protected $maximum=1000000000000;
        protected $library='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-';
        protected $library_array;
        protected $public_id="";
        protected $id;
        protected $ascii="";

        function __construct(){
            $this->library_array=str_split($this->library,1);
        }

        function __destruct(){
            $this->ascii="";
        }

        protected function get_random_id(){
            $this->id = bindec(openssl_random_pseudo_bytes(5000, $is_strong));
            while ($this->id < $this->minimum || !$is_strong || $this->id > $this->maximum) {
                $this->id = bindec(openssl_random_pseudo_bytes(5000, $is_strong));
            }
        }

        protected function base64($decimal,$library,&$return_id){
            if($decimal<64){
                $return_id.="".$library[intval($decimal)];
            }else{
                $return_id.="".$library[bcmod($decimal,64)];
                $this->base64(bcdiv($decimal,64),$library,$return_id);
            }
        }

        public function get_public_id(){
            $this->get_random_id();
            $this->base64($this->id,$this->library_array,$this->public_id);
            $a=$this->public_id;
            $this->public_id="";
            return $a;
        }

        protected function get_key($letter,$library){
            $key=array_keys($library,$letter);
            return $key[0];
        }

        protected function decode_id($public_id,$library){
            $split_array=str_split($public_id);
            $private_id=0;
            for($i=0;$i<count($split_array);$i++){
                $private_id=bcadd($private_id,bcmul($this->get_key($split_array[$i],$library),bcpow(64,$i)));
            }
            return $private_id;
        }

        public function get_decoded_id($public_id){
            return $this->decode_id($public_id,$this->library_array);
        }

        protected function get_ascii($word){
            foreach (str_split($word) as $character){
                if(ord($character)<100){
                    $this->ascii .= "0";
                }
                $this->ascii .= ord($character);
            }
        }

        public function encrypt_string($raw_string){
            $this->get_ascii($raw_string);
            $a="";
            $this->base64($this->ascii,$this->library_array,$a);

            $this->ascii="";

            return $a;
        }

        public function decrypt_string($encrypted_string){
            $a=$this->get_decoded_id($encrypted_string);
            if(strlen($a)%3==1){
                $a ="00".$a;
            }
            if(strlen($a)%3==2){
                $a ="0".$a;
            }
            $b="";
            foreach (str_split($a,3) as $value){
                $b .= chr($value);
            }
            return $b;
        }
    }
?>