<?php
/**
 * Created by PhpStorm.
 * User: MOSETI
 * Date: 10/01/2017
 * Time: 17:54
 */

    class Livescore{

        protected $url="http://tipgin.net/examples/soccer_livescore.xml";
        protected $xml="";

        function __construct(){

            $ch=curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch,CURLOPT_ENCODING, '');
            $this->xml = curl_exec($ch);
            curl_close($ch);

        }

        public function print_today_fixtures(){
            $livescore=new SimpleXMLElement($this->xml);
            $country=new Countries();

            echo "<div class='ui fluid styled accordion'>";
                foreach ($livescore->league as $league){
                    echo
                        "<div class='title'>".
                            "<i class='dropdown icon'></i>".
                            $country->get_flag($league->attributes()->country).$league->attributes()->name.
                        "</div>".
                        "<div class='content'>";
                        echo "<div class='accordion'>";
                            foreach ($league->match as $match){
                                $this->print_match($match);
                            }
                        echo "</div>";
                    echo "</div>";
                }
            echo "</div>";
        }

        private function print_match($match){
                echo
                    "<div class='title'>".
                        "<i class='dropdown icon'></i>".$match->home->attributes()->name." vs ".$match->away->attributes()->name.
                        "<div class='ui right floated header'><div class='ui black label'>Scores</div></div>".
                    "</div>".
                    "<div class='content'></div>";
//            $key=new CarlCrypt\CarlCrypt();
//            $enc_id=$key->encrypt_string($match->attributes()->static_id);
//            echo "<tr>";
//                echo "<td class='ui one wide header'>".$match->attributes()->time."</td>";
//                echo "<td class='ui six wide right aligned'>".$match->home->attributes()->name."</td>";
//
//                if($match->home->attributes()->goals != "?"){
//                    echo "<td class='ui two wide center aligned header'>".$match->home->attributes()->goals." - ".$match->away->attributes()->goals."</td>";
//                }else{
//                    echo "<td class='ui two wide center aligned'></td>";
//                }
//
//                echo "<td class='ui six wide'>".$match->away->attributes()->name."</td>";
//
//                //echo "<td class='ui four wide positive header'><a data-tooltip='Click to check prediction' data-inverted=''>Bet Prediction</a></td>";
//
//                if($match->attributes()->status == "FT"){
//                    echo "<td class='ui one wide positive header'>".$match->attributes()->status."</td>";
//                }else if(strpos($match->attributes()->status,":")){
//                    echo "<td class='one wide'>".$match->attributes()->status."</td>";
//                }else{
//                    echo "<td class='ui one wide red header'>".$match->attributes()->status."' </td>";
//                }
//
//                //echo "<tr><td colspan='5' id='".$enc_id."'></td></tr>";
//                echo "<tr>".
//                        "<td colspan='5' id='$enc_id'>".
//                            "<div class='ui fluid accordion'>".
//                                "<div class='title'><i class='dropdown icon'></i><a href='#'>Prediction for ".$match->home->attributes()->name." vs ".$match->away->attributes()->name."</a></div>".
//                                    "<div class='content'><p>Prediction</p></div>"
//                            ."</div>"
//                        ."</td>"
//                    ."</tr>";
//            echo "</tr>";
        }
    }
?>