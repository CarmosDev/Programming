/**
 * Created by MOSETI on 11/01/2017.
 */
function createCORSRequest(method, url) {
    var xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {

        // Check if the XMLHttpRequest object has a "withCredentials" property.
        // "withCredentials" only exists on XMLHTTPRequest2 objects.
        xhr.open(method, url, true);

    } else if (typeof XDomainRequest != "undefined") {

        // Otherwise, check if XDomainRequest.
        // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
        xhr = new XDomainRequest();
        xhr.open(method, url);

    } else {

        // Otherwise, CORS is not supported by the browser.
        xhr = null;

    }
    return xhr;
}
function set_xmlhttp_listeners(xml_http_object,result_container) {
    xml_http_object.onloadstart=function () {
        result_container.innerHTML=
            "<div class='ui basic very padded segment'>" +
                "<div class='ui active inverted dimmer'>" +
                    "<div class='ui text loader'>Loading</div>" +
                "</div>" +
            "</div>";
    };
    xml_http_object.onloadend=function () {};
    xml_http_object.onerror=function () {
        result_container.innerHTML=
        "<div class='ui error icon mini message'>" +
            "<i class='close icon'></i>" +
            "<i class='warning sign icon'>" +
            "</i><div class='content'>" +
                "<div class='header'>Error</div>" +
                "<p>Sorry! An error occured. Try again</p>" +
            "</div>"+
        "</div>";
    };
    xml_http_object.ontimeout=function () {
        result_container.innerHTML=
            "<div class='ui warning icon mini message'>" +
                "<i class='close icon'></i>" +
                "<i class='wait icon'>" +
                "</i><div class='content'>" +
                    "<div class='header'>Timeout</div>" +
                    "<p>Kindly check your internet connection and try again.</p>" +
                "</div>"+
            "</div>";
    };
}

function ajax_request(get_url,result_container) {
    var XML_HTTP_REQUEST;
    var OK=200;
    var URL=get_url;
    var CONTAINER=result_container;

    XML_HTTP_REQUEST=createCORSRequest('GET',get_url);

    if(!XML_HTTP_REQUEST){
        throw new Error('CORS not supported');
    }else{
        set_xmlhttp_listeners(XML_HTTP_REQUEST,CONTAINER);

        XML_HTTP_REQUEST.onreadystatechange = function () {
            switch(XML_HTTP_REQUEST.readyState){
                case XMLHttpRequest.UNSENT:
                    break;
                case XMLHttpRequest.OPENED:
                    break;
                case XMLHttpRequest.LOADING:
                    break;
                case XMLHttpRequest.HEADERS_RECEIVED:
                    break;
                case XMLHttpRequest.DONE:
                    if(XML_HTTP_REQUEST.status == OK){
                        CONTAINER.innerHTML = XML_HTTP_REQUEST.responseText;
                    }else{

                    }
                    break;
                default:
                    break;
            }
        };

        XML_HTTP_REQUEST.responseType="text";
        XML_HTTP_REQUEST.timeout=20000;

        XML_HTTP_REQUEST.open("GET",URL,true);
        XML_HTTP_REQUEST.send();
    }
}