<?php

include "header.php";

echo "
<!DOCTYPE html>
<html>
<head>
<title></title>
</head>
<body>
	<div>
		<div class='container-fluid'>
			<h4>We would love to hear your feedback.Contact Us in the form below :</h4>
			<br>
			<h3>Contact Us...</h3>
			<hr>
			<form role='form' style='background:orange; border-radius:5px; box-shadow: 10px 10px 5px #888888;' action='' method='post'>
				<br>
					<div class='spacer' style='margin-right:20px; margin-left:20px; margin-top:20px; margin-bottom:20px;'>
						<div class='form-group'>
							<h4><label>Newslinks&trade;Kenya Contact Us Form:</label></h4>
						</div>
						<div class='form-group has-feedback'>
							<label>Your Email Address...</label>
							<input class='form-control' id='eMail' type='email' placeholder='Enter your Email Address here...' required />
							<span class='glyphicon glyphicon-envelope form-control-feedback'></span>
						</div>
						<div class='form-group has-feedback'>
							<label>Subject...</label>
							<input class='form-control' id='subject' placeholder='Enter your Subject here...' required>
							<span class='glyphicon glyphicon-pencil form-control-feedback'></span>
						</div>
						<div class='form-group has-feedback'>
							<label>Message...</label>
							<textarea class='form-control' id='message' placeholder='Enter your Message here...' required></textarea>
							<span class='glyphicon glyphicon-pencil form-control-feedback'></span>
						</div>
						<div class='form-group has-feedback'>
							<button class='btn btn-info' id='sendMessage' type='submit'>Send My Message</button>
							<h5>By submitting this form , you agree to our <a>User Agreement</a></h5>
							<label >&copy;2015 Newslinks&trade;Kenya</label>
						</div>
					</div>
				</br>
			</form>
			<hr>
		</div>
	</div>

<footer>
		<div class='container-fluid'>
			<div id='terms' style='background:black;'>
					<h4 style='color:purple;'>We do not own the news and logos presented on this page.The contents of this page are the sole copyright of the individual sites that host them.</h4>
					<center><h5>&copy;2015 Newslinks&trade;Kenya</h5></center>
			</div>
		</div>
	</footer>
</body>
</html>
";

if(isset($_POST['sendMessage'])){
	$receipient = "carmoseti@gmail.com";
	$sender = $_POST['eMail'];
	$subject = $_POST['subject'];
	$message = $_POST['message'];
	
	imap_mail($receipient,$subject,$message.$sender);
}

?>