<?php
ini_set("display_errors","0");
$xml = ("https://www.google.com/trends/hottrends/atom/feed?pn=p37");

$xmlDoc = new DOMDocument();

$xmlDoc->load($xml);

$it = $xmlDoc->getElementsByTagName('item');
$trends = "";
$trendsView = "";
$len = $it->length;
	for($i=0; $i<10; $i++){
		$link = $it->item($i)->getElementsByTagName('news_item')->item(0)->getElementsByTagName('news_item_url')->item(0)->childNodes->item(0)->nodeValue;
		$title = $it->item($i)->getElementsByTagName('title')->item(0)->childNodes->item(0)->nodeValue;
		$pic = $it->item($i)->getElementsByTagName('picture')->item(0)->childNodes->item(0)->nodeValue;
		$picSource = $it->item($i)->getElementsByTagName('picture_source')->item(0)->childNodes->item(0)->nodeValue;
		$viewers= $it->item($i)->getElementsByTagName('approx_traffic')->item(0)->childNodes->item(0)->nodeValue;
		$news_title =$it->item($i)->getElementsByTagName('news_item')->item(0)->getElementsByTagName('news_item_title')->item(0)->childNodes->item(0)->nodeValue;
		$news_source =$it->item($i)->getElementsByTagName('news_item')->item(0)->getElementsByTagName('news_item_source')->item(0)->childNodes->item(0)->nodeValue;
		$news_snippet = $it->item($i)->getElementsByTagName('news_item')->item(0)->getElementsByTagName('news_item_snippet')->item(0)->childNodes->item(0)->nodeValue;
		
		$trends .= "<code style='margin-left:20px;'><a href='$link' target='_blank' data-toggle='popover' title='$title' data-content='$viewers views' data-trigger='hover' data-placement='auto bottom'>$title</a></code>";
		$trendsView .= "<div class='col-sm-4' style='background:white; height:400px;'><div class='well'><h6><b>$title</b></h6><img src='$pic'/><h6>Image Source:$picSource</h6><a href='$link' target='_blank'>$news_title by: <b>$news_source</b></a><br/>$news_snippet</div></div>";
	}

echo "
<!DOCTYPE html>
<html>
<head>
<title>NEWSLINKS&trade;Kenya</title>
 <!-- Latest compiled and minified CSS -->
<link rel='stylesheet' href='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css'>

<!-- jQuery library -->
<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>

<!-- Latest compiled JavaScript -->
<script src='http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js'></script>
<script type='text/javascript' src='https://www.google.com/jsapi'></script>

<script>
<!--Loads the feed API module 'feeds' version '1'-->
google.load('feeds', '1');
<!--Create a function to execute the API-->
function displayFeedNation(){
//Create an object with the url
var feed = new google.feeds.Feed('http://news.google.com/?output=rss');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
//number of entries
feed.setNumEntries(10);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'

var containerMarquee = document.getElementById('GoogleNewsLinks');
var htmlNationNewsLinks = '';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];

//create an element in html document
htmlNationNewsLinks += '<a href=' + entry.link + '  target=_blank' + '  style=margin-right:30px;' +'>' + entry.title + '</a>';
			}	
		containerMarquee.innerHTML = htmlNationNewsLinks;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedNation);
</script>
<script>
$(document).ready(function(){
    $('[data-toggle=\"popover\"]').popover();
});
</script>
<script>
	function showHint(str){
		if(str.length==0){
			document.getElementById('hintsSearch').innerHTML = '';
			document.getElementById('hintsSearch').style.border = 'solid 1px #DC143C';
			document.getElementById('hintsSearch').style.height = '0px';
			document.getElementById('hintsSearch').style.overflow = ''
			return;
		}
		if(window.XMLHttpRequest){
			xmlhttp = new XMLHttpRequest();
		}
		else{
			xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
		}
		xmlhttp.onreadystatechange = function(){
			if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
				document.getElementById('hintsSearch').innerHTML = xmlhttp.responseText;
				document.getElementById('hintsSearch').style.border = 'solid 1px black';
				document.getElementById('hintsSearch').style.background = 'white';
				document.getElementById('hintsSearch').style.overflow = 'scroll';
				document.getElementById('hintsSearch').style.height = '100px';
			}
		}
		xmlhttp.open('GET','newslinksSearch.php?q='+str, true);
		xmlhttp.send();
	}
</script>
<script>
	$(document).ready(function(){
		$(\"#top10\").on(\"hide.bs.collapse\",function(){
			$(\"#collapser\").html('View...<span class=\"glyphicon glyphicon-collapse-down\"></span>');
		});
		$(\"#top10\").on(\"show.bs.collapse\",function(){
			$(\"#collapser\").html('Close...<span class=\"glyphicon glyphicon-collapse-up\"></span>');
		});
	});
</script>
<script>
	//var s = 0;
	//function online(){
	//	s++;
	//	document.getElementById('online').innerHTML = s+' user online now!!!';
	//	s=s;
	//}
</script>
</head>
<body onload='online()' onunload='offline()'>
	<nav class='navbar navbar-inverse navbar-fixed-top' style='background:#DC143C;'>
		<div class='container-fluid'>
			<div class='navbar-header'>
				<a href='index.php' class='navbar-brand' style='color:black;'><span class='glyphicon glyphicon-globe'></span>      NEWSLINKS&trade;Kenya</a>
				<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#myNavbar'>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
				</button>
				<!--<kbd id='online'></kbd>-->
			</div>
			<div class='collapse navbar-collapse' id='myNavbar' >
				<h6>
				<ul class='nav navbar-nav'>
					<li><a href='index.php' style='color:black;'><strong>Latest</strong></a></li>
					<li class='dropdown'>
					<a class='dropdown-toggle' href='' data-toggle='dropdown' style='color:white;'>Kenyan News   <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li><a href='DailyNation.php' style='color:black;'>Daily Nation</a></li>
						<li><a href='TheStandard.php' style='color:black;'>The Standard</a></li>
						<li><a href='index.php' style='color:black;'>The Star</a></li>
						<li><a href='PeopleDaily.php' style='color:black;'>People Daily</a></li>
						<li><a href='CitizenKenya.php' style='color:black;'>Citizen Kenya</a></li>
						<li><a href='NairobiNews.php' style='color:black;'>Nairobi News</a></li>
						<li><a href='KBC.php' style='color:black;'>KBC</a></li>
					</ul>
					</li>
					<li><a href='Entertainment&Gossip.php' style='color:white;'>Local Entertainment & Gossip</a></li>
					<li><a href='Entertainment&GossipInt.php' style='color:white;'>International Entertainment & Gossip</a></li>
					<li class='dropdown'>
					<a class='dropdown-toggle' href='' data-toggle='dropdown' style='color:white;'>International News   <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li><a href='TheEastAfrican.php' style='color:black;'>The East African</a></li>
						<li><a href='AfricaReview.php' style='color:black;'>Africa Review</a></li>
						<li><a href='BBC.php' style='color:black;'>BBC</a></li>
						<li><a href='CNN.php' style='color:black;'>CNN</a></li>
						<li><a href='HuffingtonPost.php' style='color:black;'>Huffington Post</a></li>
						<li><a href='DailyMail.php' style='color:black;'>Daily Mail UK</a></li>
					</ul>
					</li>
					<li class='dropdown'>
					<a class='dropdown-toggle' href='' data-toggle='dropdown' style='color:white;'>Business   <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li><a href='BusinessDaily.php' style='color:black;'>Business Daily</a></li>
						<li><a href='BusinessInsider.php' style='color:black;'>Business Insider</a></li>
						<li><a href='Forbes.php' style='color:black;'>Forbes </a></li>
					</ul>
					</li>
					<li class='dropdown'>
					<a class='dropdown-toggle' href='' data-toggle='dropdown' style='color:white;'>Soccer   <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li><a href='ESPN.php' style='color:black;'>ESPN </a></li>
						<li><a href='GoalCom.php' style='color:black;'>Goal.com </a></li>
					</ul>
					</li>
					<li class='dropdown'>
					<a class='dropdown-toggle' href='' data-toggle='dropdown' style='color:white;'>Science & Tech   <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li><a href='HSW.php' style='color:black;'>How Stuff Works</a></li>
						<li><a href='cNet.php' style='color:black;'>cNet</a></li>
					</ul>
					</li>
					<li class='dropdown'>
					<a class='dropdown-toggle' href='' data-toggle='dropdown' style='color:white;'>Automobiles   <span class='caret'></span></a>
					<ul class='dropdown-menu'>
						<li><a href='AutoexpressUK.php' style='color:black;'>Autoexpress UK</a></li>
						<li><a href='BBCTopGear.php' style='color:black;'>BBC Top Gear</a></li>
					</ul>
					</li>
					<li><a href='ContactUs.php' style='color:white;'>Contact Us / Sema Nasi</a></li>
					<div class='col-sm-12'>
					<form class='form-inline' role='form'>
						<div class='form-group has-feedback'>
								<input onkeyup='showHint(this.value)' type='text' id='searchQuery' class='form-control' placeholder='Search popular stories today!!!' style='width:1300px;'>
								<span class='glyphicon glyphicon-search form-control-feedback'></span>
								<div id='hintsSearch' style='width:1300px; position:absolute; z-index:-1;'></div>
						</div>	
					</form>
					<br/>
					</div>
					<br/>
				</ul>
				</h6>
			</div>			
		</div>
	</nav>
			<hr>
	<br/>
	<br/><br/><br/><br/>
	<div class='well well-sm' style='background:#FFFF80;'>
		<h5><span class='glyphicon glyphicon-globe'></span>    Top trends in Kenya right now :    <button id='collapser' data-toggle='collapse' data-target='#top10' type='button' class='btn btn-success'>View...<span class='glyphicon glyphicon-collapse-down'></span></button></h5>
		<div id='googleTop10' >$trends<hr/></div>
		<div class='collapse' id='top10' style='border:solid 1px black; background:white; position:absolute; z-index:+1;'>$trendsView</div>
	</div>
	<div class='col-sm-12'>
		<h6>Google Advertisement</h6>
	</div>
		<div class='container-fluid'>
			<div class='row'>
				<div class='col-sm-2'>
					<img src='Images/google.png' style='height:50px; width:100px;'><h4>World News: </h4>
				</div>
				<div class='col-sm-10'>
					<marquee style=margin-top:10px; onMouseOver='this.stop()' onMouseOut='this.start()' id='GoogleNewsLinks'behavior='scroll' direction='left'></marquee>
				</div>
			</div>
		</div>
		<hr>
</body>
</html>
";

?>