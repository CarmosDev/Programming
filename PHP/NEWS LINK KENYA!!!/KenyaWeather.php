<?php

include "header.php";

	$towns = ['Nairobi','Mombasa','Kisumu','Eldoret','Lodwar','Mandera','Garissa','Lamu','Machakos','Marsabit','Nyeri','Kiambu','Thika','Nakuru','Narok','Kisii','Kakamega','Bungoma'];
	$feed = "";
	$first = "<html>
			<head>
			<title></title>
			</head>
			<body>
				<div class=container-fluid>
					<a href='http://www.wunderground.com' target='_blank'><img type='.png' src='Images/wundergroundLogo_4c_horz.png' style='height:50px;' /></a>
					<h4 style='background:grey; margin-top:10px; margin-bottom:10px;'>This Weather Feed is powered courtesy of :<a href='http://www.wunderground.com' target='_blank'>Weather Underground</a></h4>
					<div class=row>";
	$last = "</div>
				</div>
			</body>
			</html>";
	for ($i=0; $i<count($towns); $i++){
	

	//GET http://api.wunderground.com/api/Your_Key/features/settings/q/query.format
  $json_string = file_get_contents("http://api.wunderground.com/api/e555412b8a83e073/geolookup/conditions/q/".$towns[$i].".json");
  $parsed_json = json_decode($json_string);
  $location = $parsed_json->{'location'}->{'city'};
  $lat = $parsed_json->{'location'}->{'lat'};
  $lon = $parsed_json->{'location'}->{'lon'};
  $weather = $parsed_json->{'current_observation'}->{'weather'};
  $temp_c = $parsed_json->{'current_observation'}->{'temp_c'};
  $humidity = $parsed_json->{'current_observation'}->{'relative_humidity'};
  $visibility = $parsed_json->{'current_observation'}->{'visibility_km'};
  $rainfall = $parsed_json->{'current_observation'}->{'precip_today_metric'};
  $image = $parsed_json->{'current_observation'}->{'icon_url'};
  $imageattr = $parsed_json->{'current_observation'}->{'icon'};
  
  $feed .= "<div class=col-sm-3 style=height:300px;>
							<p><h3>${location}</h3></p>
							<p><h4>${weather}</h4></p>
							<img src=${image}></img>
							<p><h5>Temperature : ${temp_c} <sup>o</sup>C</h5></p>
							<p><h5>Humidity : ${humidity}</h5></p>
							<p><h5>Visibility : ${visibility} Kilometres</h5></p>
							<p><h5>Rainfall : ${rainfall} mm</h5></p>
							<hr>
						</div>";
	
	}
	echo $first.$feed.$last;
?>