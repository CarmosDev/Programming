<?php

include "header.php";

echo "
<!DOCTYPE html>
<html>
<head>
<title></title>
<script type='text/javascript' src='https://www.google.com/jsapi'></script>
<!--Daily Nation-->
<script>
<!--Loads the feed API module 'feeds' version '1'-->
google.load('feeds', '1');
<!--Create a function to execute the API-->
function displayFeedNation(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.kbc.co.ke/category/news/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentNation');
var htmlNation= '';
var containerMarquee = document.getElementById('nationNewsLinks');
var htmlNationNewsLinks = '';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlNation += '<div class=col-sm-3 style=height:300px;>' + 
		'<div class=container-fluid >' +
		'<h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'Continue reading'+
		'</a>' +
		'</h5>'+
		'<hr></div>' +
		'</div>';	
htmlNationNewsLinks += '<a href=' + entry.link + '  target=_blank' + '  style=margin-right:30px;' +'>' + entry.title + '</a>';
			}
		container.innerHTML = htmlNation;
		containerMarquee.innerHTML = htmlNationNewsLinks;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedNation);

<!--Create a function to execute the API-->
function displayFeedNationBiz(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.kbc.co.ke/category/business/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentNationBiz');
var htmlNationBiz= '';
var containerMarquee = document.getElementById('nationBizLinks');
var htmlNationBizLinks = '';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlNationBiz += '<div class=col-sm-3 style=height:300px;>' + 
		'<div class=container-fluid >' +
		'<h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'Continue reading'+
		'</a>' +
		'</h5>'+
		'<hr></div>' +
		'</div>';	
htmlNationBizLinks += '<a href=' + entry.link + '  target=_blank' + '  style=margin-right:30px;' +'>' + entry.title + '</a>';
			}
		container.innerHTML = htmlNationBiz;
		containerMarquee.innerHTML = htmlNationBizLinks;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedNationBiz);

<!--Create a function to execute the API-->
function displayFeedNationCounties(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.kbc.co.ke/category/news/international-news/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentNationCounties');
var htmlNationCounties = '';
var containerMarquee = document.getElementById('nationCountiesLinks');
var htmlNationCountiesLinks = '';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlNationCounties += '<div class=col-sm-3 style=height:300px;>' + 
		'<div class=container-fluid >' +
		'<h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'Continue reading'+
		'</a>' +
		'</h5>'+
		'<hr></div>' +
		'</div>';	
htmlNationCountiesLinks += '<a href=' + entry.link + '  target=_blank' + '  style=margin-right:30px;' +'>' + entry.title + '</a>';
			}
		container.innerHTML = htmlNationCounties;
		containerMarquee.innerHTML = htmlNationCountiesLinks;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedNationCounties);

<!--Create a function to execute the API-->
function displayFeedNationSports(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.kbc.co.ke/category/sports/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentNationSports');
var htmlNationSports = '';
var containerMarquee = document.getElementById('nationSportsLinks');
var htmlNationSportsLinks = '';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlNationSports += '<div class=col-sm-3 style=height:300px;>' + 
		'<div class=container-fluid >' +
		'<h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'Continue reading'+
		'</a>' +
		'</h5>'+
		'<hr></div>' +
		'</div>';	
htmlNationSportsLinks += '<a href=' + entry.link + '  target=_blank' + '  style=margin-right:30px;' +'>' + entry.title + '</a>';
			}
		container.innerHTML = htmlNationSports;
		containerMarquee.innerHTML = htmlNationSportsLinks;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedNationSports);

<!--Create a function to execute the API-->
function displayFeedNationOpinion(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.kbc.co.ke/category/technology/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentNationOpinion');
var htmlNationOpinion = '';
var containerMarquee = document.getElementById('nationOpinionLinks');
var htmlNationOpinionLinks = '';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlNationOpinion += '<div class=col-sm-3 style=height:300px;>' + 
		'<div class=container-fluid >' +
		'<h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'Continue reading'+
		'</a>' +
		'</h5>'+
		'<hr></div>' +
		'</div>';	
htmlNationOpinionLinks += '<a href=' + entry.link + '  target=_blank' + '  style=margin-right:30px;' +'>' + entry.title + '</a>';
			}
		container.innerHTML = htmlNationOpinion;
		containerMarquee.innerHTML = htmlNationOpinionLinks;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedNationOpinion);


</script>
<script>
$(function() {
    $('marquee').mouseover(function() {
        $(this).attr('scrollamount',0);
    }).mouseout(function() {
         $(this).attr('scrollamount',5);
    });
});
</script>
</head>
<body>
	<div class='localContentNation'>
		<div class='container-fluid'>
			<div class='row' style='background:beige;'>
				<div class='col-sm-12'>
					<a href='http://www.kbc.co.ke/' target='_blank'><img type='.png' src='Images/KBC.png' style='height:50px;' /></a>
				</div>
				<div class='col-sm-2'>
					<h4>Latest News links :</h4>
				</div>
				<div class='col-sm-10'>
					<marquee style=margin-top:10px; onMouseOver='this.stop()' onMouseOut='this.start()' id='nationNewsLinks'behavior='scroll' direction='left'></marquee>
				</div>
			</div>
			<div class='row'>
				<div id='localContentNation'>
				</div>
			</div>
			<div class='row' style='background:beige;'>
				<div class='col-sm-2'>
					<h4>Latest Business links :</h4>
				</div>
				<div class='col-sm-10'>
					<marquee style=margin-top:10px; onMouseOver='this.stop()' onMouseOut='this.start()' id='nationBizLinks'behavior='scroll' direction='left'></marquee>
				</div>
			</div>
			<div class='row'>
				<div id='localContentNationBiz'>
				</div>
			</div>
			
			<div class='row' style='background:beige;'>
				<div class='col-sm-2'>
					<h4>Latest International News links :</h4>
				</div>
				<div class='col-sm-10'>
					<marquee style=margin-top:10px; onMouseOver='this.stop()' onMouseOut='this.start()' id='nationCountiesLinks'behavior='scroll' direction='left'></marquee>
				</div>
			</div>
			<div class='row'>
				<div id='localContentNationCounties'>
				</div>
			</div>
			
			<div class='row' style='background:beige;'>
				<div class='col-sm-2'>
					<h4>Latest Sports links :</h4>
				</div>
				<div class='col-sm-10'>
					<marquee style=margin-top:10px; onMouseOver='this.stop()' onMouseOut='this.start()' id='nationSportsLinks'behavior='scroll' direction='left'></marquee>
				</div>
			</div>
			<div class='row'>
				<div id='localContentNationSports'>
				</div>
			</div>
			
			<div class='row' style='background:beige;'>
				<div class='col-sm-2'>
					<h4>Latest Technology links :</h4>
				</div>
				<div class='col-sm-10'>
					<marquee style=margin-top:10px; onMouseOver='this.stop()' onMouseOut='this.start()' id='nationOpinionLinks'behavior='scroll' direction='left'></marquee>
				</div>
			</div>
			<div class='row'>
				<div id='localContentNationOpinion'>
				</div>
			</div>
		</div>
	</div>
	
	<footer>
		<div class='container-fluid'>
			<div id='terms' style='background:black;'>
					<h4 style='color:purple;'>We do not own the news and logos presented on this page.The contents of this page are the sole copyright of the individual sites that host them.</h4>
					<center><h5>&copy;2015 Newslinks&trade;Kenya</h5></center>
			</div>
		</div>
	</footer>

</body>
</html>
";

?>