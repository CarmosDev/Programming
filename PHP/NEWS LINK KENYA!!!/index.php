<?php

include "header.php";


echo " 
<!DOCTYPE html>
<html>
<head>
<title></title>
<script type='text/javascript' src='https://www.google.com/jsapi'></script>

<!--Local News-->
<script>
<!--Loads the feed API module 'feeds' version '1'-->
google.load('feeds', '1');

<!--Create a function to execute the API-->
function displayFeedCitizen(){
//Create an object with the url
var feed = new google.feeds.Feed('http://citizentv.co.ke/news/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
feed.includeHistoricalEntries();
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentCitizen');
var htmlCitizen= '';
var htmlAd='<div class=container-fluid><h6>Google Advertisement</h6><div style=height:100px; background:grey; ></div></div>';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlCitizen += '<div class=col-sm-3 style=height:400px;margin-bottom:60px;>' + 
		'<div class=well style=background:white;><div class=container-fluid  style=background:white; >' +
		'<hr><h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'<code>Continue reading</code>'+
		'</a><br/><br/>' +
		'<span class=badge>0</span>'+
		'<hr></div></div>' +
		'</div>';	
			}
			container.innerHTML = '<br/>' + htmlCitizen;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedCitizen);

<!--Create a function to execute the API-->
function displayFeedPeopleDaily(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.mediamaxnetwork.co.ke/news/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
feed.includeHistoricalEntries();
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentPeopleDaily');
var htmlPeopleDaily= '';
var htmlAd='<div class=container-fluid><h6>Google Advertisement</h6><div style=height:100px; background:grey; ></div></div>';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlPeopleDaily += '<div class=col-sm-3 style=height:400px;margin-bottom:60px;>' + 
		'<div class=well style=background:white;><div class=container-fluid  style=background:white; >' +
		'<hr><h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'<code>Continue reading</code>'+
		'</a><br/><br/>' +
		'<span class=badge>0</span>'+
		'<hr></div></div>' +
		'</div>';	
			}
			container.innerHTML = '<br/>' + htmlPeopleDaily;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedPeopleDaily);

<!--Create a function to execute the API-->
function displayFeedTheStar(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.the-star.co.ke/the-star-feed');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
feed.includeHistoricalEntries();
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentTheStar');
var htmlTheStar= '';
var htmlAd='<div class=container-fluid><h6>Google Advertisement</h6><div style=height:100px; background:grey; ></div></div>';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlTheStar += '<div class=col-sm-3 style=height:400px;margin-bottom:60px;>' + 
		'<div class=well style=background:white;><div class=container-fluid  style=background:white; >' +
		'<hr><h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'<code>Continue reading</code>'+
		'</a><br/><br/>' +
		'<span class=badge>0</span>'+
		'<hr></div></div>' +
		'</div>';	
			}
			container.innerHTML = '<br/>' + htmlTheStar;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedTheStar);


<!--Create a function to execute the API-->
function displayFeedStandard(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.standardmedia.co.ke/rss/kenya.php');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
feed.includeHistoricalEntries();
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentStandard');
var htmlStandard= '';
var htmlAd='<div class=container-fluid><h6>Google Advertisement</h6><div style=height:100px; background:grey; ></div></div>';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlStandard += '<div class=col-sm-3 style=height:400px;margin-bottom:60px;>' + 
		'<div class=well style=background:white;><div class=container-fluid  style=background:white; >' +
		'<hr><h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'<code>Continue reading</code>'+
		'</a><br/><br/>' +
		'<span class=badge>0</span>'+
		'<hr></div></div>' +
		'</div>';	
			}
			container.innerHTML = '<br/>' + htmlStandard;
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedStandard);

<!--Create a function to execute the API-->
function displayFeedNation(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.nation.co.ke/-/1148/1148/-/view/asFeed/-/vtvnjq/-/index.xml');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
feed.includeHistoricalEntries();
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentNation');
var htmlNation= '';
var htmlAd='<div class=container-fluid><h6>Google Advertisement</h6><div style=height:100px; background:grey; ></div></div>';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlNation += '<div class=col-sm-3 style=height:400px;margin-bottom:60px;>' + 
		'<div class=well style=background:white;><div class=container-fluid  style=background:white; >' +
		'<hr><h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'<code>Continue reading</code>'+
		'</a><br/><br/>' +
		'<span class=badge>0</span>'+
		'<hr></div></div>' +
		'</div>';	
			}
		container.innerHTML ='<br/>' + htmlNation;	
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedNation);

<!--Create a function to execute the API-->
function displayFeedKBC(){
//Create an object with the url
var feed = new google.feeds.Feed('http://www.kbc.co.ke/feed/');
//results format
feed.setResultFormat(google.feeds.Feed.JSON_FORMAT);
//feed.includeHistoricalEntries();
//number of entries
feed.setNumEntries(8);
//load the feed results
feed.load(function(result){
if(!result.error){
//create a variable for the html element with the id 'localContent'
var container = document.getElementById('localContentKBC');
var htmlKBC= '';
var htmlAd='<div class=container-fluid><h6>Google Advertisement</h6><div style=height:100px; background:grey; ></div></div>';

for(var i=0;i<result.feed.entries.length;i++){
//store feed results in a variable
var entry = result.feed.entries[i];
d = new Date(entry.publishedDate);
//create an element in html document
htmlKBC += '<div class=col-sm-3 style=height:400px;margin-bottom:60px;>' + 
		'<div class=well style=background:white;><div class=container-fluid  style=background:white; >' +
		'<hr><h3>' +
		'<a href=' + 
		entry.link +
		'  target=_blank' +
		'>' +
		entry.title +
		'</a>' +
		'</h3>'+
		'<h6>'+
		d +
		'</h6>' +
		'<h5>'+
		entry.contentSnippet +
		'...'+
		'<a href=' +
		entry.link +
		'  target=_blank' +
		'>' +
		'<code>Continue reading</code>'+
		'</a><br/><br/>' +
		'<span class=badge>0</span>'+
		'<hr></div></div>' +
		'</div>';	
			}
		container.innerHTML = '<br/>' + htmlKBC;	
		}
		
	});
	
}
google.setOnLoadCallback(displayFeedKBC);
</script>
</head>
<body id='wrap' style='background:#F4F6F8;'>
	<div class='col-sm-12' style='background:beige;height:40px;'>
		<div class='container-fluid'>
			<h4>Latest Local News...<img type='.png' src='Images/Kenya.png' style='height:25px; width:50px;'/></h4>
		</div>
	</div>
	
	<div class='dailyNationImage' style='background:#2A2A2A;'>
		<div class='container-fluid'>
			<div class='row'>
				<a href='http://www.nation.co.ke/' target='_blank'><img type='.png' src='Images/DailyNation.png' style='height:50px;' /></a>
			</div>
		</div>
	</div>
	<div id='localContent'>
		<div class='container-fluid'>
			<div class='row'>
				<div id='localContentNation'>
				</div>
				<div class='container-fluid'>
					<h6>Google Advertisement</h6>
					<div style='height:100px; background:grey;'>
					</div>
				</div><br/>
				<div class='row'>
					<div class='col-sm-12' style='background:blue;'>
						<a href='http://www.standardmedia.co.ke/' target='_blank'><img type='.png' src='Images/TheStandard.png' style='height:50px;' /></a>
					</div>
				</div>
				<div id='localContentStandard'>
				</div>
				<div class='container-fluid'>
					<h6>Google Advertisement</h6>
					<div style='height:100px; background:grey;'>
					</div>
				</div><br/>
				<div class='row'>
					<div class='col-sm-12' style='background:green;'>
						<a href='http://www.mediamaxnetwork.co.ke' target='_blank'><img type='.png' src='Images/PeopleDaily.png' style='height:50px;' /></a>
					</div>
				</div>
				<div id='localContentPeopleDaily'>
				</div>
				<div class='container-fluid'>
					<h6>Google Advertisement</h6>
					<div style='height:100px; background:grey;'>
					</div>
				</div><br/>
				<div class='row'>
					<div class='col-sm-12' style='background:grey;'>
						<a href='http://citizentv.co.ke/' target='_blank'><img type='.png' src='Images/Citizen.png' style='height:50px;' /></a>
					</div>
				</div>
				<div id='localContentCitizen'>
				</div>
				<div class='container-fluid'>
					<h6>Google Advertisement</h6>
					<div style='height:100px; background:grey;'>
					</div>
				</div><br/>
				<div class='row'>
					<div class='col-sm-12' style='background:grey;'>
						<a href='http://www.kbc.co.ke/' target='_blank'><img type='.png' src='Images/KBC.png' style='height:50px;' /></a>
					</div>
				</div>
				<div id='localContentKBC'>
				</div>
				<div class='container-fluid'>
					<h6>Google Advertisement</h6>
					<div style='height:100px; background:grey;'>
					</div>
				</div><br/>
				<div class='row'>
					<div class='col-sm-12' style='background:grey;'>
						<a href='http://www.the-star.co.ke/' target='_blank'><img type='.png' src='Images/TheStar.png' style='height:50px;' /></a>
					</div>
				</div>
				<div id='localContentTheStar'>
				</div>
				<div class='container-fluid'>
					<h6>Google Advertisement</h6>
					<div style='height:100px; background:grey;'>
					</div>
				</div><br/>
			</div>
		</div>
	</div>
	<footer>
		<div class='container-fluid'>
			<div id='terms' style='background:black;'>
					<h4 style='color:purple;'>We do not own the news and logos presented on this page.The contents of this page are the sole copyright of the individual sites that host them.</h4>
					<center><h5>&copy;2015 Newslinks&trade;Kenya</h5></center>
			</div>
		</div>
	</footer>
</body>
</html>
";

?>