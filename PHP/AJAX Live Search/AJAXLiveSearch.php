<?php

//create DOM document
$xmlDoc = new DOMDocument();
//load the feed
$xmlDoc->load("http://www.nation.co.ke/-/1148/1148/-/view/asFeed/-/vtvnjq/-/index.xml");
//get links from feed
$x = $xmlDoc->getElementsByTagName("item");
//get the q parameter from url
$q= $_GET['q'];

//lookup all links from the xml file if length of q>0
if(strlen($q)>0){
	$hint="";
	for($i=0; $i<($x->length); $i++){
		$y=$x->item($i)->getElementsByTagName('title');
		$z=$x->item($i)->getElementsByTagName('link');
		
		if($y->item(0)->nodeType==1){
			//find a link matching the searched text
			if(stristr($y->item(0)->childNodes->item(0)->nodeValue , $q )){
				if($hint==""){
					$co = 1;
					$res = "result";
					$hint="<a href='".$z->item(0)->childNodes->item(0)->nodeValue ."' target='_blank'>".$y->item(0)->childNodes->item(0)->nodeValue."</a>";
				}else{
					$co++;
					$res = "results";
					$hint = $hint."<br/>"."<a href='".$z->item(0)->childNodes->item(0)->nodeValue ."' target='_blank'>".$y->item(0)->childNodes->item(0)->nodeValue."</a>";
				}
			}
		}
	}
}

//set output to "no suggestion" if no hint was found
//or to the correct values
if($hint==""){
	$response = "<div class='alert alert-danger'>No news about : <b>$q</b></div>";
}else{
	$response="<div class='alert alert-success'><kbd>$co $res below...</kbd><br/>".$hint."</div>";
}

//output
echo $response;
?>