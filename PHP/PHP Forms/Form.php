<!DOCTYPE html>
<html>
<head>
<title>PHP Form Validation</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</head>
<body>
	<?php
		//define variables
		$nameErr = $websiteErr = $emailErr = $commentsErr = $genderErr = "";
		$name = $website = $email = $comments = $gender = "";
		
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			if(empty($_POST["name"])){
				$nameErr = "Name is required!!!<br/>";
			}else{
				$name = test_input($_POST["name"]);
				if(!preg_match("/^[a-zA-Z ]*$/",$name)){
					$nameErr = "Only a valid name with whitespace and characters is allowed!!<br/>";
				}
			}
			if(empty($_POST["email"])){
				$emailErr = "Email is required!!!<br/>";
			}else{
				$email = test_input($_POST["email"]);
				if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
					$emailErr = "Invalid Email Format!!!<br/>";
				}
			}
			if(empty($_POST["website"])){
				$websiteErr = "Website is required!!!<br/>";
			}else{
				$website = test_input($_POST["website"]);
				if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
					$websiteErr = "Invalid URL<br/>";
				}
			}
			if(empty($_POST["comments"])){
				$commentsErr = "Comments are required!!!<br/>";
			}else{
				$comments = test_input($_POST["comments"]);
			}
			if(empty($_POST["gender"])){
				$genderErr = "Gender is required!!!<br/>";
			}else{
				$gender = test_input($_POST["gender"]);
			}
		}
		function test_input($data){
			$data = trim($data);
			$data = stripslashes($data);
			$data = htmlspecialchars($data);
			return $data;
		}
	?>
	<div class="container">
	<div class="page-header">
		<h3>PHP Form Validation by: </h3>
		<span class="glyphicon glyphicon-globe"></span>
		<code>CARMOS &trade; 2015</code>
	</div>
	</div>
	<div class="container" style="background:crimson;border-radius:10px;">
		<div class="alert alert-danger fade in">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<br/>
			<?php echo "$nameErr";?>
			<?php echo "$emailErr";?>
			<?php echo "$websiteErr";?>
			<?php echo "$genderErr";?>
			<?php echo "$commentsErr";?>
		</div>
		<form class="form-vertical" role="form" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">
			<div class="form-group">
				<h4>User Details Form:  *required</h4>
			</div>
			<div class="form-group has-feedback">
				<input class="form-control" placeholder="Enter your *name..." name="name" type="text" value="<?php echo $name; ?>" >
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input class="form-control" placeholder="Enter your *EMail..." name="email" type="text" value="<?php echo $email;?>" >
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input class="form-control" placeholder="Enter your *Website..." name="website" type="text" value="<?php echo $website;?>">
				<span class="glyphicon glyphicon-globe form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<textarea name="comments" class="form-control" placeholder="Enter your *comments..." ><?php echo $comments;?></textarea>
				<span class="glyphicon glyphicon-pencil form-control-feedback"></span>
			</div>
			<div class="form-group">
				<label for="gdr">*Gender: </label>
				<input id="gdr"type="radio" name="gender" <?php if(isset($gender) && $gender=="Male"){echo "checked";}?> value="Male" style="margin-left:10px;" >Male
				<input id="gdr"type="radio" name="gender" <?php if(isset($gender) && $gender=="Female"){echo "checked";}?> value="Female" style="margin-left:10px;" >Female
			</div>
			<div class="form-group">
				<button class="btn btn-success" type="submit">Submit this form</button>
			</div>
		</form>
		<hr/>
	</div>
	<div class="container">
		<h4><b>Your Input:</b></h4>
		<?php
			echo "Name=========>$name<br/>";
			echo "Email=========>$email<br/>";
			echo "Website=========>$website<br/>";
			echo "Gender=========>$gender<br/>";
			echo "Comments=========>$comments<br/>";
		?>
	</div>
</body>
</body>
</html>