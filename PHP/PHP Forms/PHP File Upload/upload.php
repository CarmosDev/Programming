<?php
//target folder
$target_dir='uploads/Pictures/';

$target_file = $target_dir. basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;

$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

//check if image is real
if(isset($_POST["submit"])){
	$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
	if($check !== false){
		echo "File is an image - ".$check["mime"].".<br/>";
		$uploadOk = 1;
	}else{
		echo "File is not an image.<br/>";
		$uploadOk = 0;
	}
}
//existence
if(file_exists($target_file)){
	echo "Sorry, file already exists.<br/>";
	$uploadOk = 0;
}
//check file size
if($_FILES["fileToUpload"]["size"] > 500000){
	echo "Sorry, your file is too large<br/>";
	$uploadOk = 0;
}
//format allowed
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
	echo "<b>Go Back to homepage: </b><a href='index.html'>Upload Photos</a><br/>";
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.<br/>";
	echo "<b>Go Back to homepage: </b><a href='index.html'>Upload Photos</a><br/>";
// if everything is ok, try to upload file
} else{
	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "<div class='alert alert-success'><a href='index.html' class='close' data-dismiss='alert' aria-label='close'>&times;</a>The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.</div>";
		include('index.html');
    } else {        
		echo "Sorry, there was an error uploading your file.<br/>";
		echo "<b>Go Back to homepage: </b><a href='index.html'>Upload Photos</a>";
    }
}
?>
