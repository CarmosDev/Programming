function onUpload(){
	var fileInput = document.getElementById('fileToUpload');
	var fileSubmit = document.getElementById('uploadButton');
	var filesToBeUploaded = document.getElementById('filesToBeUploaded');
	
	if('files' in fileInput){
		if(fileInput.files.length == 0){
			fileSubmit.disabled = true;
			fileSubmit.value = 'First Select Image / Images to Upload';
		}else if(fileInput.files.length == 1){
			fileSubmit.disabled = false;
			fileSubmit.value = 'Upload The Image';
			filesToBeUploaded.innerHTML = '<strong>'+fileInput.files.length+'</strong>  image to be uploaded<br/>';
			filesToBeUploaded.innerHTML += '<u>'+fileInput.files.length+'</u>.   Name:  <b>'+fileInput.files[0].name+'</b>   .....Size:   <b>'+Math.round((fileInput.files[0].size/1024))+'</b>   Kilobytes';
		}else if(fileInput.files.length >1){
			fileSubmit.disabled = false;
			fileSubmit.value = 'Upload : '+ fileInput.files.length +' Images';
			filesToBeUploaded.innerHTML = '<strong>'+fileInput.files.length+'</strong>  images to be uploaded<br/>';
			for(i=0;i<fileInput.files.length; i++){
				filesToBeUploaded.innerHTML += '<u>'+(i+1)+'.</u>     Name:  <b>'+fileInput.files[i].name+'</b>   .....Size:   <b>'+Math.round((fileInput.files[i].size/1024))+'</b>   Kilobytes<br/>';
			}
		}
	
	}
}