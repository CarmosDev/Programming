<?php

//start session
session_start();

#check if session exists and destroy it
if(isset($_SESSION['mtu']))
{

//kill the session
session_destroy();

//redirect the user to the login form
header('location:index.php');

}

?>