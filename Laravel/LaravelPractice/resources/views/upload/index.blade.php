<!DOCTYPE html>
<html>
<head>
    <title>Test Upload</title>
</head>
<body>
    <center>
        <form method="POST" action="/upload" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="file" name="my_file">
            <button type="submit">Upload File</button>
        </form>
    </center>
</body>
</html>