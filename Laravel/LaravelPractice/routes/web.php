<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    request()->session()->put();
    return request()->session()->all();
    return view('welcome');
});

Route::group(['prefix'=>'users'],function (){

    Route::get('{id}',function ($id){

        return $id;

    });

});

Route::post('/upload','UploadController@store');

Route::get('/upload','UploadController@index');

Route::get('/response',function(){

    return response('Hello World',200)->cookie('test_cookie','I am a cookie',1);

});
