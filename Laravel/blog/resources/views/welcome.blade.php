<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Home</title>
    </head>
    <body>
        <ul>
            @foreach($tasks as $task)
                <li><a href="/tasks/{{$task->id}}" target="_blank">{{$task->body}}</a></li>
            @endforeach
        </ul>
    </body>
</html>
