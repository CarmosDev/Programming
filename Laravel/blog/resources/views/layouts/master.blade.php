
<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
</head>

<body>

    @include('layouts.nav')

@if(session('message'))
    <div class="alert alert-success text-center" role="alert">
        {{session('message')}}
    </div>
@endif
<div class="blog-header">
    <div class="container">
        <h1 class="blog-title">The Bootstrap Blog</h1>
        <p class="lead blog-description">An example blog template built with Bootstrap.</p>
    </div>
</div>

<div class="container">

    <div class="row">

        <div class="col-sm-8 blog-main">
            @yield('content')
        </div>

        @include('layouts.sidebar')

    </div>

</div>

    @include('layouts.footer')

</body>
</html>
