<div class="blog-masthead">
    <div class="container">
        <nav class="nav blog-nav">
            <a class="nav-link active" href="/">Home</a>
            <a class="nav-link" href="#">New features</a>
            <a class="nav-link" href="#">Press</a>
            <a class="nav-link" href="#">New hires</a>
            <a class="nav-link" href="#">About</a>
            <a class="nav-link" href="/post/create">Create a new post</a>
            <a class="nav-link" href="/register">Register</a>

            @if(Auth::check())
                <a class="nav-link ml-auto" href="/logout">{{Auth::user()->name}}:Logout</a>
            @else
                <a class="nav-link ml-auto" href="/login">Login</a>
            @endif
        </nav>
    </div>
</div>