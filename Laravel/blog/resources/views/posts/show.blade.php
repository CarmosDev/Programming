@extends('layouts.master')

@section('content')
    <h1>{{$post->title}}</h1>
    <p>{!! $post->body !!}</p>
    <hr>
    <div>
        <h3>Comments : </h3>
        <ul class="list-group">
                @foreach($post->comments as $comment)
                <li class="list-group-item">
                    <strong>
                        {!! $comment->created_at->diffForHumans() !!}:&nbsp;
                    </strong>
                    {!! $comment->body !!}
                </li>
                @endforeach
        </ul>
    </div>
    <hr>
    <div class="card">
        <div class="card-block">
            @include('layouts.form_errors')
            <form method="POST" action="/posts/{{$post->id}}/comments">
                {{csrf_field()}}
                <div class="form-group">
                    <textarea name="body" placeholder="Your Comment Here" class="form-control"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Add your comment</button>
                </div>
            </form>
        </div>
    </div>
@endsection