<div class="blog-post">

    <h2 class="blog-post-title"><a href="/posts/{{$post->id}}" target="_blank">{{$post->title}}</a></h2>

    <p class="blog-post-meta">
        <b>{{$post->created_at->toFormattedDateString()}}</b> at {{$post->created_at->toTimeString()}} by
        <a href="#">{{$post->user->name}}</a></p>

    {!! $post->body !!}

    <hr>

    <div>
        <span class="badge badge-default">Tags : </span>
        @foreach($post->tags as $tag)
            <span class="badge badge-info"><a href="/posts/tags/{{$tag->name}}" style="color: white;">{{$tag->name}}</a></span>
        @endforeach
    </div>

    <hr>

</div><!-- /.blog-post -->