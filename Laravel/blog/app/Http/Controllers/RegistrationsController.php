<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;

class RegistrationsController extends Controller
{

    public function create(){

        return view('registrations.create');

    }

    public function store(RegistrationRequest $request){

        $request->persist();

        session()->flash('message','Thanks very much for signing up');

        return redirect()->home();

    }

}
