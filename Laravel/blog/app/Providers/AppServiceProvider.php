<?php

namespace App\Providers;

use App\Post;
use App\Tag;
use Illuminate\Support\ServiceProvider;
use App\Billing\Stripe;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.sidebar',function($view){
            $view->with('archives',Post::archives());
            $view->with('tags',Tag::has('posts')->pluck('name'));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(){

        $this->app->singleton(Stripe::class,function (){
            return new Stripe(config('services.stripe.key'));
        });
    }
}
