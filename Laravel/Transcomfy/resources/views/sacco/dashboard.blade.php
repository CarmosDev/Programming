@extends('layout.master')

@section('content')
    <div class="ui blue padded segment">
        <div class="ui blue center aligned huge dividing header">Sacco administration dashboard.</div>
        <div class="ui divided grid">
            <div class="four wide column">
                <div class="ui vertical fluid tabular menu">
                    <div class="active item" data-tab="sacco_buses">
                        <a href=""><i class="bus icon"></i>&nbsp;Sacco buses</a>
                    </div>
                    <div class="item" data-tab="sacco_drivers">
                        <a href=""><i class="user icon"></i>&nbsp;Sacco drivers</a>
                    </div>
                </div>
            </div>
            <div class="twelve wide column">
                <div class="ui active tab basic segment" data-tab="sacco_buses">
                    <div class="ui secondary pointing menu">
                        <a class="active item" data-tab="sacco_buses_add">
                            <i class="plus icon"></i>&nbsp;Add bus.
                        </a>
                        <a class="item" data-tab="sacco_buses_list">
                            <i class="list icon"></i>&nbsp;List of sacco buses.
                        </a>
                    </div>
                    <div class="ui basic active tab segment" data-tab="sacco_buses_add">
                        <form class="ui form" method="POST" action="" enctype="multipart/form-data">
                            <div class="required field">
                                <label for="sacco_bus_plate">Bus number plate</label>
                                <input type="text" id="sacco_bus_plate" name="sacco_bus_plate" placeholder="Enter the bus number plate here...">
                            </div>
                            <div class="required field">
                                <label for="sacco_bus_capacity">Maximum passenger capacity</label>
                                <input type="text" id="sacco_bus_capacity" name="sacco_bus_capacity" placeholder="Enter the maximum number of passengers here...">
                            </div>
                            <div class="field">
                                <button type="submit" class="ui green button">Add bus.</button>
                            </div>
                        </form>
                    </div>
                    <div class="ui basic tab segment" data-tab="sacco_buses_list">
                        <table class="ui single line celled table">
                            <thead>
                            <tr>
                                <th>Bus number plate</th>
                                <th>Maximum number of passengers</th>
                                <th>Operations</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="ui tab basic segment" data-tab="sacco_drivers">
                    <div class="ui secondary pointing menu">
                        <a class="active item" data-tab="sacco_drivers_add">
                            <i class="plus icon"></i>&nbsp;Add driver.
                        </a>
                        <a class="item" data-tab="sacco_drivers_list">
                            <i class="list icon"></i>&nbsp;List of drivers.
                        </a>
                    </div>
                    <div class="ui active tab basic segment" data-tab="sacco_drivers_add">
                        <form method="POST" enctype="multipart/form-data" action="" class="ui form">
                                <div class="required field">
                                    <label for="sacco_driver_first_name">First Name</label>
                                    <input type="text" id="sacco_driver_first_name" name="sacco_driver_first_name" placeholder="Enter driver's first name here...">
                                </div>
                                <div class="required field">
                                    <label for="sacco_driver_last_name">Last Name</label>
                                    <input type="text" id="sacco_driver_last_name" name="sacco_driver_last_name" placeholder="Enter driver's last name here...">
                                </div>
                                <div class="required field">
                                    <label for="sacco_driver_phone_number">Phone number</label>
                                    <input type="text" id="sacco_driver_phone_number" name="sacco_driver_phone_number" placeholder="Enter driver's phone number here...">
                                </div>
                                <div class="required field">
                                    <label for="sacco_driver_license">Driver License ID</label>
                                    <input type="text" id="sacco_driver_license" name="sacco_driver_license" placeholder="Enter driver's license ID here...">
                                </div>
                                <div class="field">
                                    <button class="ui green button" type="submit">Add driver.</button>
                                </div>
                            </form>
                    </div>
                    <div class="ui tab basic segment" data-tab="sacco_drivers_list">
                        <table class="ui single line celled table">
                                <thead>
                                <tr>
                                    <th>Driver ID</th>
                                    <th>Driver name</th>
                                    <th>Driver phone number</th>
                                    <th>Driver license ID</th>
                                    <th>Assigned bus</th>
                                    <th>Operations</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection