<!DOCTYPE html>
<html>
    @include('layout.head')
    <body>
        @include('layout.navigation')
        <div class="ui fluid bottom attached basic segment" style="min-height: 100%;background: lightgrey;">
            @yield('content')
        </div>
    @include('layout.footer')
    </body>
</html>