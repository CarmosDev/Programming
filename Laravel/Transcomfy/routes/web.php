<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/',function(){
   return redirect('/sacco/login');
});

Route::get('/sacco/login', function () {
    return view('sacco.welcome',[
        'title'=>'Transcomfy Admin Portal.'
    ]);
})->name('login');

Route::get('/sacco/register',function(){
    return view('sacco.register',[
        'title'=>'Transcomfy Sacco Registration.'
    ]);
});

Route::get('/sacco/dashboard',function(){
    return view('sacco.dashboard',[
        'title'=>'Transcomfy Sacco Dashboard.'
    ]);
});