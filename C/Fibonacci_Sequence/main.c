#include <stdio.h>
#include <stdlib.h>

void fibonacci_sequence(int number);
int main()
{
    int number;
    printf("Enter the number of Fibonacci numbers that you want:\n");
    scanf("%d",&number);
    printf("The following are the first %d Fibonacci numbers...\n",number);
    fibonacci_sequence(number);
    return 0;
}
void fibonacci_sequence(int number){
    int a,b,c,i;
    a=0;
    b=1;
    c=1;
    for(i=1;i<=number;i++){
        printf("%5d",a);
        a=b;
        b=c;
        c=a+b;
    }
}
