#include <stdio.h>
#include <stdlib.h>

int main()
{
    int integer1,integer2,integer3,sum,average,product,smallest,largest;
    printf("Input Three Different Integers and let me do some Arithmetic on them!!!\n");
    scanf("%d%d%d",&integer1,&integer2,&integer3);
    sum=integer1+integer2+integer3;
    average=sum/3;
    product=integer1*integer2*integer3;
    if(integer1>integer2){
        largest=integer1;
        smallest=integer2;
    }
    if(integer1<integer2){
        largest=integer2;
        smallest=integer1;
    }
    if(largest<integer3){
        largest=integer3;
    }
    if(smallest>integer3){
        smallest=integer3;
    }
    //Print the Results
    printf("\nSum is %d\nAverage is %d\nProduct is %d\nSmallest is %d\nLargest is %d",sum,average,product,smallest,largest);
    return 0;
}
