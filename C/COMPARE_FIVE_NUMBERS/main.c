#include <stdio.h>
#include <stdlib.h>
/*
    PROGRAM NAME : COMPARE_FIVE_INTEGERS
    PROGRAM DESCRIPTION : Compares five digits/integers
    LAST COMPILED: Saturday 17/10/2015
    LAST WRITTEN: Saturday 17/10/2015
    SPECIAL FEATURES : Selection Construct IF...ELSE
    NAME : Carlton Maranga Moseti
    REGISTRATION_NUMBER : P15/36821/2016
    COURSE :Introduction To Programming
*/
int main()
{
    int integer1,integer2,integer3,integer4,integer5,largest,smallest;
    printf("I shall compare Five Integers and tell you the largest and the smallest of them all\n");
    printf("Start Here...\nEnter any five numbers...(PRESS ENTER TO INPUT THE NEXT NUMBER)\n");
    scanf("%d%d%d%d%d",&integer1,&integer2,&integer3,&integer4,&integer5);

    printf("You have input these values : [%d , %d , %d , %d , %d]\n",integer1,integer2,integer3,integer4,integer5);
    if(integer1>integer2){
        largest=integer1;
        smallest=integer2;
        //Largest Of All
        if(largest<integer3){
            largest=integer3;
        }
        if(largest<integer4){
            largest=integer4;
        }
        if(largest<integer5){
            largest=integer5;
        }
        //Smallest Of All
        if(smallest>integer3){
            smallest=integer3;
        }
        if(smallest>integer4){
            smallest=integer4;
        }
        if(smallest>integer5){
            smallest=integer5;
        }
    }else{
        largest=integer2;
        smallest=integer1;
        //Largest Of All
        if(largest<integer3){
            largest=integer3;
        }
        if(largest<integer4){
            largest=integer4;
        }
        if(largest<integer5){
            largest=integer5;
        }
        //Smallest Of All
        if(smallest>integer3){
            smallest=integer3;
        }
        if(smallest>integer4){
            smallest=integer4;
        }
        if(smallest>integer5){
            smallest=integer5;
        }
    }
    if(largest==smallest){
        printf("All Numbers \"%d\"  are equal!!!",largest);
    }else{
        printf("%d is the largest and %d is the smallest",largest,smallest);
    }
    return 0;
}
