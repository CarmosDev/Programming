#include <stdio.h>
#include <stdlib.h>
/*
    PROGRAM NAME : HELLO_WORLD
    PROGRAM DESCRIPTION : Prints Hello World Statement
    LAST COMPILED: Wednesday 21/10/2015
    LAST WRITTEN: Wednesday 21/10/2015
    SPECIAL FEATURES : Use of printf()
    NAME : Carlton Maranga Moseti
    REGISTRATION_NUMBER : P15/36821/2016
    COURSE :Introduction To Programming
*/
int main()
{
    printf("Hello world!\n");
    return 0;
}
