#include <stdio.h>
#include <stdlib.h>

int main()
{
    int y=5;
    int *yPtr;

    yPtr=&y;

    printf("Address of y is: %p\nValue of yPtr is:%p\n",&y,yPtr);
    printf("Value of y is: %d\nValue of *yPtr is:%d\n",y,*yPtr);

    printf("&*yPtr : %p\n*&yPtr : %p",&*yPtr,*&yPtr);
    return 0;
}
