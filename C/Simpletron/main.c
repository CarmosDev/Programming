#include <stdio.h>
#include <stdlib.h>
/*
    PROGRAM NAME:SIMPLETRON-Simulator
    PROGRAM DESCRIPTION: Runs and debugs programs written in Simpletron Machine Language
    DATE LAST COMPILED: 10/12/2015
    DATE LAST WRITTEN: 10/11/2015
    AUTHOR NAME: MOSETI CARLTON MARANGA
    REGISTRATION NUMBER:P15/36821/2016
    COURSE:INTRODUCTION TO PROGRAMMING
*/
/*
        Instruction     Operation Code
        ===========     ==============
*/
#define READ             10
#define WRITE            11
#define LOAD             20
#define STORE            21
#define ADD              30
#define SUBTRACT         31
#define DIVIDE           32
#define MULTIPLY         33
#define BRANCH           40
#define BRANCHNEG        41
#define BRANCHZERO       42
#define HALT             43

/*
    MEMORY AND PROGRAM CONSTANTS
    ============================
*/
#define MEMORY_SIZE      100
#define SENTINEL         -99999
#define MEMORY_MIN_VALUE -9999
#define MEMORY_MAX_VALUE 9999

enum Status{FATAL_ERROR}; //Used when program encounters fatal errors

int main()
{
    int memory[MEMORY_SIZE]={0};    //Array to store memory instructions
    int location=0;                 //Variable for memory location
    int instruction=0;              //Variable to get user instruction one at a time
    int instructionCounter=0;       //Counter Variable for instructions
    int operationCode=0;            //Variable to store instruction code
    int operand=0;                  //Variable to store memory location to be executed by an instruction
    int accumulator=0;              //Variable to store Arithmetic results
    int instructionRegister=0;      //Instruction Register Variable
    int a,b;                        //Counter Variables
    enum Status programState;       //Variable to store program error state

    //User Prompt
    printf("*** Welcome to Simpletron!***\n");
    printf("*** Please enter your program one instruction ***\n");
    printf("*** (or data word) at a time. I will type the ***\n");
    printf("*** location number and a question mark (?).  ***\n");
    printf("*** You then type the word for that location. ***\n");
    printf("*** Type the sentinel -99999 to stop entering ***\n");
    printf("*** your program***\n");

    while(location<MEMORY_SIZE){    //Maximum number of instructions to be stored is equal to memory size
        do{ //Loop in-case user puts instruction values that are out of memory range
            printf("%.2d ?   ",location); //Prompt for instructions
            scanf("%d",&instruction); //Get user instruction
            if(instruction==SENTINEL){
                break; //Stop getting instructions
            }
        }while(instruction<MEMORY_MIN_VALUE || instruction>MEMORY_MAX_VALUE);
        if(instruction==SENTINEL){
            printf("*** Program loading completed ***\n");
            printf("*** Program execution begins  ***\n\n");
            break;
        }else{
            memory[location]=instruction;   //Store Instructions
        }
        location++; //Increment to next memory location
    }

    //EXECUTION
    while(instructionCounter<location){
        instructionRegister=memory[instructionCounter]; //Store memory instruction into instruction register
        operand=instructionRegister%100; //Get the operand(last two digits)
        operationCode=instructionRegister/100; //Get the operation code(first two digits)
        switch(operationCode){ //Execute operation codes matching the different cases
        case READ:
            do{ //Read values within memory range
                printf("?  ");
                scanf("%d",&memory[operand]);
            }while(memory[operand]<MEMORY_MIN_VALUE||memory[operand]>MEMORY_MAX_VALUE);
            instructionCounter++; //Next instruction
            break;
        case WRITE: //Print memory values
            printf("\n%d\n",memory[operand]);
            instructionCounter++; //Next instruction
            break;
        case LOAD: //Load value into the accumulator
            if(memory[operand]<MEMORY_MIN_VALUE || memory[operand]>MEMORY_MAX_VALUE){
                //Value to be loaded is out of memory range
                printf("*** Accumulator Overflow ***\n");
                printf("*** Simpletron execution abnormally terminated ***\n");
                programState=FATAL_ERROR;
            }else{
               accumulator=memory[operand];
            }
            instructionCounter++; //Next instruction
            break;
        case STORE: //Transfer value from accumulator to memory
            if(accumulator<MEMORY_MIN_VALUE || accumulator>MEMORY_MAX_VALUE){
                //Value to be stored is out of memory range
                printf("*** Memory Overflow ***\n");
                printf("*** Simpletron execution abnormally terminated ***\n");
                programState=FATAL_ERROR;
            }else{
                memory[operand]=accumulator;
            }
            instructionCounter++; //Next instruction
            break;
        case ADD:
            //Sum values and store result in accumulator
            accumulator +=memory[operand];
            if(accumulator<MEMORY_MIN_VALUE || accumulator>MEMORY_MAX_VALUE){
                //Sum of values is out of memory range
                printf("*** Accumulator Overflow ***\n");
                printf("*** Simpletron execution abnormally terminated ***\n");
                programState=FATAL_ERROR;
            }
            instructionCounter++; //Next instruction
            break;
        case SUBTRACT:
            //Subtract values and store result in accumulator
            accumulator -=memory[operand];
            if(accumulator<MEMORY_MIN_VALUE || accumulator>MEMORY_MAX_VALUE){
                //Difference of values is out of memory range
                printf("*** Accumulator Overflow ***\n");
                printf("*** Simpletron execution abnormally terminated ***\n");
                programState=FATAL_ERROR;
            }
            instructionCounter++; //Next instruction
            break;
        case DIVIDE:
            if(memory[operand]==0){ //divisor is zero
                printf("*** Attempt to divide by zero ***\n");
                printf("*** Simpletron execution abnormally terminated ***\n");
                programState=FATAL_ERROR;
            }else{
                accumulator /=memory[operand];
                if(accumulator<MEMORY_MIN_VALUE || accumulator>MEMORY_MAX_VALUE){
                    printf("*** Accumulator Overflow ***\n");
                    printf("*** Simpletron execution abnormally terminated ***\n");
                    programState=FATAL_ERROR;
                }
            }
            instructionCounter++; //Next instruction
            break;
        case MULTIPLY:
            accumulator *=memory[operand]; //Multiply values and store result in accumulator
            if(accumulator<MEMORY_MIN_VALUE || accumulator>MEMORY_MAX_VALUE){
                //Multiplication result is out of memory range
                printf("*** Accumulator Overflow ***\n");
                printf("*** Simpletron execution abnormally terminated ***\n");
                programState=FATAL_ERROR;
            }
            instructionCounter++; //Next instruction
            break;
        case BRANCH:
            //Branch to a specific memory location
            instructionCounter=operand;
            break;
        case BRANCHNEG:
            //Branch to a specific memory location if value in accumulator is less than zero
            if(accumulator<0){
                instructionCounter=operand;
            }else{
                instructionCounter++; //Next Instruction
            }
            break;
        case BRANCHZERO:
            //Branch to a specific memory location if value in accumulator is zero
            if(accumulator==0){
                instructionCounter=operand;
            }else{
                instructionCounter++; //Next Instruction
            }
            break;
        case HALT:
            //Program to be terminated here
            printf("*** Simpletron execution terminated ***\n");
            break;
        default:
            //Operation codes are invalid
            printf("*** Attempt to execute invalid operation code *** \n");
            printf("*** Simpletron execution abnormally terminated ***\n");
            programState=FATAL_ERROR;
            instructionCounter++; //Next Instruction
            break;
        }
        if(operationCode==HALT||programState==FATAL_ERROR){
            break; //Stop program execution in-case of HALT or FATAL_ERROR
        }
    }

    //OUTPUT
    printf("\n\nREGISTERS:\n");
    printf("accumulator        %11s%.4d\n",accumulator>=0?"+":"",accumulator);
    printf("instructionCounter %15.2d\n",instructionCounter);
    printf("instructionRegister%11s%.4d\n",
           instructionRegister>=0?"+":"-",          //Print signed register values
           instructionRegister>=0?instructionRegister:-instructionRegister);
    printf("operationCode      %15.2d\n",operationCode);
    printf("operand            %15.2d\n",operand);

    printf("\nMEMORY:\n");
    printf("%2s","");
    for(a=0;a<10;a++){
        printf("%7d",a);
    }
    printf("\n");
    for(a=0;a<100;){
        printf("%.2d",a);
        for(b=0;b<10;b++){
            printf("%2s%s%.4d",
                    "",
                    memory[a+b]>=0?"+":"", //Print signed memory values
                    memory[a+b]);
        }
        printf("\n");
        a+=10;
    }
    return 0;
}
