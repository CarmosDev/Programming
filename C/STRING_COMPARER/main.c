#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUMBER_OF_CHARACTERS 100

/*
    PROGRAM NAME:String Comparer
    PROGRAM DESCRIPTION:Compares two set of strings
    DATE LAST COMPILED: 30/11/2015
    DATE LAST WRITTEN: 28/11/2015
    AUTHOR NAME: MOSETI CARLTON MARANGA
    REGISTRATION NUMBER:P15/36821/2016
    COURSE:INTRODUCTION TO PROGRAMMING
*/

int main()
{
    char a[NUMBER_OF_CHARACTERS],b[NUMBER_OF_CHARACTERS]; //Two arrays each to hold a set of strings
    int compare; //To store result of comparison

    //Prompt for input and get the input
    printf("You shall input two strings and I shall compare them for you : \n");
    printf("Input the first string: \n");
    gets(a);
    printf("Input the second string: \n");
    gets(b);

    compare=strcmp(a,b); //Compare the strings

    //Output based on comparison result
    if(compare>0){
        printf("\n\nThe first string : %s is greater than the second string : %s\n",a,b);
    }else if(compare==0){
        printf("\n\nThe first string : %s is equal to the second string : %s\n",a,b);
    }else{
        printf("\n\nThe first string : %s is less than the second string : %s\n",a,b);
    }
    return 0;
}
