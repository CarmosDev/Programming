#include <stdio.h>
#include <stdlib.h>
//This Function Draws A Square
int draw_square(int side_length){
    if(side_length>=3){
        int a,b,c,d,e;
        d=side_length-1;
        e=side_length-2;
        for(a=0;a<side_length;a++){
            //TOP AND BOTTOM SIDES
            if(a==0 || a==d){
                for(b=0;b<side_length;b++){
                    printf("+");
                }
                printf("\n");
            }
            if(a>0 && a<d){
                printf("+");
                for(c=0;c<e;c++){
                    printf(" ");
                }
                printf("+");
                printf("\n");
            }
        }
    }else if(side_length==2){
        int a,b;
        for(a=0;a<side_length;a++){
            if(a==0 || a<side_length){
                for(b=0;b<side_length;b++){
                    printf("+");
                }
                printf("\n");
            }
        }
    }else{
        printf("+");
    }
    return 0;
}
int main()
{
    draw_square(50);
    return 0;
}
