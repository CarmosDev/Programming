#include <stdio.h>
#include <stdlib.h>
#define SIZE 100

int linear_search(const int array[],int key,int size);

int main()
{
    int numbers[SIZE];
    int element,search_key,a;

    for(a=0;a<SIZE;a++){
        numbers[a]=a*2;
    }
     printf("Enter value to be searched : \n");
     scanf("%d",&search_key);

     element=linear_search(numbers,search_key,SIZE);
     if(element!=-1){
        printf("Value %d has been found at position %d",search_key,element);
     }else{
        printf("Value %d has not been found",search_key);
     }
    return 0;
}

int linear_search(const int array[],int key,int size){
    int a;
    for(a=0;a<size;a++){
        if(key==array[a]){
            return a;
        }
    }
    return -1;
}
