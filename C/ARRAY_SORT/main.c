#include <stdio.h>
#include <stdlib.h>
#define SIZE 10

//BUBBLE SORT
int main()
{
    int a[SIZE]={1,2,56,4,7,5,3,8,7,9};
    int position,checker,interim;
    printf("Elements before sort : \n");
    for(position=0;position<SIZE;position++){
        printf("%5d",a[position]);
    }
    for(checker=1;checker<=SIZE;checker++){
        for(position=0;position<SIZE-1;position++){
            if(a[position]>a[position+1]){
                interim=a[position];
                a[position]=a[position+1];
                a[position+1]=interim;
            }
        }
    }
    printf("\nElements after sort : \n");
    for(position=0;position<SIZE;position++){
        printf("%5d",a[position]);
    }
    return 0;
}
