#include <stdio.h>
#include <stdlib.h>

#define SIZE 5

int determinant(int MATRIX[SIZE][SIZE],int n_value);
int submatrix(int O_MATRIX[SIZE][SIZE],int isolation_row,int isolation_column,int n_value,int n);

int main()
{
    int n,i,j;

    printf("Enter n value for the n x n matrix : \n");
    scanf("%d",&n);

    int matrix[n][n];
    printf("Matrix size = %d \n",n*n);

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            printf("Enter value for row %d column %d :\n",i+1,j+1);
            scanf("%d",&matrix[i][j]);
        }
    }

    printf("\n\nThe matrix is : \n");
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            printf("%-4d",matrix[i][j]);
        }
        printf("\n");
    }
    determinant(matrix,n);
    return 0;
}

int determinant(int MATRIX[SIZE][SIZE],int n_value){
    if(n_value==1){
        return MATRIX[0][0];
    }else{
        int row[n_value];
        int i=0;
        int j;

        for(j=0;j<n_value;j++){
            row[j]=MATRIX[i][j];
        }

        //for(j=0;j<n_value;j++){
            submatrix(MATRIX,0,0,n_value,n_value-1);
        //}
        return 1;
    }
}

int submatrix(int O_MATRIX[SIZE][SIZE],int isolation_row,int isolation_column,int n_value,int n){
    int i,j,input_i,input_j;
    int matrix[n][n];

    for(i=0,input_i=0;i<n_value;i++){
        if(i==isolation_row){
            continue;
        }else{
            for(j=0,input_j=0;j<n_value;j++){
                if(j==isolation_column){
                    continue;
                }else{
                    matrix[input_i][input_j]=O_MATRIX[i][j];
                    input_j++;
                }
            }
            input_i++;
        }
    }

    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            printf("%-4d",matrix[i][j]);
        }
        printf("\n");
    }
    return 0;
}
