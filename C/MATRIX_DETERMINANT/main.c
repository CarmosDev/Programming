#include <stdio.h>
#include <stdlib.h>

int main()
{
    int size_of_matrix;
    int row,column;
    printf("Enter the n size of the square matrix n x n...\n?n=");
    scanf("%d",&size_of_matrix);

    //Create and fill matrix
    int matrix[size_of_matrix][size_of_matrix];
    for(row=0;row<size_of_matrix;row++){
        for(column=0;column<size_of_matrix;column++){
            printf("Enter value for matrix[%d][%d]:\n?",row,column);
            scanf("%d",&matrix[row][column]);
        }
    }

    //Print matrix
    for(row=0;row<size_of_matrix;row++){
        printf("| ");
        for(column=0;column<size_of_matrix;column++){
            printf("%-4d",matrix[row][column]);
        }
        printf("|");
        printf("\n");
    }
    return 0;
}
