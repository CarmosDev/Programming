#include <stdio.h>
#include <stdlib.h>
#define CHARACTER_LIMIT 25
#define NO_OF_MARKS 3

int main()
{
    int a,i,x,student_number,mark[NO_OF_MARKS];
    int sum=0;
    double average;
    char name[CHARACTER_LIMIT];
    char remark[CHARACTER_LIMIT];
    FILE *studentsMarksPtr;

    printf("Welcome to the Student Marks program\n");
    printf("------------------------------------\n");
    printf("How many students do you want to record their marks.\n");
    scanf("%d",&x);
    printf("------------------------------------\n");
    if((studentsMarksPtr=fopen("student_marks.dat","a"))==NULL){
        printf("File to store data cannot be opened!\n");
    }else{
        for(i=1;i<=x;i++){
            printf("Student %d of %d :\n",i,x);
            printf("Student Number ? ");
            scanf("%d",&student_number);
            printf("Student Name ? ");
            scanf("%s",name);
            for(a=0;a<NO_OF_MARKS;a++){
                printf("Mark %d ? ",(a+1));
                scanf("%d",&mark[a]);
                while(mark[a]<0 || mark[a]>100){
                    printf("Mark out of range! Input again ? ");
                    scanf("%d",&mark[a]);
                }
                sum+=mark[a];
            }

            average=(double)sum/NO_OF_MARKS;
            fprintf(studentsMarksPtr,"%d %s %d %d %d %lf %s\n",student_number,name,mark[0],mark[1],mark[2],average,remark);
        }
    }
    return 0;
}
