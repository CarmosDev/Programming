#include <stdio.h>
#include <stdlib.h>

int main()
{
    int five_digit_int,tens_of_thousands,thousands,hundreds,tens,ones;
    printf("Enter a five digit number and ill separate its digits...\t");
    scanf("%d",&five_digit_int);
    tens_of_thousands=(five_digit_int%100000-five_digit_int%10000)/10000;
    thousands=(five_digit_int%10000-five_digit_int%1000)/1000;
    hundreds=(five_digit_int%1000-five_digit_int%100)/100;
    tens=(five_digit_int%100-five_digit_int%10)/10;
    ones=(five_digit_int%10-five_digit_int%1)/1;

    printf("%d   %d   %d   %d   %d",tens_of_thousands,thousands,hundreds,tens,ones);
    return 0;
}
