#include <stdio.h>
#include <stdlib.h>

int main()
{
    int integer1, integer2, integer3, integer4, integer5, sum;
    printf("We Shall Print The Sum Of Five Numbers...\n");
    printf("Enter The First Integer...\n");
    scanf("%d",&integer1);
    printf("Enter The Second Integer...\n");
    scanf("%d",&integer2);
    printf("Enter The Third Integer...\n");
    scanf("%d",&integer3);
    printf("Enter The Fourth Integer...\n");
    scanf("%d",&integer4);
    printf("Enter The Fifth Integer...\n");
    scanf("%d",&integer5);

    sum=integer1+integer2+integer3+integer4+integer5;
    printf("The Sum Is : \a%d",sum);
    return 0;
}
