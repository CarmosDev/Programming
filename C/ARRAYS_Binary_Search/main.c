#include <stdio.h>
#include <stdlib.h>
#define SIZE 15

int binarySearch(const int b[],int search_key,int size);
void printHeader(void);
void printRow(const int b[],int low,int mid,int high);

int main()
{
    int a[SIZE];
    int i,key,result;

    for(i=0;i<SIZE;i++){
        a[i]=2*i;
    }

    printf("Enter a number between 0 and 28 :\n");
    scanf("%d",&key);

    printHeader();

    result=binarySearch(a,key,SIZE);
    if(result!=-1){
        printf("Value : %d found in array element %d\n",key,result);
    }else{
        printf("Value : %d not found\n",key);
    }
    return 0;
}

int binarySearch(const int b[],int search_key,int size){
    int low=0;
    int high=size-1;
    int middle;
    while(low<=high){
        middle=(low+high)/2;
        printRow(b,low,middle,high);

        if(search_key==b[middle]){
            return middle;
        }else if(search_key<b[middle]){
            high=middle-1;
        }else{
            low=middle+1;
        }
    }
    return -1;
}
void printHeader(void){
    int i;
    printf("\nSubscripts:\n");

    for(i=0;i<SIZE;i++){
        printf("%3d",i);
    }
    printf("\n");
    for(i=1;i<=4*SIZE;i++){
        printf("-");
    }
    printf("\n");
}
void printRow(const int b[],int low,int mid,int high){
    int i;
    for(i=0;i<SIZE;i++){
        if (i<low || i>high) {
            printf("   ");
        }else if(i==mid){
            printf("%3d",b[i]);
        }else{
            printf("%3d",b[i]);
        }
    }
    printf("\n");
}

