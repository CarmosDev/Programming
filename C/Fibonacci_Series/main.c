#include <stdio.h>
#include <stdlib.h>

int fibonacci_numbers(int number_of_digits){
    int a=0 , b=1, c=a, i=0;
    while(i<number_of_digits){
        printf("%d",a);
        printf("\t");
        a=b;
        b=a+c;
        c=a;
        i++;
    }
    printf("\n");
    return 0;
}
int main()
{
    int number;
    printf("Enter the Number of Fibonacci Numbers that you want...\t");
    scanf("%d",&number);
    if(number==1){
        printf("The following is the first Fibonacci Number...\n");
        fibonacci_numbers(number);
    }else{
        printf("The following are the first ");
        printf("%d",number);
        printf(" Fibonacci Numbers...\n");
        fibonacci_numbers(number);
    }
    return 0;
}
