#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* ------------------------------ */
//Prototype
float squareroot(float number);
//DEFINITION OF FUNCTION
float squareroot(float number){
    int a;  //loop counter
    float x;//value
    x=1.00; //initialise x
    for(a=0;a<100;a++){ //High accuracy
        x=x-(((x*x)-number)/(2*x));
    }
    return x;
}
/* ------------------------------ */
/* ------------------------------ */
float cuberoot(float number);
float cuberoot(float number){
    int a;
    float x;
    x=1.00;
    for(a=0;a<100;a++){
        x=x-(((x*x*x)-number)/(3*x*x));
    }
    return x;
}
/* ------------------------------- */
int main()
{
    printf("BELOW ARE THE SQUAREROOTS OF NUMBERS 1 TO 100 : \n");
    printf("NUMBER\tSQUARE ROOT\tCUBE ROOT\n");
    int a;
    for(a=0;a<=10;a++){
        printf("%d\t%lf\t%lf\n",a,squareroot(a),cuberoot(a));
    }
    return 0;
}
