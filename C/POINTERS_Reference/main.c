#include <stdio.h>
#include <stdlib.h>

void cube_by_reference(int *nPtr);
int main()
{
    int number=5;
    printf("Number before execution of function : %d\n",number);
    cube_by_reference(&number);
    printf("Number after execution of function : %d\n",number);
    return 0;
}
void cube_by_reference(int *nPtr){
    *nPtr=*nPtr**nPtr**nPtr;
}
