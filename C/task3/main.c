#include <stdio.h>
#include <stdlib.h>

int main()
{
    int count=0;
    int n=4;
    while (n > 1) {
        count++;
        if (n & 1)
            n = n * 3 + 1;
        else
            n = n / 2;
    }
    printf("N is : %d. ",n);
    printf("Count is : %d.",count);
    return 0;
}
