#include <stdio.h>
#include <stdlib.h>

struct clientData{
    int acctNum;
    char lastName[15];
    char firstName[10];
    double balance;
};

int main()
{
    FILE *cFPtr;
    struct clientData client={0,"","",0};

    if((cFPtr=fopen("C:/Users/MOSETI/Documents/C Files/FILES_Random_CreditProcessingSystem/credit.dat","rb"))==NULL){
        printf("File could not be opened.\n");
    }else{
        printf("%-20s%-12s%-12s%-10s\n","Account Number","Last Name","First Name","Balance");

        while(!feof(cFPtr)){
            fread(&client,sizeof(struct clientData),1,cFPtr);
            if(client.acctNum!=0){
                printf("%-20d%-12s%-12s%-10.2f\n",client.acctNum,client.lastName,client.firstName,client.balance);
            }
        }
        fclose(cFPtr);
    }
    return 0;
}
