#include <stdio.h>
#include <stdlib.h>

int main()
{
    char fruit;
    printf("Which Of These Is Your Favourite Fruit?\n\n\a");
    printf("a)APPLES\n");
    printf("b)BANANAS\n");
    printf("c)CHERRIES\n");
    printf("d)GUAVAS\n");
    printf("e)MANGOES\n");
    printf("f)ORANGES\n\n");

    printf("Your Choice : ");
    scanf("%c",&fruit);
    switch(fruit){
    case'a':
        printf("You Like APPLES\n\a");
        break;
    case'b':
        printf("You Like BANANAS\n\a");
        break;
    case'c':
        printf("You Like CHERRIES\n\a");
        break;
    case'd':
        printf("You Like GUAVAS\n\a");
        break;
    case'e':
        printf("You Like MANGOES\n\a");
        break;
    case'f':
        printf("You Like ORANGES\n\a");
        break;
    default:
        printf("You entered an INVALID choice\n\a");
    }
    return 0;
}
