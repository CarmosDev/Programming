#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NUMBER_OF_PLAYERS 3
/*
    PROGRAM NAME:CRAPS-Game of Chance
    PROGRAM DESCRIPTION: Simulates many players playing craps(Default number of players is three)
    DATE LAST COMPILED: 04/12/2015
    DATE LAST WRITTEN: 30/11/2015
    AUTHOR NAME: MOSETI CARLTON MARANGA
    REGISTRATION NUMBER:P15/36821/2016
    COURSE:INTRODUCTION TO PROGRAMMING
*/

enum Status{CONTINUE,WON,LOST};
int rollDice(int playerId); //FUNCTION PROTOTYPE

int main()
{
    int sum,myPoint,id,i,round;
    int numberOfRolls[NUMBER_OF_PLAYERS]={0}; //INITIALISE NUMBER OF ROLLS ARRAY
    int numberOfWonRolls[NUMBER_OF_PLAYERS]={0}; //INITIALISE NUMBER OF WON ROLLS ARRAY
    enum Status gameStatus;
    enum Status playersStatus;

    srand(time(NULL)); //RANDOM GENERATION
    playersStatus=CONTINUE; //ALL PLAYERS TO CONTINUE PLAYING THE GAME
    round=0;

    while(playersStatus!=WON){ //LOOP UNTIL ALL PLAYERS HAVE WON AT LEAST TWICE
        round++;    //ROUND COUNTER
        //PRINT ROUND DETAILS
        printf("--------------------------------------------------\n");
        printf("Round : %d\n",round);
        printf("--------------------------------------------------\n");

        for(id=0;id<NUMBER_OF_PLAYERS;id++){ //ROUND ROBIN PLAY
            sum=rollDice(id); //ROLLS TWO DICE
            switch(sum){ //CHECK SUM OF DICE FOR WINS,LOSES OR POINTS
            case 7:
            case 11:
                gameStatus=WON; //PLAYER HAS WON IN THAT ROUND
            break;
            case 2:
            case 3:
            case 12:
                gameStatus=LOST; //PLAYER HAS LOST IN THAT ROUND
            break;
            default:
                gameStatus=CONTINUE; //PLAYER HAS MADE A POINT
                myPoint=sum; //STORE POINT MADE
                numberOfRolls[id]++; //COUNT ROLLS
            break;
            }
            while(gameStatus==CONTINUE){ //PLAYER TO CONTINUE PLAYING
                sum=rollDice(id); //ROLLS TWO DICE
                if(sum==myPoint){ //PLAYER REPEATS THE POINT MADE EARLIER
                    gameStatus=WON;
                }else if(sum==7){ //PLAYER ROLLS A SUM OF 7 BEFORE MAKING THE POINT
                    gameStatus=LOST;
                }else{
                    numberOfRolls[id]++; //COUNT ROLLS MADE
                }
            }
            if(gameStatus==WON){
                printf("Player %d Wins\n",id);
                numberOfWonRolls[id]++; //COUNT WON ROLLS
                numberOfRolls[id]++; //COUNT ROLLS
            }
            if(gameStatus==LOST){
                printf("Player %d Loses\nHouse Wins\n",id);
                numberOfRolls[id]++; //COUNT ROLLS
            }
            for(i=0;i<NUMBER_OF_PLAYERS;i++){ //CHECK IF ALL PLAYERS HAVE WON
                if(numberOfWonRolls[i]<2){
                    playersStatus=CONTINUE; //SINCE ALL PLAYERS HAVE NOT WON TWICE OR MORE ALL PLAYERS CONTINUE PLAYING
                    break;
                }else{
                    playersStatus=WON; //ALL PLAYERS HAVE WON TWICE OR MORE
                }
            }
            if(playersStatus==WON){ //STOPS GAME PROGRESS IF EVERYONE HAS WON TWICE
                break;
            }
        }
    }


    printf("\n\n%s%20s%30s\n","Player_Id","Number_of_Rolls","Number_of_Rolls_won"); //PRINT HEADER TITLES
    for(i=0;i<75;i++){ //PRINT UNDERLINE
        printf("-");
    }
    //PRINT PLAYER DATA
    for(id=0;id<NUMBER_OF_PLAYERS;id++){
        printf("\n%d%20d%30d\n",id,numberOfRolls[id],numberOfWonRolls[id]);
    }
    return 0;
}
int rollDice(int playerId){
    int die1=1+rand()%6; //RANDOM POINT 1
    int die2=1+rand()%6; //RANDOM POINT 2
    int workSum=die1+die2; //SUM BOTH POINTS
    printf("Player %d rolled %d + %d = %d\n",playerId,die1,die2,workSum); //PRINT RESULT OF DIE ROLL
    return workSum;
}
