#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void reverse_digits(int number);
int main()
{
    int n;
    printf("Input a number so that I can reverse its digits : \n");
    scanf("%d",&n);
    reverse_digits(n);
    return 0;
}
void reverse_digits(int number){
    if(number<10){
        printf("%d",number);
    }else{
        printf("%d",number%10);
        reverse_digits(number/10);
    }
}
