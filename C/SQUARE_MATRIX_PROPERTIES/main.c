#include <stdio.h>
#include <stdlib.h>

#define MAXIMUM_SIZE 100

struct element_property{
    int value;
    int row_position;
    int column_position;
};
int main()
{
    int matrix[MAXIMUM_SIZE][MAXIMUM_SIZE];
    int n,i,j;
    printf("Give the n value of the square matrix : \n");
    scanf("%d",&n);
    for(i=0;i<n;i++){
        printf("Row : %d\n",i+1);
        for(j=0;j<n;j++){
            printf("\tColumn : %d\n",j+1);
            printf("\tValue at (%d,%d) = ",i+1,j+1);
            scanf("%d",&matrix[i][j]);
        }
    }

    printf("\n\nYour matrix is : \n\n");
    //Print the matrix
    for(i=0;i<n;i++){
        printf("|");
        for(j=0;j<n;j++){
            printf("%-5d",matrix[i][j]);
        }
        printf("|");
        printf("\n");
    }

    //Find minimum and maximum elements
    struct element_property minimum;
    struct element_property maximum;

        //initialise the minimum
    minimum.value=matrix[0][0];
    minimum.row_position=1;
    minimum.column_position=1;

        //initialise the maximum
    maximum.value=matrix[0][0];
    maximum.row_position=1;
    maximum.column_position=1;
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            if(minimum.value>matrix[i][j]){
                minimum.value=matrix[i][j];
                minimum.row_position=i+1;
                minimum.column_position=j+1;
            }
            if(maximum.value<matrix[i][j]){
                maximum.value=matrix[i][j];
                maximum.row_position=i+1;
                maximum.column_position=j+1;
            }
        }
    }
    printf("The minimum element is %d and it is at row %d and column %d\n",minimum.value,minimum.row_position,minimum.column_position);
    printf("The maximum element is %d and it is at row %d and column %d\n",maximum.value,maximum.row_position,maximum.column_position);

    //Find the average of all elements
    int sum=0;
    float average;
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            sum+=matrix[i][j];
        }
    }
    average= (float) sum/(n*n);
    printf("The Average Value of the elements in the matrix is : %f\n",average);

    //Mode of the elements
    int distinct_elements[n*n];
    int a=0;
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
           distinct_elements[a]=matrix[i][j];
           a++;
        }
    }
    printf("\n\n");
    for(i=0;i<a;i++){
        printf("%-5d",distinct_elements[i]);
    }
    return 0;
}
