#include <stdio.h>
#include <stdlib.h>

void printCharacters(const char *sPtr);
void f(const int *xPtr);

int main()
{
    char string[]="Carlton Maranga Moseti";

    printf("The String is : ");
    printCharacters(string);

    /*Attemps to modify constant data
        int x;
        f(&x);
    */
    return 0;
}

void printCharacters(const char *sPtr){
    for(;*sPtr!='\0';sPtr++){
        printf("%c",*sPtr);
    }
}
/*Attemps to modify constant data
void f(const int *xPtr){
    *xPtr=100;
}
*/
