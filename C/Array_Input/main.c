#include <stdio.h>
#include <stdlib.h>

int main()
{
    int number[100],a,inputnumber,sum;
    sum=0;
    double average;
    printf("Welcome.How many numbers do you want to input into the array?\n");
    scanf("%d",&inputnumber);
    //Validate Input
    while(inputnumber>100 || inputnumber<1){
        printf("The minimum number of digits to be entered is 1 and the maximum number of digits to be entered is 100.\nTry again : \n");
        scanf("%d",&inputnumber);
    }
    //Capture the input
    for(a=0;a<inputnumber;a++){
        //Input prompt
        printf("Enter %d of %d numbers : \n",(a+1),inputnumber);
        scanf("%d",&number[a]);
        //Continually sum the input values
        sum+=number[a];
    }
    //Display all the numbers entered
    for(a=0;a<inputnumber;a++){
        printf("Number %d is ---> %d\n",(a+1),number[a]);
    }
    //Calculate the average
    average = (double) sum/inputnumber;
    printf("The total of the %d numbers is : %d\nThe average of the %d numbers is : %7.4f",inputnumber,sum,inputnumber,average);
    return 0;
}
