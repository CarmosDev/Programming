#include <stdio.h>
#include <stdlib.h>

int main()
{
    int account;
    char name[30];
    double balance;
    FILE *cFPtr; //File Pointer

    if((cFPtr=fopen("C:/Users/MOSETI/Documents/C Files/FILE_Create Sequential/clients.dat","r"))==NULL){
        printf("File could not be opened:\n");
    }else{
        printf("%-10s%-13s%s\n","Account","Name","Balance");
        fscanf(cFPtr,"%d%s%lf",&account,name,&balance);
        while(!feof(cFPtr)){
            printf("%-10d%-13s%7.2f\n",account,name,balance);
            fscanf(cFPtr,"%d%s%lf",&account,name,&balance);
        }
        fclose(cFPtr);
    }
    return 0;
}
