#include <stdio.h>
#include <stdlib.h>

int square(int number){
    return number*number;
}
int cube(int number){
    return number*number*number;
}
int main()
{
    int number;
    printf("Enter a number : \n");
    scanf("%d",&number);
    printf("The square of the number %d is %d and the cube is : %d",number,square(number),cube(number));
    return 0;
}
