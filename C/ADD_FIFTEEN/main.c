#include <stdio.h>
#include <stdlib.h>
/*
    PROGRAM NAME : ADD_FIFTEEN
    PROGRAM DESCRIPTION : Prints the average,sum and all fifteen floating numbers entered
    LAST COMPILED: Wednesday 21/10/2015
    LAST WRITTEN: Wednesday 21/10/2015
    NAME : Carlton Maranga Moseti
    REGISTRATION_NUMBER : P15/36821/2016
    COURSE :Introduction To Programming
*/
int main()
{
    int count;
    float average,number,sum;
    sum=0;
    printf("Enter Fifteen floating numbers...\n");
    for(count=0;count<15;count++){
        printf("Enter a number [%d of 15]\n",(count+1));
        scanf("%f",&number);
        sum+=number;
        average=(double)sum/(count+1);
        printf("The interim sum is : %8.4f\nThe interim average is : %8.4f\n\n",sum,average);
    }
    printf("Total numbers entered : %d\n",count);
    printf("The final sum is : %8.2f\nThe final average is : %8.2f",sum,average);
    return 0;
}
