#include <stdio.h>
#include <stdlib.h>

int factorial(int number);

int main()
{
    int n;
    printf("Enter the number that you want to calculate its factorial : \n? ");
    scanf("%d",&n);
    printf("%d! = %d ",n,factorial(n));
    return 0;
}

int factorial(int number){
    if(number==1 || number==0){
        return 1;
    }else{
        int result=1;
        int i;
        for(i=number;i>0;i--){
            result*=i;
        }
        return result;
    }
}
