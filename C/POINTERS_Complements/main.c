#include <stdio.h>
#include <stdlib.h>

int main()
{
    int y=5;
    int *yPtr;
    printf("Y is : %d\n",y);
    printf("&y is : %p\n",&y);

    yPtr=&y;
    printf("yPtr is : %p\n",yPtr);
    printf("*yPtr is :%d\n",*yPtr);

    printf("%p\n",&*yPtr);
    printf("%p\n",*&yPtr);
    return 0;
}
