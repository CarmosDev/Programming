#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main()
{
    int number;
    printf("How Many Characters Do You Want To Enter : (One Character Per Line)\n");
    scanf("%d",&number);
    int a,ch[number],c;
    for(a=0;a<number;a++){
        c=getchar();
        if(c!=10){
            ch[a]=c;
        }else{
            a--;
        }
    }
    printf("CHARACTER\tTYPE\n");
    for(a=0;a<number;a++){
        if(isdigit(ch[a])){
            putchar(ch[a]);
            printf("\t\tDIGIT\n");
        }else if(isalpha(ch[a])){
            putchar(ch[a]);
            printf("\t\tLETTER\n");
        }else if(isspace(ch[a])){
            putchar(ch[a]);
            printf("\t\tSPACE CHARACTER\n");
        }else if(ispunct(ch[a])){
            putchar(ch[a]);
            printf("\t\tPUNCTUATION CHARACTER\n");
        }else if(iscntrl(ch[a])){
            putchar(ch[a]);
            printf("\t\tCONTROL CHARACTER\n");
        }else{
            putchar(ch[a]);
            printf("\t\tSPECIAL CHARACTER\n");
        }
    }
    return 0;
}
