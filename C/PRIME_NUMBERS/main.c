#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*
    PROGRAM NAME : PRIME_NUMBERS
    PROGRAM DESCRIPTION : GETS THE PRIME NUMBERS
    LAST COMPILED: Wednesday 21/10/2015
    LAST WRITTEN: Wednesday 21/10/2015
    NAME : Carlton Maranga Moseti
    REGISTRATION_NUMBER : P15/36821/2016
    COURSE :Introduction To Programming
*/
int main()
{
    int number,prime_number,c;
    prime_number=1; //prime_number state is true
    printf("Enter a natural number : \n\a");
    scanf("%d",&number);
    for(c=2;c<number;c++){
        if(number%c==0){
            prime_number=0; //prime_number state is false
            break;
        }
    }
    if(prime_number){
        printf("Number %d is prime",number);
    }else{
        printf("Number %d is not prime",number);
    }
    return 0;
}
