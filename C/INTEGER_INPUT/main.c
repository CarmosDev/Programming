#include <stdio.h>
#include <stdlib.h>

int main()
{
    int integer;
    do{
        printf("Enter an Integer : \n");
        scanf("%d",&integer);
        if(integer<0){
            printf("Error!\a\a\a\a\a\n");
            break;
        }
        if(integer>100){
            printf("Number is skipped!\n\a");
            continue;
        }
        if(integer==0){
            printf("You have entered zero!!!I quit!!!\n\a\a\a\a\a");
            break;
        }
        printf("You have entered : %d\n",integer);
    }while(integer);
    return 0;
}
