#include <stdio.h>
#include <stdlib.h>

int main()
{
    int integer1,integer2;
    printf("Enter Two Numbers and I'll compare their values for you\n\a");
    printf("Number one : \n");
    scanf("%d",&integer1);
    printf("Number two : \n");
    scanf("%d",&integer2);
    if(integer1>integer2){
        printf("%d",integer1);
        printf("  is larger\a");
    }else if(integer1<integer2){
        printf("%d",integer2);
        printf("  is larger\a");
    }else{
        printf("These numbers are equal\a");
    }
    return 0;
}
