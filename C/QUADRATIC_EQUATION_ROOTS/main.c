#include <stdio.h>
#include <stdlib.h>

float squareroot(float number);
float squareroot(float number){
    int a;  //loop counter
    float x;//value
    x=1.00; //initialise x
    for(a=0;a<100;a++){ //High accuracy
        x=x-(((x*x)-number)/(2*x));
    }
    return x;
}

int main()
{
    float a,b,c,x1,x2,root_type;
    printf("Welcome to Quadratic Equation Solver\n");
    printf("Give the constant value a:\n");
    scanf("%f",&a);
    printf("Give the constant value b:\n");
    scanf("%f",&b);
    printf("Give the constant value c:\n");
    scanf("%f",&c);

    if(a==0){
        printf("Sorry!!!That is not a valid Quadratic Equation\n");
    }else{
        root_type=(b*b)-(4*a*c);
        if(root_type<0){
            printf("The equation has no real roots!!!\n");
        }else{
            x1=(-b+squareroot(root_type))/(2*a);
            x2=(-b-squareroot(root_type))/(2*a);
            printf("The roots of the quadratic equation are : \nx1 = %f\nx2 = %f\n",x1,x2);
        }
    }
    return 0;
}
