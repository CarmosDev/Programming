#include <stdio.h>
#include <stdlib.h>

int main()
{
    int n;

    printf("Enter number n of elements : \n");
    scanf("%d",&n);

    if(n<=0){
        printf("Invalid 'n'!!!\n");
    }else{
        int i;
        int elements[n];
        for(i=0;i<n;i++){
            printf("Enter element %d :\n",i+1);
            scanf("%d",&elements[i]);
        }
    }
    return 0;
}
