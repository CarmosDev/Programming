#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX_SIZE 10

int determinant(int MATRIX[MAX_SIZE][MAX_SIZE],int n_value);
int main()
{
    int n,i,j;
    printf("Input below the n value for the n x n matrix : \nn=");
    scanf("%d",&n);
    int matrix[n][n];
    for(i=0;i<n;i++){
        for(j=0;j<n;j++){
            printf("Enter (row,column) value for (%d,%d)\n",i+1,j+1);
            scanf("%d",&matrix[i][j]);
        }
    }
    for(i=0;i<n;i++){
        printf("|");
        for(j=0;j<n;j++){
            printf("%-3d",matrix[i][j]);
        }
        printf("|\n");
    }
    printf("The determinant is : %d",determinant(matrix,n));
    return 0;
}

int determinant(int MATRIX[MAX_SIZE][MAX_SIZE],int n_value){
    if(n_value==1){
        return MATRIX[0][0];
    }else{
        int new_size=n_value-1;
        int SUBMATRIX[new_size][new_size];
        int column;
        int i,j;
        int det=0;
        int new_column,new_row;
        for(column=0;column<n_value;column++){
            for(i=0;i<new_size;i++){
                for(j=0;j<new_size;j++){
                    if(j==column){
                        new_column=column+1;
                        new_row=i+1;
                        SUBMATRIX[i][j]=MATRIX[new_row][new_column];
                    }else{
                        new_column=column;
                        new_row=i+1;
                        SUBMATRIX[i][j]=MATRIX[new_row][new_column];
                    }
                }
            }
            if((column%2)==0){
                det+=MATRIX[0][column]*determinant(SUBMATRIX,new_size);
            }else{
                det-=MATRIX[0][column]*determinant(SUBMATRIX,new_size);
            }
        }
        return det;
    }
}
