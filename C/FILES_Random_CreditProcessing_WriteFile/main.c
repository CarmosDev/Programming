#include <stdio.h>
#include <stdlib.h>

struct clientData{
    int acctNum;
    char lastName[15];
    char firstName[10];
    double balance;
};

int main()
{
    FILE *cFPtr;
    struct clientData defaultClient={0,"","",0.0};

    if((cFPtr=fopen("C:/Users/MOSETI/Documents/C Files/FILES_Random_CreditProcessingSystem/credit.dat","rb+"))==NULL){
        printf("File could not be opened.\n");
    }else{
        printf("Enter account number ( 1 to 100, 0 to end input )\n?");
        scanf("%d",&defaultClient.acctNum);

        while(defaultClient.acctNum!=0){
            printf("Enter firstname, lastname, balance\n?");
            fscanf(stdin,"%s%s%lf",defaultClient.firstName,defaultClient.lastName,&defaultClient.balance);
            fseek(cFPtr,(defaultClient.acctNum-1)*sizeof(struct clientData),SEEK_SET);
            fwrite(&defaultClient,sizeof(struct clientData),1,cFPtr);

            printf("Enter account number\n?");
            scanf("%d",&defaultClient.acctNum);
        }
        fclose(cFPtr);
    }
    return 0;
}
