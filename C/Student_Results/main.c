#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main()
{
    //GET NUMBER OF STUDENTS FIRST
    int number_of_students,a;
    printf("For how many students do you want to enter their marks? \n");
    scanf("%d",&number_of_students);
    //CREATE ARRAYS BASED ON NUMBER OF STUDENTS
    char students_name[number_of_students][100];
    int english[number_of_students];
    int geography[number_of_students];
    int math[number_of_students];
    for(a=0;a<number_of_students;a++){
        printf("ENTER THE STUDENT NAME : (%d of %d)\n",(a+1),number_of_students);
        scanf("%s",&students_name[a]);
        //English Mark
        printf("ENGLISH MARK :\n");
        scanf("%d",&english[a]);
        //Geography Mark
        printf("GEOGRAPHY MARK :\n");
        scanf("%d",&geography[a]);
        //Math Mark
        printf("MATH MARK :\n");
        scanf("%d",&math[a]);
    }
    //Print
    printf("-----------------------------------------------------------------\n");
    printf("|STUDENT NAME\t|ENGLISH MARK\t|GEOGRAPHY MARK\t|MATH MARK\t|\n");
    for(a=0;a<number_of_students;a++){
        printf("|%s\t\t|%d\t\t|%d\t\t|%d\t\t|\n",students_name[a],english[a],geography[a],math[a]);
    }
    printf("-----------------------------------------------------------------\n");
    return 0;
}
