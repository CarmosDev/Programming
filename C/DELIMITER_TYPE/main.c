#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char input_string[1000];    //A string
    char *delimited_strings;
    const char *space=" ";
    const char *comma=",";
    printf("Input a sequence of characters/strings separated with either a comma or space :\n");
    gets(input_string);         //Get input string

    //TYPE OF DELIMITER
    if(strpbrk(input_string,comma)){
        delimited_strings=strtok(input_string,comma);
        printf("I Have Determined That This String Has Been Delimited With Commas.\nThe Following Are Its Tokens : \n");
        while(delimited_strings!=NULL){
            printf("%s\n",delimited_strings);
            delimited_strings=strtok(NULL,comma);
        }
    }
    if(strpbrk(input_string,space)){
        delimited_strings=strtok(input_string,space);
        printf("I Have Determined That This String Has Been Delimited With A Space.\nThe Following Are Its Tokens : \n");
        while(delimited_strings!=NULL){
            printf("%s\n",delimited_strings);
            delimited_strings=strtok(NULL,space);
        }
    }
    //Check type of delimiter

    return 0;
}
