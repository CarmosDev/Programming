#include <stdio.h>
#include <stdlib.h>

int main()
{
    int numbers[]={1,2,3,4,5,6,7,8,9,'\0'};
    int *pointer;
    pointer=numbers;

    while(*pointer!='\0'){
        printf("%d\n",*pointer);
        pointer++;
    }
    return 0;
}
