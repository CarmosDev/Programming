#include <stdio.h>
#include <stdlib.h>

int main()
{
    char message[10]="Hello";
    char *messagePtr="Hello";
    char *messagePtr2;
    messagePtr2=message;

    int numbers[]={1,2,3,4,5,6,7,8,9};
    int *numbersPtr;
    numbersPtr=numbers;
    char s[100];

    printf("%s\n",message);
    printf("%c\n",*message);
    printf("%s\n",messagePtr);
    printf("%c\n",*messagePtr);
    printf("%p\n",messagePtr2);
    printf("%c\n",*messagePtr2);
    printf("%c\n",80);

    while(*messagePtr!=NULL){
        printf("%c",*messagePtr);
        messagePtr++;
    }
    while(*numbersPtr!=NULL){
        printf("%d",*numbersPtr);
        numbersPtr++;
    }
    printf("\n");
    int a=2;
    int b=3;
    sprintf(s,"%d--%d",a,b);
    printf("%s\n",s);

    char sentence[]={"Kenya Power"};
    char *sentencePtr=sentence;
    int length=0;
    while(*sentencePtr!=NULL){
        length++;
        sentencePtr++;
    }
    printf("%d\n",length);
    int x=2;
    printf("%d\n",x++);
    printf("%d\n",++x);
    return 0;
}
