#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("%4d\n",1);
    printf("%4d\n",12);
    printf("%4d\n",123);
    printf("%4d\n",1234);
    printf("%4d\n",12345);
    printf("%4d\n",123456);
    printf("_____PRECISION_____\n");
    printf("%.4d\n",1);
    printf("%.4d\n",12);
    printf("%.4d\n",123);
    printf("%.4d\n",1234);
    printf("_____FIELD WIDTH + PRECISION_____\n");
    printf("%4.3d",1);
    return 0;
}
