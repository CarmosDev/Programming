#include <stdio.h>
#include <stdlib.h>
#define MAX_LETTERS 20

int main()
{
    FILE *myFPtr;
    int no_of_records,student_number,mark_1,mark_2,mark_3,task;
    int i;
    char student_name[MAX_LETTERS];
    double average_mark;
    char fail[]={"FAIL"};
    char pass[]={"PASS"};
    char remark[MAX_LETTERS];

    printf("Welcome to the School's Record Keeping System.\nSelect your task below:\n\t1.Input students records...\n\t2.Print students records...\nTask ?:");
    scanf("%d",&task);

    switch(task){
    case 1:
        /* FILE INPUT */
        printf("How many number of student records are to be entered?\n");
        scanf("%d",&no_of_records);
        if((myFPtr=fopen("School_Records.dat","a"))!=NULL){
            for(i=1;i<=no_of_records;i++){
                printf("Enter record : %d of %d\n",i,no_of_records);
                printf("Format: Student_Number,Student_Name,Mark_1,Mark_2,Mark_3\n");
                scanf("%d%20s%d%d%d",&student_number,student_name,&mark_1,&mark_2,&mark_3);
                average_mark=(double)(mark_1+mark_2+mark_3)/3;

                fprintf(myFPtr,"%d %s %d %d %d %4.2lf %s\n",student_number,student_name,mark_1,mark_2,mark_3,average_mark,average_mark>50?pass:fail);
            }
            fclose(myFPtr);
        }else{
            printf("Error! File could not be opened!\n");
        }
        break;
    case 2:
        printf("Select student grade category:\n\t1.EXCELLENT >80\n\t2.VERY GOOD >60\n\t3.GOOD >50\n\t4.FAIL<=50\n\t5.All Students\nGRADE CATEGORY :?\n");
        scanf("%d",&task);
        switch(task){
        case 1:
            /* FILE OUTPUT */
            if((myFPtr=fopen("School_Records.dat","r"))!=NULL){
                printf("GRADE :EXCELLENT\n");
                printf("STUDENT NUMBER\tSTUDENT_NAME\tMARK_1\tMARK_2\tMARK_3\tAVERAGE_MARK\tREMARK\n");
                fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                while(!feof(myFPtr)){
                    if(average_mark>80){
                        printf("%d\t\t%s\t\t%d\t%d\t%d\t%4.2lf\t\t%s\n",student_number,student_name,mark_1,mark_2,mark_3,average_mark,remark);
                    }
                    fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                }
            }else{
                printf("Error! File could not be read!\n");
            }
            break;
        case 2:
            /* FILE OUTPUT */
            if((myFPtr=fopen("School_Records.dat","r"))!=NULL){
                printf("GRADE :VERY GOOD\n");
                printf("STUDENT NUMBER\tSTUDENT_NAME\tMARK_1\tMARK_2\tMARK_3\tAVERAGE_MARK\tREMARK\n");
                fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                while(!feof(myFPtr)){
                    if(average_mark>60&&average_mark<=80){
                        printf("%d\t\t%s\t\t%d\t%d\t%d\t%4.2lf\t\t%s\n",student_number,student_name,mark_1,mark_2,mark_3,average_mark,remark);
                    }
                    fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                }
            }else{
                printf("Error! File could not be read!\n");
            }
            break;
        case 3:
            /* FILE OUTPUT */
            if((myFPtr=fopen("School_Records.dat","r"))!=NULL){
                printf("GRADE :GOOD\n");
                printf("STUDENT NUMBER\tSTUDENT_NAME\tMARK_1\tMARK_2\tMARK_3\tAVERAGE_MARK\tREMARK\n");
                fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                while(!feof(myFPtr)){
                    if(average_mark>50&&average_mark<=60){
                        printf("%d\t\t%s\t\t%d\t%d\t%d\t%4.2lf\t\t%s\n",student_number,student_name,mark_1,mark_2,mark_3,average_mark,remark);
                    }
                    fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                }
            }else{
                printf("Error! File could not be read!\n");
            }
            break;
        case 4:
            /* FILE OUTPUT */
            if((myFPtr=fopen("School_Records.dat","r"))!=NULL){
                printf("GRADE :FAIL\n");
                printf("STUDENT NUMBER\tSTUDENT_NAME\tMARK_1\tMARK_2\tMARK_3\tAVERAGE_MARK\tREMARK\n");
                fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                while(!feof(myFPtr)){
                    if(average_mark<=50){
                        printf("%d\t\t%s\t\t%d\t%d\t%d\t%4.2lf\t\t%s\n",student_number,student_name,mark_1,mark_2,mark_3,average_mark,remark);
                    }
                    fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                }
            }else{
                printf("Error! File could not be read!\n");
            }
            break;
        case 5:
            /* FILE OUTPUT */
            if((myFPtr=fopen("School_Records.dat","r"))!=NULL){
                printf("GRADE :ALL\n");
                printf("STUDENT NUMBER\tSTUDENT_NAME\tMARK_1\tMARK_2\tMARK_3\tAVERAGE_MARK\tREMARK\n");
                fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                while(!feof(myFPtr)){
                    printf("%d\t\t%s\t\t%d\t%d\t%d\t%4.2lf\t\t%s\n",student_number,student_name,mark_1,mark_2,mark_3,average_mark,remark);
                    fscanf(myFPtr,"%d%s%d%d%d%lf%s",&student_number,student_name,&mark_1,&mark_2,&mark_3,&average_mark,remark);
                }
            }else{
                printf("Error! File could not be read!\n");
            }
            break;
        default:
            printf("Invalid Grade Option\n");
            break;
        }
        break;
    default:
        printf("Invalid task!!!\n");
        break;
    }
    return 0;
}
