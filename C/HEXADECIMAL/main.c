#include <stdio.h>
#include <stdlib.h>
#include <math.h>

char hexadecimal_values[]={'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
char hexadecimal_value(int decimal_value);
int main()
{
    printf("Enter the two characters that you want to get their Hexadecimal values: \n");
    int i;
    for(i=1;i<=2;i++){
        int character;
        character=getchar();
        int remainder_value=character%16;
        int quotient_value=character/16;
        printf("Character at position %d......... \nCharacter : %c\nCharacter value : %d \nHexadecimal value : %c%c\n",i,character,character,hexadecimal_value(quotient_value),hexadecimal_value(remainder_value));
    }
    return 0;
}

char hexadecimal_value(int decimal_value){
    return hexadecimal_values[decimal_value];
}
