#include <stdio.h>
#include <stdlib.h>

int main()
{
    int integer1,integer2,result;
    printf("Welcome to Di_Multiplier. We shall multiply two numbers\n");
    printf("Enter the first number...\n");
    scanf("%d",&integer1);
    printf("Enter the second number...\n");
    scanf("%d",&integer2);

    result=integer1*integer2;
    printf("Answer : \a%d",result);
    return 0;
}
