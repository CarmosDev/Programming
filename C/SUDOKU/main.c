#include <stdio.h>
#include <stdlib.h>

int main()
{
    int boxes[9][9],a,b;
    for(a=0;a<9;a++){
        for(b=0;b<9;b++){
            boxes[a][b]=b+1;
        }
    }
    for(a=0;a<9;a++){
        for(b=0;b<9;b++){
            printf(" %d ",boxes[a][b]);
        }
        printf("\n");
    }
    return 0;
}
