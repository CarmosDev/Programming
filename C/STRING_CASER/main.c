#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUMBER_OF_CHARACTERS 100
/*
    PROGRAM NAME:String Case Converter
    PROGRAM DESCRIPTION:Converts input text/sentence into both lower-case and upper-case
    DATE LAST COMPILED: 30/11/2015
    DATE LAST WRITTEN: 28/11/2015
    AUTHOR NAME: MOSETI CARLTON MARANGA
    REGISTRATION NUMBER:P15/36821/2016
    COURSE:INTRODUCTION TO PROGRAMMING
*/

int main()
{
    char s[NUMBER_OF_CHARACTERS]; //Initialise string array
    int a; //Loop Counter

    printf("Enter a string to convert it to both upper-case and lower-case :\n"); //Prompt user input
    gets(s); //Get the input

    printf("UPPERCASE LETTERS:\n"); //Output header
    for(a=0;a<strlen(s);a++){ //Loop through all strings
        printf("%c",toupper(s[a])); //Print each character in upper-case
        if(a==(strlen(s)-1)){
            printf("\n\n\n"); //Create space after all strings have been printed
        }
    }
    printf("lowercase letters:\n"); //Output header
    for(a=0;a<strlen(s);a++){ //Loop through all strings
        printf("%c",tolower(s[a])); //Print each character in lower-case
        if(a==(strlen(s)-1)){
            printf("\n\n\n"); //Create space after all strings have been printed
        }
    }
    return 0;
}
