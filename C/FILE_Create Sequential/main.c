#include <stdio.h>
#include <stdlib.h>

int main()
{
    int account;
    char name[30];
    double balance;
    FILE *cFPtr; //File Pointer

    if((cFPtr=fopen("clients.dat","w"))==NULL){
        printf("File could not be opened\n");
    }else{
        printf("Enter the account, name and balance.\n");
        printf("Enter EOF (CTRL+Z) to end input\n");
        printf("?");
        scanf("%d%s%lf",&account,name,&balance);

        while(!feof(stdin)){
            fprintf(cFPtr,"%d %s %.2f\n",account,name,balance);
            printf("?");
            scanf("%d%s%lf",&account,name,&balance);
        }

        fclose(cFPtr);
    }
    return 0;
}
