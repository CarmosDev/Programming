#include <stdio.h>
#include <stdlib.h>
#define SIZE 99

double mean(int survey[],int size);
int mode(int survey[],int size);
int median(int survey[],int size);
void bubble_sort(int survey[],int size);

int main()
{
    int responses[SIZE]={
        6, 7, 8, 9, 8, 7, 8, 9, 8, 9,
        7, 8, 9, 5, 9, 8, 7, 8, 7, 8,
        6, 7, 8, 9, 3, 9, 8, 7, 8, 7,
        7, 8, 9, 8, 9, 8, 9, 7, 8, 9,
        6, 7, 8, 7, 8, 7, 9, 8, 9, 2,
        7, 8, 9, 8, 9, 8, 9, 7, 5, 3,
        5, 6, 7, 2, 5, 3, 9, 4, 6, 4,
        7, 8, 9, 6, 8, 7, 8, 9, 7, 8,
        7, 4, 4, 2, 5, 3, 8, 7, 5, 6,
        4, 5, 6, 1, 6, 5, 7, 8, 7
        };
    printf("Mean is : %lf\n",mean(responses,SIZE));
    printf("Median is : %d\n",median(responses,SIZE));
    printf("Mode : %d\n",mode(responses,SIZE));
    return 0;
}
double mean(int survey[],int size){
    int a;
    int total=0;
    double average;
    for(a=0;a<size;a++){
        total +=survey[a];
    }
    average=(double)total/SIZE;
    return average;
}
void bubble_sort(int survey[],int size){
    int a;
    int b;
    int interim;
    for(a=0;a<size;a++){
        for(b=0;b<size-1;b++){
            if(survey[b]>survey[b+1]){
                interim=survey[b];
                survey[b]=survey[b+1];
                survey[b+1]=interim;
            }
        }
    }
}
int median(int survey[],int size){
    //sort array
    bubble_sort(survey,size);
    int middle=(size/2)-1;
    return survey[middle];
}
int mode(int survey[],int size){
    //Sort array
    bubble_sort(survey,size);
    int distincts[size];
    int a,b=0;
    int elements=0;
    for(a=0;a<size;a++){
        distincts[a]=survey[b];
        elements++;
        while((distincts[a]==survey[b])){
            b++;
        }
        if(b==size){
            break;
        }
    }
    int frequency[elements+1];
    int modePosition=1;
    int max;
    //initialise
    for(a=0;a<=elements;a++){
        frequency[a]=0;
    }
    //STORE NUMBER OF COUNT VALUES
    for(a=0;a<size;a++){
        frequency[survey[a]]++;
    }
    max=frequency[0];
    for(a=1;a<=elements;a++){
        if(max<frequency[a]){
            modePosition=a;
            max=frequency[a];
        }
    }
    return distincts[modePosition-1];
}
