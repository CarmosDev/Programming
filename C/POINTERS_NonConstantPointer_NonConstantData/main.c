#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void convertToUppercase(char *sPtr);

int main()
{
    char string[]="Carlton John Maranga Moseti";
    printf("The String before conversion is : %s\n",string);
    convertToUppercase(string);
    printf("The String after conversion is: %s\n",string);

    return 0;
}

void convertToUppercase(char *sPtr){
    while(*sPtr!='\0'){
        if(islower(*sPtr)){
            *sPtr=toupper(*sPtr);
        }
        sPtr++;
    }
}
