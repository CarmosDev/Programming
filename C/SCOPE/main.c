#include <stdio.h>
#include <stdlib.h>

int x=19;
void static_variable();
void static_variable(){
    static int value=15;
    printf("The value of the static variable is : %d\n",value);
    value*=2;
}
void local_variable();
void local_variable(){
    int value=15;
    printf("The value of the local variable is : %d\n",value);
    value*=2;
}

int main()
{
    printf("Global Variable x is : %d\n",x);
    {
        int x=10;
        printf("Local variable x is : %d\n",x);
    }
    printf("Global Variable x is : %d\n",x);

    printf("The value of static and local variables at first call : \n");
    static_variable();
    local_variable();
    printf("The value of static and local variables at second call : \n");
    static_variable();
    local_variable();
    printf("The value of static and local variables at third call : \n");
    static_variable();
    local_variable();
    printf("The value of static and local variables at fourth call : \n");
    static_variable();
    local_variable();
    return 0;
}
