#include <stdio.h>
#include <stdlib.h>

int main()
{
    int Scores[5]={89,57,70,45,65};
    int a,b;
    //DISPLAY MEMORY LOCATIONS OF THE ARRAY VALUES
    for(a=0;a<5;a++){
        printf("Scores[%d] memory address is : %d\n",a,&Scores[a]);
    }
    //DISPLAY VALUES IN THE ARRAY
    for(b=0;b<5;b++){
        printf("Scores[%d] is : %d\n",b,Scores[b]);
    }
    return 0;
}
