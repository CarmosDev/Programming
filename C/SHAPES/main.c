#include <stdio.h>
#include <stdlib.h>
/*
    PROGRAM NAME : SHAPES
    PROGRAM DESCRIPTION : Draws a square, diamond, arrow and oval
    LAST COMPILED: Saturday 21/10/2015
    LAST WRITTEN: Saturday 21/10/2015
    SPECIAL FEATURES : Use of escape characters
    NAME : Carlton Maranga Moseti
    REGISTRATION_NUMBER : P15/36821/2016
    COURSE :Introduction To Programming
*/


int main()
{
    //prints the outline of shapes in various lines
    printf("+++++++++\t  +  \t  +  \t    +    \n");
    printf("+       +\t + + \t +++ \t  +   +  \n");
    printf("+       +\t+   +\t+++++\t+       +\n");
    printf("+       +\t + + \t  +  \t  +   +  \n");
    printf("+       +\t  +  \t  +  \t    +    \n");
    printf("+++++++++\t     \t  +  \t         \n");
    printf("         \t     \t  +  \t         \n");
    return 0;
}
