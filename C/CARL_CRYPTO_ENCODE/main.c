#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    unsigned seed;
    seed=time(NULL);
    int crypto_unsafe=rand();
    srand(seed);
    int crypto_safe=rand();
    printf("Seed (time) : %u \n",seed);
    printf("Unseeded : %i \n",crypto_unsafe);
    printf("Seeded : %i \n",crypto_safe);
    base_84(crypto_safe);
    return 0;
}

void base_84(int val){
    if(val<=84){
        printf("!%i,",val);
    }else{
        printf("-%i",val%84);
        base_84(val/84);
    }
}
