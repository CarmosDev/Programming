#include <iostream>

using namespace std;

/*
    CSC 211: Data Structures And Algorithms
    P15/36821/2016
    Creating and inserting an element in a Double Linked List
*/
struct dll_node{
    dll_node *left_link;
    int value;
    dll_node *right_link;
};

int main()
{
/*--------------------------------------------------------------------*/
    int n;
    std::cout << "Let us initialise a sample double linked list. How many elements do you want?" << endl;
    std::cin >> n;

    if(n<=0){
        std::cout << "Empty List!!!" << endl;
    }else{
        dll_node *H;
        dll_node *P;
        dll_node *Q;
        int i,search_value;
        for(i=1;i<=n;i++){
            std::cout << "Enter element " << i << " : " << endl;
            Q=new dll_node();
            std::cin >> Q->value;
            if(i==1){
                H=Q;
            }else{
                P->right_link=Q;
                Q->left_link=P;
            }
            P=Q;
        }
/*--------------------------------------------------------------------*/
        std::cout << endl;
        std::cout << "The elements are :" << endl;

        dll_node *read;
        read=H;
        while(read!=NULL){
            std::cout << read->value << endl;
            read=read->right_link;
        }
/*--------------------------------------------------------------------*/
        //Insert before
        std::cout << endl;
        std::cout << "Select a value from the above list to insert a new value before it" << endl;
        std::cin >> search_value;

        if(H==NULL){
            std::cout << "Empty List!!!" << endl;
        }else{
            P=H;
            while(P->value!=search_value){
                P=P->right_link;
                if(P==NULL){
                    break;
                }
            }
            if(P==NULL){
                std::cout << endl;
                std::cout << "The search value " << search_value << " cannot be found!!!" << endl;
            }else{
                Q=new dll_node();
                std::cout << endl;
                std::cout << "What do you want to insert?" << endl;
                std::cin >> Q->value;

                if(P==H){
                    P->left_link=Q;
                    Q->right_link=P;
                    H=Q;
                    Q->left_link=NULL;
                }else{
                    Q->right_link=P;
                    Q->left_link=P->left_link;
                    (P->left_link)->right_link=Q;
                    P->left_link=Q;
                }
                P=NULL;
                Q=NULL;
/*--------------------------------------------------------------------*/
                std::cout << endl;
                std::cout << "The elements are :" << endl;

                dll_node *read;
                read=H;
                while(read!=NULL){
                    std::cout << read->value << endl;
                    read=read->right_link;
                }
            }
        }
    }
    return 0;
}
