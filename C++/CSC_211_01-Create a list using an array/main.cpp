#include <iostream>

using namespace std;

int main()
{
    int n;
    std::cout << "Enter number n of elements : \n";
    std::cin >>n;

    if(n<=0){
        std::cout << "Invalid 'n'!!!\n";
    }else{
        int i;
        int elements[n];
        for(i=0;i<n;i++){
            std::cout << "Enter element " << i+1 << " : \n";
            std::cin >> elements[i];
        }

        for(i=0;i<n;i++){
            std::cout << "Element a[" << i << "] is : " << elements[i] << endl;
        }
    }
    return 0;
}
