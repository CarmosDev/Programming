#include <iostream>

using namespace std;
/*
    CSC 211: Data Structures And Algorithms
    P15/36821/2016
    Linear Lists
*/

//Functions
void print_message(char* message){
    cout << message << endl <<endl;
}
void get_int_input(int* input){
    cin >> *input;
}
void print_linear_list(int linear_list[],int number_of_elements){
    if(number_of_elements<=0){
        print_message("Empty list!!!");
    }else{
        int i;
        print_message("Your linear list :");
        for(i=0;i<number_of_elements;i++){
            cout << linear_list[i] << endl;
        }
        print_message("");
    }
}
void create_linear_list(int linear_list[],int number_of_elements){
    if(number_of_elements<=0){
        print_message("Invalid n!!!");
    }else{
        int i;
        for(i=0;i<number_of_elements;i++){
            cout << "Enter element " << i+1 << endl;
            get_int_input(&linear_list[i]);
        }
    }
}
void insert_element_into_linear_list(int linear_list[],int position,int value,int* number_of_elements){
    if(position<=0 || position > *number_of_elements){
        print_message("Invalid position!!!");
    }else{
        int index=position-1;
        int i=*number_of_elements;
        while(index<i){
            linear_list[i]=linear_list[i-1];
            i--;
        }
        linear_list[index]=value;
        *number_of_elements = *number_of_elements+1;
    }
}
void delete_element_from_linear_list(int linear_list[],int value,int* number_of_elements){
    if(*number_of_elements<=0){
        print_message("Empty list!!!");
    }else{
        //Searching
        int i=0;
        int found=1;
        while(linear_list[i]!=value){
            i++;
            if(i>= *number_of_elements){
                cout << "Sorry. Element " << value << "cannot be found in linear list!!!" << endl;
                found=0;
            }
        }
        if(!found){
            print_message("Unable to delete element from linear list");
        }else{
            *number_of_elements=*number_of_elements-1;
            while(i<*number_of_elements){
                linear_list[i]=linear_list[i+1];
                i++;
            }
        }
    }
}

int main()
{
    int number_of_elements;
/*--------------------------------------------------------------------*/
    print_message("Linear lists :");
    print_message("How many elements do you want in the linear list?");
    get_int_input(&number_of_elements);
/*--------------------------------------------------------------------*/
    int linear_list[number_of_elements+1];
    create_linear_list(linear_list,number_of_elements);
    print_linear_list(linear_list,number_of_elements);
/*--------------------------------------------------------------------*/
    if(number_of_elements>0){

        int position;
        int value;
        print_message("Enter a valid position and value of element to be inserted into that position :");
        print_message("Position :");
        get_int_input(&position);
        print_message("Value :");
        get_int_input(&value);
        insert_element_into_linear_list(linear_list,position,value,&number_of_elements);
        print_linear_list(linear_list,number_of_elements);
/*--------------------------------------------------------------------*/
        print_message("Enter a value you want to delete from the linear list :");
        get_int_input(&value);
        delete_element_from_linear_list(linear_list,value,&number_of_elements);
        print_linear_list(linear_list,number_of_elements);
    }
    return 0;
}
