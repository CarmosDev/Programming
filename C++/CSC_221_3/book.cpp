#include <iostream>
#include <string>

using namespace std;

class Book{
    public:
        string author,title,publisher = "";
        int price,stock_position = 0;
    private:
        static int successful_transactions,unsuccessful_transactions;
        int number_of_copies=0;
    public:
        void set_values(string author, string title, int price, string publisher, int stock);
        void request_copies(int number_of_copies);
        void update_book_price(int new_price);
        void buy_books();
        void cancel_transaction();
    private:
        void update_price(int new_price);
        void update_stock(int sold_books);
};
//Constructor
void Book::set_values(string author, string title, int price, string publisher, int stock=0) {
    Book::author = author;
    Book::title = title;
    Book::price = price;
    Book::publisher = publisher;
    Book::stock_position = stock;
}
//Static Variables Initialisation
int Book::unsuccessful_transactions=0;
int Book::successful_transactions=0;
//Methods
void Book::request_copies(int number_of_copies) {
    if(number_of_copies < 1 || number_of_copies > Book::stock_position){
        std::cout << "Required copies not in stock" << std::endl;
        return;
    }
    Book::number_of_copies = number_of_copies;
    std::cout << "Total cost of the requested copies is : " << Book::price * number_of_copies << std::endl;
}
void Book::update_book_price(int new_price) {
    Book::update_price(new_price);
}
void Book::update_price(int new_price) {
    Book::price = new_price;
}
void Book::update_stock(int sold_books) {
    Book::stock_position -= sold_books;
}
void Book::buy_books() {
    Book::update_stock(Book::number_of_copies);
    Book::successful_transactions++;
}
void Book::cancel_transaction() {
    Book::unsuccessful_transactions++;
}
