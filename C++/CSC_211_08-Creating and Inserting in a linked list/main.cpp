#include <iostream>

using namespace std;
/*
    CSC 211: Data Structures And Algorithms
    P15/36821/2016
    Creating and Inserting in a Linked List
*/
struct node{
    int x;
    node *next;
};

int main()
{
/*--------------------------------------------------------------------*/
    int n;

    std::cout << "How many elements do you want in the linked list ?"<< endl;
    std::cin >> n;

    if(n<=0){
        std::cout << "Invalid n !!!" << endl;
    }else{
        node *H;
        node *Q;
        node *P;
        int i=1,x;
        std::cout << endl;
        for(i;i<=n;i++){
            Q=new node();
            std::cout << "Enter x" << i <<endl;
            std::cin >> x;
            Q->x=x;
            Q->next=NULL;
            if(i==1){
                H=Q;
            }else{
                P->next=Q;
            }
            P=Q;
        }
/*--------------------------------------------------------------------*/
        node *read=H;
        std::cout << endl;
        std::cout << "Elements in the linked list are : " << endl;
        while(read->next!=NULL){
            std::cout << read->x << endl;
            read=read->next;
            if(read->next==NULL){
                std::cout << read->x << endl;
            }
        }
    }
    return 0;
}
