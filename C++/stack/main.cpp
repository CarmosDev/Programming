#include <iostream>
#define MAX_STACK_SIZE 15

using namespace std;
/*
    CSC 211: Data Structures And Algorithms
    P15/36821/2016
    Stack
*/

void print_message(char* message){
    cout << endl << message << endl;
}
void get_int_input(int* input){
    cin >> *input;
}
void print_stack(int* stack_array,int top){
    print_message("Stack :");
    for(top;top>=0;top--){
        cout << stack_array[top] <<endl;
    }
}
void push_stack(int* stack_array,int* top,int maximum_size){
    if(*top==maximum_size){
        print_message("Stack Overflow!!!");
    }else{
        print_message("Enter an element into the stack :");
        get_int_input(&stack_array[*top]);

        *top=*top+1;
        push_stack(stack_array,top,maximum_size);
    }
}
void pop_stack(int* stack_array,int* top,int maximum_size){
    if(*top<=0){
        print_message("Empty stack!!!");
    }else{
        char stack_option;
        *top=*top-1;
        print_message("Do you want to pop the stack? Y or N. ");
        cin >> stack_option;
        switch(stack_option){
            case 'Y':
                cout << "Popped value : " << stack_array[*top] << endl;
                stack_array[*top]=0;
                print_stack(stack_array,*top);
                pop_stack(stack_array,&*top,maximum_size);
                break;
            case 'N':
                print_stack(stack_array,*top);
                break;
            default:
                print_message("Invalid option!!!");
                print_stack(stack_array,*top);
                break;
        }
    }
}

int main()
{
    int stack_array[MAX_STACK_SIZE];
    int top=0;

    print_message("Stack : ");
    print_message("Maximum size :");
    cout << MAX_STACK_SIZE << endl;

    push_stack(stack_array,&top,MAX_STACK_SIZE);

    pop_stack(stack_array,&top,MAX_STACK_SIZE);
    return 0;
}
