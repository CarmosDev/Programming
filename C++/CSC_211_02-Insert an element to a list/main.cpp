#include <iostream>

using namespace std;

int main()
{
    int n=5;
    int i;
    int list_n[5]={1,2,3,4,5};

    if(n<=0){
        std::cout << "Invalid List" << endl;
    }else{
        for(i=0;i<n;i++){
            std::cout << list_n[i] << endl;
        }
        int position,value;

        std::cout << "Enter position and value to insert" << endl;
        std::cout << "Position" << endl;
        std::cin >> position;
        std::cout << "Value" << endl;
        std::cin >> value;

        if(position < 1 || position>n+1){
            std::cout << "Invalid position" << endl;
        }else{
            int j=n+1;
            while(j>position){
                list_n[j-1]=list_n[j-2];
                j--;
            }
            list_n[j-1]=value;
            n=n+1;

            for(i=0;i<n;i++){
                std::cout << list_n[i] << endl;
            }
        }
    }
    return 0;
}
