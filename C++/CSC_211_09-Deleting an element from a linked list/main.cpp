#include <iostream>

using namespace std;

/*
    CSC 211: Data Structures And Algorithms
    P15/36821/2016
    Deleting an element in a Linked List
*/

struct node{
    int x;
    node *next;
};

int main()
{
/*--------------------------------------------------------------------*/
    int n;

    std::cout << "How many elements do you want in the linked list ?"<< endl;
    std::cin >> n;

    if(n<=0){
        std::cout << "Invalid n !!!" << endl;
    }else{
        node *H;
        node *Q;
        node *P;
        int i=1,x;
        std::cout << endl;
        for(i;i<=n;i++){
            Q=new node();
            std::cout << "Enter x" << i <<endl;
            std::cin >> x;
            Q->x=x;
            Q->next=NULL;
            if(i==1){
                H=Q;
            }else{
                P->next=Q;
            }
            P=Q;
        }
/*--------------------------------------------------------------------*/
        node *read=H;
        std::cout << endl;
        std::cout << "Elements in the linked list are : " << endl;
        while(read->next!=NULL){
            std::cout << read->x << endl;
            read=read->next;
            if(read->next==NULL){
                std::cout << read->x << endl;
            }
        }
/*--------------------------------------------------------------------*/
        int del_element;
        std::cout << "Input the element you want to delete : " << endl;
        std::cin >> del_element;

        node *deleter=H;

        while(deleter->x!=del_element){
            P=deleter;
            deleter=deleter->next;
            if(deleter==NULL){
                std::cout << "Element has not been found!!!" << endl;
                break;
            }
        }
        if(deleter!=NULL){
            if(deleter==H){
                H=deleter->next;
                deleter->next=NULL;
                deleter->x=0;
            }else{
                P->next=deleter->next;
                deleter->next=NULL;
                deleter->x=0;
            }
/*--------------------------------------------------------------------*/
            node *read=H;
            std::cout << endl;
            std::cout << "Elements in the linked list are : " << endl;
            while(read->next!=NULL){
                std::cout << read->x << endl;
                read=read->next;
                if(read->next==NULL){
                    std::cout << read->x << endl;
                }
            }
        }
    }
    return 0;
}
