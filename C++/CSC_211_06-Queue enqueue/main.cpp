#include <iostream>
#define FULL_QUEUE 15

using namespace std;

int main()
{
    int R=10;
    int A[FULL_QUEUE]={1,2,3,4,5,6,7,8,9,10};
    if(R+1==FULL_QUEUE){
        std::cout << "Full Queue" << endl;
    }else{
        R=R+1;
        int x;
        std::cout << "Input x : " <<endl;
        std::cin >> x;
        A[R-1]=x;
    }
    return 0;
}
