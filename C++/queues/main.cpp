#include <iostream>
#define MAX_QUEUE_SIZE 5

using namespace std;
/*
    CSC 211: Data Structures And Algorithms
    P15/36821/2016
    Queues
*/
void print_message(char* message){
    cout << endl << message << endl;
}
void get_int_input(int* input){
    cin >> *input;
}
void print_queue(int* queue_array,int rear){
    if(rear==0){
        print_message("Empty queue!!!");
    }else{
        int i;
        for(i=0;i<rear;i++){
            cout << queue_array[i] << " ";
        }
    }
}

void enqueue(int* queue_array,int* _front,int* rear_,int maximum_queue_size){
    if(*rear_==maximum_queue_size){
        print_message("Full queue!!!");
    }else{
        print_message("Enqueue : ");
        get_int_input(&queue_array[*rear_]);
        *_front=0;
        *rear_=*rear_+1;
        enqueue(queue_array,_front,rear_,maximum_queue_size);
    }
}

void dequeue(int* queue_array,int* _front,int* rear_){
    if((*_front==0) && (*rear_==0)){
        print_message("Empty queue!!!");
    }else{
        int i=*rear_-1;
        int j=*_front;

        char queue_option;
        print_message("Dequeue ? Y or N :");
        cin >> queue_option;
        switch(queue_option){
            case 'Y':
                cout << "Dequeued :" <<queue_array[j] << endl;
                while(j<i){
                    queue_array[j]=queue_array[j+1];
                    j++;
                }
                *rear_=*rear_-1;
                print_queue(queue_array,*rear_);
                dequeue(queue_array,_front,rear_);
                break;
            case 'N':
                print_queue(queue_array,*rear_);
                break;
            default:
                print_message("Invalid option!!!");
                print_queue(queue_array,*rear_);
                break;
        }
    }
}

int main()
{
    int queue_array[MAX_QUEUE_SIZE];
    int _front=0;
    int rear_=0;

    enqueue(queue_array,&_front,&rear_,MAX_QUEUE_SIZE);
    dequeue(queue_array,&_front,&rear_);

    return 0;
}
