#include <iostream>
#define MAX_STACK_SIZE 15

using namespace std;

int main()
{
    int top=3;
    int A[MAX_STACK_SIZE]={1,2,4,3};

    std::cout << "Stack Top : " << top << endl;
    if(top==0){
        std::cout << "Empty Stack" << endl;
    }else{
        A[top]=0;
        top=top-1;
        std::cout << "Stack top after pop is : " << top << endl;
    }
    return 0;
}
