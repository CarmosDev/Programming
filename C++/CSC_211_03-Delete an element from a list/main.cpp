#include <iostream>

using namespace std;

int main()
{
    int elements[]={1,2,3,4,5,6,7,8,9};
    int n=9;
    int x;
    int i=0;

    if(n<=0){
        std::cout << "Empty List" << endl;
    }else{
        std::cout << "Enter a number x between 1 and 9 to delete : " << endl;
        std::cin >> x;

        while(elements[i]!=x){
            i++;
            if(i>=n){
                std::cout << "No x = " << x << " in list!!!"<<endl;
                break;
            }
        }
        if(i==n){
            std::cout << "Program has exited!!!" << endl;
        }else{
            int position=i+1;
            std::cout << "Position is : " << position << endl;

            while(position<n){
                elements[position-1]=elements[position];
                position++;
            }
            n=n-1;
            std::cout << "List now has " << n << " elements" << endl;

            for(i=0;i<n;i++){
                std::cout << "Element list[" << i << "] is : " << elements[i] << endl;
            }
        }
    }
    return 0;
}
