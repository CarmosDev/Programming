/*
 *P15/35280/2015
 *Server-Client RPC communication
 *Client-side code
 */

#include <stdio.h>
#include "RPCops.h"

int main(int argc, char *argv[]) {

	CLIENT *client;
	char opr;
	char *server_addr;
	int server_port;
	int num1, numA[2];
	char str;
	int *result1;
	char *result2;

	if (argc != 2 ) {
		fprintf(stderr, "Usage: %s [server address] \n", argv[0]);
		return(1); //Invalid argument number
	}

	server_addr = argv[1];
	client = clnt_create(server_addr, RPCPROGRAM, RPCPROGVERSION, "udp");

	if(!client) {
		clnt_pcreateerror(server_addr);
		fprintf(stderr, "\nFailed to establish a connection \nExiting... \n");
		return(2); //Failed to establish a connection
	}

	printf("Select operation to perform: \n[A]ddition \n[S]tring reversal \n[N]umber squaring \n: ");
	scanf("%s", &opr);

	switch(opr) {
		case 'A':
		case 'a':
			//Addition
			printf("\nInput the first number: ");
			scanf("%d", &numA[0]);
			printf("\nInput the second number: ");
			scanf("%d", &numA[1]);

			result1 = addnum_1(numA, client);

			if(result1 == (int *)NULL) { //Cast since is a pointer
				clnt_perror(client, server_addr);
				fprintf(stderr, "\nError occurred while calling the server \nExiting... \n");
				return(4); //Error occurred while calling the server
			}

			printf("\nResult: %d", result1);
			break;
		case 'S':
		case 's':
			//String reversal
			printf("\nInput the string to reverse: ");
			scanf("%s", &str);

			result2 = strrev_1(str, client);

			if(result2 == (char *)NULL) {
				clnt_perror(client, server_addr);
				fprintf(stderr, "\nError occurred while calling the server \nExiting... \n");
				return(3); //Error occurred while calling the server
			}

			printf("\nResult: %s", result2);
			break;
		case 'N':
		case 'n':
			//Number squaring
			printf("\nInput the number to square: ");
			scanf("%d", &num1);

			sqnum_1(&num1, client);

			if(result1 ==  (int *)NULL) {
				clnt_perror(client, server_addr);
				fprintf(stderr, "\nError occurred while calling the server \nExiting... \n");
				return(3); //Error occurred while calling the server
			}

			printf("\nResult: %d", num1);
			break;
		default:
			printf("\nInvalid option \nExiting... \n");
			return(3); //Nonexistent operation chosen
	}

	clnt_destroy(client);
	return 0; //Normal termination
}