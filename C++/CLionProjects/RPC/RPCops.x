/*
 *P15/35280/2015
 *Remote operations protocol ???
 */

program RPCPROGRAM {
	version RPCPROGVERSION {
		int ADDNUM(int) = 1; /* Return type and input type definition, and procedure number(1) */
		int STRREV(string) = 2; /* Procedure number(2) */
		int SQNUM(int) = 3; /* Procedure number(3) */
	} =1; /*Version  of the RPC program */
} = 0x20000001; /* Program number {20000000 - 3FFFFFFF} defined by user */