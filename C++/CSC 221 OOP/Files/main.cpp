#include <iostream>
#include <fstream>
#include <strings.h>

using namespace std;
void read_file(char[]);
char* replace_char(char* str, char find, char replace);

int main() {

    //Open a file
    FILE *file=fopen("new_file.txt","a");
    char name[1000]={};

    std::cout << "Enter a name" << endl;
    std::cin.getline(name,1000);

    std::cout << replace_char(name,' ','|') << endl;

    fprintf(file,"%s\t%d\n",name,10);


    fclose(file);

    read_file("new_file.txt");

    return 0;
}

char* replace_char(char* str, char find, char replace){
    char *current_pos = strchr(str,find);
    while (current_pos){
        *current_pos = replace;
        current_pos = strchr(current_pos,find);
    }
    return str;
}

void read_file(char filename[]){
    FILE *file = fopen(filename,"r");
    char name[1000];
    int number;
    while(!feof(file)){
        //fgets(name, sizeof(name),file);
        fscanf(file,"%s\t%d\n",name,&number);
        std::cout << "Name: " << replace_char(name,'|',' ') << endl;
        std::cout << "Number: " << number << endl;
    }
    fclose(file);
}