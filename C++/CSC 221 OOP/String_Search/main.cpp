#include <iostream>
#include <string>

using namespace std;

int main() {
    string haystack;
    string needle;

    haystack = "Nairobi";
    needle = "99";

    if(haystack.find(needle) != string::npos){
        std::cout << "Found " << endl;
    }
    return 0;
}