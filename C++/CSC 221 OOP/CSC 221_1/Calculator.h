//
// Created by MOSETI on 24/05/2017.
//

#ifndef CSC_221_1_CALCULATOR_H
#define CSC_221_1_CALCULATOR_H

#include <iostream>

class Calculator{
public:
    int number1;
    int number2;
    char operation;

private:
    int multiply(int number1,int number2);
    int add(int number1,int number2);
    int subtract(int number1,int number2);

public:
    void calculate(int number1,int number2,char operation);
};
void Calculator::calculate(int number1, int number2, char operation) {
    switch(operation){
        case 'A':
            std::cout << number1 << " + " << number2 << " = " << Calculator::add(number1,number2) << std::endl;
            break;
        case 'M':
            std::cout << number1 << " * " << number2 << " = " << Calculator::multiply(number1,number2) << std::endl;
            break;
        case 'S':
            std::cout << number1 << " - " << number2 << " = " << Calculator::subtract(number1,number2) << std::endl;
            break;
        default:
            std::cout << "Invalid operation!!!" << std::endl;
            break;
    }
}
int Calculator::add(int number1, int number2) {
    return number1+number2;
}
int Calculator::multiply(int number1, int number2) {
    return number1*number2;
}
int Calculator::subtract(int number1, int number2) {
    return number1-number2;
}

#endif //CSC_221_1_CALCULATOR_H
