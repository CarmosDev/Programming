#include <iostream>
#include "Calculator.h"

int main(){
    int number1;
    int number2;
    char operation;
    Calculator calculator;

    std::cout << "Enter number 1: "<<std::endl;
    std::cin >> number1;
    std::cout << "Enter number 2: "<<std::endl;
    std::cin >> number2;
    std::cout << "Enter operation(A = Addition,M = Multiplication, S = Subtraction): "<<std::endl;
    std::cin >> operation;

    calculator.calculate(number1,number2,operation);
    return 0;
}