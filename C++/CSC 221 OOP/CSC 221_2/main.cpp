#include <iostream>
#include "Sales.h"

int main() {
    float commission;
    float todays_sale;
    Sales sales;

    todays_sale = sales.get_today_sales();
    commission = sales.calculate_commission(todays_sale);

    std::cout << "Your commission today is : " << commission << " dollars." << std::endl;
    return 0;
}