//
// Created by MOSETI on 24/05/2017.
//

#ifndef CSC_221_2_SALES_H
#define CSC_221_2_SALES_H

#include <iostream>

class Sales{
private:
    int daily_sales;
    float commission;
public:
    float get_today_sales();
    float calculate_commission(float sales);
};

float Sales::get_today_sales() {
    float sales;

    std::cout << "What is the dollar value of your total sales today?" << std::endl;
    std::cin >> sales;

    return sales;
}

float Sales::calculate_commission(float sales) {
    if(sales>=0 && sales<1000){
        return (float) (3.0/100)*sales;
    }
    if(sales>=1000 && sales<3000){
        return (float) (3.5/100)*sales;
    }
    return (float) (4.5/100)*sales;
}

#endif //CSC_221_2_SALES_H
