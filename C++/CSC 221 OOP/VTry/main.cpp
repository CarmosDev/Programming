#include<conio.h>
#include <cstring>
#include <iostream>
#include <cstdlib>

enum VehicleClass {MotorCycle, Car, Van, SportsCar, Pickup, Truck};

struct Vehicle {
    char vehicleType[20];
    char maker[20];
    int yearOfManufacture;
    char engineNo[20];
    char registrationNo[20];
    unsigned int vehiclePrice;
    VehicleClass vehicleClass;
};

struct Owner {
    char ownerName[20];
    char fatherName[20];
    char address[30];
    int dateOfPurchase;
    int monthOfPurchase;
    int yearOfPurchase;
    char transferredFrom[20];
    Vehicle vehicle;
};

void view (Owner *p);
void transfer (Owner *p);
void register_new (Owner *p);

using namespace std;

int main()
{

    Owner owner = {"Arslan Arshad", "Muhammad Arshad", "Lahore pakistan", 17, 04, 2012, "None", {"2 wheel", "Honda", 2012, "ABC-143", "any123", 100000, Car }};
    Owner *p = &owner;
    int c;


    cout<<"Vehicle Registration System";
    cout<<endl;
    cout<<"============================";
    cout<<endl;
    cout<<endl;
    cout<<endl;
    cout<<"1- View Vehicle Information";
    cout<<endl;
    cout<<"2- Tranfer Vehicle";
    cout<<endl;
    cout<<"3- Register Vehicle";
    cout<<endl;
    cout<<"0- Exit from Program";
    cout<<endl;
    cout<<endl;
    cout<<"Enter Your Choice:";
    cin>>c;
    switch (c)
    {
        case 1:
            system("cls");
            view(p);
            break;
        case 2:
            system("cls");
            transfer(p);
            break;
        case 3:
            system("cls");
            register_new(p);
            break;
        case 0:
            exit (0);
            break;
        default:
            cout<<"\nPlease enter a valid option";
            getche();
            system("cls");
            main();
            break;

    }
}



void view(Owner *p){
    cout<<"====================";
    cout<<endl;
    cout<<"Vehicle Information:";
    cout<<endl;
    cout<<"====================";
    cout<<endl;
    cout<<endl;
    cout<<"Registeration No: "<<p->vehicle.registrationNo;
    cout<<endl;
    cout<<"Vehicle Type: "<<p->vehicle.vehicleType;
    cout<<endl;
    cout<<"Vehicle Class: "<<p->vehicle.vehicleClass;
    cout<<endl;
    cout<<"Vehicle Maker: "<<p->vehicle.maker;
    cout<<endl;
    cout<<"Year of Manufacture: "<<p->vehicle.yearOfManufacture;
    cout<<endl;
    cout<<"Engine No: "<<p->vehicle.engineNo;
    cout<<endl;
    cout<<"Vehicle Price: "<<p->vehicle.vehiclePrice;
    cout<<endl;
    cout<<endl;
    cout<<endl;

    cout<<"==================";
    cout<<endl;
    cout<<"Owner Information:";
    cout<<endl;
    cout<<"==================";
    cout<<endl;
    cout<<endl;
    cout<<"Owner Name: "<<p->ownerName;
    cout<<endl;
    cout<<"Father Name: "<<p->fatherName;
    cout<<endl;
    cout<<"Address: "<<p->address;
    cout<<endl;
    cout<<"Date of Purchase: "<<p->dateOfPurchase<<" / "<<p->monthOfPurchase<<" / "<<p->yearOfPurchase;
    cout<<endl;
    cout<<"Transfered Form: "<< p->transferredFrom;
    cout<<endl;

    getche();
    system("cls");
    main();
}


void transfer(Owner *p){

    strcpy(p->transferredFrom, p->ownerName);

    cout<<"=======================";
    cout<<endl;
    cout<<"Vehicle Transfer form:";
    cout<<endl;
    cout<<"=======================";
    cout<<endl;
    cout<<"Buyers Name: ";
    cin>>p->ownerName;
    cout<<endl;
    cout<<"Father Name: ";
    cin>>p->fatherName;
    cout<<endl;
    cout<<"Address: ";
    cin>>p->address;
    cout<<endl;
    cout<<"Date of Purchasing: ";
    cin>>p->dateOfPurchase;
    cout<<endl;
    cout<<"Month of Purchasing: ";
    cin>>p->monthOfPurchase;
    cout<<endl;
    cout<<"Year of Purchasing: ";
    cin>>p->yearOfPurchase;
    system("cls");
    view(p);
}


void register_new(Owner *p){
    cout<<"====================";
    cout<<endl;
    cout<<"Vehicle Information:";
    cout<<endl;
    cout<<"====================";
    cout<<endl;
    cout<<endl;
    cout<<"Registeration No: ";
    cin>>p->vehicle.registrationNo;
    cout<<endl;
    cout<<"Vehicle Type: ";
    cout<<p->vehicle.vehicleType;
    cout<<endl;
    cout<<"Vehicle Maker: ";
    cin>>p->vehicle.maker;
    cout<<endl;
    cout<<"Year of Manufacture: ";
    cin>>p->vehicle.yearOfManufacture;
    cout<<endl;
    cout<<"Engine No: ";
    cin>>p->vehicle.engineNo;
    cout<<endl;
    cout<<"Vehicle Price: ";
    cin>>p->vehicle.vehiclePrice;
    cout<<endl;
    cout<<endl;
    cout<<endl;

    cout<<"==================";
    cout<<endl;
    cout<<"Owner Information:";
    cout<<endl;
    cout<<"==================";
    cout<<endl;
    cout<<endl;
    cout<<"Owner Name: ";
    cin>>p->ownerName;
    cout<<endl;
    cout<<"Father Name: ";
    cin>>p->fatherName;
    cout<<endl;
    cout<<"Address: ";
    cin>>p->address;
    cout<<endl;
    cout<<"Date of Purchasing: ";
    cin>>p->dateOfPurchase;
    cout<<endl;
    cout<<"Month of Purchasing: ";
    cin>>p->monthOfPurchase;
    cout<<endl;
    cout<<"Year of Purchasing: ";
    cin>>p->yearOfPurchase;
    cout<<endl;
    system("cls");
    view(p);
}