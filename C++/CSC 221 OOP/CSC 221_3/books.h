//
// Created by MOSETI on 10/04/2017.
//

#ifndef CSC_221_3_BOOKS_H
#define CSC_221_3_BOOKS_H

#endif //CSC_221_3_BOOKS_H

#include <iostream>
#include <string>

using namespace std;

class Book{
    private:
        string* author;
        string* title;
        string* publisher;
        int* price;
        int *stock_position;
    public:
        static int successful_transactions,unsuccessful_transactions;
        Book();
        ~Book();
    private:
        int number_of_copies=0;
    public:
        void set_values(string* author,string* title,string* publisher,int* price,int* stock);
        string get_author();
        string get_title();
        string get_publisher();
        int get_price();
        int get_stock_position();
        bool request_copies(int number_of_copies);
        void update_book_price(int* new_price);
        void buy_books();
        void cancel_transaction();
    private:
        void update_price(int* new_price);
        void update_stock(int sold_books);
};
//Constructor
Book::Book() {

}
//Destructor
Book::~Book() {

}

//Setter
void Book::set_values(string* author, string* title, string* publisher, int* price, int* stock) {
    Book::author = author;
    Book::title = title;
    Book::publisher = publisher;
    Book::price = price;
    Book::stock_position = stock;
}

//Getters
string Book::get_author() {
    return *Book::author;
}
string Book::get_title() {
    return *Book::title;
}
int Book::get_price() {
    return *Book::price;
}
string Book::get_publisher() {
    return *Book::publisher;
}
int Book::get_stock_position() {
    return *Book::stock_position;
}

//Static Variables Initialisation
int Book::unsuccessful_transactions=0;
int Book::successful_transactions=0;

//Methods
bool Book::request_copies(int number_of_copies) {
    if(number_of_copies < 1 || number_of_copies > Book::get_stock_position()){
        std::cout << "Required copies not in stock" << std::endl;
        return false;
    }
    Book::number_of_copies = number_of_copies;
    std::cout << "Total cost of the requested copies is : " << Book::get_price() * number_of_copies << std::endl;
    return true;
}
void Book::update_book_price(int* new_price) {
    Book::update_price(new_price);
}
void Book::update_price(int* new_price) {
    Book::price = new_price;
}
void Book::update_stock(int sold_books) {
    Book::stock_position -= sold_books;
}
void Book::buy_books() {
    Book::update_stock(Book::number_of_copies);
    Book::successful_transactions++;
    std::cout << "Thank you for buying our products." << endl;
}
void Book::cancel_transaction() {
    std::cout << "Based on your request, the operation has been cancelled." << endl;
    Book::unsuccessful_transactions++;
}
