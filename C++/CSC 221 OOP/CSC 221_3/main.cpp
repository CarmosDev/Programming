#include <iostream>
#include "books.h"
#include <string>
#include <sstream>
#include <mem.h>
#include <cstdio>

#define MAXIMUM_STRING_LENGTH 50
#define MAXIMUM_INT_LENGTH 9

using namespace std;
string int_to_string(int a);
void print_string(string value);

int main() {
    int number_of_books;
    int i,request_copies;
    string option;
    Book* book;

    //Quit or Continue
    std::cout << "Press 'q' to quit and 'c' to continue:" << endl;
    std::cin >> option;
    if(option == "q")
        return 0;

    //Number of books prompt
    std::cout << "Enter the number of books you want to add to the library." << endl;
    std::cin >> number_of_books;
    if(number_of_books<1){
        main();
    }

    book = new Book[number_of_books];
    string* author;
    string* title;
    string* publisher;
    int* price;
    int* stock_position;
    //Stock the library
    for(i=0;i<number_of_books;i++){
        author =  new string[MAXIMUM_STRING_LENGTH];
        title = new string[MAXIMUM_STRING_LENGTH];
        publisher = new string[MAXIMUM_STRING_LENGTH];
        price = new int[MAXIMUM_INT_LENGTH];
        stock_position = new int[MAXIMUM_INT_LENGTH];
        std::cout << "----------------------------------" <<endl;
        std::cout << "Enter author :" << endl;
        std::cin>>*author;
        std::cout << "Enter title :" << endl;
        std::cin >> *title;
        std::cout << "Enter price :" << endl;
        std::cin >> *price;
        std::cout << "Enter publisher :" << endl;
        std::cin >> *publisher;
        std::cout << "Enter stock :" << endl;
        std::cin >> *stock_position;
        book[i].set_values(author,title,publisher,price,stock_position);
    }

    //Print Book list
    for(i=0;i<number_of_books;i++){
        std::cout << "----------------------------------" << endl;
        std::cout << "Author : " << book[i].get_author() << endl;
        std::cout << "Title : " << book[i].get_title() << endl;
        std::cout << "Price : " << book[i].get_price() << endl;
        std::cout << "Stock Position : " << book[i].get_stock_position() << endl;
        std::cout << "Publisher : " << book[i].get_publisher() << endl;
    }

    //Search
    string search_title;
    string search_author;

    print_string("Enter book title and author to search");
    print_string("Title : ");
    std::cin >> search_title;
    print_string("Author : ");
    std::cin >> search_author;

    for(i=0;i<number_of_books;i++){
        if((book[i].get_title().find(search_title)!=string::npos) && (book[i].get_author().find(search_author) != string::npos)){
            std::cout << "The book with title : " << search_title << " and author : " << search_author << " has been found!!!" << endl;
            std::cout << "Book details : " << endl;
            std::cout << "Book Author : " << book[i].get_author() << endl;
            std::cout << "Book Title : " << book[i].get_title() << endl;
            std::cout << "Book Price : " << book[i].get_price() << endl;
            std::cout << "Book Publisher : " << book[i].get_publisher() << endl;
            std::cout << "Book Stock : " << book[i].get_stock_position() << endl;
            break;
        }
    }
    //If not found
    if(i==number_of_books){
        std::cout << "The book with title : " << search_title << " and author : " << search_author << " has not been found!!!" << endl;
        main();
    }
    //Results
    print_string("How many copies do you want?");
    std::cin >> request_copies;
    if(book[i].request_copies(request_copies)){
        //Transaction
        std::cout << "Do you want to buy the books? 'Y' or 'N'" << endl;
        std::cin >> option;
        if(option == "Y")
            book[i].buy_books();
        if(option == "N")
            book[i].cancel_transaction();
    }
    std::cout << "Report" << endl;
    std::cout << "Remaining copies: " << book[i].get_stock_position() << endl;
    std::cout << "Successful transactions: " << book[i].successful_transactions << endl;
    std::cout << "Unsuccessful transactions: " << book[i].unsuccessful_transactions << endl;

    //Update price
    std::cout << "Do you want to update the book's price? 'Y' or 'N'" << endl;
    std::cin >> option;

    if(option == "Y"){
        print_string("Enter new price: ");
        std::cin >> *price;
        book[i].update_book_price(price);
        std::cout << "The new price is: " << book[i].get_price() << endl;
    }else if(option == "N"){
        print_string("Price not updated.");
    }else{
        print_string("Invalid option");
    }
    main();
}

string int_to_string(int a){
    ostringstream convert;
    convert << a;
    return convert.str();
}

void print_string(string value){
    std::cout << value <<endl;
}