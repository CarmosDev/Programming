//
// Created by MOSETI on 22/05/2017.
//

#include <iostream>
#include "Vehicle.h"
#include "Motorcycle.h"
#include "CommercialTruck.h"

#ifndef KRA_VEHICLEREGISTER_H
#define KRA_VEHICLEREGISTER_H

using namespace std;

class VehicleRegister{

public:
    VehicleRegister();
    ~VehicleRegister();

public:
    void register_vehicle();

protected:
    char option;
    Vehicle vehicle;
    Motorcycle motorcycle;
    CommercialTruck commercial_truck;

};

VehicleRegister::VehicleRegister() {}

VehicleRegister::~VehicleRegister() {}

void VehicleRegister::register_vehicle() {

    std::cout << "Choose type of vehicle:" << endl;
    std::cout << "A) Ordinary vehicle." << endl;
    std::cout << "B) Motorcycle." << endl;
    std::cout << "C) Commercial Truck." << endl;
    std::cout << "D) Exit." << endl;

    std::cin >> option;

    switch (option){
        case 'A':
            vehicle.register_vehicle();
            break;
        case 'B':
            motorcycle.register_vehicle();
            break;
        case 'C':
            commercial_truck.register_vehicle();
            break;
        case 'D':
            _cexit();
            break;
        default:
            std::cout << "Invalid option" << endl;
            std::cout << "" << endl;
            register_vehicle();
            break;
    }
}


#endif //KRA_VEHICLEREGISTER_H