//
// Created by MOSETI on 23/05/2017.
//

#ifndef KRA_COMMERCIALTRUCK_H
#define KRA_COMMERCIALTRUCK_H


#include <iostream>
#include "Vehicle.h"

class CommercialTruck :public Vehicle{
    public:
        string trailer_number_plate;

    public:
        CommercialTruck();
        ~CommercialTruck();
        void issue_number_plate();
};

void CommercialTruck::issue_number_plate() {
    number_plate = "XXX123X";
    std::cout << "The vehicle's number plate is: " << number_plate << endl;
    trailer_number_plate = "XXX123X";
    std::cout << "The vehicle's number plate is: " << trailer_number_plate << endl;
}

CommercialTruck::CommercialTruck() {
    tax_rate = float(10)/float(100);
    type = "Commercial Truck";
}

CommercialTruck::~CommercialTruck() {}

#endif //KRA_COMMERCIALTRUCK_H
