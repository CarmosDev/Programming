//
// Created by MOSETI on 23/05/2017.
//

#ifndef KRA_VEHICLETRANSFER_H
#define KRA_VEHICLETRANSFER_H

#include "Vehicle.h"
#include "Motorcycle.h"
#include "CommercialTruck.h"
#include "VehicleRegister.h"

class VehicleTransfer :public VehicleRegister{
    public:
        VehicleTransfer();
        ~VehicleTransfer();
        void register_vehicle();

    protected:
        Vehicle vehicle;
        Motorcycle motorcycle;
        CommercialTruck commercial_truck;
        char option;
};

VehicleTransfer::VehicleTransfer() {}

VehicleTransfer::~VehicleTransfer() {}

void VehicleTransfer::register_vehicle() {
    std::cout << "Choose type of vehicle to be transfered:" << endl;
    std::cout << "A) Ordinary vehicle." << endl;
    std::cout << "B) Motorcycle." << endl;
    std::cout << "C) Commercial Truck." << endl;
    std::cout << "D) Exit." << endl;

    std::cin >> option;

    switch (option){
        case 'A':
            vehicle.transfer_vehicle();
            break;
        case 'B':
            motorcycle.transfer_vehicle();
            break;
        case 'C':
            commercial_truck.transfer_vehicle();
            break;
        case 'D':
            _cexit();
            break;
        default:
            std::cout << "Invalid option" << endl;
            std::cout << "" << endl;
            register_vehicle();
            break;
    }
}

#endif //KRA_VEHICLETRANSFER_H
