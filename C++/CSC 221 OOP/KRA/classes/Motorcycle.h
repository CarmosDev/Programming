//
// Created by MOSETI on 23/05/2017.
//

#include "Vehicle.h"

#ifndef KRA_MOTORCYCLE_H
#define KRA_MOTORCYCLE_H

class Motorcycle :public Vehicle{

public:
    Motorcycle();
    ~Motorcycle();

protected:
    void compute_duty();
//        void register_vehicle();

};

Motorcycle::Motorcycle() {
    tax_rate = float(10)/float(100);
    type = "Motorcycle";
}

Motorcycle::~Motorcycle() {}

void Motorcycle::compute_duty() {
    total_duty = tax_rate * import_value;
}

//void Motorcycle::register_vehicle() {
//    //Get details
//    input_vehicle();
//
//    //search for existing chassis number
//    if(exists_search(chassis_number)){
//        std::cout << "Sorry, that motorcycle is already registered in our database." << endl;
//        return;
//    }
//    //Verify FBI
//    if(exists_fbi(chassis_number)){
//        std::cout << "Sorry, that motorcycle exists in the FBI database. We cannot register it" << endl;
//        return;
//    }
//
//    //Get Owner
//    owner.set_owner();
//
//    //Compute duty and tax
//    compute_duty();
//
//    print_invoice();
//
//    handle_payment();
//}

#endif //KRA_MOTORCYCLE_H