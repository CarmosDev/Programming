//
// Created by MOSETI on 23/05/2017.
//

#include <ctime>
#include "Database.h"
#include "Owner.h"

#ifndef KRA_VEHICLE_H
#define KRA_VEHICLE_H

using namespace std;

class Vehicle{
public:
    int chassis_number;
    string make;
    string type;
    int year_of_manufacture;
    int weight;
    int import_value;
    string body_color;
    string location;
    string use;
    int engine_capacity;
    string number_plate;
    float tax_rate;
    float total_duty;
    Owner owner;
    Owner new_owner;
    bool has_logbook;
    bool old_owner_verify=false;

public:
    Vehicle();
    ~Vehicle();
    void register_vehicle();
    void transfer_vehicle();

protected:
    void input_vehicle();
    bool exists_search(int chassis_number);
    bool exists_fbi(int chassis);
    void compute_duty(int engine_capacity,int year_of_manufacture,string use);
    void print_invoice();
    void handle_payment();
    void issue_number_plate();
    void issue_logbook();
    void store_in_database();
    void store_in_transfers();

protected:
    char option;
    Database database;
    string vehicle_database = "../databases/VehicleDatabase.bat";
    string transfers_database = "../databases/TransfersDatabase.bat";
};

Vehicle::Vehicle() {
    tax_rate = float(10)/float(100);
    type = "Ordinary Vehicle";
}

Vehicle::~Vehicle() {}

void Vehicle::register_vehicle() {
    //Get details
    input_vehicle();

    //search for existing chassis number
    if(exists_search(chassis_number)){
        std::cout << "Sorry, that vehicle is already registered in our database." << endl;
        return;
    }
    //Verify FBI
    if(exists_fbi(chassis_number)){
        std::cout << "Sorry, that vehicle exists in the FBI database. We cannot register it" << endl;
        return;
    }

    //Get Owner
    owner.set_owner();

    //Compute duty and tax
    compute_duty(engine_capacity,year_of_manufacture,use);

    print_invoice();

    handle_payment();
}

void Vehicle::input_vehicle() {
    std::cout << "Enter details:" << endl;
    std::cout << "Chassis Number?" << endl;
    std::cin >> chassis_number;
    std::cout << "Make?" << endl;
    std::getline(std::cin,make);
    std::getline(std::cin,make);
    std::cout << "Year Of Manufacture?" << endl;
    std::cin >> year_of_manufacture;
    std::cout << "Weight?" << endl;
    std::cin >> weight;
    std::cout << "Import value?" << endl;
    std::cin >> import_value;
    std::cout << "Body color?" << endl;
    std::getline(std::cin,body_color);
    std::getline(std::cin,body_color);
    std::cout << "Location?" << endl;
    std::getline(std::cin,location);
    std::cout << "Use?" << endl;
    std::cout << "A) Business" << endl;
    std::cout << "B) Private" << endl;
    std::cout << "C) Public Service" << endl;
    std::cin >> option;
    switch (option){
        case 'A':
            use = "Business";
            break;
        case 'B':
            use = "Private";
            break;
        case 'C':
            use = "Public Service";
            break;
        default:
            use = "Private";
            break;
    }
    std::cout << "Engine capacity?" << endl;
    std::cin >> engine_capacity;
}

bool Vehicle::exists_search(int chassis_number) {
    return database.verify_existence(chassis_number);
}

bool Vehicle::exists_fbi(int chassis_number) {
    return database.verify_fbi(chassis_number);
}

void Vehicle::compute_duty(int engine_capacity, int year_of_manufacture, string use="") {
    if(engine_capacity>0 && engine_capacity<=1000){
        tax_rate = float(14)/float(100);
    }else if(engine_capacity>1000 && engine_capacity<=2000){
        tax_rate = float(17)/float(100);
    }else{
        tax_rate = float(20)/float(100);
    }

    time_t time_now= time(0);
    struct tm *now = localtime(&time_now);
    int current_year = now->tm_year + 1900;

    if((current_year-year_of_manufacture)<5){
        tax_rate+=float(4)/float(100);
    }else if((current_year-year_of_manufacture)>5 && (current_year-year_of_manufacture)<10){
        tax_rate+=float(3)/float(100);
    }else{
        tax_rate+=float(2)/float(100);
    }

    if(use == "Private"){
        tax_rate +=float(5)/float(100);
    } else if(use == "Business"){
        tax_rate +=float(4)/float(100);
    }else{
        tax_rate +=float(3)/float(100);
    }

    total_duty = import_value * tax_rate;
}

void Vehicle::print_invoice() {
    std::cout << "Invoice: " << endl;
    std::cout << "Total tax: " << total_duty << endl;
}

void Vehicle::handle_payment() {
    std::cout << "Pay now? Y or N" << endl;
    std::cin >> option;

    switch (option){
        case 'Y':
            //issue number plate
            issue_number_plate();
            //issue logbook
            issue_logbook();
            store_in_database();
            break;
        case 'N':
            handle_payment();
            break;
        default:
            std::cout << "Invalid option. Try again." << endl;
            std::cout << "" << endl;
            handle_payment();
            break;
    }
}

void Vehicle::issue_number_plate() {
    number_plate = "XXX123X";
    std::cout << "The vehicle's number plate is: " << number_plate << endl;
}

void Vehicle::issue_logbook(){
    std::cout << "You can now collect your logbook." << endl;
}

void Vehicle::store_in_database() {
    FILE *file = fopen(vehicle_database.c_str(),"a");
    fprintf(file,"%d\t%s\t%s\t%d\t%d\t%d\t%s\t%s\t%s\t%d\t%s\t%f\t%f\t%d\t%s\t%d\n",
            chassis_number,type.c_str(),make.c_str(),year_of_manufacture,weight,import_value,
            body_color.c_str(),location.c_str(),use.c_str(),engine_capacity,number_plate.c_str(),
            tax_rate,total_duty,
            owner.identification_number,owner.name.c_str(),owner.phone_number);
    fclose(file);
}

void Vehicle::transfer_vehicle() {
    //Get logbook
    std::cout << "Got the logbook? Y or N" << endl;
    std::cin >> option;

    switch (option){
        case 'Y':
            has_logbook = true;
            break;
        case 'N':
            transfer_vehicle();
            break;
        default:
            transfer_vehicle();
            break;
    }

    //Get old owner details
    owner.set_owner();

    //Old owner verify
    while(!old_owner_verify){
        std::cout << "Has the old owner verified the transfer? Y or N" << endl;
        std::cin >> option;

        switch (option){
            case 'Y':
                old_owner_verify = true;
                break;
            case 'N':
                break;
            default:
                break;
        }
    }

    //Set new owner
    new_owner.set_owner();

    //Get duty parameters
    std::cout << "Enter vehicle chassis number:" << endl;
    std::cin >> chassis_number;

    std::cout << "Enter vehicle engine capacity:" << endl;
    std::cin >> engine_capacity;

    std::cout << "Enter vehicle year of manufacture:" << endl;
    std::cin >> year_of_manufacture;

    //Compute duty
    compute_duty(engine_capacity,year_of_manufacture);

    //Print invoice
    print_invoice();

    //Post payment
    std::cout << "Pay now? Y or N" << endl;
    std::cin >> option;

    switch (option){
        case 'Y':
            //issue logbook
            issue_logbook();
            //store
            store_in_transfers();
            break;
        case 'N':
            break;
        default:
            std::cout << "Invalid option. Try again." << endl;
            std::cout << "" << endl;
            break;
    }

}

void Vehicle::store_in_transfers() {
    FILE *file = fopen(transfers_database.c_str(),"a");
    fprintf(file,"%d\t%s\t%d\t%d\t%f\t%d\t%s\t%d\t%d\t%s\t%d\t\n",
            chassis_number,type.c_str(),year_of_manufacture,engine_capacity,total_duty,
            owner.identification_number,owner.name.c_str(),owner.phone_number,
            new_owner.identification_number,new_owner.name.c_str(),new_owner.phone_number);
    fclose(file);
}
#endif //KRA_VEHICLE_H