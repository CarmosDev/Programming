//
// Created by MOSETI on 22/05/2017.
//

#include <iostream>
#include "VehicleRegister.h"
#include "VehicleTransfer.h"

#ifndef KRA_KRAOPERATIONS_H
#define KRA_KRAOPERATIONS_H

using namespace std;

class KRAOperations{

public:
    KRAOperations();
    ~KRAOperations();

public:
    void start_operation();

protected:
    char option;
    VehicleRegister vehicleRegister;
    VehicleTransfer vehicleTransfer;
};

KRAOperations::KRAOperations() {}

KRAOperations::~KRAOperations() {}

void KRAOperations::start_operation() {

    std::cout << "Welcome to KRA Vehicle Management System." << endl;
    std::cout << "Choose any of the tasks below:" << endl;
    std::cout << "A) New vehicle registration." << endl;
    std::cout << "B) Registered vehicle transfer." << endl;
    std::cout << "C) Records management." << endl;
    std::cout << "D) Exit." << endl;

    std::cin >> option;

    switch (option){
        case 'A':
            vehicleRegister.register_vehicle();
            break;
        case 'B':
            vehicleTransfer.register_vehicle();
            break;
        case 'C':
            break;
        case 'D':
            _cexit();
            break;
        default:
            std::cout << "Invalid option" << endl;
            std::cout << "" << endl;
            start_operation();
            break;
    }

}

#endif //KRA_KRAOPERATIONS_H