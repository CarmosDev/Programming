//
// Created by MOSETI on 23/05/2017.
//

#include <iostream>

#ifndef KRA_OWNER_H
#define KRA_OWNER_H

using namespace std;

class Owner{
    public:
        int identification_number;
        string name;
        int phone_number;

    public:
        Owner();
        ~Owner();
        void set_owner();
};

Owner::Owner() {}

Owner::~Owner() {}

void Owner::set_owner() {

    std::cout << "Enter owner details:" << endl;
    std::cout << "Identification Number?" << endl;
    std::cin >> identification_number;
    std::cout << "Name?" << endl;
    std::getline(std::cin,name);
    std::getline(std::cin,name);
    std::cout << "Phone Number?" << endl;
    std::cin >> phone_number;
}


#endif //KRA_OWNER_H