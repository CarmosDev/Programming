//
// Created by MOSETI on 23/05/2017.
//

#include <iostream>
#include <cstdio>

#ifndef KRA_DATABASE_H
#define KRA_DATABASE_H

using namespace std;

class Database{

public:
    Database();
    ~Database();

private:
    string vehicle_database = "../databases/VehicleDatabase.bat";
    string fbi_database = "../databases/FBIDatabase.bat";

public:
    bool verify_existence(int chassis_number);
    bool verify_fbi(int chassis_number);
};

Database::Database() {}

Database::~Database() {}

bool Database::verify_existence(int chassis_number) {
    FILE *file = fopen(vehicle_database.c_str(),"r");
    int number;

    char null_char[50];
    int null_int;
    float null_float;

    while(!feof(file)){
        fscanf(file,"%d\t%s\t%d\t%d\t%d\t%s\t%s\t%s\t%d\t%s\t%f\t%f\t%d\t%s\t%d\n",
               &number,null_char,&null_int,&null_int,&null_int,null_char,null_char, null_char,
               &null_int,null_char,&null_float,&null_float,&null_int,null_char,&null_int);
        if(chassis_number == number){
            return true;
        }
    }

    fclose(file);

    return false;
}

bool Database::verify_fbi(int chassis_number){
    FILE *file = fopen(fbi_database.c_str(),"r");
    int number;

    char null_char[50];
    int null_int;
    float null_float;

    while(!feof(file)){
        fscanf(file,"%d\t%s\t%d\t%d\t%d\t%s\t%s\t%s\t%d\t%s\t%f\t%f\t%d\t%s\t%d\n",
               &number,null_char,&null_int,&null_int,&null_int,null_char,null_char, null_char,
               &null_int,null_char,&null_float,&null_float,&null_int,null_char,&null_int);
        if(chassis_number == number){
            return true;
        }
    }

    fclose(file);

    return false;
}

#endif //KRA_DATABASE_H
