//
// Created by MOSETI on 20/05/2017.
//

#include <iostream>
#include <cstdio>
#include "OldVehicleRegistration.h"

#ifndef CSC_221_KRA_KRAOPERATIONS_H
#define CSC_221_KRA_KRAOPERATIONS_H

#endif //CSC_221_KRA_KRAOPERATIONS_H

using namespace std;


class KRAOperations{

    public:
        KRAOperations();
        ~KRAOperations();

    private:
        char operation;
        VehicleRegistration *newVehicleRegistration;
        OldVehicleRegistration *oldVehicleRegistration;

    private:
        void print_message(char *message);

    public:
        void start_operations();

};

KRAOperations::KRAOperations() {
    newVehicleRegistration = new VehicleRegistration();
}

KRAOperations::~KRAOperations() {
}

void KRAOperations::start_operations() {
    print_message("Welcome to KRA Vehicle Registration System.");
    print_message("Choose your operations from the list below:");
    print_message("A) Register a new vehicle.");
    print_message("B) Transfer ownership of registered vehicle.");
    print_message("C) Exit.");

    std::cin >> operation;

    switch(operation){
        case 'A':
            newVehicleRegistration->register_vehicle();
            break;
        case 'B':
            oldVehicleRegistration->register_vehicle();
            break;
        case 'C':
            _c_exit();
            break;
        default:
            print_message("Invalid choice. Try again");
            start_operations();
            break;
    }

}

void KRAOperations::print_message(char *message) {
    std::cout << message << endl;
}