//
// Created by MOSETI on 20/05/2017.
//

#include <cstdio>
#include <iostream>
#include <cstring>
#include <strings.h>

#ifndef CSC_221_KRA_VEHICLE_H
#define CSC_221_KRA_VEHICLE_H

#endif //CSC_221_KRA_VEHICLE_H

using namespace std;

class Vehicle{
    public:
        int chassis_number;
        char *type;
        char make[100];
        int engine_capacity;
        int year_of_manufacture;
        int tareweight;
        int value;
        char body_color[20];
        char *use;
        char location[100];
        char owner_name[100];
        int owner_telephone;
        int owner_id_number;
        float tax_rate;
        float total_tax;
        char *number_plate;
        char *trailer_number_plate;

    public:
        Vehicle();
        ~Vehicle();

    public:
        void store_vehicle();
        char* replace_char(char* str, char find=' ', char replace='|');
};

Vehicle::Vehicle() {
    tax_rate = 10;
}

Vehicle::~Vehicle() {
}

void Vehicle::store_vehicle() {

    FILE *file=fopen("VehicleDatabase.txt","a");

    fprintf(file,"%d\t%s\t%s\t%d\t%d\t%d\t%d\t%s\t%s\t%s\t%s\t%d\t%d\t%f\t%f\n",
        chassis_number,replace_char(type),replace_char(make),engine_capacity,year_of_manufacture,tareweight,value,
        body_color,replace_char(use),replace_char(location),replace_char(owner_name),owner_telephone,owner_id_number,tax_rate,total_tax
    );

    fclose(file);
}

char* Vehicle::replace_char(char *str, char find, char replace) {
    char *current_pos = strchr(str,find);
    while (current_pos){
        *current_pos = replace;
        current_pos = strchr(current_pos,find);
    }
    return str;
}
