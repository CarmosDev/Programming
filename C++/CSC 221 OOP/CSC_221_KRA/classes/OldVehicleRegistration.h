//
// Created by MOSETI on 20/05/2017.
//

#include "VehicleRegistration.h"

#ifndef CSC_221_KRA_NEWVEHICLEREGISTRATION_H
#define CSC_221_KRA_NEWVEHICLEREGISTRATION_H

#endif //CSC_221_KRA_NEWVEHICLEREGISTRATION_H

struct Owner{
    int id_number;
    char name[100];
    int telephone;
};

class OldVehicleRegistration :public VehicleRegistration{

    public:
        OldVehicleRegistration();
        ~OldVehicleRegistration();

    public:
        void register_vehicle();
        void get_logbook();
        void get_old_owner();
        void update_owner();
        void confirmation();
        void issue_new_logbook();

    protected:
        bool has_logbook=false;
        Owner *old_owner;
        Owner *new_owner;
};

OldVehicleRegistration::OldVehicleRegistration() {
}

OldVehicleRegistration::~OldVehicleRegistration(){
}

void OldVehicleRegistration::register_vehicle() {
    //Present logbook
    get_logbook();
    //Get old owner's details
    get_old_owner();
//    //Contact and verify
//    verify_owner();
    //Update with new details
    update_owner();
    //Both owners confirm
    confirmation();
    //Print invoice
    set_tax();
    //Compute tax
    print_invoice();
    //Issue new logbook
    issue_new_logbook();
}

void OldVehicleRegistration::get_logbook() {
    char response;
    //Ensure logbook has been presented
    std::cout << "Have you received the logbook? Y or N" << endl;
    std::cin >> response;

    switch (response){
        case 'Y':
            has_logbook = true;
            break;
        case 'N':
            get_logbook();
            break;
        default:
            std::cout << "Invalid choice!" << endl << endl;
            get_logbook();
            break;
    }
}

void OldVehicleRegistration::get_old_owner() {
    std::cout << "Enter old owner's details: " << endl;
    std::cout << "Name?" << endl;
    std::cin.getline(old_owner->name,100);
    std::cin.getline(old_owner->name,100);
    std::cout << "Id Number?" << endl;
    std::cin >> old_owner->id_number;
    std::cout << "Telephone?" << endl;
    std::cin >> old_owner->telephone;
}

void OldVehicleRegistration::update_owner() {
    std::cout << "Enter new owner's details: " << endl;
    std::cout << "Name?" << endl;
    std::cin.getline(new_owner->name,100);
    std::cin.getline(new_owner->name,100);
    std::cout << "Id Number?" << endl;
    std::cin >> new_owner->id_number;
    std::cout << "Telephone?" << endl;
    std::cin >> new_owner->telephone;

    strcpy(new_owner->name,vehicle->owner_name);
    vehicle->owner_telephone = new_owner->telephone;
    vehicle->owner_id_number = new_owner->id_number;
}

void OldVehicleRegistration::confirmation() {
    char option;
    std::cout << "Confirm vehicle transfer? Y or N" << endl;
    switch (option){
        case 'Y':
            vehicle->store_vehicle();
            break;
        case 'N':
            confirmation();
            break;
        default:
            break;
    }
}

void OldVehicleRegistration::issue_new_logbook(){
    if(has_logbook){
        std::cout << "You will be issued with a new logbook" << endl;
    }
}