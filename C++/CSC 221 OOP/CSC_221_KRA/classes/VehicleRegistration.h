//
// Created by MOSETI on 20/05/2017.
//


#include <time.h>
#include <strings.h>
#include "Vehicle.h"

#ifndef CSC_221_KRA_VEHICLEREGISTRATION_H
#define CSC_221_KRA_VEHICLEREGISTRATION_H

#endif //CSC_221_KRA_VEHICLEREGISTRATION_H

using namespace std;

class VehicleRegistration{

    public:
        VehicleRegistration();
        ~VehicleRegistration();
    protected:
        bool payment_status=false;
        Vehicle *vehicle;
        char string_input[100]={};

    protected:
        bool search(int chassis_number);
        void compute_tax();
        void compute_tax(int engine_capacity,int year_of_manufacture);
        bool fbi_verify(int chassis_number);
        void print_invoice();
        void post_payment();
        void input_vehicle_details();
        void issue_number_plate(int number_plates=1);
        void set_tax();

    private:
        char option;
        void set_option(char option);
        void set_vehicle_type(char option);

    public:
        void register_vehicle();

};

VehicleRegistration::VehicleRegistration() {
    vehicle = new Vehicle();
}

VehicleRegistration::~VehicleRegistration() {}

void VehicleRegistration::input_vehicle_details() {

    std::cout << "Enter vehicle details: " << endl;

    std::cout << "Chassis Number?(10 digits) ";
    std::cin >> vehicle->chassis_number;

    if(search(vehicle->chassis_number)){ //Vehicle exists?
        std::cout << "Sorry. The vehicle with the above chassis number exists." << endl;
        _c_exit();
    }

    std::cout << "Type? ";
    std::cout << "A) Motorcycle" << endl;
    std::cout << "B) Ordinary Vehicle" << endl;
    std::cout << "C) Commercial Truck" << endl;
    std::cin >> option;
    set_vehicle_type(option);

    std::cout << "Make? ";
    std::cin.getline(vehicle->make,100);
    std::cin.getline(vehicle->make,100);

    std::cout << "Engine capacity in cc? ";
    std::cin >> vehicle->engine_capacity;

    std::cout << "Year of manufacture? ";
    std::cin >> vehicle->year_of_manufacture;

    std::cout << "Tareweight? ";
    std::cin >> vehicle->tareweight;

    std::cout << "Cost value? ";
    std::cin >> vehicle->value;

    std::cout << "Body color? ";
    std::cin.getline(vehicle->body_color,20);
    std::cin.getline(vehicle->body_color,20);

    std::cout << "Vehicle use? " << endl;
    std::cout << "A) Private" << endl;
    std::cout << "B) Business" << endl;
    std::cout << "C) Public Service Vehicle" << endl;
    std::cin >> option;
    set_option(option);

    std::cout << "Location? ";
    std::cin.getline(vehicle->location,100);
    std::cin.getline(vehicle->location,100);

    std::cout << "Owner Name? ";
    std::cin.getline(vehicle->owner_name,100);
    std::cin.getline(vehicle->owner_name,100);

    std::cout << "Owner Telephone number? ";
    std::cin >> vehicle->owner_telephone;

    std::cout << "Owner ID number? ";
    std::cin >> vehicle->owner_id_number;
}

void VehicleRegistration::set_vehicle_type(char option) {
    switch (option){
        case 'A':
            vehicle->type = "Motorcycle";
            break;
        case 'B':
            vehicle->type = "Ordinary Vehicle";
            break;
        case 'C':
            vehicle->type = "Commercial Truck";
            break;
        default:
            vehicle->type = "Ordinary Vehicle";
            break;
    }
}

void VehicleRegistration::set_option(char option) {
    switch (option){
        case 'A':
            vehicle->use = "Private";
            break;
        case 'B':
            vehicle->use = "Business";
            break;
        case 'C':
            vehicle->use = "Public Service Vehicle";
            break;
        default:
            vehicle->use = "Private";
            break;
    }
}

void VehicleRegistration::register_vehicle() {
    //Get details
    input_vehicle_details();

    //FBI Verify
    if(!fbi_verify(vehicle->chassis_number)){
        //Compute tax
        set_tax();
        //Print invoice
        print_invoice();
        //Post payment: Issue number plate and logbook
        post_payment();
        //Exit
        _c_exit();

    }

    std::cout << "Sorry. We cannot register your vehicle because it is in the FBI list of suspected cars obtained by fraud." << endl;
    _c_exit();
}

void VehicleRegistration::set_tax() {
    if(vehicle->type == "Motorcycle"){
        compute_tax();
    }else{
        compute_tax(vehicle->engine_capacity,vehicle->year_of_manufacture);
    }
}

bool VehicleRegistration::search(int chassis_number) {
    FILE *file = fopen("VehicleDatabase.txt","r");
    int file_chassis_number;
    while(!feof(file)){
        fscanf(file,"%d\n",&file_chassis_number);
        //If search finds.
        if(file_chassis_number == chassis_number){
            return true;
        }
    }
    fclose(file);
    //If search does not find.
    return false;
}

void VehicleRegistration::compute_tax() {
    vehicle->total_tax = vehicle->value * vehicle->tax_rate/100;
}

void VehicleRegistration::compute_tax(int engine_capacity, int year_of_manufacture) {

        if(engine_capacity>0 && engine_capacity<=1000){
            vehicle->tax_rate = 14;
        }else if(engine_capacity>1000 && engine_capacity<=2000){
            vehicle->tax_rate = 17;
        }else{
            vehicle->tax_rate = 20;
        }

        time_t time_now= time(0);
        struct tm *now = localtime(&time_now);
        int current_year = now->tm_year + 1900;

        if((current_year-year_of_manufacture)<5){
            vehicle->tax_rate+=4;
        }else if((current_year-year_of_manufacture)>5 && (current_year-year_of_manufacture)<10){
            vehicle->tax_rate+=3;
        }else{
            vehicle->tax_rate+=2;
        }

        if(vehicle->use == "Private"){
            vehicle->tax_rate +=5;
        } else if(vehicle->use == "Business"){
            vehicle->tax_rate +=4;
        }else{
            vehicle->tax_rate +=3;
        }

        vehicle->total_tax = vehicle->value * vehicle->tax_rate/100;
}

bool VehicleRegistration::fbi_verify(int chassis_number) {

    FILE *file = fopen("FBI_List.txt","r");
    int file_chassis_number;
    while(!feof(file)){
        fscanf(file,"%d",&file_chassis_number);
        //If search finds.
        if(file_chassis_number == chassis_number){
            return true;
        }
    }
    fclose(file);
    //If search does not find.
    return false;

}

void VehicleRegistration::print_invoice() {
    std::cout << "Invoice: "<< endl;
    std::cout << "Total taxation rate is: " << vehicle->tax_rate << endl;
    std::cout << "Total duty due: " << vehicle->total_tax << endl;

    std::cout << "Has the cost been paid for: Y or N" << endl;
    std::cin >> option;

    switch(option){
        case 'Y':
            payment_status = true;
            break;
        case 'N':
            print_invoice();
            break;
        default:
            print_invoice();
            break;
    }

}

void VehicleRegistration::post_payment() {
    if(payment_status){
        if(vehicle->type == "Commercial Truck"){
            issue_number_plate(2);
        }else{
            issue_number_plate();
        }

        vehicle->store_vehicle();
    }
}

void VehicleRegistration::issue_number_plate(int number_plates) {
    vehicle->number_plate = "XXX123X";
    if(number_plates==1){
        std::cout << "The number plate is : " << vehicle->number_plate << endl;
    }else{
        vehicle->trailer_number_plate = "XXX456X";
        std::cout << "The main number plate is : " << vehicle->number_plate << endl;
        std::cout << "The trailer number plate is : " << vehicle->trailer_number_plate << endl;
    }
}