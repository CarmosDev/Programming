from django import forms
from django.forms import ModelForm
from . import widgets
from .models import UserFile

class NameForm(forms.Form):

    your_name = forms.CharField(max_length=20,min_length=5,label='Your Name')
    about = forms.CharField(widget=widgets.SemanticUITextInput())
    your_photo = forms.ImageField()
    birth_year = forms.DateField(widget=forms.SelectDateWidget(years=(
        '2000','2001','2002','2003','2004','2005',
    )))
    favorite_colors = forms.MultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        choices=(
            ('red', 'Red'),
            ('orange', 'Orange'),
            ('yellow', 'Yellow'),
            ('green', 'Green'),
            ('blue', 'Blue'),
            ('indigo', 'Indigo'),
            ('violet', 'Violet'),
        )
    )

class ContactForm(forms.Form):
    subject = forms.CharField(max_length=20)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)

class FileForm(ModelForm):
    class Meta:
        model = UserFile
        fields = [
            'user_file'
        ]