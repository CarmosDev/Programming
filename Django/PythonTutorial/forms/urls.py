from django.conf.urls import include,url
from . import views
from django.contrib.auth.decorators import login_required

app_name = 'forms'

urlpatterns = [
    url(r'^$',views.IndexView.as_view(),name='index'),
    url(r'^file_upload/$',views.FileView.as_view(),name='file_upload'),
]