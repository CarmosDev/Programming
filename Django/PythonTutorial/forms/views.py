from django.shortcuts import render,reverse
from django.http import HttpResponse,HttpResponseRedirect
from .forms import NameForm,ContactForm,FileForm
from django.views import View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# @method_decorator(login_required,name='dispatch')
class IndexView(View):

    def get(self,request):
        name_form = NameForm()
        contact_form = ContactForm()
        return HttpResponse(render(request, 'forms/index.html', {
            'name_form': name_form,
            'contact_form': contact_form,
        }))

    def post(self,request):
        name_form = NameForm(request.POST)
        contact_form = ContactForm()

        if name_form.is_valid():
            your_name = name_form.cleaned_data['your_name']

        return HttpResponse(render(request, 'forms/index.html', {
            'name_form': name_form,
            'contact_form': contact_form,
        }))

# Create your views here.
# def index(request):
#
#     if request.method == "POST":
#         name_form = NameForm(request.POST)
#         contact_form = ContactForm()
#
#         if name_form.is_valid():
#
#             your_name = name_form.cleaned_data['your_name']
#
#             return HttpResponse(render(request, 'forms/index.html'))
#
#     else:
#         name_form = NameForm()
#         contact_form = ContactForm()
#
#     return HttpResponse(render(request,'forms/index.html',{
#         'name_form': name_form,
#         'contact_form': contact_form,
#     }))

class FileView(View):

    def get(self,request):
        file_form = FileForm()
        return HttpResponse(render(request,'forms/file_upload.html',{
            'file_form': file_form
        }))

    def post(self,request):
        file_form = FileForm(request.POST)
        file_form.clean()

        if file_form.is_valid():

            file_form.save()
            file_form = FileForm()
            return HttpResponseRedirect('forms:file_upload',{
                'success': 'File has been uploaded successfully.',
                'file_form': file_form
            })

        else:
            return HttpResponse(render(request,'forms/file_upload.html',{
                'file_form':file_form
            }))