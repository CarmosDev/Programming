from django import forms
from django.utils.safestring import mark_safe

class SemanticUITextInput(forms.TextInput):

    class Media:
        js = [
            'https://cdn.jsdelivr.net/semantic-ui/2.2.13/semantic.min.js'
        ]
        css = (
            'https://cdn.jsdelivr.net/semantic-ui/2.2.13/semantic.min.css'
        )

    def render(self, name, value, attrs=None, renderer=None):
        html = '<div class="ui left icon input">' \
               '<input type="text"  name=%(name)s placeholder="Type text here..." value=%(value)s %(attrs)s >' \
               '<i class="write icon"></i>' \
               '</div>'%\
               {
                   'name': name,
                   'value': value,
                   'attrs':attrs,
               }
        return mark_safe(html)