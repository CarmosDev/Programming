from django.shortcuts import render,reverse
from django.http import HttpResponseRedirect,HttpResponse
from django.utils.crypto import get_random_string
from django.conf import settings

# Create your views here.\
def index(request):

    request.session['games'] = ['Soccer','Boat Racing','Basketball','Hockey','Volleyball']

    #request.session.flush()

    # request.session.set_test_cookie()

    # request.session.delete_test_cookie()

    return render(request,'files_upload/index.html')

def upload(request):

    def handle_uploaded_files(files):
        for file in files:
            filepath = new_file_path(get_random_string(128))
            with open(filepath + '.' + get_file_extension(file.name), 'wb+') as destination:   # Has File Extension
            # with open(filepath, 'wb+') as destination:
                for chunk in file.chunks():
                    destination.write(chunk)
        return

    def new_file_path(file_name):
        return settings.MEDIA_ROOT + '/' + file_name

    def get_file_extension(file_name):
        file_name = str(file_name)
        file_extension_index = len(file_name.split('.')) - 1
        return file_name.split('.')[file_extension_index].lower()

    if request.method == "POST":
        files = request.FILES.getlist('files_input')
        handle_uploaded_files(files)

        # file = request.FILES['files_input']
        # response = ''
        # file_name = str(file.name)
        # response += file_name
        # response += '<br>'
        # file_extension_index = len(file_name.split('.'))-1
        # file_extension = file_name.split('.')[file_extension_index]
        # response += file_extension
        # response += '<br>'
        # response += str(file.size)
        # response += '<br>'
        # response += str(file.content_type)
        # response += '<br>'
        # response += str(file.charset)
        # return HttpResponse(response)

        return HttpResponseRedirect(reverse('files_upload:index'))
    return HttpResponseRedirect(reverse('files_upload:index'))