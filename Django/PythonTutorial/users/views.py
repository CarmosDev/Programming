from django.shortcuts import render,reverse
from django.contrib.auth.models import User
from .forms import UserRegistrationForm,UserChangePasswordForm,UserLoginForm
from django.http import HttpResponseRedirect,HttpResponse
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    return render(request,'users/index.html')

def register(request):
    if request.method == 'POST':
        user_registration_form = UserRegistrationForm(request.POST)

        if user_registration_form.is_valid():
            # new_user = User.objects.create_user(
            #     username=request.POST['username'],
            #     email=request.POST['email'],
            #     password=request.POST['password'],
            #     first_name=request.POST['first_name'],
            #     last_name=request.POST['last_name'],
            # )
            # new_user.save()
            user_registration_form.save()
            request.session['successful_registration'] = True
            return HttpResponseRedirect(reverse('users:register'))

        return render(request, 'users/register.html', {
            'user_registration_form': user_registration_form,
            'has_registered': False,
        })

    # GET
    user_registration_form = UserRegistrationForm()
    has_registered = False
    try:
        has_registered = request.session['successful_registration']
        del request.session['successful_registration']
    except(KeyError):
        pass

    return render(request,'users/register.html',{
        'user_registration_form': user_registration_form,
        'has_registered': has_registered,
    })

def change_password(request):
    if request.method == "POST":
        user = User.objects.get(username=request.POST['username'])
        user.set_password(request.POST['password'])
        user.save()
        request.session['successful_password_change'] = True
        return HttpResponseRedirect(reverse('users:change_password'))

    change_password_form = UserChangePasswordForm()
    has_changed_password = False
    try:
        has_changed_password = request.session['successful_password_change']
        del request.session['successful_password_change']
    except(KeyError):
        pass
    return render(request,'users/change_password.html',{
        'change_password_form': change_password_form,
        'has_changed_password': has_changed_password,
    })

def user_login(request):
    if request.method == "POST":
        user = authenticate(request,
                            username=request.POST['username'],
                            password=request.POST['password']
                            )
        if user is not None:
            login(request,user)
            return HttpResponseRedirect(reverse('users:details'))
        else:
            login_form = UserLoginForm()
            return HttpResponse(render(request,'users/login.html',{
                'error_message': 'User <b>{}</b> does not exist.'.format(request.POST['username']),
                'login_form': login_form
            }))

    login_form = UserLoginForm()
    return render(request,'users/login.html',{
        'login_form': login_form,
    })

# @login_required(login_url='users:login')
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('users:login'))

def details(request):
    return render(request,'users/details.html')