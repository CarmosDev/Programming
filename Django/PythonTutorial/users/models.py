from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import BaseUserManager

# Create your models here.
class CustomUser(AbstractBaseUser):

    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    username = models.CharField(max_length=25)
    email_address = models.EmailField(unique=True)
    date_of_birth = models.DateField()
    profile_picture = models.ImageField()

    USERNAME_FIELD = 'email_address'
    EMAIL_FIELD = 'email_address'
    REQUIRED_FIELDS = [
        'first_name','last_name','username',
    ]
    is_active = True
    is_staff = False
    is_superuser = False


    def get_full_name(self):
        return self.first_name + ' ' + self.last_name

    def get_short_name(self):
        return self.username

class CustomUserManager(BaseUserManager):
    def create_user(self,email_address,first_name,last_name,username,date_of_birth,profile_picture,password):
        new_user = CustomUser()
        new_user.first_name=first_name
        new_user.last_name=last_name
        new_user.set_password(password)
        new_user.username=username
        new_user.date_of_birth=date_of_birth
        new_user.profile_picture=profile_picture
        new_user.email_address=email_address
        new_user.save()
        return new_user

    def create_super_user(self,email_address,first_name,last_name,username,date_of_birth,profile_picture,password):
        new_user = CustomUser()
        new_user.first_name=first_name
        new_user.last_name=last_name
        new_user.set_password(password)
        new_user.username=username
        new_user.date_of_birth=date_of_birth
        new_user.profile_picture=profile_picture
        new_user.email_address=email_address
        new_user.is_superuser=True
        new_user.save()
        return new_user