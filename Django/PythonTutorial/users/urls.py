from django.conf.urls import url
from . import views

app_name = 'users'

urlpatterns = [
    url(r'^$',views.index,name='index'),
    url(r'^register/$',views.register,name='register'),
    url(r'^change_password',views.change_password,name='change_password'),
    url(r'^login/$',views.user_login,name='login'),
    url(r'^logout/$',views.user_logout,name='logout'),
    url(r'^details/',views.details,name='details')
]