from django.forms import ModelForm
from django.contrib.auth.models import User

class UserRegistrationForm(ModelForm):
    class Meta:
        model = User
        fields = ['first_name','last_name','email','username','password',]

class UserChangePasswordForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'username','password',
        ]

class UserLoginForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 'password',
        ]