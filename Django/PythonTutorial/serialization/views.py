from django.shortcuts import render
from django.core import serializers
from django.http import HttpResponse
from reportlab.pdfgen import canvas

# Create your views here.
def index(request):
    data = {
        'sample_list': [1, 2, 3, 4, 5, 6, 7, 8, 9],
        'sample_tuple': (1, 2, 3, 4, 5, 6, 7, 8, 9),
        'sample_dictionary': {
            'key': 'value',
            'number': 1,
        }
    }

    # xml_data = serializers.serialize('xml',data)

    # return HttpResponse(xml_data)
    pdf_response = HttpResponse(content_type='application/pdf')
    pdf_response['Content-Disposition'] = 'attachment; filename="some_filename.pdf"'

    pdf = canvas.Canvas(pdf_response)
    pdf.drawString(100,100,"Hello World")
    pdf.showPage()
    pdf.save()

    return pdf_response