from django import forms

class MailComposeForm(forms.Form):
    sender_email_address = forms.EmailField(widget=forms.EmailInput)
    subject = forms.CharField(max_length=100,widget=forms.TextInput)
    message = forms.CharField(widget=forms.Textarea)
    recepients = forms.CharField(widget=forms.Textarea(
        attrs={
            'placeholder':'Enter a list of comma(,) separated email addresses.',
        }
    ))
    attachments = forms.FileField(required=False,widget=forms.FileInput(
        attrs={
            'multiple':'',
        }
    ))