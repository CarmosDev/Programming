from django.shortcuts import render,reverse
from .forms import MailComposeForm
from django.views import View
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.core.mail import  EmailMessage

# Create your views here.
class IndexView(View):
    def get(self,request):
        mail_compose_form = MailComposeForm()
        try:
            mail_status=request.session['mail_status']
            del request.session['mail_status']
        except(KeyError):
            mail_status=None
        return render(request, 'mail/index.html', context={
            'mail_compose_form': mail_compose_form,
            'mail_status':mail_status,
        })

    def post(self,request):
        mail_compose_form = MailComposeForm(request.POST)
        if mail_compose_form.is_valid():
            # status=send_mail(
            #     subject=mail_compose_form.cleaned_data['subject'],
            #     message=mail_compose_form.cleaned_data['message'],
            #     from_email=mail_compose_form.cleaned_data['sender_email_address'],
            #     recipient_list=str(mail_compose_form.cleaned_data['recepients']).split(','),
            # )

            email = EmailMessage()
            email.subject = mail_compose_form.cleaned_data['subject']
            email.body = mail_compose_form.cleaned_data['message']
            email.from_email = mail_compose_form.cleaned_data['sender_email_address']
            email.to = str(mail_compose_form.cleaned_data['recepients']).split(',')
            email.bcc = str(mail_compose_form.cleaned_data['recepients']).split(',')
            email.cc = str(mail_compose_form.cleaned_data['recepients']).split(',')
            email.reply_to = str(mail_compose_form.cleaned_data['sender_email_address']).split(',')

            handle_file_attachments(email,request.FILES.getlist('attachments'))

            email.send()

            request.session['mail_status'] = 1

            return HttpResponseRedirect(reverse('mail:compose'))

        request.session['mail_status'] = 0
        return render(request,'mail/index.html',{
            'mail_compose_form':mail_compose_form,
        })

def handle_file_attachments(email,files):
    for file in files:
        file_data = b''
        for chunk in file.chunks():
            file_data += chunk

        email.attach(file.name,file_data,file.content_type)
    return
