from django.conf.urls import url
from . import views

app_name = 'mail'

urlpatterns=[
    url(r'^$',views.IndexView.as_view(),name='compose'),
    url(r'^send/$',views.IndexView.as_view(),name='send'),
]