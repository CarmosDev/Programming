from django.shortcuts import render,reverse
from django.urls import reverse_lazy
from django.views.generic import ListView,CreateView,UpdateView,DeleteView,DetailView
from .models import Publisher,Author
from django.views import View
from django.http import JsonResponse
from .forms import BookForm
from django.http import HttpResponseRedirect,HttpResponse

# Create your views here.
class PublisherList(ListView):
    model = Publisher
    context_object_name = 'publisher_list'


class AuthorCreate(CreateView):
    model = Author
    fields = [
        'salutation',
        'name',
        'email',
    ]

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)

class AuthorDetail(DetailView):
    model = Author

class AuthorUpdate(UpdateView):
    model = Author
    fields = [
        'salutation',
        'name',
        'email'
    ]

class AuthorDelete(DeleteView):
    model = Author
    success_url = reverse_lazy('views:author_list')

class BookCreate(View):
    def get(self,request):
        book_form = BookForm()
        try:
            success_mesage = request.session['success_message']
            del request.session['success_message']
        except(KeyError):
            success_mesage = None

        return HttpResponse(render(request,'views/book_form.html',context={
            'book_form': book_form,
            'success_message': success_mesage,
        }))
        # return render(request,'views/book_form.html',{'book_form': book_form,})

    def post(self,request):
        book_form = BookForm(request.POST,files=request.FILES)

        if book_form.is_valid():
            book_form.save()
            request.session['success_message'] = 'Book has been successfully uploaded.'
            return HttpResponseRedirect(reverse('views:add_book'))
        else:
            return HttpResponse(render(request, 'views/book_form.html', {
                'book_form': book_form,
            }))