from django.conf.urls import url
from . import views

app_name = 'views'

urlpatterns = [
    url(r'^publishers/$',views.PublisherList.as_view(),name='publishers'),
    url(r'^authors/create/$',views.AuthorCreate.as_view(),name='author_create'),
    url(r'^authors/(?P<pk>[0-9]+)/details$',views.AuthorDetail.as_view(),name='author_detail'),
    url(r'^authors/(?P<pk>[0-9]+)/update$',views.AuthorUpdate.as_view(),name='author_update'),
    url(r'^authors/(?P<pk>[0-9]+)/delete$',views.AuthorDelete.as_view(),name='author_delete'),
    url(r'^add_book/$',views.BookCreate.as_view(),name='add_book')
]