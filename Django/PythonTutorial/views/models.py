from django.db import models
from django.shortcuts import reverse
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string

# Create your models here.
class Publisher(models.Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=50)
    city = models.CharField(max_length=60)
    state_province = models.CharField(max_length=30)
    country = models.CharField(max_length=50)
    website = models.URLField()

    class Meta:
        ordering = ["-name"]

    def __str__(self):
        return self.name

class Author(models.Model):
    salutation = models.CharField(max_length=10)
    name = models.CharField(max_length=200)
    email = models.EmailField()
    # created_by = models.ForeignKey(User,on_delete=models.CASCADE)
    #headshot = models.ImageField(upload_to='author_headshots')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('view:author_detail',kwargs={
            'pk':self.pk
        })

class Book(models.Model):
    def user_directory_path(self, filename):
        return get_random_string(128)

    title = models.CharField(max_length=100)
    authors = models.ManyToManyField('Author')
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    publication_date = models.DateField()
    pdf_file = models.FileField(upload_to=user_directory_path,default=None)

