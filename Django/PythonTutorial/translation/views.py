from django.shortcuts import render
from django.utils import translation
from django.http import HttpResponse,HttpResponseRedirect
import pytz

# Create your views here.
def index(request):
    translation.activate('de')
    output = translation.gettext('Welcome to my site.')

    # count=2
    # page = ngettext(
    #     'There is %(count)d object',
    #     'There are %(count)d objects',
    #     count
    # )%{
    #     'count':count
    # }

    # return HttpResponse(output)
    return render(request,'translation/index.html',{
        'timezones': pytz.all_timezones
    })