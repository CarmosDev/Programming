from django.shortcuts import render
from django.utils.crypto import get_random_string
from .models import File

# Create your views here.
def upload(request):
    file = File()
    file.file_name = get_random_string(length=64)
