from django.db import models

# Create your models here.
class File(models.Model):
    # <app_name>.<parent_model>
    user = models.ForeignKey('users.User',on_delete=models.CASCADE)
    file_name = models.CharField(max_length=200)
    file_size = models.BigIntegerField(default=0)
    file_type = models.CharField(max_length=10)
    file_location = models.CharField(max_length=200)
    file_availability = models.CharField(max_length=5,default='Yes')

    def __str__(self):
        return str(self.file_name)