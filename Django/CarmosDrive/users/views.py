from django.shortcuts import render,reverse
from .models import User
from django.http import HttpResponseRedirect

# Create your views here.
def create(request):

    new_user = User()
    new_user.first_name = request.POST['first_name']
    new_user.last_name = request.POST['last_name']
    new_user.email_address = request.POST['email_address']

    # Default
    # new_user.space_used = 0
    # new_user.maximum_storage = 5368709120
    new_user.save()

    return HttpResponseRedirect(reverse('user:home'))