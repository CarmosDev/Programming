from django.db import models

# Create your models here.

class User(models.Model):
    first_name = models.CharField(max_length=25)
    last_name = models.CharField(max_length=25)
    email_address = models.CharField(max_length=25)
    space_used = models.BigIntegerField(default=0)
    maximum_storage = models.BigIntegerField(default=5368709120) # 5GB

    def __str__(self):
        return str(self.first_name) + " " + str(self.last_name)