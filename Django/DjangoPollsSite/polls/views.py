from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,Http404,HttpResponseRedirect
from .models import Question,Choice
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.template import loader

# Create your views here.

# def index(request):
#     latest_question_list = Question.objects.order_by('-pub_date')[:5]
#     #template = loader.get_template('polls/index.html')
#     context = {
#         'latest_question_list':latest_question_list,
#     }
#     return render(request, 'polls/index.html', context)#Alternative: return HttpResponse(template.render(context,request))
#
# def detail(request,question_id):
#     # Long method
#     # try:
#     #     question=Question.objects.get(pk=question_id)
#     # except:
#     #     raise Http404("Question with id {} does not exist".format(question_id))
#
#     question = get_object_or_404(Question, pk=question_id)
#     context = {
#         'question': question,
#     }
#     # response = "You're looking at the results of question {}."
#     # return HttpResponse(response.format(question_id))
#     return render(request, 'polls/detail.html', context)
#
# def results(request, question_id):
#     question = get_object_or_404(Question,pk=question_id)
#     return render(request,'polls/results.html',context={
#         'question':question,
#     })
#     # return HttpResponse("You're looking at question {} results".format(question_id))

def vote(request, question_id):
    question = get_object_or_404(Question,pk=question_id)
    try:
        selected_choice=question.choice_set.get(pk=request.POST['choice'])
    except(KeyError,Choice.DoesNotExist):
        #Redisplay Voting Form
        return render(request,'polls/detail.html',context={
            'question':question,
            'error_message':"You did not select a choice.",
        })
    else:
        # Update choice votes data
        selected_choice.votes +=1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
    #return HttpResponse("You're voting on question {}".format(question_id))

class IndexView(generic.ListView):

    # Default template name : <app_name>/<model_name>_list.html. has to be overridden
    template_name = 'polls/index.html'
    # Default context object name is <model_name>_list and is thus overridden
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """" Return the last 5 published questions """
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:5]

class DetailView(generic.DetailView):

    # Default template name : <app_name>/<model_name>_detail.html. has to be overridden
    template_name = 'polls/detail.html'
    model = Question

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'
