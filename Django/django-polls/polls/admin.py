from django.contrib import admin
from .models import Question,Choice

# Register your models here.
# admin.site.register(Question)
# admin.site.register(Choice)

# Custom admin

class ChoiceInline(admin.StackedInline):

    model = Choice
    # Provide 3 fields for Choices by default
    extra = 3

class ChoiceTabuarInline(admin.TabularInline):

    model = Choice
    # Provide 3 fields for Choices by default
    extra = 3

class QuestionAdmin(admin.ModelAdmin):

    # Change the order of form fields
    # fields = [
    #     'pub_date',
    #     'question_text',
    # ]

    # Tuples
    # (Fieldset_Title,Dictionary with 'fields' key)

    fieldsets = [
        ('Question Title',{'fields':[
            'question_text',
        ]}),
        ('Date Information',{
            'fields':[
                'pub_date',
            ],
            'classes':[
                'collapse',
            ],
        }),
    ]

    #inlines = [ChoiceInline]
    inlines = [ChoiceTabuarInline]

    # list_display admin option, which is a tuple of field names to display, as columns, on the change list page for the object:
    list_display = (
        'question_text',
        'pub_date',
        'was_published_recently',
    )

    list_filter = [
        'pub_date',
    ]

    search_fields = [
        'question_text',
    ]

    list_per_page = 100

admin.site.register(Question,QuestionAdmin)